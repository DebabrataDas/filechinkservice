package com.arc.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;


public final class FileReaderUtil
{
	private final static Logger LOG = Logger.getLogger(FileReaderUtil.class);
	
	/**
	 * Resolves a file from various location..
	 * @param fileName
	 * @return
	 * @throws ApplicationFault
	 */
    public static File getFile(final String fileName) 
    {
		File aFile = new File(fileName);
		if (aFile.exists()) return aFile;
		
		aFile = new File("/" + fileName);
		if (aFile.exists()) return aFile;

		aFile = new File("conf/" + fileName);
		if (aFile.exists()) return aFile;
		
		aFile = new File("resources/" + fileName);
		if (aFile.exists()) return aFile;
		
		try {
			URL resource = FileReaderUtil.class.getClassLoader().getResource(fileName);
			if ( resource != null) aFile = new File(resource.toURI());
		} 
		catch (URISyntaxException ex) {
			throw new RuntimeException(ex);
		}

		if (aFile.exists()) return aFile;

		throw new RuntimeException("FileResourceUtil > File does not exist :" + fileName);
	}
	
	/**
	 * Give in Strings
	 * @param fileName
	 * @return
	 * @throws ApplicationFault
	 */
	public static String toString(String fileName) 
	{
		
		File aFile = getFile(fileName);
		BufferedReader reader = null;
		InputStream stream = null;
		StringBuilder sb = new StringBuilder();
		try {
			stream = new FileInputStream(aFile); 
			reader = new BufferedReader ( new InputStreamReader (stream) );
			String line = null;
			String newline = getLineSeaprator();
			while((line=reader.readLine())!=null) {
				if (line.length() == 0) continue;
				sb.append(line).append(newline);	
			}
			return sb.toString();
		} 
		catch (Exception ex) 
		{
			throw new RuntimeException(ex);
		} 
		finally 
		{
			try {if ( null != reader ) reader.close();
			} catch (Exception ex) {LOG.error("FileReaderUtil", ex);}
			try {if ( null != stream) stream.close();
			} catch (Exception ex) {LOG.error("FileReaderUtil", ex);}
		}
	}
	
	/**
	 * Returns the contents of the file in a byte array.
	 */
	public static byte[] getBytes(File file) 
    {
    	
    	InputStream is = null;
    	try {
    		is = new FileInputStream(file);
    	
	        long length = file.length();
	        if (length > Integer.MAX_VALUE) {
	        	throw new RuntimeException(file.getAbsolutePath() + 
	        		" file is too long, " + length + " byte. Max allowed limit is " + Integer.MAX_VALUE);
	        }
    
	        byte[] bytes = new byte[(int)length]; // Create the byte array to hold the data
	        int offset = 0; // Read in the bytes
	        int numRead = 0;
	        
	        while (offset < bytes.length
	        	&& (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
	        	offset += numRead;
	        }
	        
	        if (offset < bytes.length) { // Ensure all the bytes have been read in
	            throw new RuntimeException("Could not completely read file, " + file.getAbsolutePath());
	        }
	        
	        return bytes;
        } catch (IOException ex) {
        	LOG.fatal("util.FileReaderUtil > ", ex);
        	throw new RuntimeException(ex);
        } finally {
        	try { if ( null != is ) is.close(); } 
        	catch (Exception ex) {LOG.warn("util.FileReaderUtil > ", ex);} 
        }        	
    }
	
	
	public static List<String> toLines(String fileName) {
		
		File aFile = getFile(fileName);
		BufferedReader reader = null;
		InputStream stream = null;
		try {
			stream = new FileInputStream(aFile); 
			reader = new BufferedReader ( new InputStreamReader (stream) );
			List<String> lines = new ArrayList<String>();
			String line = null;
			while((line=reader.readLine())!=null) {
				if (line.length() == 0) continue;
				char first=line.charAt(0);
				switch (first) {
					case ' ' : case '\n' : case '#' :  // skip blank & comment lines
					continue;
				}
				lines.add(line);	
			}
			return lines;
		} catch (Exception ex) {
			LOG.fatal("util.FileReaderUtil", ex);			
			throw new RuntimeException(ex);
		} finally {
			try {if ( null != reader ) reader.close();
			} catch (Exception ex) {LOG.warn("util.FileReaderUtil", ex);}
			try {if ( null != stream) stream.close();
			} catch (Exception ex) {LOG.warn("util.FileReaderUtil", ex);}
		}
	}

	public static String getLineSeaprator() {
		  String seperator = System.getProperty ("line.seperator");
		  if ( null == seperator) return "\n";
		  else return seperator;
	  }
}
