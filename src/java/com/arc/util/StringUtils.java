/**
Copy Right : C2014 ARC Document Solutions. All Rights Reserved.

This software is the confidential and proprietary information of ARC
Document Solutions, INC. You shall not disclose such Confidential
Information and shall use it only in accordance with the terms of the
license agreement you entered into with ARC.The software may not be resold
or redistributed. Usage is governed by the FOLDER-FILE SYNC TLicense
Agreement.   Unauthorized reproduction or distribution is subject to civil
and criminal penalties. This notice may not be removed from this file
*/
package com.arc.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;

import com.arc.util.conf.ArcConfig;

public final class StringUtils {
	  
	 public static final String Empty = ""; 
	 public static final String FatalPrefix = "****\n";
	 
	 public static final char SEPARATOR_FIELD_WORD = '~';
	 public static final String SEPARATOR_FIELD_WORD_STR = "~";
	 public static final char SEPARAOTOR_FIELD = ';';
	 public static final String SEPARAOTOR_FIELD_STR = ";";
	 public static final char SEPARATOR_RECORD = ':';
	 public static final String SEPARATOR_RECORD_STR = ":";
	 public static final char PADDING_CHAR = '_';
	 
	 public static final List<String> EMPTY_LIST = new ArrayList<String>();
	 public static final Set<String> EMPTY_SET = new HashSet<String>();
	 public static final String[] EmptyArray = new String[]{};
	
	  /**
	   * Make a string representation of the exception.
	   * @param e The exception to stringify
	   * @return A string with exception name and call stack.
	   */
	  public final static String stringifyException(Throwable e) {
	    StringWriter stm = new StringWriter();
	    PrintWriter wrt = new PrintWriter(stm);
	    e.printStackTrace(wrt);
	    wrt.close();
	    return stm.toString();
	  }

	  /**
	   * Checks if a string is empty
	   * @param text String value to check
	   * @return true/false.
	   */
	  public final static boolean isEmpty (final String text) {
	    if (null == text ) return true;
	    if ( text.length() == 0 ) return true;
	    else return false;
	  }
	  
	  public final static boolean isNonEmpty (final String text) {
		    if (null == text ) return false;
		    if ( text.length() == 0 ) return false;
		    return true;
	  }	  

	  /**
	   * Given an array of strings, return a comma-separated list of its elements.
	   * @param strs Array of strings
	   * @return Empty string if strs.length is 0, comma separated list of strings
	   * otherwise
	   */
	  
	  public final static String arrayToString(final String[] strs) {
	    return arrayToString(strs, ',');
	  }
	  
	  public final static String getLineSeaprator() {
		  String seperator = System.getProperty ("line.seperator");
		  if ( null == seperator) return "\n";
		  else return seperator;
	  }
	  
	  public final static String arrayToString(final String[] strs,final char delim) {
	    if (strs == null || strs.length == 0) { return ""; }
	    StringBuffer sbuf = new StringBuffer();
	    sbuf.append(strs[0]);
	    for (int idx = 1; idx < strs.length; idx++) {
	      sbuf.append(delim);
	      sbuf.append(strs[idx]);
	    }
	    return sbuf.toString();
	 }

	  /**
	   * returns an arraylist of strings  
	   * @param str the comma seperated string values
	   * @return the arraylist of the comma seperated string values
	   */
	  public final static String[] getStrings(final String str){
		  return getStrings(str, ",");
	  }

	  /**
	   * returns an arraylist of strings  
	   * @param str the delimiter seperated string values
	   * @param delimiter
	   * @return the arraylist of the comma seperated string values
	   */
	  public final static String[] getStrings(final String str,final String delimiter){
	    if (isEmpty(str)) return null;
	    StringTokenizer tokenizer = new StringTokenizer (str,delimiter);
	    List<String> values = new ArrayList<String>();
	    while (tokenizer.hasMoreTokens()) {
	      values.add(tokenizer.nextToken());
	    }
	    return (String[])values.toArray(new String[values.size()]);
	  }

	  public final static String[] getStrings (final String line,final char delim) {
		  String[] result = new String[]{ line, null };
		  if (line == null) return result;
		  
		  int splitIndex = line.indexOf(delim);
		  if ( -1 != splitIndex) {
				result[0] = line.substring(0,splitIndex);
				if ( line.length() > splitIndex )
				result[1] = line.substring(splitIndex+1);
		  }
		  return result;
	  }
	  
	  public final static List<String> fastSplit(final String text, char separator) {
		  return fastSplit (null, text, separator);
	  }
	  
	  public final static List<String> fastSplit(List<String> result , final String text,final char separator) {
		  if (isEmpty(text)) return null;

		  if ( null == result) result = new ArrayList<String>();
		  int index1 = 0;
		  int index2 = text.indexOf(separator);

		  if ( index2 >= 0 ) {
			  String token = null;
			  while (index2 >= 0) {
				  token = text.substring(index1, index2);
				  result.add(token);
				  index1 = index2 + 1;
				  index2 = text.indexOf(separator, index1);
				  if ( index2 < 0 ) index1--;
			  }
		            
			  if (index1 < text.length() - 1) {
				  result.add(text.substring(index1+1));
			  }
			  
		  } else {
			  result.add(text);
		  }
		  
		  return result;
	  }	  
	  
	  public static final void fastSplit( final String[] result, final int[] positions, 
		final String text, final char separator) {
		  
		if (text == null) return;
		if (text.length() == 0) return;

		int index1 = 0;
		int index2 = text.indexOf(separator);

		int pos = -1;
		int resultSeq = 0;
		if ( index2 >= 0 ) {
			String token = null;
			while (index2 >= 0) {
				pos++;
				for ( int aPos: positions ) {
					if ( pos != aPos) continue;
					token = text.substring(index1, index2);
					result[resultSeq++] = token;
					break;
				}
				index1 = index2 + 1;
				index2 = text.indexOf(separator, index1);
				if ( index2 < 0 ) index1--;
			}
		            
			if (index1 < text.length() - 1) {
				pos++;
				for ( int aPos: positions ) {
					if ( pos != aPos) continue;
					result[resultSeq++] = text.substring(index1+1);
					break;
				}
			}
			  
		  } else {
			  pos++;
			  for ( int aPos: positions ) {
				  if ( pos != aPos) continue;
					result[resultSeq++] = text;
				  break;
			  }
		  }
	  }	  
	  
	  
	  public final static String firstTokens(final String text,final char separator,final int tokens) {
		  if (isEmpty(text)) return null;

		  int index1 = 0;
		  int index2 = text.indexOf(separator);
		  StringBuilder sb = new StringBuilder();
		  String token = null;
		  int loop = 0;
		  while (index2 >= 0 && loop < tokens) {
			  token = text.substring(index1, index2);
			  sb.append(token).append(' ');
			  loop++;
			  index1 = index2 + 1;
			  index2 = text.indexOf(separator, index1);
		  }
	            
		  if ((index1 < text.length() - 1) && loop < tokens) {
			  sb.append(token);
		  }
		  return sb.toString();
	  }
	  
	  
	  public final static List<String> arrayToList(final String[] strA){
		  if ( null == strA) return null;
		  List<String> strL = new ArrayList<String>(strA.length);
		  for (String aStr : strA) {
			  strL.add(aStr);
		  }
		  return strL;
	  }
	  
	  
	  /**
	   * returns an arraylist of strings  
	   * @param str the comma seperated string values
	   * @return the arraylist of the comma seperated string values
	   */
	  public final static Set<String> getUniqueStrings(final String str){
	    if (str == null)
	      return EMPTY_SET;

	    StringTokenizer tokenizer = new StringTokenizer (str,",");
	    Set<String> values = new HashSet<String>();
	    while (tokenizer.hasMoreTokens()) {
	      values.add(tokenizer.nextToken());
	    }
	    return values;
	  }
	  
	  /**
	   * 
	   * @param input - The original String
	   * @param padding - The padding character
	   * @param finalLength - Final length we want to have 
	   * @return
	   */
	  public final static String pad( String input,final char padding,final int finalLength) {
		  int length = 0;
		  if ( null != input ) length = input.length();
		  else input = "";
		  
		  if ( finalLength > length ) {
		  		StringBuilder sb = new StringBuilder(finalLength);
		  		sb.append(input);
		  		for ( int i=length; i< finalLength; i++ ) sb.append(padding);
		  		return sb.toString();
		  } else {
			  return input.substring(0,finalLength);
		  } 
	  }
	  
	  public final static String encodeXml(String text) {
		  text = text.replaceAll("<", "&lt;");
		  text = text.replaceAll(">", "&gt;");
		  text = text.replaceAll("&", "&amp;");
		  return text;
	  }
	  
	  public final static String removeNonBreakingSpaces(final String text)
	  {
		  if (StringUtils.isEmpty(text)) return StringUtils.Empty;
		  
		  StringBuilder sb = new StringBuilder(text.length());
		  for (char ch : text.toCharArray())
		  {
			  if ((int)ch != 160) sb.append(ch);
		  }
		  return sb.toString();
	  }
	  
	  /**
	   * Removes lucene special characters from the text.
	   * Known speical characters are: + - && || ! ( ) { } [ ] ^ " ~ * ? : \
	   * @param text
	   * @return
	   */
	  public final static String escapeLucene(final String text)
	  {
		  if (StringUtils.isEmpty(text)) return StringUtils.Empty;
		  
		  StringBuilder sb = new StringBuilder(text.length());
		  for (char ch : text.toCharArray())
		  {
			  switch(ch)
			  {
			  	case '+': break;
			  	case '-': break;
			  	case '&': break;
			  	case '|': break;
			  	case '!': break;
			  	case '{': break;
			  	case '}': break;
			  	case '(': break;
			  	case ')': break;
			  	case '[': break;
			  	case ']': break;
			  	case '^': break;
			  	case '"': break;
			  	case '~': break;
			  	case '*': break;
			  	case '?': break;
			  	case ':': break;
			  	case '\\': break;
			  	default:
			  		if ((int)ch != 160) sb.append(ch);
			  }
		  }
		  return sb.toString();
	  }
	  
	  /**
	   * Removes special characters from the text, fit for a filename.
	   * Known speical characters are: + - && || ! ( ) { } [ ] ^ " ~ * ? : \
	   * @param text
	   * @return
	   */
	  public final static String escapeFilename(final String text)
	  {
		  if (StringUtils.isEmpty(text)) return StringUtils.Empty;
		  
		  StringBuilder sb = new StringBuilder(text.length());
		  for (char ch : text.toCharArray())
		  {
			  switch(ch)
			  {
			  	case '+': break;
			  	case '&': break;
			  	case '|': break;
			  	case '!': break;
			  	case '{': break;
			  	case '}': break;
			  	case '(': break;
			  	case ')': break;
			  	case '[': break;
			  	case ']': break;
			  	case '^': break;
			  	case '"': break;
			  	case '~': break;
			  	case '*': break;
			  	case '?': break;
			  	case ':': break;
			  	case '\\': break;
			  	case '/': break;
			  	case '\'': break;
			  	case '`': break;
			  	case ' ': 
			  		sb.append('_');
			  		break;
			  	default:
			  		if ((int)ch != 160) sb.append(ch);
			  }
		  }
		  return sb.toString();
	  }
	  
	  public final static String decode(final String text) {
		if ( null == text) return null;
		final char[] arrText = text.toCharArray();
		final int arrTextT = arrText.length;
		StringBuilder sb = new StringBuilder(arrTextT);
		char first,second,third,forth;
		for ( int i=0; i<arrTextT; i++ ) {
			first = arrText[i];
			if ( first == '-' ) {
				if ( i+3 < arrTextT) {
					forth = arrText[i+3];
					if ( forth == '-') {
						second = arrText[i+1];
						third = arrText[i+2];
						
						if ( second == 'a' && third == 'm') {
							sb.append("&"); i = i + 3;continue;
						} else if ( second == 'e' && third == 'q') {
							sb.append('='); i = i + 3;continue;
						} else if ( second == 'p' && third == 'l') {
							sb.append('+'); i = i + 3;continue;
						}else if ( second == 'n' && third == 'l') {
							sb.append("<BR />"); i = i + 3;continue;
						} else if ( second == 'c' && third == 'o') {
							sb.append(':'); i = i + 3;continue;
						} else if ( second == 't' && third == 'l') {
							sb.append('~'); i = i + 3;continue;
						} else if ( second == 'l' && third == 't') {
							sb.append("<"); i = i + 3;continue;
						} else if ( second == 'g' && third == 't') {
							sb.append(">"); i = i + 3;continue;
						}
					}
				}
			}
			sb.append(first);
		}
		String replacedMsg = sb.toString().replace("/\\\'", "'");
		return replacedMsg;
	  }
	  
	  /**
	   * from \/ to .
	   * c:\\\abhinash//karan/haha..ram.to
	   * c:...abinash..karan.haha..ram.to
	   * @param str
	   * @param from
	   * @param to
	   * @return
	   */
	  public final static String replaceMultipleCharsToAnotherChar(final String str,final char[] replacables,final char replaceBy) {
		  final StringBuilder sb = new StringBuilder(str.length());
		  final char[] chars = str.toCharArray();
		  boolean replaced = false;
		  
		  for (char c : chars) {
			  
			  replaced = false;
			  for (char replacable : replacables ) {
				if ( c == replacable) {
					sb.append(replaceBy);
					replaced = true;
					break;
				}
			  }
			  if ( replaced ) continue;
			  
			  sb.append(c);
		  }
		  return sb.toString();
	  }
	  
	  public final static int totalSighings(final String str,final char find) {
		  if ( null == str) return 0;
		  final char[] chars = str.toCharArray();
		  int counter = 0;
		  for (char c : chars) {
			  if ( c == find) counter++; 
		  }
		  return counter;
	  }
	  
	  public final static String encodeText(final String text, final String encoding)
	  {
		  try
		  {
			  return URLEncoder.encode(text, encoding);
		  } 
		  catch (UnsupportedEncodingException e)
		  {
			  return text;
		  }
	  }
	  
	  public final static String escapeXml(final String text)
	  {
		  return StringEscapeUtils.escapeXml(text);
	  }
	  
	  public final static String encodeText(final String text)
	  {
		  if (StringUtils.isEmpty(text)) return text;
		  return text.replace(" ", "%20");
	  }
	  
	  public final static String padWithQuotes(final String text)
	  {
		  if (isEmpty(text)) return text;
		  StringBuilder sb = new StringBuilder(text.length() + 2);
		  sb.append("\"").append(text).append("\"");
		  return sb.toString();
	  }
	  
	  private final static Pattern pattern = Pattern.compile("\\s+");
	  public final static String stripExtraSpace(String input)
	  {
		  Matcher matcher = pattern.matcher(input);
		  return matcher.replaceAll(" ");
	  }
	  
	  public final static String getId(final String text) {
		  if (isEmpty(text)) return text;
		  return new Integer(text.hashCode()).toString();
	  }
	  
	  public final static String[] toStringArray(final Object[] array) {
		  String[] newArray = new String[array.length];

		  for (int i = 0; i < array.length; i++) {
			  newArray[i] = String.valueOf(array[i]);
		  }
		  return newArray;
	}	  
}
