package com.arc.util.log;

import org.apache.log4j.Logger;

public final class Log4JLoggerWebApi implements ILogger {

	Logger l = Logger.getLogger("webapidownlogger");
	
	@Override
	public final boolean isTraceEnabled() {
		return l.isTraceEnabled();
	}

	@Override
	public final  void trace(final Object message, final Throwable t) {
		l.trace(message, t);
	}

	@Override
	public final void trace(final Object message) {
		l.trace(message);
	}

	@Override
	public final void debug(final Object message, final Throwable t) {
		l.debug(message, t);
	}

	@Override
	public final void debug(final Object message) {
		l.debug(message);
	}

	@Override
	public final void error(final Object message, final Throwable t) {
		l.error(message,t);
	}

	@Override
	public final void error(final Object message) {
		l.error(message);
	}

	@Override
	public final void fatal(final Object message, final Throwable t) {
		l.fatal(message,t);
	}

	@Override
	public final void fatal(final Object message) {
		l.fatal(message);
		
	}

	@Override
	public final void info(final Object message, final Throwable t) {
		l.info(message,t);
	}

	@Override
	public final void info(final Object message) {
		l.info(message);
	}

	@Override
	public boolean isDebugEnabled() {
		return l.isDebugEnabled();
	}

	@Override
	public boolean isInfoEnabled() {
		return l.isInfoEnabled();
	}

	@Override
	public final void warn(final Object message, final Throwable t) {
		l.warn(message,t);
	}

	@Override
	public final void warn(final Object message) {
		l.warn(message);
	}



}
