package com.arc.util.log;

import java.util.Date;

import org.apache.log4j.Logger;

public final class ConsoleLogger implements ILogger {

	private static final int TRACE = 0;
	private static final int DEBUG = 1;
	private static final int INFO = 2;
	private static final int WARN = 3;
	private static final int ERROR = 4;
	private static final int FATAL = 5;
	
	private String className = ConsoleLogger.class.getName();
	
	public ConsoleLogger(){
	}
	
	public ConsoleLogger(String clazz){
		this.className = clazz;
	}
	
	@Override
	public final boolean isTraceEnabled() {
		return false;
	}

	@Override
	public final void trace(final Object message, final Throwable t) {
		if ( ! isTraceEnabled()) return;
		System.err.println(getPrefix(TRACE) + message.toString());
		t.printStackTrace();
	}

	@Override
	public final void trace(final Object message) {
		if ( ! isTraceEnabled()) return;
		System.err.println(getPrefix(TRACE) + message.toString());
	}

	@Override
	public final void debug(final Object message, final Throwable t) {
		if ( ! isDebugEnabled()) return;
		System.err.println(getPrefix(DEBUG) + message.toString());
		t.printStackTrace();
	}

	@Override
	public final void debug(final Object message) {
		if ( ! isDebugEnabled()) return;
		System.err.println(getPrefix(DEBUG) + message.toString());
	}

	@Override
	public final void error(final Object message, final Throwable t) {
		System.err.println(getPrefix(ERROR) + message.toString());
		t.printStackTrace();
	}

	@Override
	public final void error(final Object message) {
		System.err.println(getPrefix(ERROR) + message.toString());
	}

	@Override
	public final void fatal(final Object message, final Throwable t) {
		System.err.println(getPrefix(FATAL) + message.toString());
		t.printStackTrace();
	}

	@Override
	public final void fatal(final Object message) {
		System.err.println(getPrefix(FATAL) + message.toString());
	}

	@Override
	public final void info(final Object message, final Throwable t) {
		if ( ! isInfoEnabled()) return;
		System.err.println(getPrefix(INFO) + message.toString());
		t.printStackTrace();
	}

	@Override
	public final void info(final Object message) {
		if ( ! isInfoEnabled()) return;
		System.err.println(getPrefix(INFO) + message.toString());
	}

	@Override
	public final boolean isDebugEnabled() {
		return Logger.getRootLogger().isDebugEnabled();
	}

	@Override
	public final boolean isInfoEnabled() {
		return Logger.getRootLogger().isInfoEnabled();
	}

	@Override
	public final void warn(final Object message, final Throwable t) {
		System.err.println(getPrefix(WARN) + message.toString());
		t.printStackTrace();
	}

	@Override
	public final void warn(final Object message) {
		System.err.println(getPrefix(WARN) + message.toString());
	}
	
	public final String getPrefix(final int level) {
		StringBuilder sb = new StringBuilder();
		sb.append(this.className);
		switch ( level) {
			case  TRACE:
				sb.append("TRACE>");
				break;
			case  DEBUG:
				sb.append("DEBUG>");
				break;
			case  INFO:
				sb.append("INFO>");
				break;
			case  WARN:
				sb.append("WARN>");
				break;
			case  ERROR:
				sb.append("ERROR>");
				break;
			case  FATAL:
				sb.append("FATAL>");
				break;
		}
		sb.append(new Date().toString()).append(" : ");
		return  sb.toString(); 
	}
}
