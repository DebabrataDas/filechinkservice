package com.arc.util.log;

public interface ILogger {

	
	boolean isTraceEnabled();

	
	void trace(Object message, Throwable t);

	
	void trace(Object message);

	
	void debug(Object message, Throwable t);

	
	void debug(Object message);

	
	void error(Object message, Throwable t);

	
	void error(Object message);

	
	void fatal(Object message, Throwable t);

	
	void fatal(Object message);

	
	void info(Object message, Throwable t);

	
	void info(Object message);

	
	boolean isDebugEnabled();

	
	boolean isInfoEnabled();

	
	void warn(Object message, Throwable t);

	
	void warn(Object message);

}
