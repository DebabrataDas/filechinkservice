/**
Copy Right : C2014 ARC Document Solutions. All Rights Reserved.

This software is the confidential and proprietary information of ARC
Document Solutions, INC. You shall not disclose such Confidential
Information and shall use it only in accordance with the terms of the
license agreement you entered into with ARC.The software may not be resold
or redistributed. Usage is governed by the FOLDER-FILE SYNC TLicense
Agreement.   Unauthorized reproduction or distribution is subject to civil
and criminal penalties. This notice may not be removed from this file
*/
package com.arc.util.log;

public final class ArcLogger {
	//public final static ILogger l = new ConsoleLogger();
	public final static ILogger l = new Log4JLogger();
	public final static ILogger l1 = new Log4JLoggerUmq();
	public final static ILogger l2 = new Log4JLoggerWebApi();

	
	/*public final static ILogger getConsoleLoggerInstance( String className )
	{
		return new ConsoleLogger(className);
	}*/
}
