package com.arc.util;

import java.io.FileOutputStream;

import com.arc.util.log.ArcLogger;

public class FileWriterUtil 
{
	/**
	 * Append the information to the file
	 * @param fileName
	 * @param text
	 */
	public static boolean appendToFile(String fileName, byte[] data) 
	{
	    try
	    {
	    	FileOutputStream out = new FileOutputStream(fileName, true);
	        out.write(data);
	        out.flush();
	        out.close();
	        return true;
        } 
	    catch (Exception ex) 
	    {
	    	ArcLogger.l.error(ex);
        	ex.printStackTrace(System.err);
        	return false;
        }
	}
}