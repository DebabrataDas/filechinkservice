/**
Copy Right : C2014 ARC Document Solutions. All Rights Reserved.

This software is the confidential and proprietary information of ARC
Document Solutions, INC. You shall not disclose such Confidential
Information and shall use it only in accordance with the terms of the
license agreement you entered into with ARC.The software may not be resold
or redistributed. Usage is governed by the FOLDER-FILE SYNC TLicense
Agreement.   Unauthorized reproduction or distribution is subject to civil
and criminal penalties. This notice may not be removed from this file
*/
package com.arc.util.conf;

/** Base class for things that may be configured with a {@link Configuration}. */
public final class Configured implements Configurable {

  private Configuration conf; 

  /** Construct a Configured. */
  public Configured(final Configuration conf) {
	  this.setConf(conf); 
  }

  // inherit javadoc
  public final void setConf(final Configuration conf) {
    this.conf = conf;
  }

  // inherit javadoc
  public final Configuration getConf() {
    return conf;
  }

}
