/**
Copy Right : C2014 ARC Document Solutions. All Rights Reserved.

This software is the confidential and proprietary information of ARC
Document Solutions, INC. You shall not disclose such Confidential
Information and shall use it only in accordance with the terms of the
license agreement you entered into with ARC.The software may not be resold
or redistributed. Usage is governed by the FOLDER-FILE SYNC TLicense
Agreement.   Unauthorized reproduction or distribution is subject to civil
and criminal penalties. This notice may not be removed from this file
*/
package com.arc.util.conf;

/** Something that may be configured with a {@link Configuration}. */
public interface Configurable {

  /** Set the configuration to be used by this object. */
  void setConf(Configuration conf);

  /** Return the configuration used by this object. */
  Configuration getConf(); 
}

