package com.arc.util.conf;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.arc.chunk.ChunkSetting;
import com.arc.util.FileReaderUtil;
import com.arc.util.log.ArcLogger;
import com.bizosys.hsearch.byteutils.ByteUtil;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public final class ArcConfig {
	public static final Configuration conf = new Configuration();
	public static final char COMMA = ',';
	public static final char PIPE = '|';

	/**
	 * Chunk Settings Json { "doc":
	 * 
	 * ( Comma Separated File Types, Pipe Separated Values )
	 * docType|magic_section_bytesize|content_section_bytesize|hash_algo
	 */

	public static final String CHUNK_SPLIT_SETTINGS_FILE = conf.get("chunk.split.settings.file","/home/arc/backend-workspace/arcfilechunk-code/data/chunkSettings.json");		//subjected to change

	public static Map<String, ChunkSetting> chunkSplitSettings = new HashMap<String, ChunkSetting>();



	static {
		String chunkSplitSettingsJSon = "";
		try {
			chunkSplitSettingsJSon = FileReaderUtil.toString(CHUNK_SPLIT_SETTINGS_FILE);
		} catch (Exception e) {
			System.out.println("Chunksss missing ");
			ArcLogger.l.fatal("Split Settings file not found.");
		}

		JsonElement element = new JsonParser().parse(chunkSplitSettingsJSon);
		JsonObject jobject = element.getAsJsonObject();

		for (Entry<String, JsonElement> jsonM : jobject.entrySet()) {
			String key = jsonM.getKey();
			JsonElement settings = jsonM.getValue();
			JsonObject settingsObj = settings.getAsJsonObject();

			ChunkSetting chunkSetting = new ChunkSetting();
			chunkSetting.magicSectionChunkSize = settingsObj.getAsJsonPrimitive("magic_section_bytesize").getAsInt();
			chunkSetting.contentSectionChunkSize = settingsObj.getAsJsonPrimitive("content_section_bytesize")
					.getAsInt();
			chunkSetting.hashAlgo = settingsObj.getAsJsonPrimitive("hash_algo").getAsString();

			chunkSplitSettings.put(key, chunkSetting);
		}
	}

	/**
	 * System run settings
	 */
	public static final boolean TEST_MODE = conf.getBoolean("test.mode", true);

	/**
	 * System Authentication Settings
	 */
	public static final String AUTHENTICATE_URL = conf.get("authenticate.url",
			"https://pwservicestg1.planwellcollaborate.com/Web/api/Authentication");
	
	public static final String S3ACCESS_KEY_URL = conf.get("s3access.api.url","https://staging.skysite.com/skysiteapi/api/Account/GetAccountBucketInfo");
	
	public static String servertoken = conf.get("awskey.rest.servertoken", "I+44PtjI3wVSa33ql3ab0w==");	//subjected to change
	
	public static String AES_KEY = conf.get("awskey.rest.aeskey", "LOG");//subjected to change

	public static String moduleid = conf.get("awskey.rest.moduleid", "PWP");	//subjected to change
	
	public static final int AUTHENTICATE_MAX_TIMEOUT = conf.getInt("rest.authenticate.max.timeout", 14400); // 4*60*60

	public static final int AUTHENTICATE_MIN_TIMEOUT = conf.getInt("rest.authenticate.min.timeout", 3600); // 1*60*60

	public static final String AUTHENTICATE_FATAL = conf.get("rest.authenticate.fatal", "CONTACT_ADMIN");

	public static final String LOGIN_PARAMETER = conf.get("rest.param.login", "loginid");

	public static final String PASSWORD_PARAMETER = conf.get("rest.param.password", "password");

	public static final String CONTENT_TYPE = conf.get("rest.contenttype", "application/json");

	public static final String AUTHENTICATE_PARAM_TOKENKEY = conf.get("rest.param.tokenkey", "tokenkey");

	public static final String AUTHENTICATE_PARAM_TOKEN = conf.get("rest.param.arctoken", "arctoken");

	public static final String ALLOWED_HOSTS = conf.get("rest.hosts.allow", "127.0.0.1,localhost,0:0:0:0:0:0:0:1");

	public static final int AUTHENTICATE_TIMEOUT = conf.getInt("rest.timeout", 1000);

	public static final int CHUNK_HEADER_SIZE = conf.getInt("chunk.header.size", 8);

	/**
	 * Monitoring Parameters
	 */
	public static final String REQUEST_VELOCITY = conf.get("request.velocity.name", "Request Velocity");
	public static final String ACTIVE_SESSION_PARAM = conf.get("active.session.name", "Active Sessions");
	public static final String AVG_RESPONSE_PARAM = conf.get("avg.response.name", "Average Response Time");
	public static final String MIN_RESPONSE_PARAM = conf.get("min.response.name", "Min Response Time");
	public static final String MAX_RESPONSE_PARAM = conf.get("max.response.name", "Max Response Time");
	public static final String AVG_BYTES_PARAM = conf.get("avg.bytes.name", "Average Bytes");
	public static final String MIN_BYTES_PARAM = conf.get("min.bytes.name", "Min Bytes");
	public static final String MAX_BYTES_PARAM = conf.get("max.bytes.name", "Max Bytes");

	public static final String HBASE_READ_REQUEST_VELOCITY = conf.get("hbase.read.request.velocity.name",
			"HBase Read Request Velocity");
	public static final String HBASE_ACTIVE_READ_SESSION_PARAM = conf.get("hbase.active.read.session.name",
			"HBase Active Read Sessions");
	public static final String HBASE_AVG_READ_RESPONSE_PARAM = conf.get("hbase.avg.read.response.name",
			"HBase Average Read Time");
	public static final String HBASE_MIN_READ_RESPONSE_PARAM = conf.get("hbase.min.read.response.name",
			"HBase Min Read Time");
	public static final String HBASE_MAX_READ_RESPONSE_PARAM = conf.get("hbase.max.read.response.name",
			"HBase Max Read Time");
	public static final String HBASE_AVG_READ_BYTES_PARAM = conf.get("hbase.avg.read.bytes.name",
			"HBase Average Read Bytes");
	public static final String HBASE_MIN_READ_BYTES_PARAM = conf.get("hbase.min.read.bytes.name",
			"HBase Min Read Bytes");
	public static final String HBASE_MAX_READ_BYTES_PARAM = conf.get("hbase.max.read.bytes.name",
			"HBase Max Read Bytes");

	public static final String HBASE_WRITE_REQUEST_VELOCITY = conf.get("hbase.write.request.velocity.name",
			"HBase Write Request Velocity");
	public static final String HBASE_ACTIVE_WRITE_SESSION_PARAM = conf.get("hbase.active.write.session.name",
			"HBase Active Write Sessions");
	public static final String HBASE_AVG_WRITE_RESPONSE_PARAM = conf.get("hbase.avg.write.response.name",
			"HBase Average Write Time");
	public static final String HBASE_MIN_WRITE_RESPONSE_PARAM = conf.get("hbase.min.write.response.name",
			"HBase Min Write Time");
	public static final String HBASE_MAX_WRITE_RESPONSE_PARAM = conf.get("hbase.max.write.response.name",
			"HBase Max Write Time");
	public static final String HBASE_AVG_WRITE_BYTES_PARAM = conf.get("hbase.avg.write.bytes.name",
			"HBase Average Write Bytes");
	public static final String HBASE_MIN_WRITE_BYTES_PARAM = conf.get("hbase.min.write.bytes.name",
			"HBase Min Write Bytes");
	public static final String HBASE_MAX_WRITE_BYTES_PARAM = conf.get("hbase.max.write.bytes.name",
			"HBase Max Write Bytes");

	// Breathe time for each process
	public static final long BREATHE_TIME_MILLIS = conf.getLong("breathe.time.millis", 50);
	public static final String CHUNK_HASH = conf.get("chunk.hash", "chunkHash");
	public static final String CHUNK_HEADER = conf.get("chunk.header", "chunkHeader");
	public static final String CHUNKS_REFERENCED = conf.get("chunks.referenced", "chunksReferenced");
	public static final String CHUNKS_JSON = conf.get("chunks.json", "chunksJSON");
	public static final String CHUNK_OFFSET_JSON = conf.get("chunk.offset.json", "chunkOffsetJSON");
	public static final String S3_CONTAINER_PATH = conf.get("s3.container.path", "s3ContainerPath");
	public static final String FOLDER_NAME = "folderName";
	public static final String LOGICAL_FILE_NAME = "logicalFileName";
	public static final int FILE_ID_PREFIX = 85;	//subjected to change
	public static final int REVISION_ID_PREFIX = 85; //subjected to change

	public static final String ACCOUNT_ID = conf.get("account.id", "accountId");
	public static final String MULTI_ID = conf.get("multi.id", "count");
	public static final String PROJECT_ID = conf.get("project.id", "projectId");
	public static final String REVISION = conf.get("revision", "revision");
	public static final String BASE_REVISION = conf.get("base.revision", "baseRevision");
	public static final String FILE_ID = conf.get("file.id", "fileId");
	public static final String REVISION_ID = conf.get("revision.id", "revisionId");
	public static final String FILE_LENGTH = conf.get("file.length", "fileLength");
	public static final String END_BYTE = conf.get("end.byte", "endByte");
	public static final String START_BYTE = conf.get("start.byte", "startByte");
	public static final String MAGIC_HEADER = conf.get("magic.header", "magicHeader");
	public static final String THUMBNAIL_TYPE = conf.get("thumbnail.type", "thumbnailType");

	public static final String DOWNLOAD_URL_TYPE = conf.get("download.url.type", "DOWNLOAD");
	public static final String UPLOAD_URL_TYPE = conf.get("upload.url.type", "UPLOAD");
	public static final String URL_TYPE = conf.get("url.type", "urlType");
	public static final String FILE_NAME = conf.get("file.name", "fileName");
	public static final byte[] COL_DEFAULT_BYTES = ByteUtil.toBytes(conf.get("col.default.name", "default"));
	public static final byte[] ROW_DEFAULT_BYTES = ByteUtil.toBytes(conf.get("row.default.value", "1"));
	public static final byte[] HBASE_FAMILY_BYTES = ByteUtil.toBytes(conf.get("hbase.family.name", "1"));
	
	//subjected to change
	// staging tables info
	/*
	 * public final static String LINKS_TABLE =
	 * conf.get("s3.container.mapping.table","S3LINKS-SKYSITE"); public final
	 * static String FILEID_TABLE =
	 * conf.get("fileid.table.name","FILEID-SKYSITE"); public final static
	 * String REVISIONID_TABLE =
	 * conf.get("revisionid.table.name","REVISIONID-SKYSITE"); public final
	 * static String CHUNKS_TABLE =
	 * conf.get("chunks.table.name","CHUNKS-SKYSITE"); public final static
	 * String FILECHUNKS_TABLE =
	 * conf.get("filechunks.table.name","FILECHUNKS-SKYSITE"); public final
	 * static String FILEPARROTHASH_TABLE =
	 * conf.get("fileparrothash.table.name","FILEPARROTHASH-SKYSITE");
	 */

	// production tables info
	//subjected to change
	
	public final static String LINKS_TABLE = conf.get("s3.container.mapping.table", "S3LINKS-SKYSITE");
	public final static String FILEID_TABLE = conf.get("fileid.table.name", "FILEID-SKYSITE");
	public final static String REVISIONID_TABLE = conf.get("revisionid.table.name", "REVISIONID-SKYSITE");
	public final static String CHUNKS_TABLE = conf.get("chunks.table.name", "CHUNKS-SKYSITE");
	public final static String FILECHUNKS_TABLE = conf.get("filechunks.table.name", "FILECHUNKS-SKYSITE");
	public final static String FILEPARROTHASH_TABLE = conf.get("fileparrothash.table.name", "FILEPARROTHASH-SKYSITE");
	
	public final static String S3ACCESS_TABLE = conf.get("s3access.table.name", "S3ACCESS-SKYSITE");
	public final static String S3ACCESS_API_JSON_HEADER_KEY = conf.get("s3access.api.json.header.key","EncryptAccountBucketInfo");
	public final static String S3ACCESS_API_PARAMETER = conf.get("s3access.api.parameter","pwAccountId");
	public final static String S3ACCESS_API_HEADER_KEY = conf.get("s3access.api.header.key1","servertoken");
	public final static String S3ACCESS_API_HEADER_KEY_PWP = conf.get("s3access.api.header.key2","moduleid");
	
	public static final byte[] COL_BASE_REVISION_BYTES = ByteUtil
			.toBytes(conf.get("base.revision.col.name", "baseRevision"));
	public static final byte[] COL_CHUNK_OFFSET_JSON_BYTES = ByteUtil
			.toBytes(conf.get("offsetjson.col.name", "chunkOffsetJSON"));
	public static final byte[] COL_CHUNKS_JSON_BYTES = ByteUtil
			.toBytes(conf.get("chunkjson.column.name", "chunksJSON"));
	
	public static final byte[] COL_BUCKET_NAME_BYTES = ByteUtil
			.toBytes(conf.get("bucket.name.col.name", "bucketName"));
	public static final byte[] COL_ACCESS_KEY_BYTES = ByteUtil
			.toBytes(conf.get("accessKey.col.name", "accessKey"));
	public static final byte[] COL_SECRETE_KEY_BYTES = ByteUtil
			.toBytes(conf.get("secreteKey.col.name", "secreteKey"));

	public static final String METHOD_POST = conf.get("method.post", "POST");

	public static final String TEMP_FILE = conf.get("temp.file", "/tmp/tempFile");
	public static final String TEMP_DIRECTORY = conf.get("temp.directory", "/tmp/");
	public static final String PATCH_EXTENSION = conf.get("patch.extension", ".patch");

	public static final String ACTION = conf.get("action", "action");
	public static final String REQUEST_PARAM_KEY = conf.get("request.param.key", "jsondata");
	public static final byte[] responseOKBytes = conf.get("response.ok", "OK").getBytes();
	public static final String REST_FATAL = conf.get("rest.exception.fatal", "CONTACT_ADMIN");

	public static final long FILE_ID_START = conf.getLong("file.id.start", 1L);

	public static final long REVISION_ID_START = conf.getLong("revision.id.start", 1L);
	public static final byte[] COL_EXT_BYTES = ByteUtil.toBytes(conf.get("extension.column.name", "ext"));;
	// UMQ related configuration

	//subjected to change
	// staging umq info

	
	  public static final String UMQ_URL_IP = conf.get("umq.url.ip","umqstg-1413839612.us-west-1.elb.amazonaws.com"); 
	  public static final String UMQ_QUEUE_NAME = conf.get("umq.queuename", "SkysiteStitch"); 
	  public static final String ROUTING_KEY = conf.get("umq.routingkey", "Default"); 
	  public static final String PUBLISHER_ID = conf.get("umq.producerId","CID-952-11634196886950132"); 
	  public static final String CONSUMER_ID = conf.get("umq.consumerId", "CID-822-11634276808618365");
	  public static final int BATCH_SIZE = conf.getInt("umq.batchsize", 3); 
	  public static final String KEY_SEPARATOR = "|$|";
	 

	// dev umq info

	
/*	 public static final String UMQ_URL_IP = conf.get("umq.url.ip","10.10.87.244:8080"); 
	 public static final String UMQ_QUEUE_NAME = conf.get("umq.queuename", "SKYSITEStitchLocal"); 
	 public static final String ROUTING_KEY = conf.get("umq.routingkey", "Default");
	 public static final String PUBLISHER_ID = conf.get("umq.producerId","CID-605-244341593945807"); 
	 public static final String CONSUMER_ID =conf.get("umq.consumerId", "CID-790-244390005376964");
	 public static final int BATCH_SIZE = conf.getInt("umq.batchsize", 3); 
	 public static final String KEY_SEPARATOR = "|$|";*/
	 

	// production umq info
	/*
	public static final String UMQ_URL_IP = conf.get("umq.url.ip", "umqprod-1594450894.us-west-1.elb.amazonaws.com");
	public static final String UMQ_QUEUE_NAME = conf.get("umq.queuename", "SkysiteStitchProd");
	public static final String ROUTING_KEY = conf.get("umq.routingkey", "Default");
	public static final String PUBLISHER_ID = conf.get("umq.producerId", "CID-667-8465517849468285");
	public static final String CONSUMER_ID = conf.get("umq.consumerId", "CID-94-8465583818208642");
	public static final int BATCH_SIZE = conf.getInt("umq.batchsize", 3);
	public static final String KEY_SEPARATOR = "|$|";
	*/

}
