package com.arc.hbase;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.arc.filechunk.EncryptionException;
import com.arc.filechunk.s3utils.AESEncryption;


import org.apache.hadoop.hbase.client.Put;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;

import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.arc.filechunk.DecryptionException;
import com.arc.filechunk.InvalidResponseExcepton;
import com.arc.filechunk.model.BucketInfo;

import com.arc.util.conf.ArcConfig;

import com.bizosys.hsearch.byteutils.ByteUtil;


public class BucketInfoCache {
	static Logger l = Logger.getLogger(BucketInfoCache.class);
	private static final String API_URL = ArcConfig.S3ACCESS_KEY_URL;
	private static final byte[] HBASE_FAMILY_BYTES = ArcConfig.HBASE_FAMILY_BYTES;
	private static final byte[] HBASE_COLUMN_BUCKET_NAME_BYTES = ArcConfig.COL_BUCKET_NAME_BYTES;
	private static final byte[] HBASE_COLUMN_ACCESS_KEY_BYTES = ArcConfig.COL_ACCESS_KEY_BYTES;
	private static final byte[] HBASE_COLUMN_SECRETE_KEY_BYTES = ArcConfig.COL_SECRETE_KEY_BYTES;
	private final static String S3ACCESS_TABLE = ArcConfig.S3ACCESS_TABLE;
	private final static String EQUAL = "=";
	private final static String QUESTION_MARK = "?";
	private static Map<String,BucketInfo> bucketInfoM = new HashMap<String, BucketInfo>();


	private static AESEncryption ENCRYPTOR;
	static {
		try {
			ENCRYPTOR = AESEncryption.getInstance();
		} catch (EncryptionException e) {
			l.error("Couldn't create AES encryption object");
			e.printStackTrace();
		}
	}

	private BucketInfoCache(){
		
	}
		
	public static  boolean refreshBucketInfo(String accountId){
		try {
			
			BucketInfo bucket = getBucketInfoFromAPI(accountId);
			putBucketInfo(accountId, bucket);
			l.info("Bucket Info for accountId: "+accountId+" refreshed ");
			return true;
		} catch (InvalidResponseExcepton e) {
			l.error("Got Invalid response from API : "+e.getMessage());
			return false;
			
		}
	}
	
	
	private static BucketInfo getBucketInfoFromAPI(String accountId)throws InvalidResponseExcepton {
		CloseableHttpClient client = HttpClients.createDefault();
		String URL = API_URL + QUESTION_MARK + ArcConfig.S3ACCESS_API_PARAMETER + EQUAL + accountId;
		HttpGet publishmsg = new HttpGet(URL);
		publishmsg.addHeader(ArcConfig.S3ACCESS_API_HEADER_KEY, ArcConfig.servertoken);
		publishmsg.addHeader(ArcConfig.S3ACCESS_API_HEADER_KEY_PWP, ArcConfig.moduleid);
		HttpResponse responsepublish;
		try {
			responsepublish = client.execute(publishmsg);
			String responseString = new BasicResponseHandler().handleResponse(responsepublish);
			System.out.println(responseString);
			String encryptedBucketInfo = new JSONObject(responseString).getString(ArcConfig.S3ACCESS_API_JSON_HEADER_KEY);
			responseString  = decryptData(ByteUtil.toBytes(encryptedBucketInfo));
			System.out.println(responseString);
			JSONObject response = new JSONObject(responseString);
			
			String bucketName = response.getString("BucketName");
			String accessKey =  response.getString("AWSAccessKey");
			String secreteKey = response.getString("AWSSecretKey");
			l.debug("received  bucket information API for accountId = "+accountId+" bucketName = "+bucketName+" accessKey :"+accessKey+" secreteKey"+secreteKey);
			if(!isValidresponse(bucketName,accessKey,secreteKey))throw new InvalidResponseExcepton("decryption",accountId, bucketName, accessKey, secreteKey);
			
			return new BucketInfo(bucketName, accessKey, secreteKey);
			
		} catch (JSONException | IOException | DecryptionException ex) {
			
			throw new InvalidResponseExcepton(ex.getClass().toString(),null,null,null,null);
			
		}	
	}

	
	private static boolean isValidresponse(String bucketNameE, String accessKeyE, String secreteKeyE) {
		if(bucketNameE == null ||
				bucketNameE.equals("")||
				accessKeyE == null ||
				accessKeyE.equals("")||
				secreteKeyE == null ||
				secreteKeyE.equals("")||
				bucketNameE.equals("null")||
				accessKeyE.equals("null")||
				secreteKeyE.equals("null")
				)
			return false;
		return true;
	}


	


	public static  BucketInfo getBucketInfo(String accountId){
		if(bucketInfoM.isEmpty())
			try {
				loadCacheFromHbase();
			} catch (IOException e) {
				l.error(e.getMessage());
			} catch (DecryptionException e) {
				l.error("Got Decryption Exception");
			}
		BucketInfo bucket = bucketInfoM.get(accountId);
		if(bucket == null)
			refreshBucketInfo(accountId);
		bucket = bucketInfoM.get(accountId);
		return bucket;
	}
	
	public static BucketInfo getBucketInfo(int accountId){
		return getBucketInfo(Integer.toString(accountId));
	}
	
	public static void  putBucketInfo(String accountId,BucketInfo bucket){
		if(bucketInfoM == null) bucketInfoM = new HashMap<String, BucketInfo>();
		
		if(accountId == null || !isValidresponse(bucket.getBucketName(), bucket.getAccessKey(), bucket.getSecreteKey()) )return;
		
		try {
			updateHbaseTable(accountId,bucket);
			bucketInfoM.put(accountId, bucket);
			l.debug("accountId = "+accountId+" , is inserted");
		} catch (IOException e) {
			l.error(e.getMessage());
		} catch (EncryptionException e) {
			l.error("Got EncryptionException "+e);
		}
		return;
	}

	private static void updateHbaseTable(String accountId, BucketInfo bucket) throws IOException, EncryptionException {
		HbaseTableOperation hto = new HbaseTableOperation();
		Put put = new Put(ByteUtil.toBytes(accountId));
		byte[] bucketName = encryptString(bucket.getBucketName());
		byte[] accessKey = encryptString(bucket.getAccessKey());
		byte[] secretKey = encryptString(bucket.getSecreteKey());
		put.add(HBASE_FAMILY_BYTES, HBASE_COLUMN_BUCKET_NAME_BYTES,bucketName);
		put.add(HBASE_FAMILY_BYTES, HBASE_COLUMN_ACCESS_KEY_BYTES, accessKey);
		put.add(HBASE_FAMILY_BYTES, HBASE_COLUMN_SECRETE_KEY_BYTES, secretKey);
		hto.writeToTable(S3ACCESS_TABLE, put);
		
	}


	public static void loadCacheFromHbase() throws IOException, DecryptionException {
		HbaseTableOperation hto = new HbaseTableOperation();
		try {
			bucketInfoM.putAll(hto.scanS3AcessTable());
		} catch (DecryptionException e) {
			l.error("Got Decryption Exception");
			throw e;
		}

	}
	
	public static boolean refreshAllBuckets(){
		try{
			l.info("Refreshing all buckets exist in Hbase");
			HbaseTableOperation hto = new HbaseTableOperation();
			Set<String> keyset = hto.scanS3AcessTable().keySet();
			for(String accountId : keyset){
				refreshBucketInfo(accountId);
			}
			l.info("all buckets refreshed");
			return true;
		}catch(IOException ex){
			l.error(ex.getMessage());
			return false;
		} catch (DecryptionException e) {
			l.error("Got Decryption Exception");
			return false;
		}

	}
	
	private static byte[] encryptString(String input) throws EncryptionException {
		byte[] result;
		result = ENCRYPTOR.encrypt(input);
		return result;
	}
	private static String decryptData(byte[] bytes) throws DecryptionException {
		String result = "";
		result = ENCRYPTOR.decrypt(bytes);
		return result;
	}


	
	public static void main(String args[]) throws InvalidResponseExcepton, EncryptionException{
		BucketInfo bucket = getBucketInfoFromAPI("11616507");
		if(bucket.getBucketName().equals("null"))System.out.println("true");;
		AESEncryption instance = AESEncryption.getInstance();
		System.out.println(new String(instance.encrypt(bucket.getBucketName())));
		System.out.println(bucket.getBucketName()+"\t"+bucket.getAccessKey()+"\t"+bucket.getSecreteKey());
	}
	
}
