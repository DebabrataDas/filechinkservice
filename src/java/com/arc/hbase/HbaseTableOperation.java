package com.arc.hbase;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.arc.filechunk.DecryptionException;
import com.arc.filechunk.EncryptionException;
import com.arc.filechunk.s3utils.AESEncryption;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Durability;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.log4j.Logger;

import com.arc.filechunk.model.BucketInfo;
import com.arc.management.MonitorHBase;
import com.arc.util.conf.ArcConfig;
import com.bizosys.hsearch.byteutils.ByteUtil;
import com.bizosys.hsearch.hbase.HBaseFacade;
import com.bizosys.hsearch.hbase.HTableWrapper;

public class HbaseTableOperation
{
	private static final byte[] ROW_DEFAULT_BYTES = ArcConfig.ROW_DEFAULT_BYTES;
	private static final byte[] COL_DEFAULT_BYTES = ArcConfig.COL_DEFAULT_BYTES;
	private static final byte[] HBASE_FAMILY_BYTES = ArcConfig.HBASE_FAMILY_BYTES;
	private final static String CHUNKS_TABLE = ArcConfig.CHUNKS_TABLE;
	private final static String FILEID_TABLE = ArcConfig.FILEID_TABLE;
	private final static String REVISIONID_TABLE = ArcConfig.REVISIONID_TABLE;
	private AESEncryption ENCRYPTOR;
	static Logger l = Logger.getLogger(HbaseTableOperation.class);
	
	HBaseFacade facade = null;
	HTableWrapper table = null;
	Configuration config = HBaseConfiguration.create();
    HBaseAdmin adm = new HBaseAdmin(config);
	
	public HbaseTableOperation() throws IOException
	{
		l.debug("Initializing Hbase Table operation");
		try{
			ENCRYPTOR = AESEncryption.getInstance();
			facade = HBaseFacade.getInstance();
		}catch(IOException e){
			l.error("Got Hbase initialiaztion error :"+e);
            e.printStackTrace();
		}catch (EncryptionException e) {
            l.error("ERROR in creating Encryption object :"+e);
            e.printStackTrace();
        }
		
	}
	
	public final long getFileIds( long incrementAmount ) throws Exception
	{
		long valueAfterIncrement = 0;
		try
		{
			
			  adm.isMasterRunning();
			l.debug("Getting fileIds");
			MonitorHBase.getInstance().onWriteEnter();
			table = facade.getTable(FILEID_TABLE);
			valueAfterIncrement = table.incrementColumnValue(ROW_DEFAULT_BYTES, 
					HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, incrementAmount);
			table.flushCommits();
			return valueAfterIncrement;
		} 
		catch (Exception e)
		{
			l.debug("Exception occured while getting FileIds");
			System.err.println("Exception in getFileIds " + e.getMessage());
			throw e;
		}
		finally
		{
			MonitorHBase.getInstance().onWriteExit(null, 0);
			if ( null != table) {
				facade.putTable(table);
				table = null;
				//table.close();
			}
		}
	}
	
	public final long getRevisionIds( long incrementAmount ) throws Exception
	{
		long valueAfterIncrement = 0;
		try
		{
			
			  adm.isMasterRunning();
			MonitorHBase.getInstance().onWriteEnter();
			table = facade.getTable(REVISIONID_TABLE);
			valueAfterIncrement = table.incrementColumnValue(ROW_DEFAULT_BYTES, 
					HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, incrementAmount);
			table.flushCommits();
			return valueAfterIncrement;
		} 
		catch (Exception e)
		{
			l.debug("Exception occured while getting revisionIds");
			System.err.println("Exception in getFileIds " + e.getMessage());
			throw e;
		}
		finally
		{
			MonitorHBase.getInstance().onWriteExit(null, 0);
			if ( null != table) {
				facade.putTable(table);
				table = null;
				//table.close();
			}
		}
	}
	
	public final void addToChunks(final int accountId, final String chunkHeader, 
			final String chunkHash) throws Exception
	{
		
		  adm.isMasterRunning();
		StringBuilder sb = new StringBuilder();
		
		sb.append(accountId).append('_')
			.append(chunkHeader).append('_')
			.append(chunkHash);
		
		byte[] chunksKey = ByteUtil.toBytes(sb.toString());
		byte[] defaultNumReferences = ByteUtil.toBytes(0);
		
		writeToTable(CHUNKS_TABLE, HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, chunksKey, 
				defaultNumReferences);
	}

	public final void writeToTable(final String tableName, final byte[] family, final byte[] column, 
			final byte[] key, final byte[] value) throws Exception
	{
		try
		{
			adm.isMasterRunning();
			MonitorHBase.getInstance().onWriteEnter();
			table = facade.getTable(tableName);
			Put put = new Put(key) ;
			put.add(family, column, value);
			table.put(put);
			table.flushCommits();
		} 
		catch (Exception e)
		{
			System.err.println("Exception in writeToTable " + e.getMessage());
			throw e;
		}
		finally
		{
			MonitorHBase.getInstance().onWriteExit(null, value.length);
			if ( null != table) {
				facade.putTable(table);
				table = null;
				//table.close();
			}
		}
	}
	
	public final void writeToTable(final String tableName, Put put) throws IOException
	{

		try
		{
			adm.isMasterRunning();
			MonitorHBase.getInstance().onWriteEnter();
			table = facade.getTable(tableName);
			table.put(put);
			table.flushCommits();
		} 
		catch (IOException e)
		{
			System.err.println("Exception in writeToTable " + e.getMessage());
			throw e;
		}
		finally
		{
			MonitorHBase.getInstance().onWriteExit(null, put.size());
			if ( null != table) {
				facade.putTable(table);
				table = null;
				//table.close();
			}
		}
	}

	public final void writeBatchToTable(final String tableName, final List<Put> putL ) throws Exception
	{
		int size = 0;
		try
		{
			adm.isMasterRunning();
			MonitorHBase.getInstance().onWriteEnter();
			table = facade.getTable(tableName);
			table.put(putL);
			table.flushCommits();
		} 
		catch (Exception e)
		{
			System.err.println("Exception in writeBatchToTable " + e.getMessage());
			throw e;
		}
		finally
		{
			MonitorHBase.getInstance().onWriteExit(null, size);
			if ( null != table) {
				facade.putTable(table);
				table = null;
				//table.close();
			}
		}
	}

	public final Result readResultFromTable(final String tableName, final byte[] key) throws Exception
	{
		Get get = null;
		Result result = null;
		try
		{
			adm.isMasterRunning();
			MonitorHBase.getInstance().onReadEnter();
			table = facade.getTable(tableName);
			get = new Get(key);
			result = table.get(get);
		}
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			MonitorHBase.getInstance().onReadExit(null, result.size());
			if (null != table) {
				try {facade.putTable(table);} catch (Exception ex) {ex.printStackTrace();}
			}
			table = null;
		}
		return result;
	}
	
	public final byte[] readValueFromTable(final String tableName, final byte[] familyName, 
			final byte[] columnName, final byte[] key) throws Exception
	{
		byte[] value = null;
		try
		{
			adm.isMasterRunning();
			MonitorHBase.getInstance().onReadEnter();
			table = facade.getTable(tableName);
			Get get = new Get(key);
			Result result = table.get(get);
			value = result.getValue(familyName, columnName);
		} 
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			MonitorHBase.getInstance().onReadExit(null, ((null == value ) ? 0 : value.length));
			if (null != table) {
				try {facade.putTable(table);} catch (Exception ex) {ex.printStackTrace();}
			}
			table = null;
		}
		return value;
	}
	
	
	public final HashMap<String,BucketInfo> scanS3AcessTable() throws IOException, DecryptionException {
		ResultScanner scanner = null;
		try {
			
			HashMap<String,BucketInfo> bucketInfoM = new HashMap<String, BucketInfo>();
			table = facade.getTable(ArcConfig.S3ACCESS_TABLE);
			Scan scan = new Scan();
			
			scan.setCaching(10000);
			scan.setMaxVersions(1);
			scan.addColumn(HBASE_FAMILY_BYTES, ArcConfig.COL_BUCKET_NAME_BYTES);
			scan.addColumn(HBASE_FAMILY_BYTES, ArcConfig.COL_ACCESS_KEY_BYTES);
			scan.addColumn(HBASE_FAMILY_BYTES, ArcConfig.COL_SECRETE_KEY_BYTES);
			scanner = table.getScanner(scan);
			
			for(Result r : scanner){
				byte[] accountIdB = r.getRow();
				byte[] bucketNameB = r.getValue(HBASE_FAMILY_BYTES, ArcConfig.COL_BUCKET_NAME_BYTES);
				byte[] accessKeyB = r.getValue(HBASE_FAMILY_BYTES, ArcConfig.COL_ACCESS_KEY_BYTES);
				byte[] secreteKeyB = r.getValue(HBASE_FAMILY_BYTES, ArcConfig.COL_SECRETE_KEY_BYTES);
				
				String accountId = ByteUtil.toString(accountIdB, 0, accountIdB.length);
                String bucketName = decryptData(bucketNameB);
                String accessKey = decryptData(accessKeyB);
                String secreteKey = decryptData(secreteKeyB);
				
				bucketInfoM.put(accountId, new BucketInfo(bucketName, accessKey, secreteKey));
				l.debug("Data from Hbase loaded . size = "+bucketInfoM.size()+"\n");
				
			}
			return bucketInfoM;
			
		} catch (IOException e) {
			throw e;
		}finally {
			if (null != scanner) {
				try {scanner.close();} catch (Exception ex) {ex.printStackTrace();}
			}
			
			if (null != table) {
				try {facade.putTable(table);} catch (Exception ex) {ex.printStackTrace();}
			}	
		}
		
	}
	
    private String decryptData(byte[] bytes) throws DecryptionException {
        String result = "";
        result = ENCRYPTOR.decrypt(bytes);
        return result;
 
    }
	public final void incrementColumnValue( String tableName, byte[] row, byte[] family, byte[] qualifier, long amount )
	{
		try
		{
			adm.isMasterRunning();
			MonitorHBase.getInstance().onWriteEnter();
			table = facade.getTable(tableName);
//			table.incrementColumnValue(row, family, qualifier, amount);
			table.tableInterface.incrementColumnValue(row, family, qualifier, amount, Durability.ASYNC_WAL);
			table.flushCommits();
		} 
		catch (Exception e)
		{
			System.err.println("Exception in incrementColumnValue " + e.getMessage());
		}
		finally
		{
			MonitorHBase.getInstance().onWriteExit(null, 0);
			if ( null != table) {
				facade.putTable(table);
	
				table = null;		
				
			}
		}
	}
	
	
	
	
	
	public final void incrementRowBatch( String tableName, List<byte[]> rowL, byte[] family, byte[] qualifier, long amount )
	{
		try
		{
			adm.isMasterRunning();
			MonitorHBase.getInstance().onWriteEnter();
			table = facade.getTable(tableName);
			for ( byte[] row : rowL )
			{
				table.incrementColumnValue(row, family, qualifier, amount);
			}
			table.flushCommits();
		} 
		catch (Exception e)
		{
			System.err.println("Exception in incrementColumnValue " + e.getMessage());
		}
		finally
		{
			MonitorHBase.getInstance().onWriteExit(null, 0);
			if ( null != table) {
				facade.putTable(table);
				
				table = null;
				
			}
		}
	}

	public final void deleteFromTable(final String tableName, final byte[] familyName, 
			final byte[] columnName, final byte[] key) throws Exception
	{
		Delete delete = null;
		try
		{
			adm.isMasterRunning();
			table = facade.getTable(tableName);
			delete = new Delete(key);
			table.delete(delete);
		} 
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			if (null != table) {
				try {facade.putTable(table);} catch (Exception ex) {ex.printStackTrace();}
			}
			table = null;
		}
	}
	
	public final void deleteFromTable(final String tableName, Delete delete) throws Exception
	{
		try
		{
			adm.isMasterRunning();
			table = facade.getTable(tableName);
			table.delete(delete);
		} 
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			if (null != table) {
				try {facade.putTable(table);} catch (Exception ex) {ex.printStackTrace();}
			}
			table = null;
		}
	}
	
}