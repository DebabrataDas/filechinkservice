package com.arc.hbase.job;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;

import com.arc.util.conf.ArcConfig;
import com.bizosys.hsearch.byteutils.ByteUtil;
import com.bizosys.hsearch.hbase.HBaseFacade;
import com.bizosys.hsearch.hbase.HTableWrapper;

public final class CleanUpJob
{
	private static final byte[] COL_DEFAULT_BYTES = ArcConfig.COL_DEFAULT_BYTES;
	private static final byte[] HBASE_FAMILY_BYTES = 
			ArcConfig.HBASE_FAMILY_BYTES;
	private final static String CHUNKS_TABLE = ArcConfig.CHUNKS_TABLE;
	private final static String FILEPARROTHASH_TABLE = 
			ArcConfig.FILEPARROTHASH_TABLE;
	
	HBaseFacade facade = null;
	HTableWrapper table = null;
	
	public CleanUpJob() throws IOException
	{
		facade = HBaseFacade.getInstance();
	}
	
	private final void process() throws IOException 
	{
		long start = System.currentTimeMillis();
		cleanFileParrotHash();
		long end = System.currentTimeMillis();
		
		start = System.currentTimeMillis();
		cleanChunks();
		end = System.currentTimeMillis();
	}
	
	private final void cleanFileParrotHash() throws IOException
	{
		try
		{
			table = facade.getTable(FILEPARROTHASH_TABLE);
			Scan scan = new Scan();
			scan.addFamily(HBASE_FAMILY_BYTES);
			ResultScanner rs = table.getScanner(scan);
			List<Delete> deleteL = new ArrayList<Delete>(256);
			for(Result result : rs)
			{
				List<Cell> cells = result.listCells();
				for(Cell cell : cells)
				{
					if(cell.getValueLength() == 0)
					{
						byte[] qualifier = new byte[cell.getQualifierLength()];
						System.arraycopy(cell.getQualifierArray(), cell.getQualifierOffset(), qualifier, 0, cell.getQualifierLength());
						Delete del = new Delete(result.getRow());
						del.deleteColumn(HBASE_FAMILY_BYTES, qualifier);
						deleteL.add(del);
					}

				}
			}
			table.delete(deleteL);
		}
		catch(IOException e)
		{
			throw e;
		}
		finally
		{
			if ( null != table) 
			{
				facade.putTable(table);
				table = null;
			}
		}
	}
	
	private final void cleanChunks() throws IOException
	{
		try
		{
			table = facade.getTable(CHUNKS_TABLE);
			Scan scan = new Scan();
			scan.addColumn(HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES);
			ResultScanner rs = table.getScanner(scan);
			List<Delete> deleteL = new ArrayList<Delete>(256);
			long reference = 0;
			for(Result result : rs)
			{
				reference = ByteUtil.toLong(result.value(), 0);
				if( reference == 0 )
				{
					Delete del = new Delete(result.getRow());
					deleteL.add(del);
				}
			}
			table.delete(deleteL);
		} 
		catch (IOException e)
		{
			throw e;
		}
		finally
		{
			if ( null != table) 
			{
				facade.putTable(table);
				table = null;
			}
		}
	}
	
	public static void main(String[] args) throws IOException
	{
		CleanUpJob cuj = new CleanUpJob();
		cuj.process();
	}
	
}
