package com.arc.umq.dynamicservice;



import com.fasterxml.jackson.annotation.JsonProperty;

public class QueueMessageCount {

@JsonProperty("success")
private Boolean success;
@JsonProperty("totCount")
private Integer totCount;

@JsonProperty("success")
public Boolean getSuccess() {
return success;
}

@JsonProperty("success")
public void setSuccess(Boolean success) {
this.success = success;
}

@JsonProperty("totCount")
public Integer getTotCount() {
return totCount;
}

@JsonProperty("totCount")
public void setTotCount(Integer totCount) {
this.totCount = totCount;
}



}