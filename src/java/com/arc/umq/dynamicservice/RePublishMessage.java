package com.arc.umq.dynamicservice;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Properties;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

public class RePublishMessage {
	private static String id;
	

	public static void main(String[] args) throws ClientProtocolException, IOException {
		// TODO Auto-generated method stub
		String instanceid =null;

		
			id = args[0];	//get pid from command line argument
			Properties prop = new Properties();
			InputStream input = null;

			try {
				input = new FileInputStream("/root/chunkcode/pidinstidmapr.properties"); 

				// load a properties file
				prop.load(input);
				instanceid = prop.getProperty(id); //read the corresponding instanceid from pid
				// get the property value and print it out

			} catch (IOException ex) {
				ex.printStackTrace();
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

		  
		String umqip = "umqprod-1594450894.us-west-1.elb.amazonaws.com";
		String consumerid = "CID-94-8465583818208642";
		String republishurl = "http://"+umqip+"/mq-service/service/v1/rest/consumer/republish?clientId=";
		
		String url = republishurl+consumerid+"&instanceId="+instanceid;
		
		HttpClient client = new DefaultHttpClient();
		HttpGet publishmsg = new HttpGet(url);
		HttpResponse responsepublish;
		try {
			responsepublish = client.execute(publishmsg);
			String responseString = new BasicResponseHandler().handleResponse(responsepublish);
			System.out.println(responseString);
		} catch (IOException e) {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("/root/chunkcode/republishfailed.txt",true)));
			out.println("Inside RePublishMessage: Message failed for instanceid "+instanceid+" and pid "+id+" at time "+new Date());
			out.close();
			System.out.println("failure");
			e.printStackTrace();

		}
		
	}

}
