package com.arc.umq.dynamicservice;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import com.fasterxml.jackson.databind.ObjectMapper;

public class UMQCount {

	/*
	 * private static final String umq_base_url = "http://" +
	 * ArcConfig.UMQ_URL_IP + "/mq-service/service/v1/rest/"; private static
	 * final String umq_msg_count_url = umq_base_url +
	 * "queue/queueMsgCount?clientId=" + ArcConfig.PUBLISHER_ID;
	 */
	static String url = "http://umqprod-1594450894.us-west-1.elb.amazonaws.com/mq-service/service/v1/rest/queue/queueMsgCount?clientId=CID-667-8465517849468285";
	static final HttpParams httpParams = new BasicHttpParams();
	static HttpClient client;
	static HttpGet publishmsg = new HttpGet(url);
	static HttpResponse responsepublish;
	static QueueMessageCount queuemsgcount;

	public static void main(String[] args) throws IOException {

	
		try {
			HttpConnectionParams.setConnectionTimeout(httpParams, 5000);
			client = new DefaultHttpClient(httpParams);
			responsepublish = client.execute(publishmsg);		
			String responseString = new BasicResponseHandler().handleResponse(responsepublish);
			ObjectMapper mapper = new ObjectMapper();
			queuemsgcount = mapper.readValue(responseString, QueueMessageCount.class);
			int count = queuemsgcount.getTotCount();
			System.out.println(count);
		} catch (IOException e) {
			e.printStackTrace();
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("/root/chunkcode/republishfailed.txt",true)));
			out.println("Inside umqcount: cannot connect to url: "+url+" at time: "+new Date());
			out.close();

		}

	}

}
