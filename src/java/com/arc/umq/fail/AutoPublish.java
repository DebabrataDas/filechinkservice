package com.arc.umq.fail;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

@SuppressWarnings("deprecation")
public class AutoPublish {

	static String url = "http://umqprod-1594450894.us-west-1.elb.amazonaws.com/mq-service/service/v1/rest/producer/publish";
	static BufferedReader br;
	static BufferedWriter out;

	public static void main(String[] args) throws IOException {

		String umqdownfilepath = "/mnt/logs/arcfilechunkskysiteumqdown.log";
		try {
			br = new BufferedReader(new FileReader(umqdownfilepath));
			String line = br.readLine();

			while (line != null) {
				String[] split = line.split("JSON message failed: ");
				String secondSubString = split[1];
				String messagetostitch = secondSubString.substring(0, secondSubString.length());
				System.out.println(messagetostitch);
				final HttpParams httpParams = new BasicHttpParams();
			    HttpConnectionParams.setConnectionTimeout(httpParams, 30000);
				HttpClient client = new DefaultHttpClient(httpParams);
				HttpPost publishmsg = new HttpPost(url);

				StringEntity inputpublish = new StringEntity(messagetostitch);
				publishmsg.addHeader("Content-Type", "application/json");
				publishmsg.setEntity(inputpublish);
				HttpResponse responsepublish = client.execute(publishmsg);
				String responseString = new BasicResponseHandler().handleResponse(responsepublish);
				if (responseString.contains("Message Published Successfully")) {
					System.out.println("Stitch successful");

				}
				line = br.readLine();
			}
			out = new BufferedWriter(new FileWriter(umqdownfilepath, false));

		} catch (IOException e) {
			System.out.println(e);
		} finally {

			br.close();
			out.flush();
			out.close();
		}

	}

}
