package com.arc.umq.service;

import java.util.List;


public class ConsumeWrapper extends BaseResponse{

	private String clientId;
	//--processed message details
	private Integer processedCount;
	private Integer successCount;
	private Integer failureCount;
	private List<MessageResponse> msgs;
	private Integer msgCount;
	
	
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public Integer getProcessedCount() {
		return processedCount;
	}
	public void setProcessedCount(Integer processedCount) {
		this.processedCount = processedCount;
	}
	public Integer getSuccessCount() {
		return successCount;
	}
	public void setSuccessCount(Integer successCount) {
		this.successCount = successCount;
	}
	public Integer getFailureCount() {
		return failureCount;
	}
	public void setFailureCount(Integer failureCount) {
		this.failureCount = failureCount;
	}
	public List<MessageResponse> getMsgs() {
		return msgs;
	}
	public void setMsgs(List<MessageResponse> msgs) {
		this.msgs = msgs;
	}
	public Integer getMsgCount() {
		return msgCount;
	}
	public void setMsgCount(Integer msgCount) {
		this.msgCount = msgCount;
	}

	
}
