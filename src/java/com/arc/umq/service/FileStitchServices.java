package com.arc.umq.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.metastatic.rsync.Rdiff;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.CompleteMultipartUploadRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadResult;
import com.amazonaws.services.s3.model.PartETag;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.UploadPartRequest;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerConfiguration;
import com.amazonaws.services.s3.transfer.Upload;
import com.arc.hbase.BucketInfoCache;
import com.arc.hbase.HbaseTableOperation;
import com.arc.filechunk.s3utils.GeneratePresignedUrlAndUploadObject;
import com.arc.service.FileStitchService;
import com.arc.util.FileReaderUtil;
import com.arc.util.conf.ArcConfig;
import com.arc.util.log.ArcLogger;
import com.bizosys.hsearch.PerformanceLogger.L;
import com.bizosys.hsearch.byteutils.ByteUtil;
import com.bizosys.hsearch.hbase.HBaseException;
import com.bizosys.hsearch.treetable.compiler.FileWriterUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

public class FileStitchServices implements Runnable{

	
	private int accountId;
	private String bucketName;
	private long fileId;
	private long revision;
	private int projectId;
	private long baserevision;
	private String threadName;
	private String msgId;

	

	public FileStitchServices(int accountId,String bucketName, long fileId, long revision,int projectId, long baserevision,String threadName,String msgId) {
		this.accountId = accountId;
		this.bucketName = bucketName;
		this.fileId = fileId;
		this.revision = revision;
		this.projectId = projectId;
		this.baserevision = baserevision;
		this.threadName=threadName;
		this.msgId=msgId;
		
	}

	public int getAccountId() {
		return accountId;
	}


	public long getFileId() {
		return fileId;
	}



	public long getRevision() {
		return revision;
	}

	public int getProjectId() {
		return projectId;
	}



	public long getBaserevision() {
		return baserevision;
	}


	public String getThreadName() {
		return threadName;
	}


	public String getMsgId() {
		return msgId;
	}


	static Date dt = new Date(System.currentTimeMillis());
	static SimpleDateFormat ft = new SimpleDateFormat("yyyy:MM:dd-hh:mm:ss");
	private  final static String TEMP_FILE = ArcConfig.TEMP_FILE;
	private  final static String TEMP_DIRECTORY = ArcConfig.TEMP_DIRECTORY;

	private  final static String PATCH_EXTENSION = ArcConfig.PATCH_EXTENSION;

	private  final static char UNDERSCORE = '_';

	private  final static String CHUNK_HASH = ArcConfig.CHUNK_HASH;
	private  final static String CHUNK_HEADER = ArcConfig.CHUNK_HEADER;

	private final  String LINKS_TABLE = ArcConfig.LINKS_TABLE;
	private final static  String FILECHUNKS_TABLE = ArcConfig.FILECHUNKS_TABLE;
	private  final static byte[] COL_CHUNKS_JSON_BYTES = ArcConfig.COL_CHUNKS_JSON_BYTES;
	private  final byte[] COL_DEFAULT_BYTES = ArcConfig.COL_DEFAULT_BYTES;
	private  final static byte[] HBASE_FAMILY_BYTES = ArcConfig.HBASE_FAMILY_BYTES;

	private  final static Rdiff rdiff = new Rdiff();

	private  final static int numRetries = 3;
	private  final static byte[] COL_EXT_BYTES = ArcConfig.COL_EXT_BYTES;
	private  final String DOT = ".";
	
	
	/**
	 * The file stitch service
	 * @param accountId in which file is present
	 * @param fileId of file
	 * @param revision of file
	 * @param projectId in which file is present
	 * @param baseRevision of file
	 * @throws Exception
	 */
	public final static int process(final int accountId,final String bucketName, final long fileId, final long revision,final int projectId, final long baseRevision,final String threeadName) throws Exception
	{
	
		String S3_SECRET_KEY = BucketInfoCache.getBucketInfo(accountId).getSecreteKey();
		String S3_ACCESS_KEY = BucketInfoCache.getBucketInfo(accountId).getAccessKey();
		AmazonS3 s3client = new AmazonS3Client(	new BasicAWSCredentials( S3_ACCESS_KEY,	S3_SECRET_KEY));
		String projectIdStr = Integer.toString(projectId);
		ArcLogger.l.debug("Stitch service accound Id: "+accountId+" projectId: "+projectId+" revision: "+revision+" s3AccessKey: "+S3_ACCESS_KEY+" S3secretKey: "+S3_SECRET_KEY+" bucketName:"+bucketName);
		
		
		ArcLogger.l.debug("Stitch process started at: "+ft.format(dt));
		final HbaseTableOperation hto = new HbaseTableOperation();

		//Get s3 bucket for accountId
		//byte[] s3BucketNameB = hto.readValueFromTable(LINKS_TABLE, HBASE_FAMILY_BYTES,COL_DEFAULT_BYTES, ByteUtil.toBytes(accountId));


		String s3BucketName = bucketName;
		
		
		ArcLogger.l.debug("Getting bucket Name corresponding to accounntId: "+accountId+" bucket name is:"+s3BucketName);
		StringBuilder sb = new StringBuilder();

		if( revision == baseRevision )
		{

			try {

				ArcLogger.l.debug("Current revision is equal to base revision");
				// Is a base revision. Take the entire file chunks and stitch
				String tmpFilePath = TEMP_FILE+threeadName;    // /tmp/tempFile+threeadName
				File tmpFile = new File(tmpFilePath);

				ArcLogger.l.debug("File path to download chunks: "+tmpFilePath);
				sb.setLength(0);
				sb.append(accountId).append(UNDERSCORE).append(projectId).append(UNDERSCORE)
				.append(fileId).append(UNDERSCORE).append(revision);
				String fileKey = sb.toString();
				byte[] key = ByteUtil.toBytes(fileKey);

				sb.setLength(0);
				sb.append(fileId).append(UNDERSCORE).append(revision);
				byte[] extB = hto.readValueFromTable(FILECHUNKS_TABLE, HBASE_FAMILY_BYTES,COL_EXT_BYTES, key);
				String ext = ByteUtil.toString(extB, 0, extB.length);
				String fileName = sb.toString()+ext;
				
				//Check whether the key exists in S3 or not,if present do nothing,else proceed
				boolean keyExist = checkKeyExist(S3_ACCESS_KEY, S3_SECRET_KEY, s3BucketName, accountId+"/"+projectId+"/"+fileName);
				if(keyExist)return 0;
				
				
				File stitchedFile = new File(TEMP_DIRECTORY+"/"+threeadName +fileName);


				if ( stitchedFile.exists() ) stitchedFile.delete();

				//Get all the chunks from filechunk table
				ArcLogger.l.debug("Getting all chunks from FILECHUNKS table");
				byte[] chunksJsonB = hto.readValueFromTable(FILECHUNKS_TABLE, HBASE_FAMILY_BYTES,
						COL_CHUNKS_JSON_BYTES, key);
				String chunksJson = ByteUtil.toString(chunksJsonB, 0, chunksJsonB.length);
				JsonArray chunksArray = new JsonParser().parse(chunksJson).getAsJsonArray();
				ArcLogger.l.debug("Total chunks to be downloaded for stitching: "+chunksArray.size());
				System.out.println("Total chunks to be downloaded for stitching: "+chunksArray.size());
				boolean flag = true;
				for(int i = 0 ; i < chunksArray.size() ; i++)
				{
					String chunkHeader = chunksArray.get(i).getAsJsonObject().getAsJsonPrimitive(CHUNK_HEADER).getAsString();
					String chunkHash = chunksArray.get(i).getAsJsonObject().getAsJsonPrimitive(CHUNK_HASH).getAsString();
					String chunkName = chunkHeader +"_" +chunkHash ;

					System.out.println("Downloading chunk number "+(i+1));
					String chunkDownloadLink = GeneratePresignedUrlAndUploadObject.generatePresignedDownloadUrlStitch(accountId+"/"+"chunks"+"/"+chunkName , s3BucketName, s3client);
					//download all the chunks
					boolean success = false;
					int trials = numRetries;

					while ( (trials-- > 0) && !success )
					{
						success = stitchFile(chunkDownloadLink, tmpFile, stitchedFile);
						flag = success;
					}
					if(!success)
					{
						boolean success1 = stitchedFile.delete();
						boolean tempFileDelete = tmpFile.delete();
						if (success1 && tempFileDelete) {
							ArcLogger.l.debug("file has been deleted from local /tmp directory");
						}
						throw new AmazonS3Exception("Problem while downloading chunks");
					}
				}
				//upload the file to s3
				ArcLogger.l.debug("File Name after stitching: "+stitchedFile.toString());
				ArcLogger.l.debug("Upload of stitched File to S3 started");

				// Checking file size
				if(stitchedFile.length() >0 && flag == true){
					ArcLogger.l.info("inside multiPartUpload true");
					multiPartUpload(S3_ACCESS_KEY, S3_SECRET_KEY, s3BucketName,  accountId+"/"+projectId+"/"+fileName, TEMP_DIRECTORY+"/"+threeadName +fileName);
				}else{
					ArcLogger.l.debug("File size is less than zero byte or disk space issue. ignoring upload");
				}
				ArcLogger.l.debug("Stitched file has been uploaded successfully");
				//Deleting file from local /tmp folder
				boolean success = stitchedFile.delete();
				boolean tempFileDelete = tmpFile.delete();
				if (success && tempFileDelete) {
					ArcLogger.l.debug("file has been deleted from local /tmp directory");
				}


			}catch(FileNotFoundException ex){
				ArcLogger.l.error("File not found exception", ex);
				throw ex;
			}
			catch(HBaseException ex){
				ArcLogger.l.error("Hbase has caused some problem while stitching the file");
				throw ex;
			}catch(AmazonClientException ex){
				ArcLogger.l2.error("cannot fetch aws credentials as failed to connect to url: "+ArcConfig.S3ACCESS_KEY_URL);
				ArcLogger.l.error("cannot fetch aws credentials as failed to connect to url: "+ArcConfig.S3ACCESS_KEY_URL);
				BucketInfoCache.refreshBucketInfo(Integer.toString(accountId));
				
				String accesskey = BucketInfoCache.getBucketInfo(accountId).getAccessKey(); //fetch access key dynamically
				String secretkey = BucketInfoCache.getBucketInfo(accountId).getSecreteKey();	//fetch secret key dynamically
				ArcLogger.l.error("Amazon exception while stitching the file...", ex);
				throw ex;
			}
			catch(Exception ex){
				ArcLogger.l.error("Exception while stitching the file", ex);
				throw ex;
			}
		}
		else
		{

			try{
				sb.setLength(0);
				sb.append(accountId).append(UNDERSCORE).append(projectId).append(UNDERSCORE)
				.append(fileId).append(UNDERSCORE).append(revision);
				String fileKey = sb.toString();
				byte[] key = ByteUtil.toBytes(fileKey);

				sb.setLength(0);
				sb.append(fileId).append(UNDERSCORE).append(revision);
				byte[] extB = hto.readValueFromTable(FILECHUNKS_TABLE, HBASE_FAMILY_BYTES,COL_EXT_BYTES, key);
				String ext=ByteUtil.toString(extB, 0, extB.length);
				String newFileName = sb.toString()+ext;
				
				//Check whether the Key exists in S3 or not,if present do nothing,else proceed
				boolean keyExist=  checkKeyExist(S3_ACCESS_KEY, S3_SECRET_KEY, s3BucketName, accountId+"/"+projectId+"/"+newFileName);
				if(keyExist)return 0;

				ArcLogger.l.debug("Current revision is not base revision. stitch process needs patch and base revision file not all chunks");
				// Is not a base revision. Stitch from base revision and patch
				//download the patch for this version from s3
				sb.setLength(0);
				sb.append(fileId).append(UNDERSCORE).append(revision).append(PATCH_EXTENSION);
				String patchFileName = sb.toString();
				String patchFileDownloadUrl = GeneratePresignedUrlAndUploadObject.generatePresignedDownloadUrlStitch(accountId+"/"+"chunks"+"/"+patchFileName , s3BucketName, s3client);

				File patchFile = new File(TEMP_DIRECTORY+"/"+threeadName +patchFileName);
				URL patchDownloadUrl = new URL(patchFileDownloadUrl);
				ArcLogger.l.debug("Patch file is downloading to "+patchFile.toString());
				try{
					FileWriterUtil.downloadToFile(patchDownloadUrl.openStream(), patchFile);
				}
				catch(Exception ex)
				{
					ArcLogger.l.error("Patch file Download failed. please check your network and access key");
				}


				//download base version file from s3
				sb.setLength(0);
				sb.append(fileId).append(UNDERSCORE).append(baseRevision);
				String baseRevisionFileName = sb.toString();

				String baseRevisionDownloadLink =
						GeneratePresignedUrlAndUploadObject.generatePresignedDownloadUrlStitch(accountId+"/"+projectIdStr+"/"+baseRevisionFileName , s3BucketName, s3client);
				File baseRevisionFile = new File(TEMP_DIRECTORY+"/"+threeadName +baseRevisionFileName);
				URL baseRevisionFileDownloadUrl = new URL(baseRevisionDownloadLink);
				ArcLogger.l.debug("Downloading base revision...");
				try{
					FileWriterUtil.downloadToFile(baseRevisionFileDownloadUrl.openStream(), baseRevisionFile);
				}
				catch(Exception ex){
					ArcLogger.l.error("Failed to Download Base revision. check your network ans access key");
				}
				//create the new file using base version and patch

				
				File newFile = new File(TEMP_DIRECTORY+"/"+threeadName +newFileName);
				ArcLogger.l.debug("making complete file using patch and base revision file");
				try{
					rdiff.rebuildFile(baseRevisionFile, new FileInputStream(patchFile),
							new FileOutputStream(newFile));
				}
				catch(Exception ex){
					ArcLogger.l.error("Error while making complete file from base and patch file");
				}
				ArcLogger.l.debug("Complete file success!! uploading the same..");
				//TODO: Create the entries for the hbase tables [FILEPARROTHASH, FILECHUNKS AND CHUNKS]

				//upload new file to s3
				multiPartUpload(S3_ACCESS_KEY, S3_SECRET_KEY, s3BucketName, accountId+"/"+projectId+"/"+newFileName, TEMP_DIRECTORY+"/"+threeadName +newFileName);

				ArcLogger.l.debug("Stitched file has been uploaded successfully");
				//Deleting file from local /tmp folder
				boolean success = newFile.delete();
			
				if (success) {
					ArcLogger.l.debug("file has been deleted from local /tmp directory");
				}

			}catch(FileNotFoundException ex){
				ArcLogger.l.error("File not found exception", ex);
				throw ex;
			}
			catch(HBaseException ex){
				ArcLogger.l.error("Hbase has caused some problem while stitching the file");
				throw ex;
			}catch(AmazonClientException ex){
				ArcLogger.l2.error("cannot fetch aws credentials as failed to connect to url: "+ArcConfig.S3ACCESS_KEY_URL);
				ArcLogger.l.error("cannot fetch aws credentials as failed to connect to url: "+ArcConfig.S3ACCESS_KEY_URL);
				
				BucketInfoCache.refreshBucketInfo(Integer.toString(accountId));
		
				ArcLogger.l.error("Amazon exception while stitching the file...", ex);
				throw ex;
			}
			catch(Exception ex){
				ArcLogger.l.error("Exception while stitching the file", ex);
				throw ex;
			}

		}
		/*Date dt2 = new Date(System.currentTimeMillis());
		SimpleDateFormat ft2 = new SimpleDateFormat("yyyy:MM:dd-hh:mm:ss");
		ArcLogger.l.debug("File stitch success and ended at "+ft2.format(dt2));*/
		
		return 1;
	}




	private static boolean checkKeyExist(String accessKey, String secretKey, String bucketName,String key){
		try{
			AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
			AmazonS3 s3Client = new AmazonS3Client(credentials);
			S3Object object = s3Client.getObject(bucketName, key);
			if(object !=null && object.getKey()!=null ){
				return true;
			}
			return true;
		} catch (AmazonServiceException e) {
			ArcLogger.l.debug("Not an old file exception: "+e);
			return false;
		}
	}

	/**
	 * Download the chunk from cloud and append it to a file.
	 * @param chunkLink Link from where chunk is to be downloaded
	 * @param tmpFile file in which downloaded chunk is stored
	 * @param stitchedFile file to which cunk is appended
	 * @return true if chunk is downloaded and appended successfully
	 * @throws Exception
	 */
	private final static boolean stitchFile(String chunkLink, File tmpFile, File stitchedFile)
	{
		try
		{
			URL downloadLink = new URL(chunkLink);
			FileWriterUtil.downloadToFile(downloadLink.openStream(), tmpFile);
			byte[] chunkByte = FileReaderUtil.getBytes(tmpFile);
			//stitch them to create file
			//System.out.println("CHUNK BYTE LENGTH: " +chunkByte.length);
			boolean success = com.arc.util.FileWriterUtil.appendToFile(stitchedFile.getAbsolutePath(), chunkByte);
			if (success == true)
				return true;
			else
				return false;
		}
		catch ( Exception e )
		{
			ArcLogger.l.error("Error in downloading individual chunk and stitching into a file");
			return false;
		}
	}



	private static void multiPartUpload(String accessKey, String secretKey, String bucketName, String key, String filePath) throws Exception{


		boolean upldFlag = true;

		File file = new File(filePath);
		if (!file.isFile()) {
			throw new Exception("Error accessing file: " + filePath);
		}
		AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
		AmazonS3 s3Client = new AmazonS3Client(credentials);

		// Create a list of UploadPartResponse objects. We have one of these for each part upload.
		List<PartETag> partETags = new ArrayList<PartETag>();

		// Step 1: Initialize.
		// keyName = projectId + File.separator + VIEW + File.separator + keyName;
		System.out.println("1: Key name : " + key);

		InitiateMultipartUploadRequest initRequest = new InitiateMultipartUploadRequest(bucketName, key);

		InitiateMultipartUploadResult initResponse = s3Client.initiateMultipartUpload(initRequest);

		ArcLogger.l.debug("Check the file length and set the chunk size to 5MB");
		System.out.println("Check the file length and set the chunk size to 5MB");
		long contentLength = file.length();
		// Set chunk size to 5 MB.
		long partSize = 5 * 1024 * 1024;


		// Step 2: Now start Uploading the parts.
		long filePosition = 0;
		for (int i = 1; filePosition < contentLength; i++) {
			ArcLogger.l.debug("Starting Upload Part [" + i + "]. ");
			System.out.println("Starting Upload Part [" + i + "]. ");
			// The final part could be less than 5 MB in size. So, we need
			// to
			// adjust the part size.
			partSize = Math.min(partSize, (contentLength - filePosition));

			// Create request to upload a part.
			UploadPartRequest uploadRequest = new UploadPartRequest().withBucketName(bucketName).withKey(key)
					.withUploadId(initResponse.getUploadId()).withPartNumber(i).withFileOffset(filePosition).withFile(file)
					.withPartSize(partSize);

			// Upload part and add response to our list.
			partETags.add(s3Client.uploadPart(uploadRequest).getPartETag());
			ArcLogger.l.debug("Upload Part [" + i + "] Completed. ");
			System.out.println("Upload Part [" + i + "] Completed. ");
			
			filePosition += partSize;
		}

		// Step 3: Complete the upload.
		CompleteMultipartUploadRequest compRequest = new CompleteMultipartUploadRequest(bucketName, key,initResponse.getUploadId(), partETags);
		s3Client.completeMultipartUpload(compRequest);

		GetObjectRequest request = new GetObjectRequest(bucketName, key);
		if (s3Client.getObject(request).getObjectMetadata().getContentLength() > 0) {
			upldFlag = true;
			ArcLogger.l.debug("Upload completed successfully into [" + key + "]");
			System.out.println("Upload completed successfully into [" + key + "]");
		}

	}


	

public static void multiTransfer(String accessKey, String secretKey, String bucketName, String key, String filePath) throws Exception{


        File file = new File(filePath);
        if (!file.isFile()) {
            throw new Exception("Error accessing file: " + filePath);
        }

        AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        AmazonS3 s3Client = new AmazonS3Client(credentials);

        TransferManagerConfiguration tmConfig = new TransferManagerConfiguration();
        // Sets the minimum part size for upload parts.
        tmConfig.setMinimumUploadPartSize(5 * 1024 * 1024);
        // Sets the size threshold in bytes for when to use multipart uploads.
        tmConfig.setMultipartUploadThreshold(5 * 1024 * 1024);

        ThreadPoolExecutor executor = new ThreadPoolExecutor(5,10,5000,TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
        TransferManager manager = new TransferManager(s3Client,executor);
        manager.setConfiguration(tmConfig);


        Upload fileUpload = manager.upload(bucketName, key, file);
        fileUpload.waitForCompletion();
        manager.shutdownNow();

        //TODO: Implement Resume Upload if failed...


        GetObjectRequest request = new GetObjectRequest(bucketName, key);
        if (s3Client.getObject(request).getObjectMetadata().getContentLength() > 0) {

            System.out.println("Upload completed successfully into [" + key + "]");
        }

    }



	@Override
	public void run() {
		int negackcount = 1;
		try {
			
		int procstatus=	process(accountId,bucketName, fileId, revision, projectId, baserevision,threadName);
		//File not processed as it already exist in S3.
		if(procstatus==0){
			//Now send success acknowledge to filestitch queue
			UMQServiceAdaptor.getInstance().acknowledgeMsg(msgId, 1, "File-ID: "+fileId+" already exist in S3,So not processed");

		}else{
			//Now send success acknowledge to filestitch queue
			UMQServiceAdaptor.getInstance().acknowledgeMsg(msgId, 1, "File-ID: "+fileId+" stitched and uploaded successfullly.");
		} } catch (Exception e) {
			ArcLogger.l.error("Error processing File stitch : "+e);
			if(negackcount == 1) {
				try {
					process(accountId,bucketName, fileId, revision, projectId, baserevision,threadName);
				} catch (Exception e1) {
					e1.printStackTrace();
					ArcLogger.l.error("Negative acknowlegement received hence trying to acknowledge the process again");
					negackcount ++;
				}
			}
			//If File could-not be processed due to some error then Send negative acknowledge with retry=false.
			UMQServiceAdaptor.getInstance().acknowledgeMsg(msgId, 0, "File-ID: "+fileId+" stitching failed due to error->>");

		}
		
	}




	public static void main(String[] args) throws Exception
	{

		FileStitchServices.process(141395,"indiatesting", 811916, 811916, 155392, 811916, "811916");
	}
} 
