package com.arc.umq.service;

import com.arc.util.conf.ArcConfig;
import com.arc.util.log.ArcLogger;

public class PollerService {

	public static void main(String[] args) {
		String instanceid = null;
		String logfilename = null;
		
		//check if no of instanceid arguments passed through command line is 1 and log name through command line is 2
		if(args.length != 2){
			System.out.println("Wrong number of arguments passed. Pass below argument as example");
			System.out.println("java -cp /root/chunkcode/arcfilechunk-skysite.jar:/root/chunkcode/lib/*:/etc/hbase/conf com.arc.umq.service.PollerService"+ArcConfig.UMQ_QUEUE_NAME+"2 &");
			System.exit(0);
		}
		
			instanceid = args[0];	//capture the instanceid argument passed through commandline in a variable
			logfilename = args[1];
			System.setProperty("userinputstitchlog.log",logfilename+".log");
		try {
			FileStitchPoller poller=new FileStitchPoller(instanceid);
			Thread pollingThread = new Thread(poller);
			pollingThread.setDaemon(true);
			pollingThread.start();

			pollingThread.join();
		} catch (InterruptedException e) {
			
		}
		ArcLogger.l.info("((((( ----- Poller Service Running successfully ----- )))))");
		
		System.out.println("Polling service running successfully!!!");
	}

}
