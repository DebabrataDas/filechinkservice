package com.arc.umq.service;


import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class BaseResponse {
	public Boolean success;
	public String status;
	public int totCount;

}
