package com.arc.umq.service;

import java.io.Serializable;
import java.util.List;

public class PublishRequest implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String clientId;
	private List<String> messages;
	
	/*private CaseInsensitiveMap customAttributes;
	private String parentId;
	private int priority=5;
	*/
	
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public List<String> getMessages() {
		return messages;
	}
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	
	
}
