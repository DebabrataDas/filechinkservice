package com.arc.umq.service;


public class MessageResponse {
	
	private String msgid;
	private String msg;
	private int remainingAttempts;
	public String getMsgid() {
		return msgid;
	}
	public void setMsgid(String msgid) {
		this.msgid = msgid;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public int getRemainingAttempts() {
		return remainingAttempts;
	}
	public void setRemainingAttempts(int remainingAttemps) {
		this.remainingAttempts = remainingAttemps;
	}
	
	
	
	

}
