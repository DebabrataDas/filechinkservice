package com.arc.umq.service;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.arc.filechunk.DecryptionException;
import com.arc.hbase.BucketInfoCache;
import com.arc.util.conf.ArcConfig;
import com.arc.util.log.ArcLogger;

public class FileStitchPoller implements Runnable{


	private String instanceid;

	public FileStitchPoller(String instanceid) {
		this.instanceid = instanceid;
	}

	private void processStitch(int batchSize) throws InterruptedException {
		ArcLogger.l.info(" Entering FileStitchPoller.processStitch() with instanceid: " + instanceid);

		if (!instanceid.contains(ArcConfig.UMQ_QUEUE_NAME)) {
			System.out.println(
					"Parameter not in proper format. Please enter instance id starting as " + ArcConfig.UMQ_QUEUE_NAME);
			ArcLogger.l.info(
					"Parameter not in proper format. Please enter instance id starting as " + ArcConfig.UMQ_QUEUE_NAME);
			System.exit(0);

		}
			ConsumeWrapper response=UMQServiceAdaptor.getInstance().getMesage(batchSize,instanceid);	
		if(response!=null && response.getMsgCount()>0){
			List<MessageResponse> msgsResponse=response.getMsgs();
			for(MessageResponse msgrsp:msgsResponse ){
				if(msgrsp.getMsgid()!=null && msgrsp.getMsg()!=null){
					String chckmsgpattern = ArcConfig.KEY_SEPARATOR;
					ArcLogger.l.info("The message is: "+msgrsp.getMsg());
					int countnopatterntimes = StringUtils.countMatches(msgrsp.getMsg(), chckmsgpattern);
					if(countnopatterntimes == 5) {
						StitchExecuter.startStitch(msgrsp.getMsgid(), msgrsp.getMsg());
					} else{
						ArcLogger.l.info("The message is not in valid format.");	
						UMQServiceAdaptor.getInstance().acknowledgeMsg(msgrsp.getMsgid(), 1, "File-ID: "+msgrsp.getMsg()+" stitching failed due to error->>");
					}
				}
			}
		}else {
			// When No message in file-stitch UMQ queue,wait for 15 seconds
			// before polling again
			// ArcLogger.l.info("No Message in UMQ,hence sleeping for 15
			// seconds....");

			try {
				Thread.sleep(15000);
			} catch (InterruptedException e) {
				ArcLogger.l.error("Failed to sleep while no message in queue: " + e);
			}
		}

	}


	@Override
	public void run() {
		try {
			BucketInfoCache.loadCacheFromHbase();
		} catch (IOException e1) {
			ArcLogger.l.error("couldn't load cache from hbase "+e1.getMessage());
			e1.printStackTrace();
		} catch (DecryptionException e) {
			ArcLogger.l.error("ERROR in Decryption "+e.getMessage());
			e.printStackTrace();
		}
		while(true){
			
			if(StitchExecuter.getThreadCount()<ArcConfig.BATCH_SIZE){
				int fetchSize=ArcConfig.BATCH_SIZE - StitchExecuter.getThreadCount();
				ArcLogger.l.info("UMQ filestitch queue fetch size: "+fetchSize);

				try {
					processStitch(fetchSize);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				//Wait for 10 seconds to check thread count again
				try {
					Thread.sleep(15000);
				} catch (InterruptedException e) {
					ArcLogger.l.error("Failed to sleep while all pool worked thread busy: "+e);
				}
			}


		}
	}

}	

