package com.arc.umq.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import com.arc.util.conf.ArcConfig;
import com.arc.util.log.ArcLogger;
import com.google.gson.Gson;

public class UMQServiceAdaptor{

	private static final String publishId=ArcConfig.PUBLISHER_ID;
	private static final String consumerId=ArcConfig.CONSUMER_ID;

	private static final String umq_base_url = "http://"+ArcConfig.UMQ_URL_IP+"/mq-service/service/v1/rest/";
	private static final String umq_publish_url = umq_base_url+"producer/publish";
	private static final String umq_consume_url = umq_base_url+"consumer/consume?clientId="+consumerId+"&batchSize=";
	private static final String umq_acknowledge_url = umq_base_url+"consumer/acknowledge";


	private static UMQServiceAdaptor instance = null;
	
	public static UMQServiceAdaptor getInstance()
	{
		if ( null != instance) return instance;
		synchronized (UMQServiceAdaptor.class) 
		{
			if ( null != instance ) return instance;	
			instance = new UMQServiceAdaptor();	
		}
		return instance;
	}


	public void publishMessage(String msg){
		String msgAsJson = null;
		ArcLogger.l.info("Going to publish msg to filestitch queue: "+msg);	
		try {
			HttpPost client=new HttpPost(umq_publish_url);	
			client.setHeader("Content-type", "application/json");
			//Preparing Publish message request for UMQ
			PublishRequest req=new PublishRequest();
			req.setClientId(publishId);
			List<String> msgs=new ArrayList<String>();
			msgs.add(msg);
			req.setMessages(msgs);
			msgAsJson=new Gson().toJson(req);
			ArcLogger.l.info("JSON MSG to be publis : "+msgAsJson);
			client.setEntity(new StringEntity(msgAsJson));
			HttpResponse response = (new DefaultHttpClient()).execute(client);
			ArcLogger.l.info("Message Published to filestitch queue successfully--> "+response.getStatusLine());
			if(!response.getStatusLine().toString().contains("HTTP/1.1 200")){
				ArcLogger.l1.error("JSON message failed: "+msgAsJson);
				ArcLogger.l.error("JSON message failed: "+msgAsJson);
				ArcLogger.l.error("Failed publishing message to filestitch queue: ");
			}
		} catch (IOException e) {
			ArcLogger.l1.error("JSON message failed: "+msgAsJson);
			ArcLogger.l.error("JSON message failed: "+msgAsJson);
			ArcLogger.l.error("Failed publishing message to filestitch queue: "+e);
		}
	}



	public ConsumeWrapper getMesage(int batchSize,String instanceid) {
		ArcLogger.l.info("Consuming message from filestitch queue: ");	
		String responseFromQueue = null;
		ConsumeWrapper responseVO = null;
		try {
			//consume the msg with the instanceid
			ArcLogger.l.info("Message started consuming with instanceid :  "+instanceid);
			URL MQ_URL = new URL(umq_consume_url+batchSize+"&instanceId="+instanceid);
			HttpURLConnection connection = (HttpURLConnection) MQ_URL.openConnection();
			connection.setRequestMethod("GET");
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setRequestProperty("Accept-Charset", "UTF-8");
			connection.setRequestProperty("Content-Type", "application/json");

			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String output = null;
			StringBuilder responseBuilder = new StringBuilder();
			while ((output = bufferedReader.readLine()) != null) {
				responseBuilder.append(output);
			}
			bufferedReader.close();
			responseFromQueue = responseBuilder.toString();
			
			Gson gson = new Gson();
			responseVO = gson.fromJson(responseFromQueue, ConsumeWrapper.class);
			
			if(responseVO.getMsgCount() > 0)
			ArcLogger.l.info("Message consumed from UMQ :  "+responseFromQueue);	
			
		} catch (IOException e) {
			ArcLogger.l.error("Message consumed Failed from UMQ: "+responseFromQueue);
			ArcLogger.l.error("Failed while consuming message from filestitch queue: "+e);
		}

		return responseVO;


	}

	
	public void acknowledgeMsg(String msgId,int ackStatus,String remark) {
		ArcLogger.l.info("Going to acknowledge processed Msg-ID to filestitch queue: "+msgId);	
		String ackresponseFromQueue = null;
		try {
			AckDetail ackDtl=new AckDetail();
			ackDtl.setMessageId(msgId);
			ackDtl.setAckStatus(ackStatus);
			ackDtl.setRetriable(false);	
		
			ackDtl.setRemark(remark);

			List<AckDetail> ackDtls=new ArrayList<AckDetail>();
			ackDtls.add(ackDtl);

			AcknowldgementReq req = new AcknowldgementReq();
			req.setClientId(consumerId);
			req.setAckDetail(ackDtls);

			URL acknowledgeURL = new URL(umq_acknowledge_url);
			HttpURLConnection ackConnection = (HttpURLConnection) acknowledgeURL.openConnection();
			ackConnection.setRequestMethod("POST");
			ackConnection.setDoInput(true);
			ackConnection.setDoOutput(true);
			ackConnection.setRequestProperty("Accept-Charset", "UTF-8");
			ackConnection.setRequestProperty("Content-Type", "application/json");
			OutputStreamWriter dosWriter = new OutputStreamWriter(ackConnection.getOutputStream(), "UTF-8");
			Gson gson = new Gson();
			//System.out.println("Ack Request = " + gson.toJson(req));
			dosWriter.write(gson.toJson(req));
			dosWriter.close();
			
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(ackConnection.getInputStream()));
			String output = null;
			StringBuilder responseBuilder = new StringBuilder();
			while ((output = bufferedReader.readLine()) != null) {
				responseBuilder.append(output);
			}
			bufferedReader.close();
			ackresponseFromQueue = responseBuilder.toString();
			
			ArcLogger.l.info("Ack Response from Queue: "+ackresponseFromQueue);			
			
			
			ArcLogger.l.info("Acknowledgement done to filestsich queue successfully");	
			
		} catch (IOException e) {
			ArcLogger.l.error("Ack Response Failed from Queue: "+ackresponseFromQueue);	
			ArcLogger.l.error("Failed while acknowledging message to filestitch queue: "+e);
		}

	}




	public long getMsgCount(String clientId) {
		ArcLogger.l.info("Starting message count from file stitch queue.");
		try {
		URL MQ_URL = new URL(umq_consume_url);
		HttpURLConnection connection = (HttpURLConnection) MQ_URL.openConnection();
		connection.setRequestMethod("GET");
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setRequestProperty("Accept-Charset", "UTF-8");
		connection.setRequestProperty("Content-Type", "application/json");

		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String output = null;
		StringBuilder responseBuilder = new StringBuilder();
		while ((output = bufferedReader.readLine()) != null) {
			responseBuilder.append(output);
		}
		bufferedReader.close();
		String responseFromQueue = responseBuilder.toString();
		ArcLogger.l.info("Msg count recieved from filestitch queue: "+responseFromQueue);
		
		return Long.parseLong(responseFromQueue.split(",")[1].replace("}", "").trim().split(":")[1]);
		} catch (IOException e) {
			ArcLogger.l.error("Failed to get message count from filestitch queue: "+e);
		}

		return 0;
	}

	
	
	public static void main(String[] args) {
		UMQServiceAdaptor ad=new UMQServiceAdaptor();
		//ad.publishMessage("test message");
		//ad.getMesage(0);
	}

}
