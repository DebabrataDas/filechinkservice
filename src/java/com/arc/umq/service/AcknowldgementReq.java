package com.arc.umq.service;

import java.util.List;

public class AcknowldgementReq{
	
	private String clientId;
	private List<AckDetail> ackDetail;
	
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public List<AckDetail> getAckDetail() {
		return ackDetail;
	}
	public void setAckDetail(List<AckDetail> ackDetail) {
		this.ackDetail = ackDetail;
	}
	

}
