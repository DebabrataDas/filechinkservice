package com.arc.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.arc.hbase.BucketInfoCache;
import com.arc.util.conf.ArcConfig;
import com.bizosys.hsearch.byteutils.SortedBytesArray;
import com.bizosys.hsearch.byteutils.Storable;

public class BucketChange implements Runnable{
	
	private class MetaInfo {
		int accountId;
		int projectId;
		String oldBucketName;
		String newBucketName;
		
		MetaInfo(int accountid, int projectid, String oldbucket, String newbucket)
		{
			//System.out.println("Information added in MetaInfo objects");
			this.accountId = accountid;
			this.projectId = projectid;
			this.oldBucketName = oldbucket;
			this.newBucketName = newbucket;
		}
	}
	static Logger l = Logger.getLogger(BucketChange.class);
	
	private static BucketChange instance;
	private static long BREATHE_TIME_MILLIS = ArcConfig.BREATHE_TIME_MILLIS;
	

	
	BlockingQueue<MetaInfo> blockingQueue = null; 
	
	
	public static BucketChange getInstance() throws IOException{
		if(null != instance) return instance;
		synchronized(BucketChange.class)
		{
			if(null != instance) return instance;
			BlockingQueue<MetaInfo> newBucketQueue = new LinkedBlockingQueue<MetaInfo>();
			instance = new BucketChange(newBucketQueue);
			
			Thread offlineThread = new Thread(instance);
			offlineThread.setDaemon(true);
			offlineThread.start();
		}
		return instance;
	}
	
	private BucketChange(BlockingQueue<MetaInfo> blockingQueue) throws IOException
	{
		this.blockingQueue = blockingQueue;
	}
	public BucketChange() throws IOException
	{
		
	}
	
	
	public final void requestNewBucket(final int accountId, final int projectId, final String oldBucket, final String newBucket) throws IOException 
	{
		//System.out.println("Requesting for new existing bucket");
		MetaInfo info = new MetaInfo(accountId, projectId, oldBucket, newBucket);
		blockingQueue.add(info);
	}
	
	
	public final int getQueueSize() 
	{
		if ( null == blockingQueue) return 0;
		else 
			{
				l.debug("blocking Queue size is : "+blockingQueue.size());
				return blockingQueue.size();
			}
	}
	
	List<MetaInfo> dataL = new ArrayList<MetaInfo>(512);
	
	
	private static void copyFromOldBucketToNewBucket(int accountId, int projectId, String oldBucket, String newBucket)
	{
		//System.out.println("copyFromOldBucketToNewBucket called");
		String sourceKey = Integer.toString(accountId);
		//sourceKey = sourceKey;
		
		try {
            // Copying object
			
			String accessKey1 = BucketInfoCache.getBucketInfo(accountId).getAccessKey();
			String secretKey1 = BucketInfoCache.getBucketInfo(accountId).getSecreteKey();
			
			 AWSCredentials credentials1 = new BasicAWSCredentials(accessKey1, secretKey1);
			 AmazonS3 s3client = new AmazonS3Client(credentials1);
			 
			String destinationKey = Integer.toString(accountId);
			destinationKey = destinationKey+"/";
            CopyObjectRequest copyObjRequest = new CopyObjectRequest(oldBucket, sourceKey, newBucket, destinationKey);
            System.out.println("Copying object.");
            s3client.copyObject(copyObjRequest);
            
            System.out.println("Deleting files and folders from old bucket");
            
		} catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, " +
            		"which means your request made it " + 
            		"to Amazon S3, but was rejected with an error " +
                    "response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, " +
            		"which means the client encountered " +
                    "an internal error while trying to " +
                    " communicate with S3, " +
                    "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
		
		//Deleting the old objects from old bucket
		
            try {
            	String accessKey1 = BucketInfoCache.getBucketInfo(accountId).getAccessKey();
   			 String secretKey1 = BucketInfoCache.getBucketInfo(accountId).getSecreteKey();
    			
    			 AWSCredentials credentials1 = new BasicAWSCredentials(accessKey1, secretKey1);
    			 AmazonS3 s3client = new AmazonS3Client(credentials1);
                s3client.deleteObject(new DeleteObjectRequest(oldBucket, sourceKey));
            } catch (AmazonServiceException ase) {
                System.out.println("Caught an AmazonServiceException.");
                System.out.println("Error Message:    " + ase.getMessage());
                System.out.println("HTTP Status Code: " + ase.getStatusCode());
                System.out.println("AWS Error Code:   " + ase.getErrorCode());
                System.out.println("Error Type:       " + ase.getErrorType());
                System.out.println("Request ID:       " + ase.getRequestId());
            } catch (AmazonClientException ace) {
                System.out.println("Caught an AmazonClientException.");
                System.out.println("Error Message: " + ace.getMessage());
            }

        
	}

	@Override
	public void run() {

		long startTime = System.currentTimeMillis();
		while (true) 
		{
			try 
			{
				int size = this.blockingQueue.size();
				
				
				if ( size > 1024 ) size = 1024;
			
				if ( size < 1 ) size = 1; 
				l.debug("Taking data from blocking queue and adding them in a list dataL");
				for ( int i=0; i<size; i++) 
				{
					dataL.add(this.blockingQueue.take());
				}
		        
				sink(dataL);
				dataL.clear();
			} 
			catch (Exception ex) 
			{
				l.error("Can't stitch the chunks. there is some problem in sink method");
				l.fatal("BatchProcessor > ",  ex);
			}
			finally
			{
				long endTime = System.currentTimeMillis();
				long sleepFor = BREATHE_TIME_MILLIS - (endTime - startTime);
				
				if ( sleepFor > 0 )
				{
					try 
					{
						Thread.sleep(sleepFor);
						startTime = endTime;
					} 
					catch (InterruptedException e) 
					{
						l.error("Some error while sleeping the thread in BucketChange thread:"+e.toString() );
						e.printStackTrace();
						Thread.currentThread().interrupt();
					}
				}
			}
		}
	
		
	}
	
	private final void sink(final List<MetaInfo> dataL) throws Exception 
	{
		
		for ( MetaInfo data : dataL )
		{
			
			int accountId = data.accountId;
			int projectId = data.projectId;
			String oldBucket = data.oldBucketName;
			String newBucket = data.newBucketName;
			
			//System.out.println(accountId+"  "+projectId+"  "+oldBucket+"  "+newBucket);
			try{
			BucketChange.copyFromOldBucketToNewBucket(accountId, projectId, oldBucket, newBucket);
			}
			catch(Exception ex){
				l.error("Problem occur while Copying the objects to new bucket");
			}
		}
	}
	
	public static void main(String[] args) throws IOException {
		copyFromOldBucketToNewBucket(675467, 232323, "chunktes", "chunktes2");
		
		 String bucketName = "12210";
		 
		 String key = "153470/";
		 
		//System.out.println(doesObjectExists(bucketName, key));
	}
	
	public static boolean doesObjectExists(String bucketName, String key, int accountId) {
	    try {
	    	
	    	String accessKey1 = BucketInfoCache.getBucketInfo(accountId).getAccessKey();
			 String secretKey1 = BucketInfoCache.getBucketInfo(accountId).getSecreteKey();
			
			 AWSCredentials credentials1 = new BasicAWSCredentials(accessKey1, secretKey1);
			 AmazonS3 s3client = new AmazonS3Client(credentials1);
			 s3client.getObjectMetadata(bucketName, key); 
			 
	    } catch(AmazonServiceException e) {
	        return false;
	    }
	    return true;
	}
}
