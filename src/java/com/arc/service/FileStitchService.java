package com.arc.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URL;
import java.sql.Date;
import java.util.*;
import java.text.*;

import org.apache.hadoop.fs.DF;
import org.apache.log4j.Logger;
import org.metastatic.rsync.Rdiff;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.CompleteMultipartUploadRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadResult;
import com.amazonaws.services.s3.model.PartETag;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.UploadPartRequest;
import com.arc.hbase.BucketInfoCache;
import com.arc.hbase.HbaseTableOperation;
import com.arc.filechunk.s3utils.FileUpload;
import com.arc.filechunk.s3utils.GeneratePresignedUrlAndUploadObject;
import com.arc.util.FileReaderUtil;
import com.arc.util.conf.ArcConfig;
import com.arc.util.log.ArcLogger;
import com.bizosys.hsearch.byteutils.ByteUtil;
import com.bizosys.hsearch.hbase.HBaseException;
import com.bizosys.hsearch.treetable.compiler.FileWriterUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

public final class FileStitchService
{
	static Logger l = Logger.getLogger(FileStitchService.class);
	static Date dt = new Date(System.currentTimeMillis());
	static SimpleDateFormat ft = new SimpleDateFormat("yyyy:MM:dd-hh:mm:ss");
	private static final String TEMP_FILE = ArcConfig.TEMP_FILE;
	private static final String TEMP_DIRECTORY = ArcConfig.TEMP_DIRECTORY;

	private static final String PATCH_EXTENSION = ArcConfig.PATCH_EXTENSION;

	private static final char UNDERSCORE = '_';

	private static final String CHUNK_HASH = ArcConfig.CHUNK_HASH;
	private static final String CHUNK_HEADER = ArcConfig.CHUNK_HEADER;

	private final static String LINKS_TABLE = ArcConfig.LINKS_TABLE;
	private final static String FILECHUNKS_TABLE = ArcConfig.FILECHUNKS_TABLE;
	private static final byte[] COL_CHUNKS_JSON_BYTES = ArcConfig.COL_CHUNKS_JSON_BYTES;
	private static final byte[] COL_DEFAULT_BYTES = ArcConfig.COL_DEFAULT_BYTES;
	private static final byte[] HBASE_FAMILY_BYTES = ArcConfig.HBASE_FAMILY_BYTES;

	private static final Rdiff rdiff = new Rdiff();

	private static final int numRetries = 3;
	private static final byte[] COL_EXT_BYTES = ArcConfig.COL_EXT_BYTES;
	private static final String DOT = ".";
	
	/**
	 * The file stitch service 
	 * @param accountId in which file is present
	 * @param fileId of file
	 * @param revision of file
	 * @param projectId in which file is present
	 * @param baseRevision of file
	 * @throws Exception
	 */
	public static final void process(final int accountId, final long fileId, final long revision, 
			final int projectId, final long baseRevision) throws Exception
	{
		String S3_ACCESS_KEY = BucketInfoCache.getBucketInfo(accountId).getAccessKey();
		String S3_SECRET_KEY = BucketInfoCache.getBucketInfo(accountId).getSecreteKey();
		AmazonS3 s3client = new AmazonS3Client(
				new BasicAWSCredentials( S3_ACCESS_KEY, 
						S3_SECRET_KEY));
		String projectIdStr = Integer.toString(projectId);
		l.debug("Inside stitch process");
		l.debug("Stitch process started at: "+ft.format(dt));
		final HbaseTableOperation hto = new HbaseTableOperation();

		//Get s3 bucket for accountId
		byte[] s3BucketNameB = hto.readValueFromTable(LINKS_TABLE, HBASE_FAMILY_BYTES, 
				COL_DEFAULT_BYTES, ByteUtil.toBytes(accountId));


		String s3BucketName = ByteUtil.toString(s3BucketNameB, 0, s3BucketNameB.length);
		l.debug("Getting bucket Name corresponding to accounntId: "+accountId+" bucket name is:"+s3BucketName);
		StringBuilder sb = new StringBuilder();

		if( revision == baseRevision )
		{

			try {

				l.debug("Current revision is equal to base revision");
				// Is a base revision. Take the entire file chunks and stitch
				String tmpFilePath = TEMP_FILE;    // /tmp/tempFile
				File tmpFile = new File(tmpFilePath);

				l.debug("File path to download chunks: "+tmpFilePath);
				sb.setLength(0);
				sb.append(accountId).append(UNDERSCORE).append(projectId).append(UNDERSCORE)
				.append(fileId).append(UNDERSCORE).append(revision);
				String fileKey = sb.toString();
				byte[] key = ByteUtil.toBytes(fileKey);

				sb.setLength(0);
				sb.append(fileId).append(UNDERSCORE).append(revision);
				byte[] extB = hto.readValueFromTable(FILECHUNKS_TABLE, HBASE_FAMILY_BYTES,COL_EXT_BYTES, key);
				String ext=ByteUtil.toString(extB, 0, extB.length);
				String fileName = sb.toString()+ext;
				File stitchedFile = new File(TEMP_DIRECTORY +fileName);


				if ( stitchedFile.exists() ) stitchedFile.delete();

				//Get all the chunks from filechunk table
				l.debug("Getting all chunks from FILECHUNKS table");
				byte[] chunksJsonB = hto.readValueFromTable(FILECHUNKS_TABLE, HBASE_FAMILY_BYTES, 
						COL_CHUNKS_JSON_BYTES, key);
				String chunksJson = ByteUtil.toString(chunksJsonB, 0, chunksJsonB.length);
				
				l.debug(chunksJson);
				System.out.println(chunksJson);
				JsonArray chunksArray = new JsonParser().parse(chunksJson).getAsJsonArray();
				l.debug("Total chunks to be downloaded for stitching: "+chunksArray.size());
				for(int i = 0 ; i < chunksArray.size() ; i++)
				{
					String chunkHeader = chunksArray.get(i).getAsJsonObject().getAsJsonPrimitive(CHUNK_HEADER).getAsString();
					String chunkHash = chunksArray.get(i).getAsJsonObject().getAsJsonPrimitive(CHUNK_HASH).getAsString();
					String chunkName = chunkHeader +"_" +chunkHash ;

					String chunkDownloadLink = GeneratePresignedUrlAndUploadObject.generatePresignedDownloadUrlStitch("chunks"+"/"+chunkName , s3BucketName, s3client);
					//download all the chunks
					boolean success = false;
					int trials = numRetries;

					while ( (trials-- > 0) && !success )
					{
						success = stitchFile(chunkDownloadLink, tmpFile, stitchedFile);
					}
					if(!success)
					{
						throw new AmazonS3Exception("Problem while downloading chunks");
					}
				}
				//upload the file to s3
				l.debug("File Name after stitching: "+stitchedFile.toString());
				//String fileUploadUrlPath = GeneratePresignedUrlAndUploadObject.generatePresignedUploadUrl(accountId+"/"+projectIdStr+"/"+fileName , s3BucketName, s3client);
				//URL fileUploadUrl = new URL(fileUploadUrlPath);
				//byte[] fileBytes = FileReaderUtil.getBytes(stitchedFile);
				l.debug("Upload of stitched File to S3 started");

				// Checking file size 
				//FileUpload.uploadObject(fileUploadUrl,fileBytes);
				
				if(stitchedFile.length() >0 )
				multiPartUpload(S3_ACCESS_KEY, S3_SECRET_KEY, s3BucketName,  projectId+"/"+fileName, TEMP_DIRECTORY +fileName);
				else{
					l.debug("File size is less than zero byte. ignoring upload");
				}
				l.debug("Stitched file has been uploaded successfully");
				//Deleting file from local /tmp folder
				boolean success = stitchedFile.delete();
				if (success) {
					l.debug("file has been deleted from local /tmp directory");
				}


			}catch(FileNotFoundException ex){
				l.error("File not found exception", ex);
				throw ex;
			}
			catch(HBaseException ex){
				l.error("Hbase has caused some problem while stitching the file");
				throw ex;
			}catch(AmazonClientException ex){
				l.error("Amazon exception while stitching the file...", ex);
				throw ex;
			}
			catch(Exception ex){
				l.error("Exception while stitching the file", ex);
				throw ex;
			}
		}
		else
		{
			
			try{
				
		
			l.debug("Current revision is not base revision. stitch process needs patch and base revision file not all chunks");
			// Is not a base revision. Stitch from base revision and patch
			//download the patch for this version from s3
			sb.setLength(0);
			sb.append(fileId).append(UNDERSCORE).append(revision).append(PATCH_EXTENSION);
			String patchFileName = sb.toString();
			String patchFileDownloadUrl = GeneratePresignedUrlAndUploadObject.generatePresignedDownloadUrlStitch(projectIdStr+"/"+patchFileName , s3BucketName, s3client);

			File patchFile = new File(TEMP_DIRECTORY +patchFileName);
			URL patchDownloadUrl = new URL(patchFileDownloadUrl);
			l.debug("Patch file is downloading to "+patchFile.toString());
			try{
				FileWriterUtil.downloadToFile(patchDownloadUrl.openStream(), patchFile);
			}
			catch(Exception ex)
			{
				l.error("Patch file Download failed. please check your network and access key");
			}


			//download base version file from s3
			sb.setLength(0);
			sb.append(fileId).append(UNDERSCORE).append(baseRevision);
			String baseRevisionFileName = sb.toString();

			String baseRevisionDownloadLink = 
					GeneratePresignedUrlAndUploadObject.generatePresignedDownloadUrlStitch(projectIdStr+"/"+baseRevisionFileName , s3BucketName, s3client);
			File baseRevisionFile = new File(TEMP_DIRECTORY +baseRevisionFileName);
			URL baseRevisionFileDownloadUrl = new URL(baseRevisionDownloadLink);
			l.debug("Downloading base revision...");
			try{
				FileWriterUtil.downloadToFile(baseRevisionFileDownloadUrl.openStream(), baseRevisionFile);
			}
			catch(Exception ex){
				l.error("Failed to Download Base revision. check your network ans access key");
			}
			//create the new file using base version and patch
			sb.setLength(0);
			sb.append(accountId).append(UNDERSCORE).append(projectId).append(UNDERSCORE)
			.append(fileId).append(UNDERSCORE).append(revision);
			String fileKey = sb.toString();
			byte[] key = ByteUtil.toBytes(fileKey);

			sb.setLength(0);
			sb.append(fileId).append(UNDERSCORE).append(revision);
			byte[] extB = hto.readValueFromTable(FILECHUNKS_TABLE, HBASE_FAMILY_BYTES,COL_EXT_BYTES, key);
			String ext=ByteUtil.toString(extB, 0, extB.length);


			String newFileName = sb.toString()+ext;
			File newFile = new File(TEMP_DIRECTORY +newFileName);
			l.debug("making complete file using patch and base revision file");
			try{
				rdiff.rebuildFile(baseRevisionFile, new FileInputStream(patchFile), 
						new FileOutputStream(newFile));
			}
			catch(Exception ex){
				l.error("Error while making complete file from base and patch file");
			}
			l.debug("Complete file success!! uploading the same..");
			//TODO: Create the entries for the hbase tables [FILEPARROTHASH, FILECHUNKS AND CHUNKS]

			//upload new file to s3
			//String fileUploadUrlPath = GeneratePresignedUrlAndUploadObject.generatePresignedUploadUrl(accountId+"/"+projectIdStr+"/"+newFileName , s3BucketName, s3client);
			//URL fileUploadUrl = new URL(fileUploadUrlPath);
			//TODO: Stream code 


			//byte[] fileBytes = FileReaderUtil.getBytes(new File(TEMP_DIRECTORY + newFileName));
			
				// FileUpload.uploadObject(fileUploadUrl,fileBytes);
				multiPartUpload(S3_ACCESS_KEY, S3_SECRET_KEY, s3BucketName, projectId+"/"+newFileName, TEMP_DIRECTORY +newFileName);

				l.debug("Stitched file has been uploaded successfully");
				//Deleting file from local /tmp folder
				boolean success = newFile.delete();
				if (success) {
					l.debug("file has been deleted from local /tmp directory");
				}
	
		}catch(FileNotFoundException ex){
			l.error("File not found exception", ex);
			throw ex;
		}
		catch(HBaseException ex){
			l.error("Hbase has caused some problem while stitching the file");
			throw ex;
		}catch(AmazonClientException ex){
			l.error("Amazon exception while stitching the file...", ex);
			throw ex;
		}
		catch(Exception ex){
			l.error("Exception while stitching the file", ex);
			throw ex;
		}
			
		}
		Date dt2 = new Date(System.currentTimeMillis());
		SimpleDateFormat ft2 = new SimpleDateFormat("yyyy:MM:dd-hh:mm:ss");
		l.debug("File stitch success and ended at "+ft2.format(dt2));
	}

	/**
	 * Download the chunk from cloud and append it to a file.
	 * @param chunkLink Link from where chunk is to be downloaded
	 * @param tmpFile file in which downloaded chunk is stored
	 * @param stitchedFile file to which cunk is appended
	 * @return true if chunk is downloaded and appended successfully
	 * @throws Exception 
	 */
	private final static boolean stitchFile(String chunkLink, File tmpFile, File stitchedFile)
	{
		try
		{
			URL downloadLink = new URL(chunkLink);
			FileWriterUtil.downloadToFile(downloadLink.openStream(), tmpFile);
			byte[] chunkByte = FileReaderUtil.getBytes(tmpFile);
			//stitch them to create file
			//System.out.println("CHUNK BYTE LENGTH: " +chunkByte.length);
			com.arc.util.FileWriterUtil.appendToFile(stitchedFile.getAbsolutePath(), chunkByte);
			return true;
		}
		catch ( Exception e )
		{
			l.error("Error in downloading indivisual chunk and stitching into a file");
			return false;
		}
	}

	

	public static void multiPartUpload(String accessKey, String secretKey, String bucketName, String key, String filePath) throws Exception{


		boolean upldFlag = true;

		File file = new File(filePath);
		if (!file.isFile()) {
			throw new Exception("Error accessing file: " + filePath);
		}
		AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
		AmazonS3 s3Client = new AmazonS3Client(credentials);

		// Create a list of UploadPartResponse objects. We have one of these for each part upload.
		List<PartETag> partETags = new ArrayList<PartETag>();

		// Step 1: Initialize.
		// keyName = projectId + File.separator + VIEW + File.separator + keyName;
		System.out.println("1: Key name : " + key);

		InitiateMultipartUploadRequest initRequest = new InitiateMultipartUploadRequest(bucketName, key);

		InitiateMultipartUploadResult initResponse = s3Client.initiateMultipartUpload(initRequest);

		System.out.println("Check the file length and set the chunk size to 5MB");
		long contentLength = file.length();
		// Set chunk size to 5 MB.
		long partSize = 5 * 1024 * 1024;


		// Step 2: Now start Uploading the parts.
		long filePosition = 0;
		for (int i = 1; filePosition < contentLength; i++) {
			System.out.println("Starting Upload Part [" + i + "]. ");
			// The final part could be less than 5 MB in size. So, we need
			// to
			// adjust the part size.
			partSize = Math.min(partSize, (contentLength - filePosition));

			// Create request to upload a part.
			UploadPartRequest uploadRequest = new UploadPartRequest().withBucketName(bucketName).withKey(key)
					.withUploadId(initResponse.getUploadId()).withPartNumber(i).withFileOffset(filePosition).withFile(file)
					.withPartSize(partSize);

			// Upload part and add response to our list.
			partETags.add(s3Client.uploadPart(uploadRequest).getPartETag());
			System.out.println("Upload Part [" + i + "] Completed. ");
			filePosition += partSize;
		}

		// Step 3: Complete the upload.
		CompleteMultipartUploadRequest compRequest = new CompleteMultipartUploadRequest(bucketName, key,initResponse.getUploadId(), partETags);

		s3Client.completeMultipartUpload(compRequest);

		GetObjectRequest request = new GetObjectRequest(bucketName, key);
		if (s3Client.getObject(request).getObjectMetadata().getContentLength() > 0) {
			upldFlag = true;
			System.out.println("Upload completed successfully into [" + key + "]");
		}

	}
	
	public static void main(String[] args) throws Exception
	{

		FileStitchService.process(6734567, 81197, 81197, 23351, 81197);
	}
}
