package com.arc.service;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.util.Bytes;

import com.bizosys.hsearch.byteutils.ByteUtil;
import com.bizosys.hsearch.hbase.HBaseFacade;
import com.bizosys.hsearch.hbase.HTableWrapper;

public class InitializeHBaseTables
{
	
	/*private static final byte[] ROW_DEFAULT_BYTES = ArcConfig.ROW_DEFAULT_BYTES;
	private static final byte[] COL_DEFAULT_BYTES = ArcConfig.COL_DEFAULT_BYTES;
	private static final byte[] HBASE_FAMILY_BYTES = ArcConfig.HBASE_FAMILY_BYTES;
	private final static String FILEID_TABLE = ArcConfig.FILEID_TABLE;
	private final static String REVISIONID_TABLE = ArcConfig.REVISIONID_TABLE;
	
	public static void main(String[] args) throws Exception
	{
		HbaseTableOperation hto = new HbaseTableOperation();
		
		hto.writeToTable(FILEID_TABLE, HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, ROW_DEFAULT_BYTES, ByteUtil.toBytes(ArcConfig.FILE_ID_START));
		
		hto.writeToTable(REVISIONID_TABLE, HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, ROW_DEFAULT_BYTES, ByteUtil.toBytes(ArcConfig.REVISION_ID_START));
	}*/
	
	private static final byte[] ROW_DEFAULT_BYTES = ByteUtil.toBytes("120988_ZWNobyAiZGk=_YzM2YTQzN2Y2YTQzYmIxMWY3N2Y4N2ZiYjYwOWIxYzY=");
	private static final byte[] COL_DEFAULT_BYTES = ByteUtil.toBytes("default");
	private static final byte[] HBASE_FAMILY_BYTES = ByteUtil.toBytes("1");
	private final static String FILEID_TABLE = "FILEID";
	private final static String REVISIONID_TABLE = "REVISIONID";
	private final static byte[] FILE_ID_START = ByteUtil.toBytes(215741L);
	private final static byte[] REVISION_ID_START = ByteUtil.toBytes(418963L);
	static HBaseFacade facade = null;
	static HTableWrapper table = null;
	static HTableWrapper table2 = null;
	public static void main(String[] args) throws Exception
	{
		//System.out.println("File Id start "+FILE_ID_START);
		//System.out.println("Revision Id start "+REVISION_ID_START);
		//System.out.println("File Id start "+"215741L".getBytes());
		try
		{
			facade = HBaseFacade.getInstance();
			//table = facade.getTable(FILEID_TABLE);
			//Put put = new Put(ROW_DEFAULT_BYTES) ;
			//put.add(HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, FILE_ID_START);
			//table.put(put);
			//table.flushCommits();
			
			table2 = facade.getTable(REVISIONID_TABLE);
			//Put put2 = new Put(ROW_DEFAULT_BYTES) ;
			//put2.add(HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, REVISION_ID_START);
			//table2.put(put2);
			//table2.flushCommits();
		} 
		catch (Exception e)
		{
			System.err.println("Exception in writeToTable " + e.getMessage());
			throw e;
		}
		
		/*byte[] value = null;
		table = facade.getTable("CHUNKS");
		Get get = new Get(ROW_DEFAULT_BYTES);
		Result result = table.get(get);
		value = result.getValue(HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES);
		
		System.out.println(""+ByteUtil.toString(value, 0, value.length));*/
		table = facade.getTable("CHUNKS");
		Scan scan = new Scan();

	      // Scanning the required columns
	      scan.addColumn(Bytes.toBytes("1"), Bytes.toBytes("default"));
	      //scan.addColumn(Bytes.toBytes("personal"), Bytes.toBytes("city"));

	      // Getting the scan result
	      ResultScanner scanner = table.getScanner(scan);

	      // Reading values from scan result
	      for (Result result = scanner.next(); result != null; result = scanner.next())

	      //System.out.println("Found row : " +Bytes.toInt(result.getValue(Bytes.toBytes("1"), Bytes.toBytes("default")), 4) );
	      //closing the scanner
	      scanner.close();
	      
	      
	      
	   }
	}
	
	
	

