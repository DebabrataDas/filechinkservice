package com.arc.rest.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.arc.hbase.HbaseTableOperation;
import com.arc.util.conf.ArcConfig;
import com.arc.util.log.ArcLogger;

public class ChunkReferenceSink  implements Runnable  
{
	private static ChunkReferenceSink instance = null;
	private final static String CHUNKS_TABLE = ArcConfig.CHUNKS_TABLE;
	private static final byte[] COL_DEFAULT_BYTES = ArcConfig.COL_DEFAULT_BYTES;
	private static final byte[] HBASE_FAMILY_BYTES = ArcConfig.HBASE_FAMILY_BYTES;
	
	private static long BREATHE_TIME_MILLIS = ArcConfig.BREATHE_TIME_MILLIS;

	public static ChunkReferenceSink getInstance() throws IOException 
	{
		if ( null != instance) return instance;
		synchronized (ChunkReferenceSink.class) 
		{
			if ( null != instance ) return instance;
			BlockingQueue<byte[]> msgQueue = new LinkedBlockingQueue<byte[]>();
			instance = new ChunkReferenceSink(msgQueue);
			Thread offlineThread = new Thread(instance);
			offlineThread.setDaemon(true);
			offlineThread.start();
		}
		return instance;
	}
	
	BlockingQueue<byte[]> blockingQueue = null; 
	
	private ChunkReferenceSink() 
	{
	}
	
	private ChunkReferenceSink ( BlockingQueue<byte[]> blockingQueue) throws IOException 
	{
		this.blockingQueue = blockingQueue;
	}
	
	/**
	 * Add the referenced chunk to blocking queue
	 * @param referencedChunk
	 * @throws IOException
	 */
	public final void addReference(byte[] referencedChunk) throws IOException 
	{
		if ( null == referencedChunk ) return;
		blockingQueue.add(referencedChunk);
	}

	/**
	 * Return blocking queue size
	 * @return
	 */
	public final int getQueueSize() 
	{
		if ( null == blockingQueue) return 0;
		else return blockingQueue.size();
	}
	
	List<byte[]> dataL = new ArrayList<byte[]>(1024);
	public void run() 
	{
		long startTime = System.currentTimeMillis();
		while (true) 
		{
			try 
			{
				int size = this.blockingQueue.size();
				
				/**
				 * Process 1024 chunk at one time.
				 */
				if ( size > 1024 ) size = 1024;
				
				/**
				 * Block if none is available. 
				 * Retrieves and removes the head of this queue, waiting if necessary 
				 * until an element becomes available.
				 */
				if ( size < 1 ) size = 1; 
				
				for ( int i=0; i<size; i++) 
				{
					dataL.add(this.blockingQueue.take());
				}
		
				sink(dataL);
				dataL.clear();
			} 
			catch (Exception ex) 
			{
				ArcLogger.l.fatal("BatchProcessor > ",  ex);
			} 
			finally
			{
				long endTime = System.currentTimeMillis();
				long sleepFor = BREATHE_TIME_MILLIS - (endTime - startTime);
				
				if ( sleepFor > 0 )
				{
					try 
					{
						Thread.sleep(sleepFor);
						startTime = endTime;
					} 
					catch (InterruptedException e) 
					{
						e.printStackTrace();
						Thread.currentThread().interrupt();
					}
				}
			}
		}
	}
	
	/**
	 * Increments the references for all the chunks
	 * @param dataL
	 * @throws Exception
	 */
	private final void sink(final List<byte[]> dataL) throws Exception 
	{
		HbaseTableOperation hbaseOperation = new HbaseTableOperation();
		for ( byte[] chunk : dataL )
		{
			hbaseOperation.incrementColumnValue(CHUNKS_TABLE, chunk, HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, 1);
		}
	}
}