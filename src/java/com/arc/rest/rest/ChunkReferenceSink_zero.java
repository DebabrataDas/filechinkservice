package com.arc.rest.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.arc.hbase.HbaseTableOperation;
import com.arc.util.conf.ArcConfig;
import com.arc.util.log.ArcLogger;

public class ChunkReferenceSink_zero  implements Runnable  
{
	static Logger l = Logger.getLogger(ChunkReferenceSink_zero.class);
	private static ChunkReferenceSink_zero instance1 = null;
	private final static String CHUNKS_TABLE = ArcConfig.CHUNKS_TABLE;
	private static final byte[] COL_DEFAULT_BYTES = ArcConfig.COL_DEFAULT_BYTES;
	private static final byte[] HBASE_FAMILY_BYTES = ArcConfig.HBASE_FAMILY_BYTES;
	
	private static long BREATHE_TIME_MILLIS = ArcConfig.BREATHE_TIME_MILLIS;

	public static ChunkReferenceSink_zero getInstance() throws IOException 
	{
		if ( null != instance1) return instance1;
		synchronized (ChunkReferenceSink_zero.class) 
		{
			if ( null != instance1 ) return instance1;
			BlockingQueue<byte[]> msgQueue = new LinkedBlockingQueue<byte[]>();
			instance1 = new ChunkReferenceSink_zero(msgQueue);
			Thread offlineThread = new Thread(instance1);
			offlineThread.setDaemon(true);
			offlineThread.start();
		}
		return instance1;
	}
	
	BlockingQueue<byte[]> blockingQueue1 = null; 
	
	private ChunkReferenceSink_zero() 
	{
	}
	
	private ChunkReferenceSink_zero ( BlockingQueue<byte[]> blockingQueue1) throws IOException 
	{
		this.blockingQueue1 = blockingQueue1;
	}
	
	/**
	 * Add the referenced chunk to blocking queue
	 * @param referencedChunk
	 * @throws IOException
	 */
	public final void addReference(byte[] referencedChunk) throws IOException 
	{
		l.debug("referencedChunk : "+ referencedChunk);
		if ( null == referencedChunk ) return;
		blockingQueue1.add(referencedChunk);
	}

	/**
	 * Return blocking queue size
	 * @return
	 */
	public final int getQueueSize() 
	{
		if ( null == blockingQueue1) return 0;
		else return blockingQueue1.size();
	}
	
	List<byte[]> dataL1 = new ArrayList<byte[]>(1024);
	public void run() 
	{
		long startTime = System.currentTimeMillis();
		while (true) 
		{
			try 
			{
				int size = this.blockingQueue1.size();
				
				/**
				 * Process 1024 chunk at one time.
				 */
				if ( size > 1024 ) size = 1024;
				
				/**
				 * Block if none is available. 
				 * Retrieves and removes the head of this queue, waiting if necessary 
				 * until an element becomes available.
				 */
				if ( size < 1 ) size = 1; 
				
				for ( int i=0; i<size; i++) 
				{
					dataL1.add(this.blockingQueue1.take());
				}
		
				sink(dataL1);
				dataL1.clear();
			} 
			catch (Exception ex) 
			{
				ArcLogger.l.fatal("BatchProcessor > ",  ex);
			} 
			finally
			{
				long endTime = System.currentTimeMillis();
				long sleepFor = BREATHE_TIME_MILLIS - (endTime - startTime);
				
				if ( sleepFor > 0 )
				{
					try 
					{
						Thread.sleep(sleepFor);
						startTime = endTime;
					} 
					catch (InterruptedException e) 
					{
						e.printStackTrace();
						Thread.currentThread().interrupt();
					}
				}
			}
		}
	}
	
	/**
	 * Increments the references for all the chunks
	 * @param dataL
	 * @throws Exception
	 */
	private final void sink(final List<byte[]> dataL) throws Exception 
	{
		HbaseTableOperation hbaseOperation = new HbaseTableOperation();
		for ( byte[] chunk : dataL )
		{
			hbaseOperation.incrementColumnValue(CHUNKS_TABLE, chunk, HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, 1);
		}
	}
}