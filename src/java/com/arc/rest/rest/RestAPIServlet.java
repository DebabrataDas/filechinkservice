package com.arc.rest.rest;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.MasterNotRunningException;
import org.apache.hadoop.hbase.ZooKeeperConnectionException;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.log4j.Logger;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.arc.filechunk.DecryptionException;
import com.arc.hbase.BucketInfoCache;
import com.arc.hbase.HbaseTableOperation;
import com.arc.management.MonitorHealth;
import com.arc.management.MonitorRequest;
import com.arc.management.MonitorRequest.RequestAPIMetrics;
import com.arc.service.BucketChange;
import com.arc.util.conf.ArcConfig;
import com.arc.util.log.ArcLogger;
import com.bizosys.hsearch.byteutils.ByteUtil;
import com.bizosys.hsearch.hbase.HBaseFacade;
import com.bizosys.hsearch.hbase.HReader;
import com.bizosys.hsearch.hbase.HTableWrapper;
import com.bizosys.hsearch.util.LineReaderUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Servlet exposing the Rest APIs
 * @author pramod
 *
 */
public final class RestAPIServlet extends HttpServlet
{
	static Logger l = Logger.getLogger(RestAPIServlet.class);
	private static final char UNDERSCORE = '_';
	/**
	 * Monitoring Metrics
	 */
	private static final RequestAPIMetrics DELETE_FILE_API_METRIC = 
			new RequestAPIMetrics("DELETE_FILE");
	private static final RequestAPIMetrics DOWNLOAD_PATH_API_METRIC = 
			new RequestAPIMetrics("GET_DOWNLOAD_PATH");
	private static final RequestAPIMetrics GET_FILE_CHUNKS_API_METRIC = 
			new RequestAPIMetrics("GET_FILE_CHUNKS");
	private static final RequestAPIMetrics MATCHING_CHUNKS_API_METRIC = 
			new RequestAPIMetrics("FIND_MATCHING_CHUNKS");
	private static final RequestAPIMetrics DUPLICATE_FILE_API_METRIC = 
			new RequestAPIMetrics("FIND_DUPLICATE_FILE");
	private static final RequestAPIMetrics FIND_OFFSETS_API_METRIC = 
			new RequestAPIMetrics("FIND_FILE_CHUNK_OFFSETS");
	private static final RequestAPIMetrics POSSIBLE_DUPLICATE_API_METRIC = 
			new RequestAPIMetrics("FIND_POSSIBLE_FILE_DUPLICATE");
	private static final RequestAPIMetrics UPLOAD_FILE_API_METRIC = 
			new RequestAPIMetrics("UPLOAD_FILE");
	private static final RequestAPIMetrics ADD_CONTAINER_API_METRIC = 
			new RequestAPIMetrics("ADD_CONTAINER_FOR_ACCOUNT");

	/**
	 * Class level conatants
	 */
	private static final String CHUNK_HASH = ArcConfig.CHUNK_HASH;
	private static final String CHUNK_HEADER = ArcConfig.CHUNK_HEADER;
	private static final String CHUNKS_REFERENCED = ArcConfig.CHUNKS_REFERENCED;
	private static final String CHUNKS_JSON = ArcConfig.CHUNKS_JSON;
	private static final String CHUNK_OFFSET_JSON = ArcConfig.CHUNK_OFFSET_JSON;
	private static final String S3_CONTAINER_PATH = ArcConfig.S3_CONTAINER_PATH;
	private static final String FOLDER_NAME = ArcConfig.FOLDER_NAME;
	private static final String ACCOUNT_ID = ArcConfig.ACCOUNT_ID;
	private static final String PROJECT_ID = ArcConfig.PROJECT_ID;
	private static final String REVISION = ArcConfig.REVISION;
	private static final String BASE_REVISION = ArcConfig.BASE_REVISION;
	private static final String FILE_ID = ArcConfig.FILE_ID;
	private static final String REVISION_ID = ArcConfig.REVISION_ID;
	private static final String MULTI_ID = ArcConfig.MULTI_ID;
	private static final String FILE_LENGTH = ArcConfig.FILE_LENGTH;
	private static final String END_BYTE = ArcConfig.END_BYTE;
	private static final String START_BYTE = ArcConfig.START_BYTE;
	private static final String MAGIC_HEADER = ArcConfig.MAGIC_HEADER;
	private static final String THUMBNAIL_TYPE = ArcConfig.THUMBNAIL_TYPE;
	private static final boolean TEST_MODE = ArcConfig.TEST_MODE;
	private static boolean IS_FILEID_TABLE_INITIALIZED = false;
	private static boolean IS_REVISIONID_TABLE_INITIALIZED = false;
	private final static byte[] FILE_ID_START = ByteUtil.toBytes(ArcConfig.FILE_ID_START);
	private final static byte[] REVISION_ID_START = ByteUtil.toBytes(ArcConfig.REVISION_ID_START);
	private static final String REVISIONID_TABLE = ArcConfig.REVISIONID_TABLE;
	private static final String FILEID_TABLE = ArcConfig.FILEID_TABLE;
	private static final byte[] ROW_DEFAULT_BYTES = ByteUtil.toBytes("1");


	private static final char SQUARE_BRACKET_START = '[';
	private static final char SQUARE_BRACKET_END = ']';
	private static final char CURLY_BRACE_START = '{';
	private static final char CURLY_BRACE_END = '}';
	private static final char COMMA = ',';
	private static final char COLON = ':';
	private static final char QUOTE = '"';
	private static final byte[] responseOKBytes = ArcConfig.responseOKBytes;

	private static final String ACTION = ArcConfig.ACTION;
	private static final String REQUEST_PARAM_KEY = ArcConfig.REQUEST_PARAM_KEY;

	private static long lastFileId = 0L;
	private static long maxStoredFileId = -1L;
	private static long lastRevisionId = 0L;
	private static long maxStoredRevisionId = -1L;
	private static final long FILEID_INCREMENT_AMOUNT = 1;
	private static final long REVISIONID_INCREMENT_AMOUNT = 1;
	private static final int FILE_ID_PREFIX = ArcConfig.FILE_ID_PREFIX; 
	private static final int REVISION_ID_PREFIX = ArcConfig.REVISION_ID_PREFIX; 
	/**
	 * HBase Related Constants
	 */
	private static final byte[] COL_BASE_REVISION_BYTES = 
			ArcConfig.COL_BASE_REVISION_BYTES;
	private static final byte[] COL_CHUNKS_JSON_BYTES = 
			ArcConfig.COL_CHUNKS_JSON_BYTES;
	private static final byte[] COL_CHUNK_OFFSET_JSON_BYTES = 
			ArcConfig.COL_CHUNK_OFFSET_JSON_BYTES;
	private static final byte[] COL_DEFAULT_BYTES = ArcConfig.COL_DEFAULT_BYTES;
	private static final byte[] COL_EXT_BYTES = ArcConfig.COL_EXT_BYTES;
	private static final byte[] HBASE_FAMILY_BYTES = 
			ArcConfig.HBASE_FAMILY_BYTES;
	private final static String LINKS_TABLE = ArcConfig.LINKS_TABLE;
	private final static String CHUNKS_TABLE = ArcConfig.CHUNKS_TABLE;
	private final static String FILECHUNKS_TABLE = ArcConfig.FILECHUNKS_TABLE;
	private final static String FILEPARROTHASH_TABLE = 
			ArcConfig.FILEPARROTHASH_TABLE;

	Configuration config = HBaseConfiguration.create();

	MonitorRequest requestMonitor = MonitorRequest.getInstance();

	private static final long serialVersionUID = 1L;

	private static final boolean isDebugEnabled = true;//l.isDebugEnabled();
	private static final String EXT = "ext";
	private static final String ROW_KEY = "rowKey";
	private static final String CHUNK_FOLDER = "chunks";

	private FileStitchSink fileSink = null;
	private ChunkReferenceSink referenceSink = null;

	@Override
	public void init() throws ServletException
	{
		l.debug("Initailazing RestAPI Servlet....");
		super.init();
		initMonitoring();
		warmUpServer();
		
		try 
		{
			BucketInfoCache.loadCacheFromHbase();
			fileSink = FileStitchSink.getInstance();
			referenceSink = ChunkReferenceSink.getInstance();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DecryptionException e) {
			l.error("ENCRYPTOR couldn't initialized ");
			e.printStackTrace();
		}
	}

	/**
	 * Registers request monitors for each API
	 */
	private void initMonitoring()
	{
		if ( isDebugEnabled ){
			l.debug("Registering Metrics for all APIs");
		}
		MonitorHealth.getInstance();
		requestMonitor.register(ADD_CONTAINER_API_METRIC);
		requestMonitor.register(UPLOAD_FILE_API_METRIC);
		requestMonitor.register(POSSIBLE_DUPLICATE_API_METRIC);
		requestMonitor.register(FIND_OFFSETS_API_METRIC);
		requestMonitor.register(DUPLICATE_FILE_API_METRIC);
		requestMonitor.register(MATCHING_CHUNKS_API_METRIC);
		requestMonitor.register(GET_FILE_CHUNKS_API_METRIC);
		requestMonitor.register(DOWNLOAD_PATH_API_METRIC);
		requestMonitor.register(DELETE_FILE_API_METRIC);
	}

	/**
	 * Loading of JSON Libraries. Warm up so that future calls are faster.
	 */
	private void warmUpServer()
	{
		String sampleJson = "{action:\"UPLOAD_FILE\",chunksUploaded: "
				+ "[{\"chunkHeader\":\"KioqKioqKio=\",\"chunkHash\":"
				+ "\"NDdjNmIxNWFjYTkzODczZjU4NTIyZjI2MzAwZDE4MWQ=\"},"
				+ "{\"chunkHeader\":\"KioqKioqKio=\", \"chunkHash\":"
				+ "\"NDdjNmIxNWFjYTkzODczZjU4NTIyZjI2MzAwZDE4MWQ=\"}],"
				+ "chunksReferenced:[{\"chunkHeader\":\"KioqKioqKio=\", "
				+ "\"chunkHash\":"
				+ "\"NDdjNmIxNWFjYTkzODczZjU4NTIyZjI2MzAwZDE4MWQ=\"},"
				+ "{\"chunkHeader\":\"KioqKioqKio=\", \"chunkHash\":"
				+ "\"NDdjNmIxNWFjYTkzODczZjU4NTIyZjI2MzAwZDE4MWQ=\"}]}";

		JsonElement element = new JsonParser().parse(sampleJson);
		JsonObject  jobject = element.getAsJsonObject();
		String action = jobject.getAsJsonPrimitive("action").getAsString();
		JsonArray chunksReferenced = jobject.getAsJsonArray("chunksUploaded");
	}

	/**
	 * The GET Method. Nothing to do as all requests are POST 
	 */
	public final void doGet(HttpServletRequest request, 
			HttpServletResponse response)
	{

	}

	public final void doPost(final HttpServletRequest request, 
			HttpServletResponse response) throws IOException
	{
		if ( isDebugEnabled ){
			l.debug("Post Request Received");
		}
		String reqJson = request.getParameter(REQUEST_PARAM_KEY);
		OutputStream out = response.getOutputStream();
		try 
		{
			l.debug("Entering to process kolkata1");
			process(out, reqJson);
		} 
		catch (Exception e) 
		{
			l.debug(e.getMessage());
			response.setContentType("text/html");
			response.setCharacterEncoding("UTF-8");
			response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
			//response.getWriter().write("Something went wrong: "+"\n" + e.getMessage());
			//response.getWriter().write("{\"errorCode\":"+HttpServletResponse.SC_EXPECTATION_FAILED+",\"errorMsg\":\""+"this is simple error msg"+"\"}");
			out.write(("Something went wrong: " + e.getMessage()).getBytes());
		}
	}

	public final void process(OutputStream out, final String reqJson) throws Exception
	{

		StringBuilder responseJsonB = new StringBuilder(256);
		String action = "";
		try
		{
			HbaseTableOperation hbaseOperation = new HbaseTableOperation();

			JsonObject jobject = null;
			try {
				JsonElement element = new JsonParser().parse(reqJson);
				jobject = element.getAsJsonObject();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				throw e;
			}

			action = jobject.getAsJsonPrimitive(ACTION).getAsString();
			int requestByteSize = reqJson.getBytes().length;


			if ( isDebugEnabled ){
				l.debug("Processing Request for Action: " +action);
			}

			if(action.equals("ADD_CONTAINER_FOR_ACCOUNT"))
			{
				if ( isDebugEnabled )
					l.debug("Entering in ADD_CONTAINER_FOR_ACCOUNT...");
				requestMonitor.onEnter(ADD_CONTAINER_API_METRIC);
				setContainerForAccount(responseJsonB, hbaseOperation, jobject);
				requestMonitor.onExit(ADD_CONTAINER_API_METRIC, null, 
						requestByteSize, responseOKBytes.length);
				out.write(responseOKBytes);
				//setContainerForAccount(responseJsonB, hbaseOperation, jobject);
				if ( isDebugEnabled )
					l.debug("Exiting ADD_CONTAINER_FOR_ACCOUNT");
			}
			else if ( action.equals("UPLOAD_FILE") )
			{
				if ( isDebugEnabled )
					l.debug("Entering UPLOAD_FILE");
				requestMonitor.onEnter(UPLOAD_FILE_API_METRIC);
				boolean isSetResponse = false;

				try {
					uploadFile(responseJsonB, hbaseOperation, jobject);
				} catch (Exception e) {

					isSetResponse = true;
					throw new Exception(e);

				}
				requestMonitor.onExit(UPLOAD_FILE_API_METRIC, null, 
						requestByteSize, responseOKBytes.length);
				out.write(responseOKBytes);
			}
			else if(action.equals("FIND_POSSIBLE_FILE_DUPLICATE"))
			{
				if ( isDebugEnabled )
					l.debug("Entering FIND_POSSIBLE_FILE_DUPLICATE");
				requestMonitor.onEnter(POSSIBLE_DUPLICATE_API_METRIC);
				boolean isSetResponse = false;
				try {
					findPossibleFileDuplicate(responseJsonB, 
							hbaseOperation, jobject);
					byte[] responseB = responseJsonB.toString().getBytes(); 
					out.write(responseB);
					requestMonitor.onExit(POSSIBLE_DUPLICATE_API_METRIC, null, 
							requestByteSize, responseB.length);

				} catch (Exception e) {
					throw new Exception(e);

				}

			}
			else if(action.equals("FIND_FILE_CHUNK_OFFSETS"))
			{
				if ( isDebugEnabled )
					l.debug("Entering FIND_FILE_CHUNK_OFFSETS");
				requestMonitor.onEnter(FIND_OFFSETS_API_METRIC);

				try {
					findFileChunkOffsets(responseJsonB, hbaseOperation, jobject);
					byte[] responseB = responseJsonB.toString().getBytes(); 
					requestMonitor.onExit(FIND_OFFSETS_API_METRIC, null, 
							requestByteSize, responseB.length);
					out.write(responseB);
				} catch (Exception e) {
					throw new Exception("Hbase service is temporarly Down");

				}


			}
			else if( action.equals("FIND_DUPLICATE_FILE") )
			{
				if ( isDebugEnabled )
					l.debug("Entering FIND_DUPLICATE_FILE");
				requestMonitor.onEnter(DUPLICATE_FILE_API_METRIC);
				boolean isSetResponse = false;
				try {
					findDuplicateFile(responseJsonB, hbaseOperation, jobject);
					byte[] responseB = responseJsonB.toString().getBytes(); 
					requestMonitor.onExit(DUPLICATE_FILE_API_METRIC, null, 
							requestByteSize, responseB.length);
					out.write(responseB);
				} catch (Exception e) {
					throw new Exception("Hbase service is temporarly Down");
				}

			}
			else if( action.equals("FIND_MATCHING_CHUNKS") )
			{
				if ( isDebugEnabled )
					l.debug("Entering FIND_MATCHING_CHUNKS");
				requestMonitor.onEnter(MATCHING_CHUNKS_API_METRIC);
				boolean isSetResponse = false;
				try {
					findMatchingChunks(responseJsonB, hbaseOperation, jobject);
					byte[] responseB = responseJsonB.toString().getBytes(); 
					requestMonitor.onExit(MATCHING_CHUNKS_API_METRIC, null, 
							requestByteSize, responseB.length);
					out.write(responseB);
				} catch (Exception e) {
					throw new Exception("Hbase service is temporarly Down");
				}

			}
			else if( action.equals("GET_FILE_CHUNKS") )
			{
				if ( isDebugEnabled )
					l.debug("Entering GET_FILE_CHUNKS");
				requestMonitor.onEnter(GET_FILE_CHUNKS_API_METRIC);
				boolean isSetResponse = false;
				try {
					getFileChunks(responseJsonB, hbaseOperation, jobject);
					byte[] responseB = responseJsonB.toString().getBytes(); 
					requestMonitor.onExit(GET_FILE_CHUNKS_API_METRIC, null, 
							requestByteSize, responseB.length);
					out.write(responseB);
				} catch (Exception e) {
					throw new Exception("Hbase service is temporarly Down");
				}

			}
			else if( action.equals("GET_CONTAINER") )
			{
				if ( isDebugEnabled )
					l.debug("Entering GET_CONTAINER");
				requestMonitor.onEnter(DOWNLOAD_PATH_API_METRIC);
				boolean isSetResponse = false;
				try {
					getContainer(responseJsonB, hbaseOperation, jobject);
					byte[] responseB = responseJsonB.toString().getBytes(); 
					requestMonitor.onExit(DOWNLOAD_PATH_API_METRIC, null, 
							requestByteSize, responseB.length);
					out.write(responseB);
				} catch (Exception e) {
					throw new Exception("Hbase service is temporarly Down");
				}

			}
			else if( action.equals("GET_FILE_ID") )
			{
				if ( isDebugEnabled )
					l.debug("Entering GET_FILE_ID");

				try {
					String fileId = getFileId(responseJsonB, hbaseOperation);
					out.write(fileId.getBytes());
					l.debug("REVISION iD : "+fileId);
				} catch (Exception e) {

					throw e;
				}

			}else if (action.equals("GET_MULTI_IDS")) {
				if (isDebugEnabled)
					l.debug("Entering GET_MULTI_IDS");

				try {
					String multiId = getMultiId(responseJsonB, hbaseOperation, jobject);
					out.write(multiId.getBytes());
					l.debug("MULTI ID : " + multiId);

					/*
					 * String revisionId = getRevisionId(responseJsonB,
					 * hbaseOperation); out.write(revisionId.getBytes());
					 * l.debug("REVISION iD : "+revisionId);
					 */
				} catch (Exception e) {
					l.debug("Printing e " + e.getMessage());
					throw e;
				}

			}
			else if( action.equals("GET_REVISION_ID") )
			{
				if ( isDebugEnabled )
					l.debug("Entring GET_REVISION_ID");


				try {
					String revisionId = getRevisionId(responseJsonB, hbaseOperation);
					out.write(revisionId.getBytes());
					l.debug("REVISION iD : "+revisionId);
				} catch (Exception e) {
					l.debug("Entering to revision exception kolkata6");
					l.debug("Printing e "+e.getMessage());
					throw e;
				}

			}
			else if( action.equals("DELETE_FILE") )
			{
				if ( isDebugEnabled )
					l.debug("Entring DELETE_FILE");
				requestMonitor.onEnter(DELETE_FILE_API_METRIC);
				boolean isSetResponse = false;
				try {
					deleteFile(responseJsonB, hbaseOperation, jobject);
					requestMonitor.onExit(DELETE_FILE_API_METRIC, null, 
							requestByteSize, responseOKBytes.length);
					out.write(responseOKBytes);
				} catch (Exception e) {

					throw new Exception("Hbase service is temporarly Down");
				}

			}
			else if(action.equals("INSERT_TO_CHUNKS_TABLE_WITH_ZERO_REFERENCE"))
			{
				boolean isSetResponse = false;
				try {
					insertIntoChunkTableWithZeroReference(responseJsonB, hbaseOperation, jobject);
					out.write(responseOKBytes);
				} catch (Exception e) {
					throw new Exception(e);
				}
			}
		}
		catch(Exception e)
		{
			l.error("RestAPIServlet - Error processing request.", e);
			//out.write(e.toString().getBytes());
			//out.write(("Something went wrong"+ e.getMessage().toString()).getBytes());
			e.printStackTrace();
			throw e;
		}
	}

	private void exceptionBuilder(StringBuilder responseJsonB)
	{
		responseJsonB.append(CURLY_BRACE_START)
		.append(QUOTE).append("values").append(QUOTE).append(COLON)
		.append(SQUARE_BRACKET_START)
		.append(CURLY_BRACE_START)
		.append(QUOTE).append("Exception").append(QUOTE).append(COLON).append(QUOTE).append("Hbase service is temporarly Down").append(QUOTE).append(COMMA)
		.append(QUOTE).append("errorType").append(QUOTE).append(COLON).append(QUOTE).append("").append(QUOTE).append(COMMA)
		.append(QUOTE).append("statusCode").append(QUOTE).append(COLON).append(QUOTE).append("502").append(QUOTE).append(COMMA)
		.append(QUOTE).append("successMsg").append(QUOTE).append(COLON).append(QUOTE).append("").append(QUOTE).append(COMMA)
		.append(QUOTE).append("errorCode").append(QUOTE).append(COLON).append(QUOTE).append("").append(QUOTE)
		.append(CURLY_BRACE_END);
		responseJsonB.append(SQUARE_BRACKET_END).append(CURLY_BRACE_END);
	}
	private void insertIntoChunkTableWithZeroReference(StringBuilder responseJsonB, HbaseTableOperation hbaseOperation,
			JsonObject jobject) throws Exception {
		if ( isDebugEnabled )
			l.debug("Inside INSERT_TO_CHUNKS_TABLE_WITH_ZERO_REFERENCE");


		JsonArray chunksReferenced = jobject.getAsJsonArray(CHUNKS_REFERENCED);

		if ( null != chunksReferenced )
		{
			int referencedChunksSize = chunksReferenced.size();
			StringBuilder referenceChunkB = new StringBuilder(256);

			for ( int i = 0; i < referencedChunksSize; i++ )
			{
				referenceChunkB.setLength(0);
				JsonObject chunkObj = 
						chunksReferenced.get(i).getAsJsonObject();

				referenceChunkB.append(chunkObj.getAsJsonPrimitive(ACCOUNT_ID).getAsString()).append(UNDERSCORE)
				.append(chunkObj.getAsJsonPrimitive(CHUNK_HEADER)
						.getAsString()).append(UNDERSCORE)
						.append(chunkObj.getAsJsonPrimitive(CHUNK_HASH).getAsString());
				String key = referenceChunkB.toString();
				l.debug("Inserting into chunk table with zero reference");
				Integer a =0;
				
				hbaseOperation.writeToTable(CHUNKS_TABLE, HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, Bytes.toBytes(key), Bytes.toBytes(0L));
				
			}
		}


	}

	private synchronized final String getFileId(
			StringBuilder responseJsonB, HbaseTableOperation hbaseOperation) throws Exception
	{
		String fileId;
		String fileIdStr="";
		long id;
		/*synchronized ("")
		{*/
		//if ( lastFileId > maxStoredFileId )
		//{
		if(!IS_FILEID_TABLE_INITIALIZED)
		{

			try
			{

				l.info("Checking whether FILEID table is empty or not");
				byte[] fileIdBytes = hbaseOperation.readValueFromTable(FILEID_TABLE, HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, ROW_DEFAULT_BYTES);

				fileIdStr = ByteUtil.toString(fileIdBytes, 0, fileIdBytes.length);

				//valueS = ByteUtil.toString(value, 0, value.length);
				l.info("FILEID table is not empty.current value in FILEID Table : "+fileIdStr);
				l.info("Length of Value in FILEID Table : "+fileIdStr.length());


			}
			catch (Exception e) {
				fileIdStr="";

			}


			if(fileIdStr == null || fileIdStr == "")
			{
				try{


					l.info("FileId is either null or empty. so initializing FileId and RevisionId");
					byte[] ROW_DEFAULT_BYTES = ArcConfig.ROW_DEFAULT_BYTES;
					byte[] COL_DEFAULT_BYTES = ArcConfig.COL_DEFAULT_BYTES;
					byte[] HBASE_FAMILY_BYTES = ArcConfig.HBASE_FAMILY_BYTES;
					String FILEID_TABLE = ArcConfig.FILEID_TABLE;
					String REVISIONID_TABLE = ArcConfig.REVISIONID_TABLE;


					HbaseTableOperation hto = new HbaseTableOperation();
					l.info("writing to FILEID table for the first time");
					hto.writeToTable(FILEID_TABLE, HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, ROW_DEFAULT_BYTES, ByteUtil.toBytes(ArcConfig.FILE_ID_START));

					l.info("writing to REVISIONID table for the first time");
					hto.writeToTable(REVISIONID_TABLE, HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, ROW_DEFAULT_BYTES, ByteUtil.toBytes(ArcConfig.REVISION_ID_START));

				}catch (Exception e) {

					throw e;
				}

			}

			IS_FILEID_TABLE_INITIALIZED = true;
			IS_REVISIONID_TABLE_INITIALIZED = true;

		}
		long ids = hbaseOperation.getFileIds(FILEID_INCREMENT_AMOUNT);

		l.info("Current value of fileId in FILEID table: "+ids);
		//lastFileId = ids - FILEID_INCREMENT_AMOUNT + 1;
		//maxStoredFileId = ids;
		//}
		//id = lastFileId++;
		id = ids;
		//}

		StringBuilder sb = new StringBuilder(32);
		if( TEST_MODE )
		{
			sb.append(FILE_ID_PREFIX).append(id);
		}
		else
		{
			//Date date = new Date();
			//DateFormat df = new SimpleDateFormat("yy");
			//sb.append(df.format(date)).append(id);
			sb.append(FILE_ID_PREFIX).append(id);
		}
		fileId = sb.toString();
		l.debug("fileid: "+fileId);
		responseJsonB.append(CURLY_BRACE_START)
		.append(QUOTE).append("values").append(QUOTE).append(COLON)
		.append(SQUARE_BRACKET_START).append(CURLY_BRACE_START)
		.append(QUOTE).append(FILE_ID).append(QUOTE).append(COLON).append(fileId)
		.append(CURLY_BRACE_END).append(SQUARE_BRACKET_END)
		.append(CURLY_BRACE_END);	
		return responseJsonB.toString();
	}

	private synchronized final String getRevisionId(
			StringBuilder responseJsonB, HbaseTableOperation hbaseOperation) throws Exception
	{

		String revisionIdStr;
		String revisionId;
		long id;
		/*synchronized ("")
		{*/
		//if ( lastRevisionId > maxStoredRevisionId )
		//{

		if(!IS_REVISIONID_TABLE_INITIALIZED)
		{
			try
			{

				l.info("reading value of RevisionId from REVISIONID table");
				byte[] revisionIdBytes = hbaseOperation.readValueFromTable(REVISIONID_TABLE, HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, ROW_DEFAULT_BYTES);

				revisionIdStr = ByteUtil.toString(revisionIdBytes, 0, revisionIdBytes.length);

				//valueS = ByteUtil.toString(value, 0, value.length);
				l.info("Current Value in REVISION Table : "+revisionIdStr);
				l.info("Length of the revisionId in REVISION Table : "+revisionIdStr.length());



			}
			catch (Exception e) {
				revisionIdStr = "";

			}


			if(revisionIdStr == null || revisionIdStr == "")
			{
				try{

					l.info("Current value of revisionId is either null or empty. so initializing bothe FileId and RevisionId");
					byte[] ROW_DEFAULT_BYTES = ArcConfig.ROW_DEFAULT_BYTES;
					byte[] COL_DEFAULT_BYTES = ArcConfig.COL_DEFAULT_BYTES;
					byte[] HBASE_FAMILY_BYTES = ArcConfig.HBASE_FAMILY_BYTES;
					String FILEID_TABLE = ArcConfig.FILEID_TABLE;
					String REVISIONID_TABLE = ArcConfig.REVISIONID_TABLE;


					HbaseTableOperation hto = new HbaseTableOperation();
					l.info("writing to FILEID table for the first time");
					hto.writeToTable(FILEID_TABLE, HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, ROW_DEFAULT_BYTES, ByteUtil.toBytes(ArcConfig.FILE_ID_START));

					l.info("writing to REVISIONID table for the first time");
					hto.writeToTable(REVISIONID_TABLE, HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, ROW_DEFAULT_BYTES, ByteUtil.toBytes(ArcConfig.REVISION_ID_START));
				}catch (Exception e) {

					throw e;
				}
			}

			IS_FILEID_TABLE_INITIALIZED = true;
			IS_REVISIONID_TABLE_INITIALIZED = true;

		}


		long ids;
		try {
			ids = hbaseOperation.getRevisionIds(REVISIONID_INCREMENT_AMOUNT);
		} catch (Exception e) {

			l.error("Couldn't get revisionId");;
			throw e;
		}
		l.info("Current value of REVISIONId in REVISIONID table: "+ids);
		//lastRevisionId = ids - REVISIONID_INCREMENT_AMOUNT + 1;
		//maxStoredRevisionId = ids;
		//}
		//id = lastRevisionId++;
		id = ids;
		//}
		StringBuilder sb = new StringBuilder(32);
		if( TEST_MODE )
		{
			sb.append(REVISION_ID_PREFIX).append(id);
		}
		else
		{
			//Date date = new Date();
			//DateFormat df = new SimpleDateFormat("yy");
			//sb.append(df.format(date)).append(id);
			sb.append(REVISION_ID_PREFIX).append(id);
		}
		revisionId = sb.toString();
		l.debug("Revision Id: "+revisionId);
		responseJsonB.append(CURLY_BRACE_START)
		.append(QUOTE).append("values").append(QUOTE).append(COLON)
		.append(SQUARE_BRACKET_START).append(CURLY_BRACE_START)
		.append(QUOTE).append(REVISION_ID).append(QUOTE).append(COLON).append(revisionId)
		.append(CURLY_BRACE_END).append(SQUARE_BRACKET_END)
		.append(CURLY_BRACE_END);

		l.debug("Json for revisonId is : "+responseJsonB);
		return responseJsonB.toString();
	}

	/**
	 * Deletes file entry from database and decrements the chunks references
	 * @param responseJsonB
	 * @param hbaseOperation
	 * @param jobject
	 * @throws Exception
	 */
	private final void deleteFile(StringBuilder responseJsonB, 
			final HbaseTableOperation hbaseOperation, final JsonObject jobject) 
					throws Exception
	{
		if ( isDebugEnabled )
			l.debug("Inside deleteFile");
		int accountId = jobject.getAsJsonPrimitive(ACCOUNT_ID).getAsInt();
		String magicHeader = 
				jobject.getAsJsonPrimitive(MAGIC_HEADER).getAsString();
		short startByte = jobject.getAsJsonPrimitive(START_BYTE).getAsShort();
		short endByte = jobject.getAsJsonPrimitive(END_BYTE).getAsShort();
		int fileLength = jobject.getAsJsonPrimitive(FILE_LENGTH).getAsInt();
		long fileId = jobject.getAsJsonPrimitive(FILE_ID).getAsLong();
		long revision = jobject.getAsJsonPrimitive(REVISION).getAsLong();
		int projectId = jobject.getAsJsonPrimitive(PROJECT_ID).getAsInt();
		if ( isDebugEnabled )
			l.debug("Deleting file for AccountId: "+accountId+" and fileId: "+fileId);

		deleteFileFromFileParrotHash( hbaseOperation, accountId, magicHeader, 
				startByte, endByte, fileLength, fileId, revision);
		l.debug("entering inside delete file from FILECHUNKS");
		deletFileFromFileChunks(hbaseOperation, accountId, projectId, fileId, 
				revision);

		JsonArray chunksToDelete = jobject.getAsJsonArray(CHUNKS_REFERENCED);

		List<Put> chunksReferencedPutL = new ArrayList<Put>();
		for ( int i = 0; i < chunksToDelete.size(); i++ )
		{
			JsonObject chunkObj = chunksToDelete.get(i).getAsJsonObject();
			String chunkHeader = 
					chunkObj.getAsJsonPrimitive(CHUNK_HEADER).getAsString();
			String chunkHash = 
					chunkObj.getAsJsonPrimitive(CHUNK_HASH).getAsString();

			StringBuilder referenceChunkB = new StringBuilder(256);

			referenceChunkB.append(accountId).append(UNDERSCORE)
			.append(chunkHeader).append(UNDERSCORE)
			.append(chunkHash);

			byte[] chunksKey = ByteUtil.toBytes(referenceChunkB.toString());
			byte[] value = hbaseOperation.readValueFromTable(CHUNKS_TABLE, 
					HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, chunksKey);
			long reference = (null == value) ? 0 : ByteUtil.toLong(value, 0) - 1;

			Put put = new Put(chunksKey);
			put.add(HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, 
					ByteUtil.toBytes(reference));
			chunksReferencedPutL.add(put);
		}
		hbaseOperation.writeBatchToTable(CHUNKS_TABLE, chunksReferencedPutL);
	}

	/**
	 * Add file entry to database and increment the chunks refernces
	 * @param responseJsonB
	 * @param hbaseOperation
	 * @param jobject
	 * @throws Exception
	 */
	private final void uploadFile(StringBuilder responseJsonB, 
			final HbaseTableOperation hbaseOperation, final JsonObject jobject) 
					throws Exception
	{
		if ( isDebugEnabled )
			l.debug("Inside uploadFile");
		long start = System.currentTimeMillis();

		char thumbnailType = 
				jobject.getAsJsonPrimitive(THUMBNAIL_TYPE).getAsCharacter();
		int accountId = jobject.getAsJsonPrimitive(ACCOUNT_ID).getAsInt();
		String magicHeader = 
				jobject.getAsJsonPrimitive(MAGIC_HEADER).getAsString();
		short startByte = jobject.getAsJsonPrimitive(START_BYTE).getAsShort();
		short endByte = jobject.getAsJsonPrimitive(END_BYTE).getAsShort();
		int fileLength = jobject.getAsJsonPrimitive(FILE_LENGTH).getAsInt();
		long fileId = jobject.getAsJsonPrimitive(FILE_ID).getAsLong();
		long revision = jobject.getAsJsonPrimitive(REVISION).getAsLong();
		String bucketNameReq = jobject.getAsJsonPrimitive("bucketName").getAsString();
		
		String bucketName = BucketInfoCache.getBucketInfo(accountId).getBucketName();
		
		if(!bucketName.equals(bucketNameReq)){
			BucketInfoCache.refreshBucketInfo(Integer.toString(accountId));
			bucketName = BucketInfoCache.getBucketInfo(accountId).getBucketName();
		}
		
		long baseRevision = jobject.getAsJsonPrimitive(BASE_REVISION).getAsLong();
		String ext=jobject.getAsJsonPrimitive(EXT).getAsString();
		int projectId = jobject.getAsJsonPrimitive(PROJECT_ID).getAsInt();



		String chunkOffsetJSON = 
				jobject.getAsJsonPrimitive(CHUNK_OFFSET_JSON).getAsString();
		String chunksJSON = 
				jobject.getAsJsonPrimitive(CHUNKS_JSON).getAsString();

		JsonArray chunksReferenced = jobject.getAsJsonArray(CHUNKS_REFERENCED);
		if ( isDebugEnabled )
			l.debug("Total chunks referenced with the file: "+chunksReferenced.size());
		boolean[] transactions = new boolean[3];
		Arrays.fill(transactions, false);
		try
		{
			//			- ADD OR APPEND FILE TO THUMBNAIL
			StringBuilder parrotKey = new StringBuilder();
			parrotKey.append(accountId).append(UNDERSCORE)
			.append(magicHeader).append(UNDERSCORE)
			.append(startByte).append(UNDERSCORE)
			.append(endByte).append(UNDERSCORE)
			.append(fileLength);

			byte[] parrotKeyBytes = ByteUtil.toBytes(parrotKey.toString());
			byte[] columnName = ByteUtil.toBytes(fileId);
			byte[] revisionBytes = null;

			if ( 'N' == thumbnailType )
			{
				revisionBytes = ByteUtil.toBytes(Long.toString(revision));
			}
			else if ( 'A' == thumbnailType )
			{

				byte[] revisions = 
						hbaseOperation.readValueFromTable(FILEPARROTHASH_TABLE, 
								HBASE_FAMILY_BYTES, columnName, parrotKeyBytes);

				if( null != revisions )
				{
					StringBuilder revisionValuesB = new StringBuilder();
					revisionValuesB
					.append(ByteUtil.toString(revisions, 0, revisions.length))
					.append(COMMA).append(revision);
					revisionBytes = 
							ByteUtil.toBytes(revisionValuesB.toString());
				}
			}
			else
			{
				l.error("ERROR in Thumbnail Type. please check your Json");
				throw new IOException("Unknown Thumbnail Type. Should be "
						+ "either N for new file or A for new revision.");
			}
			if ( isDebugEnabled )
				l.debug("writing to FILEPARROTHASH table");
			hbaseOperation.writeToTable(FILEPARROTHASH_TABLE, 
					HBASE_FAMILY_BYTES, columnName, parrotKeyBytes, 
					revisionBytes);
			transactions[0] = true;


			//			- ADD FILE TO FILECHUNKS
			StringBuilder sb = new StringBuilder();

			sb.append(accountId).append(UNDERSCORE)
			.append(projectId).append(UNDERSCORE)
			.append(fileId).append(UNDERSCORE)
			.append(revision);

			Put fileChunksPut = new Put(ByteUtil.toBytes(sb.toString()));
			fileChunksPut.add(HBASE_FAMILY_BYTES, COL_CHUNK_OFFSET_JSON_BYTES, 
					ByteUtil.toBytes(chunkOffsetJSON));
			fileChunksPut.add(HBASE_FAMILY_BYTES, COL_CHUNKS_JSON_BYTES, 
					ByteUtil.toBytes(chunksJSON));
			fileChunksPut.add(HBASE_FAMILY_BYTES, COL_BASE_REVISION_BYTES, 
					ByteUtil.toBytes(baseRevision));
			//Adding column for file extension
			fileChunksPut.add(HBASE_FAMILY_BYTES, COL_EXT_BYTES, 
					ByteUtil.toBytes(ext));

			l.debug("writing to FILECHUNK table");

			hbaseOperation.writeToTable(FILECHUNKS_TABLE, fileChunksPut);
			transactions[1] = true;

			//			- INCREMENT CHUNK REFERENCE (*)
			if ( isDebugEnabled )
				l.debug("Adding chunks to blocking queue");
			if ( null != chunksReferenced )
			{
				int referencedChunksSize = chunksReferenced.size();
				StringBuilder referenceChunkB = new StringBuilder(256);

				for ( int i = 0; i < referencedChunksSize; i++ )
				{
					referenceChunkB.setLength(0);
					JsonObject chunkObj = 
							chunksReferenced.get(i).getAsJsonObject();

					referenceChunkB.append(accountId).append(UNDERSCORE)
					.append(chunkObj.getAsJsonPrimitive(CHUNK_HEADER)
							.getAsString()).append(UNDERSCORE)
							.append(chunkObj.getAsJsonPrimitive(CHUNK_HASH)
									.getAsString());
					referenceSink.addReference(
							ByteUtil.toBytes(referenceChunkB.toString()));
				}
			}
			transactions[2] = true;

			// Add task to file sink for further asynchronous processing.
			if ( isDebugEnabled )
				l.debug("Entering inside fileSink.addTask");
			fileSink.addTask(accountId, bucketName, fileId, revision, projectId, baseRevision);
		}
		catch( Exception e )
		{
			l.fatal("File Updation Failed: Account Id: " +accountId +
					", File Id: " +fileId +", Revision: " +revision, e);
			l.fatal("Attempting to rollback the transaction.");
			for ( int i = 0 ; i < 4; i++ )
			{
				// If transaction has failed
				if ( !transactions[i] ){
					try
					{
						// Rollback previous transactions ( Delete the passed transactions. Generate alert for Nagios. )
						if ( i > 0)
						{
							// Delete from parrot hash
							deleteFileFromFileParrotHash(hbaseOperation, accountId, magicHeader, startByte, 
									endByte, fileLength, fileId, revision);
						}

						if ( i > 1)
						{
							// Delete from filechunks
							projectId = jobject.getAsJsonPrimitive(PROJECT_ID).getAsInt();
							deletFileFromFileChunks(hbaseOperation, accountId, projectId, fileId, revision);
						}
					}
					catch( Exception ex)
					{
						l.fatal("Transaction Rollback Failed: Account Id: " +accountId +", File Id: " +fileId +", Revision: " +revision);
						throw new Exception("Something went wrong in Hbase. Transaction Rollback Failed");
					}
					break;
				}
			}

			throw new Exception("Something went wrong in Hbase. Transaction Rollback Failed");
		}

		long end = System.currentTimeMillis();
		l.info("TIME TAKEN FOR INCREMENT OPERATION IN FILE UPLOAD: " +(end - start));
	}

	/**
	 * Deletes file entry from filechunks table
	 * @param hbaseOperation
	 * @param accountId
	 * @param projectId
	 * @param fileId
	 * @param revision
	 * @throws Exception
	 */
	private final void deletFileFromFileChunks(final HbaseTableOperation hbaseOperation, 
			final int accountId, final int projectId, final long fileId, 
			final long revision ) throws Exception 
	{
		if ( isDebugEnabled )
			l.debug("Inside deleteFileFromFileChunks");
		StringBuilder sb = new StringBuilder();

		sb.append(accountId).append(UNDERSCORE)
		.append(projectId).append(UNDERSCORE)
		.append(fileId).append(UNDERSCORE)
		.append(revision);

		byte[] fileChunksKey = ByteUtil.toBytes(sb.toString());

		Delete delete = new Delete(fileChunksKey);
		delete.deleteFamily(HBASE_FAMILY_BYTES);
		hbaseOperation.deleteFromTable(FILECHUNKS_TABLE, delete);
		if ( isDebugEnabled )
			l.debug("Deleted one row with key: "+sb);
	}

	/**
	 * Deletes file entry from fileparrothash table
	 * @param hbaseOperation
	 * @param accountId
	 * @param magicHeader
	 * @param startByte
	 * @param endByte
	 * @param fileLength
	 * @param fileId
	 * @param revision
	 * @throws Exception
	 */
	private final void deleteFileFromFileParrotHash(final HbaseTableOperation hbaseOperation, 
			final int accountId, final String magicHeader, final short startByte, 
			final short endByte, final int fileLength, final long fileId, 
			final long revision) throws Exception 
	{
		if ( isDebugEnabled )
			l.debug("Inside deleteFileFromParrotHash");
		byte[] columnName = ByteUtil.toBytes(fileId);
		StringBuilder sb = new StringBuilder();

		sb.append(accountId).append(UNDERSCORE)
		.append(magicHeader).append(UNDERSCORE)
		.append(startByte).append(UNDERSCORE)
		.append(endByte).append(UNDERSCORE)
		.append(fileLength);

		byte[] parrotKey = ByteUtil.toBytes(sb.toString());
		byte[] value = hbaseOperation.readValueFromTable(FILEPARROTHASH_TABLE, HBASE_FAMILY_BYTES, 
				columnName, parrotKey);

		if(null != value)
		{
			String valueS = ByteUtil.toString(value, 0, value.length);

			int start = valueS.indexOf(Long.toString(revision));
			if( start > -1)
			{
				int end = valueS.indexOf(COMMA,start);
				valueS = ( -1 == end ) ? ( ( 0 == start ) ? "" : valueS.substring(0,start-1) )
						: ( valueS.substring(0,start) + valueS.substring(end +1) );

				hbaseOperation.writeToTable(FILEPARROTHASH_TABLE, HBASE_FAMILY_BYTES, columnName, 
						parrotKey, ByteUtil.toBytes(valueS));
			}
		}
	}

	/**
	 * Fetches container for provided account id
	 * @param responseJsonB
	 * @param hbaseOperation
	 * @param jobject
	 * @throws Exception
	 */
	private final void getContainer(StringBuilder responseJsonB, 
			final HbaseTableOperation hbaseOperation, final JsonObject jobject) throws Exception 
	{
		if ( isDebugEnabled )
			l.debug("Inside getContainer. Based on AccountId, it returns bucketKey");
		int accountId = jobject.getAsJsonPrimitive(ACCOUNT_ID).getAsInt();

		byte[] key = ByteUtil.toBytes(accountId);
		byte[] value = hbaseOperation.readValueFromTable(LINKS_TABLE, 
				HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, key);

		String result = (null == value) ? null : ByteUtil.toString(value, 0, value.length);
		if ( isDebugEnabled )
			l.debug("BucketName for Account "+accountId+" is: "+result);
		responseJsonB.append(CURLY_BRACE_START)
		.append("values").append(COLON).append(SQUARE_BRACKET_START)
		.append(CURLY_BRACE_START)
		.append(QUOTE).append(ACCOUNT_ID).append(QUOTE).append(COLON).append(accountId)
		.append(COMMA).append(QUOTE).append(S3_CONTAINER_PATH).append(QUOTE).append(COLON)
		.append(QUOTE).append(result).append(QUOTE)
		.append(CURLY_BRACE_END)
		.append(SQUARE_BRACKET_END)
		.append(CURLY_BRACE_END);
	}

	/**
	 * Fetches chunks json for provided file
	 * @param responseJsonB
	 * @param hbaseOperation
	 * @param jobject
	 * @throws Exception
	 */
	
	private synchronized final String getMultiId(StringBuilder responseJsonB, final HbaseTableOperation hbaseOperation,
			final JsonObject jobject) throws Exception {
		responseJsonB.append(CURLY_BRACE_START).append(QUOTE).append("values").append(QUOTE).append(COLON)
		.append(SQUARE_BRACKET_START);

String fileId;
String revisionId;
String fileIdStr = "";
String revisionIdStr = "";

if (isDebugEnabled)
	l.debug("Inside getMultiId");
// get multiId field from Rest API where request provided be like
// jsondata={"action":"GET_MULTI_IDS",multiId:10} where multiId denotes
// the no of fileid_revisionid combos to be fetched.
int multiid = jobject.getAsJsonPrimitive(MULTI_ID).getAsInt();
if (isDebugEnabled)
	l.debug("Inside getMultiId for multiid: " + multiid);
// loop runs as per requirement of no. of multiIds required to be
// fetched
try {
	byte[] fileIdBytes = hbaseOperation.readValueFromTable(FILEID_TABLE, HBASE_FAMILY_BYTES,
			COL_DEFAULT_BYTES, ROW_DEFAULT_BYTES);
	fileIdStr = ByteUtil.toString(fileIdBytes, 0, fileIdBytes.length);
	byte[] revisionIdBytes = hbaseOperation.readValueFromTable(REVISIONID_TABLE, HBASE_FAMILY_BYTES,
			COL_DEFAULT_BYTES, ROW_DEFAULT_BYTES);
	revisionIdStr = ByteUtil.toString(revisionIdBytes, 0, revisionIdBytes.length);
	l.info("FILEID table is not empty.current value in FILEID Table : " + fileIdStr);
	l.info("Length of Value in FILEID Table : " + fileIdStr.length());
	l.info("REVISIONID table is not empty.current value in REVISIONID Table : " + revisionIdStr);
	l.info("Length of Value in REVISIONID Table : " + revisionIdStr.length());
} catch (Exception e) {
	fileIdStr = "";
	revisionIdStr = "";
}
// check if tables are empty or not. if empty it will create a
// default row
if (fileIdStr == null || fileIdStr == "" || revisionIdStr == null || revisionIdStr == "") {
	try {

		l.info("FileId is either null or empty. so initializing FileId and RevisionId");
		byte[] ROW_DEFAULT_BYTES = ArcConfig.ROW_DEFAULT_BYTES;
		byte[] COL_DEFAULT_BYTES = ArcConfig.COL_DEFAULT_BYTES;
		byte[] HBASE_FAMILY_BYTES = ArcConfig.HBASE_FAMILY_BYTES;
		String FILEID_TABLE = ArcConfig.FILEID_TABLE;
		String REVISIONID_TABLE = ArcConfig.REVISIONID_TABLE;

		HbaseTableOperation hto = new HbaseTableOperation();
		l.info("writing to FILEID table for the first time");
		hto.writeToTable(FILEID_TABLE, HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, ROW_DEFAULT_BYTES,
				ByteUtil.toBytes(ArcConfig.FILE_ID_START));

		l.info("writing to REVISIONID table for the first time");
		hto.writeToTable(REVISIONID_TABLE, HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, ROW_DEFAULT_BYTES,
				ByteUtil.toBytes(ArcConfig.REVISION_ID_START));

	} catch (Exception e) {

		throw e;
	}

}
for (int i = 0; i < multiid; i++) {
	

	IS_FILEID_TABLE_INITIALIZED = true;
	IS_REVISIONID_TABLE_INITIALIZED = true;
	long fileidtemp;
	long revisionidtemp;

	// increment the fileid/revisionid amount by 1 while fetching
	long fileids = hbaseOperation.getFileIds(FILEID_INCREMENT_AMOUNT);
	long revisionids = hbaseOperation.getRevisionIds(REVISIONID_INCREMENT_AMOUNT);
	l.info("Current value of fileId in FILEID table: " + fileids);
	l.info("Current value of revisionId in REVISIONID table: " + fileids);
	fileidtemp = fileids;
	revisionidtemp = revisionids;

	StringBuilder sb = new StringBuilder(32);
	// append the incremented value to the fileidprefix(77).Like if
	// first fileid be 7721 next fileid is 7722,next being 7723 and so
	// on
	if (TEST_MODE) {
		sb.append(FILE_ID_PREFIX).append(fileidtemp);
	} else {
		sb.append(FILE_ID_PREFIX).append(fileidtemp);
	}
	fileId = sb.toString();
	l.debug("fileid: " + fileId);
	// append the fileid to the response json
	
	responseJsonB.append(CURLY_BRACE_START).append(QUOTE).append(FILE_ID).append(UNDERSCORE).append(REVISION_ID).append(QUOTE).append(COLON)
			.append(QUOTE).append(fileId).append(UNDERSCORE); 
	
	StringBuilder sb1 = new StringBuilder(32);
	// append the incremented value to the revisionidprefix(77).Like if
	// first revisionid be 7721 next revisionid is 7722,next being 7723
	// and so on
	if (TEST_MODE) {
		sb1.append(REVISION_ID_PREFIX).append(revisionidtemp);
	} else {
		sb1.append(FILE_ID_PREFIX).append(revisionidtemp);
	}

	revisionId = sb1.toString();
	if (i != multiid - 1) {
		l.debug("revisionid: " + revisionId);
		// append the revisionId to the response json
		responseJsonB.append(revisionId).append(QUOTE).append(CURLY_BRACE_END).append(COMMA);
		
	} else {
		l.debug("revisionid: " + revisionId);
		responseJsonB.append(revisionId).append(QUOTE).append(CURLY_BRACE_END);
	}

}
responseJsonB.append(SQUARE_BRACKET_END).append(CURLY_BRACE_END);
return responseJsonB.toString();

}
	
	private final void getFileChunks(StringBuilder responseJsonB, 
			final HbaseTableOperation hbaseOperation, final JsonObject jobject) throws Exception 
	{
		if ( isDebugEnabled )
			l.debug("Inside getFileChunks");
		int accountId = jobject.getAsJsonPrimitive(ACCOUNT_ID).getAsInt();
		int projectId = jobject.getAsJsonPrimitive(PROJECT_ID).getAsInt();
		long fileId = jobject.getAsJsonPrimitive(FILE_ID).getAsLong();
		long revision = jobject.getAsJsonPrimitive(REVISION).getAsLong();

		StringBuilder sb = new StringBuilder();

		sb.append(accountId).append(UNDERSCORE)
		.append(projectId).append(UNDERSCORE)
		.append(fileId).append(UNDERSCORE)
		.append(revision);

		byte[] fileChunksJSON = hbaseOperation.readValueFromTable(FILECHUNKS_TABLE, 
				HBASE_FAMILY_BYTES, COL_CHUNKS_JSON_BYTES, ByteUtil.toBytes(sb.toString()));

		String fileChunks = (null == fileChunksJSON) ? null 
				: ByteUtil.toString(fileChunksJSON, 0, fileChunksJSON.length);

		if ( null == fileChunks )
		{

			responseJsonB.append(CURLY_BRACE_START)
			.append("values").append(COLON).append(SQUARE_BRACKET_START)
			.append(SQUARE_BRACKET_END)
			.append(CURLY_BRACE_END);
		}
		else
		{

			String rootFolder = accountId+"/"+"chunks";
			String resultM = fileChunks.replaceAll("\"", "\\\\\"");
			responseJsonB.append(CURLY_BRACE_START)
			.append("values").append(COLON).append(SQUARE_BRACKET_START)
			.append(CURLY_BRACE_START)
			.append(QUOTE).append(ACCOUNT_ID).append(QUOTE).append(COLON).append(accountId).append(COMMA)
			.append(QUOTE).append("rootFolder").append(QUOTE).append(COLON).append(QUOTE).append(rootFolder).append(QUOTE).append(COMMA)
			.append(QUOTE).append(PROJECT_ID).append(QUOTE).append(COLON).append(projectId).append(COMMA)
			.append(QUOTE).append(FILE_ID).append(QUOTE).append(COLON).append(fileId).append(COMMA)
			.append(QUOTE).append(REVISION).append(QUOTE).append(COLON).append(revision).append(COMMA)
			.append(QUOTE).append(CHUNKS_JSON).append(QUOTE).append(COLON)
			.append(QUOTE).append(resultM).append(QUOTE)
			//					.append(QUOTE).append(fileChunks).append(QUOTE)
			.append(CURLY_BRACE_END)
			.append(SQUARE_BRACKET_END)
			.append(CURLY_BRACE_END);
		}
		
		
	}

	/**
	 * Fetche duplicate chunks from database
	 * @param responseJsonB
	 * @param hbaseOperation
	 * @param jobject
	 * @throws Exception
	 */
	private final void findMatchingChunks(StringBuilder responseJsonB, 
			final HbaseTableOperation hbaseOperation, final JsonObject jobject) throws Exception 
	{

		responseJsonB.append(CURLY_BRACE_START)
		.append("values").append(COLON).append(SQUARE_BRACKET_START);

		JsonArray chunks = jobject.getAsJsonArray("chunks");

		boolean isFirst = true;

		StringBuilder sb = new StringBuilder(64);
		for(int i = 0; i < chunks.size(); i++)
		{
			JsonObject jObjectArray = chunks.get(i).getAsJsonObject();
			int accountId = jObjectArray.getAsJsonPrimitive(ACCOUNT_ID).getAsInt();
			String chunkHeader = jObjectArray.getAsJsonPrimitive(CHUNK_HEADER).getAsString();
			String chunkHash = jObjectArray.getAsJsonPrimitive(CHUNK_HASH).getAsString();
			sb.setLength(0);

			sb.append(accountId).append(UNDERSCORE)
			.append(chunkHeader).append(UNDERSCORE)
			.append(chunkHash);

			byte[] chunksKey = ByteUtil.toBytes(sb.toString());
			boolean result = HReader.exists(CHUNKS_TABLE, chunksKey);

			if(result)
			{
				if(isFirst) isFirst = false;
				else responseJsonB.append(COMMA);

				responseJsonB.append(CURLY_BRACE_START)
				.append(QUOTE).append(ACCOUNT_ID).append(QUOTE).append(COLON)
				.append(accountId).append(COMMA)
				.append(QUOTE).append(CHUNK_HEADER).append(QUOTE).append(COLON)
				.append(QUOTE).append(chunkHeader).append(QUOTE).append(COMMA)
				.append(QUOTE).append(CHUNK_HASH).append(QUOTE).append(COLON)
				.append(QUOTE).append(chunkHash).append(QUOTE)
				.append(CURLY_BRACE_END);
			}
		}
		responseJsonB.append(SQUARE_BRACKET_END)
		.append(CURLY_BRACE_END);

		if ( isDebugEnabled ){
			l.debug("RestAPIServlet::findMatchingChunks - Response JSON: " +responseJsonB.toString());
		}

	}

	/**
	 * Fetches duplicate file from database
	 * @param responseJsonB
	 * @param hbaseOperation
	 * @param jobject
	 * @throws Exception
	 */
	private final void findDuplicateFile(StringBuilder responseJsonB, 
			final HbaseTableOperation hbaseOperation, final JsonObject jobject) throws Exception 
	{
		if ( isDebugEnabled )
			l.debug("Inside findDuplicateFile");
		int accountId = jobject.getAsJsonPrimitive(ACCOUNT_ID).getAsInt();
		int projectId = jobject.getAsJsonPrimitive(PROJECT_ID).getAsInt();
		long fileId = jobject.getAsJsonPrimitive(FILE_ID).getAsLong();
		long revision = jobject.getAsJsonPrimitive(REVISION).getAsLong();
		String chunksJSON = jobject.getAsJsonPrimitive(CHUNKS_JSON).getAsString();

		StringBuilder sb = new StringBuilder(64);

		sb.append(accountId).append(UNDERSCORE)
		.append(projectId).append(UNDERSCORE)
		.append(fileId).append(UNDERSCORE)
		.append(revision);

		byte[] fileChunksKey = ByteUtil.toBytes(sb.toString());

		byte[] fileChunksJSONB = hbaseOperation.readValueFromTable(FILECHUNKS_TABLE, 
				HBASE_FAMILY_BYTES, COL_CHUNKS_JSON_BYTES, fileChunksKey);

		boolean isMatching = false;
		if(null != fileChunksJSONB)
			isMatching = ByteUtil.compareBytes(fileChunksJSONB, ByteUtil.toBytes(chunksJSON));
		if ( isDebugEnabled )
			l.debug("Is matching found?: "+isMatching);
		String fileChunksJson = (isMatching) ? 
				ByteUtil.toString(fileChunksJSONB, 0, fileChunksJSONB.length) : null;

				if ( null == fileChunksJson )
				{
					responseJsonB.append(CURLY_BRACE_START)
					.append("values").append(COLON).append(SQUARE_BRACKET_START)
					.append(SQUARE_BRACKET_END)
					.append(CURLY_BRACE_END);
				}
				else
				{
					String resultM = fileChunksJson.replaceAll("\"", "\\\\\"");
					responseJsonB.append(CURLY_BRACE_START)
					.append("values").append(COLON).append(SQUARE_BRACKET_START)
					.append(CURLY_BRACE_START)
					.append(QUOTE).append(ACCOUNT_ID).append(QUOTE).append(COLON).append(accountId).append(COMMA)
					.append(QUOTE).append(PROJECT_ID).append(QUOTE).append(COLON).append(projectId).append(COMMA)
					.append(QUOTE).append(FILE_ID).append(QUOTE).append(COLON).append(fileId).append(COMMA)
					.append(QUOTE).append(REVISION).append(QUOTE).append(COLON).append(revision).append(COMMA)
					.append(QUOTE).append(CHUNKS_JSON).append(QUOTE).append(COLON)
					.append(QUOTE).append(resultM).append(QUOTE)
					//					.append(QUOTE).append(fileChunksJson).append(QUOTE)
					.append(CURLY_BRACE_END)
					.append(SQUARE_BRACKET_END)
					.append(CURLY_BRACE_END);
				}
	}

	/**
	 * Fetches chunkoffset json for respective file
	 * @param responseJsonB
	 * @param hbaseOperation
	 * @param jobject
	 * @throws Exception
	 */
	private final void findFileChunkOffsets(StringBuilder responseJsonB, final HbaseTableOperation hbaseOperation, 
			final JsonObject jobject) throws Exception 
	{
		if ( isDebugEnabled )
			l.debug("Inside findFileChunkOffsets");
		int accountId = jobject.getAsJsonPrimitive(ACCOUNT_ID).getAsInt();
		int projectId = jobject.getAsJsonPrimitive(PROJECT_ID).getAsInt();
		long fileId = jobject.getAsJsonPrimitive(FILE_ID).getAsLong();
		long revision = jobject.getAsJsonPrimitive(REVISION).getAsLong();

		StringBuilder sb = new StringBuilder(64);

		sb.append(accountId).append(UNDERSCORE)
		.append(projectId).append(UNDERSCORE)
		.append(fileId).append(UNDERSCORE)
		.append(revision);





		byte[] value;

		value = hbaseOperation.readValueFromTable(FILECHUNKS_TABLE, HBASE_FAMILY_BYTES, 
				COL_CHUNK_OFFSET_JSON_BYTES, ByteUtil.toBytes(sb.toString()));


		if ( null == value )
		{
			responseJsonB.append(CURLY_BRACE_START)
			.append("values").append(COLON).append(SQUARE_BRACKET_START)
			.append(SQUARE_BRACKET_END)
			.append(CURLY_BRACE_END);

		}
		else
		{
			String chunkOffset = ByteUtil.toString(value, 0, value.length);
			responseJsonB.append(CURLY_BRACE_START)
			.append("values").append(COLON).append(SQUARE_BRACKET_START)
			.append(CURLY_BRACE_START)
			.append(QUOTE).append(ACCOUNT_ID).append(QUOTE).append(COLON).append(accountId).append(COMMA)
			.append(QUOTE).append(PROJECT_ID).append(QUOTE).append(COLON).append(projectId).append(COMMA)
			.append(QUOTE).append(FILE_ID).append(QUOTE).append(COLON).append(fileId).append(COMMA)
			.append(QUOTE).append(REVISION).append(QUOTE).append(COLON).append(revision).append(COMMA)
			.append(QUOTE).append(CHUNK_OFFSET_JSON).append(QUOTE).append(COLON)
			.append(QUOTE).append(chunkOffset).append(QUOTE)
			.append(CURLY_BRACE_END)
			.append(SQUARE_BRACKET_END)
			.append(CURLY_BRACE_END);
		}

	}

	/**
	 * Fetches possible duplicate files 
	 * @param responseJsonB
	 * @param hbaseOperation
	 * @param jobject
	 * @throws Exception
	 */
	private final void findPossibleFileDuplicate(StringBuilder responseJsonB, final HbaseTableOperation hbaseOperation, 
			final JsonObject jobject) throws Exception 
	{
		if ( isDebugEnabled )
			l.debug("inside findPossibleFileDuplicate");
		int accountId = jobject.getAsJsonPrimitive(ACCOUNT_ID).getAsInt();
		String magicHeader = jobject.getAsJsonPrimitive(MAGIC_HEADER).getAsString();
		short startByte = jobject.getAsJsonPrimitive(START_BYTE).getAsShort();
		short endByte = jobject.getAsJsonPrimitive(END_BYTE).getAsShort();
		int fileLength = jobject.getAsJsonPrimitive(FILE_LENGTH).getAsInt();

		StringBuilder parrotKeyB = new StringBuilder();
		parrotKeyB.append(accountId).append(UNDERSCORE)
		.append(magicHeader).append(UNDERSCORE)
		.append(startByte).append(UNDERSCORE)
		.append(endByte).append(UNDERSCORE)
		.append(fileLength);

		byte[] parrotKey = ByteUtil.toBytes(parrotKeyB.toString());

		responseJsonB.append(CURLY_BRACE_START)
		.append(QUOTE).append("values").append(QUOTE).append(COLON)
		.append(SQUARE_BRACKET_START);


		Result result = null;
		try
		{

			result = hbaseOperation.readResultFromTable(FILEPARROTHASH_TABLE, parrotKey);
			List<Cell> valueCells = result.listCells();
			if(null != valueCells)
			{
				boolean isFirst = true;
				for ( Cell cell : valueCells )
				{
					byte[] fileIdB = new byte[cell.getQualifierLength()];
					byte[] revisionB = new byte[cell.getValueLength()];

					System.arraycopy(cell.getQualifierArray(), cell.getQualifierOffset(), fileIdB, 0, cell.getQualifierLength());
					System.arraycopy(cell.getValueArray(), cell.getValueOffset(), revisionB, 0, cell.getValueLength());

					String revisions = ByteUtil.toString(revisionB, 0, revisionB.length);
					long fileId = ByteUtil.toLong(fileIdB, 0);

					List<String> revisionL = new ArrayList<String>();
					LineReaderUtil.fastSplit(revisionL, revisions, COMMA);

					for(String aRevision : revisionL)
					{
						if ( isFirst ) isFirst = false;
						else responseJsonB.append(COMMA);

						responseJsonB.append(CURLY_BRACE_START)
						.append(QUOTE).append(FILE_ID).append(QUOTE).append(COLON)
						.append(fileId).append(COMMA)
						.append(QUOTE).append(REVISION).append(QUOTE).append(COLON)
						.append(aRevision)
						.append(CURLY_BRACE_END);
					}
				}
			}


			responseJsonB.append(SQUARE_BRACKET_END).append(CURLY_BRACE_END);


		} 
		catch (Exception e)
		{
			throw e;

		}
	}

	/**
	 * Sets container for account id
	 * @param responseJsonB
	 * @param hbaseOperation
	 * @param jobject
	 * @throws Exception
	 */
	private final String setContainerForAccount(StringBuilder responseJsonB, final HbaseTableOperation hbaseOperation, 
			final JsonObject jobject) throws Exception 
	{
		int accountId = jobject.getAsJsonPrimitive(ACCOUNT_ID).getAsInt();
		
		String folderName = jobject.getAsJsonPrimitive(FOLDER_NAME).getAsString();
		
//		String s3ContainerPathReq = jobject.getAsJsonPrimitive(S3_CONTAINER_PATH).getAsString();
		String s3ContainerPath = BucketInfoCache.getBucketInfo(accountId).getBucketName();
		
//		if( !s3ContainerPathReq.equals(s3ContainerPath) ){
//			BucketInfoCache.refreshBucketInfo(Integer.toString(accountId));
//			s3ContainerPath = BucketInfoCache.getBucketInfo(accountId).getBucketName();
//		}
		
		int folderNameInt = Integer.parseInt(folderName);
		l.info("FolderName or ProjectId is: "+folderName);
		String FOLDER_SUFFIX = "/";
		String bucketName = s3ContainerPath.trim();
		String accountIdStr = Integer.toString(accountId);

		String accessKey = BucketInfoCache.getBucketInfo(accountId).getAccessKey();
		String secretKey = BucketInfoCache.getBucketInfo(accountId).getSecreteKey();
		AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
		AmazonS3 s3client = new AmazonS3Client(credentials);
		

		BucketCrossOriginConfiguration configuration = new BucketCrossOriginConfiguration();

		final HbaseTableOperation hto = new HbaseTableOperation();



		responseJsonB.append(CURLY_BRACE_START)
		.append(QUOTE).append("values").append(QUOTE).append(COLON)
		.append(SQUARE_BRACKET_START);


		//Get s3 bucket for accountId
		String s3BucketName = "";
		try
		{
			byte[] s3BucketNameB = hto.readValueFromTable(LINKS_TABLE, HBASE_FAMILY_BYTES, 
					COL_DEFAULT_BYTES, ByteUtil.toBytes(accountId));

			s3BucketName = ByteUtil.toString(s3BucketNameB, 0, s3BucketNameB.length);
			if(s3BucketName == "" || s3BucketName == null)
			{
				l.info(accountId + " is not registered. Registering ... ");
			}
		}
		catch( Exception ex)
		{
			s3BucketName = "";
			l.info(accountId + " is not registered. Registering ... ");
		}

		l.info("NAME  of BUCKET corresponding to the ACCOUNT "+ accountId+ " is: "+s3BucketName);
		l.debug("You have requested for bucket: "+s3ContainerPath);
		long startTime = System.currentTimeMillis();
		s3BucketName = s3BucketName.trim();

		if(s3BucketName == null || s3BucketName =="")
		{

			try {
				hbaseOperation.writeToTable(LINKS_TABLE, HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, 
						ByteUtil.toBytes(accountId), ByteUtil.toBytes(s3ContainerPath));


			} catch (Exception e) {
				throw e; 
			}
			try
			{
				//s3client.createBucket(new CreateBucketRequest(bucketName));
				if(!(s3client.doesBucketExist(bucketName)))
				{
					Bucket buck=s3client.createBucket(bucketName);

					l.debug("New bucket: "+buck +" created successfully");

					List<CORSRule> rules = new ArrayList<CORSRule>();

					CORSRule rule2 = new CORSRule()
					.withId("CORSRule2")
					.withAllowedMethods(Arrays.asList(new CORSRule.AllowedMethods[] { 
							CORSRule.AllowedMethods.PUT,CORSRule.AllowedMethods.POST,CORSRule.AllowedMethods.GET}))
							.withAllowedOrigins(Arrays.asList(new String[] {"*"}))
							.withAllowedHeaders(Arrays.asList(new String[] {"*"}))
							.withMaxAgeSeconds(3000)
							.withExposedHeaders(Arrays.asList(new String[] {"ETag"}));

					configuration.setRules(Arrays.asList(new CORSRule[] {rule2}));

					// Add the configuration to the bucket. 
					s3client.setBucketCrossOriginConfiguration(bucketName, configuration);

					try{
						ObjectMetadata metadata = new ObjectMetadata();
						metadata.setContentLength(0);

						InputStream emptyContent = new ByteArrayInputStream(new byte[0]);

						// Making folder in Bucket(accountId/projectId)
						PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, accountIdStr + FOLDER_SUFFIX+folderName+FOLDER_SUFFIX ,emptyContent, metadata);

						s3client.putObject(putObjectRequest);

						ObjectMetadata metadata2 = new ObjectMetadata();
						metadata2.setContentLength(0);

						InputStream emptyContent2 = new ByteArrayInputStream(new byte[0]);

						// Making folder in Bucket(accountId/chunks)
						PutObjectRequest putObjectRequest2 = new PutObjectRequest(bucketName, accountIdStr + FOLDER_SUFFIX+CHUNK_FOLDER+FOLDER_SUFFIX ,emptyContent2, metadata2);

						s3client.putObject(putObjectRequest2);
					}catch(Exception e)
					{
						throw e;
					}



				}
				else{

					l.debug("Bucket with name: "+bucketName+" is already there in S3");
					l.debug("Trying to create a folder for project");

					if(!doesObjectExists(s3ContainerPath, accountIdStr + FOLDER_SUFFIX,accountId))
					{
						try{
							ObjectMetadata metadata = new ObjectMetadata();
							metadata.setContentLength(0);

							InputStream emptyContent = new ByteArrayInputStream(new byte[0]);

							// Making folder in Bucket(accountId/projectId)
							PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, accountIdStr + FOLDER_SUFFIX+folderName+FOLDER_SUFFIX ,emptyContent, metadata);

							s3client.putObject(putObjectRequest);

							ObjectMetadata metadata2 = new ObjectMetadata();
							metadata2.setContentLength(0);

							InputStream emptyContent2 = new ByteArrayInputStream(new byte[0]);

							// Making folder in Bucket(accountId/chunks)
							PutObjectRequest putObjectRequest2 = new PutObjectRequest(bucketName, accountIdStr + FOLDER_SUFFIX+CHUNK_FOLDER+FOLDER_SUFFIX ,emptyContent2, metadata2);

							s3client.putObject(putObjectRequest2);
						}catch(Exception e)
						{
							throw e;
						}

					}
					else if(!doesObjectExists(s3ContainerPath, accountIdStr + FOLDER_SUFFIX+CHUNK_FOLDER+FOLDER_SUFFIX,accountId))
					{
						ObjectMetadata metadata = new ObjectMetadata();
						metadata.setContentLength(0);

						InputStream emptyContent = new ByteArrayInputStream(new byte[0]);

						// Making folder in Bucket(accountId/projectId)
						PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, accountIdStr + FOLDER_SUFFIX+CHUNK_FOLDER+FOLDER_SUFFIX ,emptyContent, metadata);

						s3client.putObject(putObjectRequest);
					}
					else if(!doesObjectExists(s3ContainerPath, accountIdStr + FOLDER_SUFFIX+folderName+FOLDER_SUFFIX,accountId))
					{
						ObjectMetadata metadata = new ObjectMetadata();
						metadata.setContentLength(0);

						InputStream emptyContent = new ByteArrayInputStream(new byte[0]);

						// Making folder in Bucket(accountId/projectId)
						PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, accountIdStr + FOLDER_SUFFIX+folderName+FOLDER_SUFFIX ,emptyContent, metadata);

						s3client.putObject(putObjectRequest);
					}

				}

			} catch (AmazonClientException ase) {
				if ( isDebugEnabled )
					l.debug("BucketName: "+bucketName +" ERROR: Bucket creation was not successful");
				hbaseOperation.deleteFromTable(LINKS_TABLE, HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, ByteUtil.toBytes(accountId));
				
				BucketInfoCache.refreshBucketInfo(Integer.toString(accountId));
				//System.out.println("Caught an AmazonServiceException, which " +"means your request made it " +
				//		"to Amazon S3, but was rejected with an error response" + " for some reason.");
				l.error(ase.getMessage());
				//System.out.println("Error Message:    " + ase.getMessage());
				//System.out.println("HTTP Status Code: " + ((AmazonServiceException) ase).getStatusCode());
				l.error(ase.getMessage());
				//System.out.println("AWS Error Code:   " + ((AmazonServiceException) ase).getErrorCode());
				l.error(((AmazonServiceException) ase).getErrorCode());
				//System.out.println("Error Type:       " + ((AmazonServiceException) ase).getErrorType());
				l.error(((AmazonServiceException) ase).getErrorType());
				//System.out.println("Request ID:       " + ((AmazonServiceException) ase).getRequestId());
				l.error(((AmazonServiceException) ase).getRequestId());



				throw ase;


			}


		}
		else
		{
			l.debug("Requested Bucket:"+bucketName+": Old Bucket:"+s3BucketName+":");
			if(s3BucketName != bucketName)
			{
				l.debug("Your Bucket is being changed");
				hbaseOperation.writeToTable(LINKS_TABLE, HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, 
						ByteUtil.toBytes(accountId), ByteUtil.toBytes(s3ContainerPath));

				BucketChange bucketchange = new BucketChange();
				bucketchange.getInstance().requestNewBucket(accountId, folderNameInt, s3BucketName, bucketName);

			}

			else{
				try
				{

					if(!doesObjectExists(s3ContainerPath, accountIdStr + FOLDER_SUFFIX,accountId))
					{
						try{
							ObjectMetadata metadata = new ObjectMetadata();
							metadata.setContentLength(0);

							InputStream emptyContent = new ByteArrayInputStream(new byte[0]);

							// Making folder in Bucket(accountId/projectId)
							PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, accountIdStr + FOLDER_SUFFIX+folderName+FOLDER_SUFFIX ,emptyContent, metadata);

							s3client.putObject(putObjectRequest);

							ObjectMetadata metadata2 = new ObjectMetadata();
							metadata2.setContentLength(0);

							InputStream emptyContent2 = new ByteArrayInputStream(new byte[0]);

							// Making folder in Bucket(accountId/chunks)
							PutObjectRequest putObjectRequest2 = new PutObjectRequest(bucketName, accountIdStr + FOLDER_SUFFIX+CHUNK_FOLDER+FOLDER_SUFFIX ,emptyContent2, metadata2);

							s3client.putObject(putObjectRequest2);
						}catch(Exception e)
						{
							throw e;
						}

					}
					else if(!doesObjectExists(s3ContainerPath, accountIdStr + FOLDER_SUFFIX+CHUNK_FOLDER+FOLDER_SUFFIX,accountId))
					{
						ObjectMetadata metadata = new ObjectMetadata();
						metadata.setContentLength(0);

						InputStream emptyContent = new ByteArrayInputStream(new byte[0]);

						// Making folder in Bucket(accountId/projectId)
						PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, accountIdStr + FOLDER_SUFFIX+CHUNK_FOLDER+FOLDER_SUFFIX ,emptyContent, metadata);

						s3client.putObject(putObjectRequest);
					}
					else if(!doesObjectExists(s3ContainerPath, accountIdStr + FOLDER_SUFFIX+folderName+FOLDER_SUFFIX,accountId))
					{
						ObjectMetadata metadata = new ObjectMetadata();
						metadata.setContentLength(0);

						InputStream emptyContent = new ByteArrayInputStream(new byte[0]);

						// Making folder in Bucket(accountId/projectId)
						PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, accountIdStr + FOLDER_SUFFIX+folderName+FOLDER_SUFFIX ,emptyContent, metadata);

						s3client.putObject(putObjectRequest);
					}
				} catch (AmazonClientException ase) {
					BucketInfoCache.refreshBucketInfo(Integer.toString(accountId));
					l.error("ERROR: Bucket is already there but it failed to write to database.");
					//System.out.println("Caught an AmazonServiceException, which " +"means your request made it " +
					//		"to Amazon S3, but was rejected with an error response" + " for some reason.");
					l.error(ase.getMessage());
					//System.out.println("Error Message:    " + ase.getMessage());
					//System.out.println("HTTP Status Code: " + ((AmazonServiceException) ase).getStatusCode());
					//System.out.println("AWS Error Code:   " + ((AmazonServiceException) ase).getErrorCode());
					//System.out.println("Error Type:       " + ((AmazonServiceException) ase).getErrorType());
					//System.out.println("Request ID:       " + ((AmazonServiceException) ase).getRequestId());


					throw ase;
				}
			}




		}

		long endTime = System.currentTimeMillis();


		l.info("Time take to create a bucket "+(endTime-startTime));

		// Get location.
		//String bucketLocation = s3client.getBucketLocation(new GetBucketLocationRequest(bucketName));
		//System.out.println("bucket location = " + bucketLocation);


		l.debug("final value"+responseJsonB.toString());
		return responseJsonB.toString();

	}

	public static boolean doesObjectExists(String bucketName, String key,int accountId) {
		try {

			String accessKey1 = BucketInfoCache.getBucketInfo(accountId).getAccessKey();
			String secretKey1 = BucketInfoCache.getBucketInfo(accountId).getSecreteKey();

			AWSCredentials credentials1 = new BasicAWSCredentials(accessKey1, secretKey1);
			AmazonS3 s3client = new AmazonS3Client(credentials1);
			s3client.getObjectMetadata(bucketName, key); 

		} catch(AmazonServiceException e) {
			return false;
		}
		return true;
	}
	
	public static void main(String[] args) throws Exception {
		HbaseTableOperation hbaseOperation = new HbaseTableOperation();
		StringBuilder sb = new StringBuilder();
		StringBuilder responseJsonB = new StringBuilder();
		
		int accountId = 6734567;
		int projectId = 23351;
		int fileId = 81197;
		int revision = 81197;
		
		sb.append(accountId).append(UNDERSCORE)
		.append(projectId).append(UNDERSCORE)
		.append(fileId).append(UNDERSCORE)
		.append(revision);
		byte[] fileChunksJSON = hbaseOperation.readValueFromTable(FILECHUNKS_TABLE, 
				HBASE_FAMILY_BYTES, COL_CHUNKS_JSON_BYTES, ByteUtil.toBytes(sb.toString()));

		String fileChunks = (null == fileChunksJSON) ? null 
				: ByteUtil.toString(fileChunksJSON, 0, fileChunksJSON.length);
		
		
		if ( null == fileChunks )
		{

			responseJsonB.append(CURLY_BRACE_START)
			.append("values").append(COLON).append(SQUARE_BRACKET_START)
			.append(SQUARE_BRACKET_END)
			.append(CURLY_BRACE_END);
		}
		else
		{

			String resultM = fileChunks.replaceAll("\"", "\\\\\"");
			responseJsonB.append(CURLY_BRACE_START)
			.append("values").append(COLON).append(SQUARE_BRACKET_START)
			.append(CURLY_BRACE_START)
			.append(QUOTE).append(ACCOUNT_ID).append(QUOTE).append(COLON).append(accountId).append(COMMA)
			.append(QUOTE).append(PROJECT_ID).append(QUOTE).append(COLON).append(projectId).append(COMMA)
			.append(QUOTE).append(FILE_ID).append(QUOTE).append(COLON).append(fileId).append(COMMA)
			.append(QUOTE).append(REVISION).append(QUOTE).append(COLON).append(revision).append(COMMA)
			.append(QUOTE).append(CHUNKS_JSON).append(QUOTE).append(COLON)
			.append(QUOTE).append(resultM).append(QUOTE)
			//					.append(QUOTE).append(fileChunks).append(QUOTE)
			.append(CURLY_BRACE_END)
			.append(SQUARE_BRACKET_END)
			.append(CURLY_BRACE_END);
		}
		
		//System.out.println(responseJsonB);
		
		
	}
}
