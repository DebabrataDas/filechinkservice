package com.arc.rest.rest;

import java.io.IOException;

import com.arc.umq.service.UMQServiceAdaptor;
import com.arc.util.conf.ArcConfig;
import com.arc.util.log.ArcLogger;

public class FileStitchSink {
	
	private static FileStitchSink instance = null;
	
	public static FileStitchSink getInstance() throws IOException 
	{
		if ( null != instance) return instance;
		synchronized (FileStitchSink.class) 
		{
			if ( null != instance ) return instance;
			instance = new FileStitchSink();
		}
		return instance;
	}
	
		
	private FileStitchSink() 
	{
	}
	
			
	/**
	 * Add task blocking queue
	 * @param accountId
	 * @param fileId
	 * @param revision
	 * @param projectId
	 * @param baseRevision
	 * @throws IOException
	 */
	public final void addTask(final int accountId, final String bucketName,final long fileId, final long revision, final int projectId, final long baseRevision) throws IOException 
	{
		ArcLogger.l.info("<<<< Inside addTask in FileStitchSink");
		String taskBytes=accountId+ArcConfig.KEY_SEPARATOR+bucketName+ArcConfig.KEY_SEPARATOR+fileId+ArcConfig.KEY_SEPARATOR+revision+ArcConfig.KEY_SEPARATOR+projectId+ArcConfig.KEY_SEPARATOR+baseRevision;
		
		if ( null == taskBytes ) return;
		ArcLogger.l.info("adding key to UMQ filestitch queue: "+ taskBytes);
		UMQServiceAdaptor.getInstance().publishMessage(taskBytes);
		ArcLogger.l.info("added task in UMQ successfully >>>>>");
			
	}
	
}








//OLD CODE

/*package com.arc.rest.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.arc.service.FileStitchService;
import com.arc.util.conf.ArcConfig;
import com.arc.util.log.ArcLogger;
import com.bizosys.hsearch.byteutils.SortedBytesArray;
import com.bizosys.hsearch.byteutils.Storable;

public class FileStitchSink  implements Runnable  
{
	static Logger l = Logger.getLogger(FileStitchSink.class);
	private static FileStitchSink instance = null;
	private static long BREATHE_TIME_MILLIS = ArcConfig.BREATHE_TIME_MILLIS;
	
	public static FileStitchSink getInstance() throws IOException 
	{
		if ( null != instance) return instance;
		synchronized (FileStitchSink.class) 
		{
			if ( null != instance ) return instance;
			BlockingQueue<byte[]> msgQueue = new LinkedBlockingQueue<byte[]>();
			instance = new FileStitchSink(msgQueue);
			Thread offlineThread = new Thread(instance);
			offlineThread.setDaemon(true);
			offlineThread.start();
		}
		return instance;
	}
	
	BlockingQueue<byte[]> blockingQueue = null; 
	
	private FileStitchSink() 
	{
	}
	
	private FileStitchSink ( BlockingQueue<byte[]> blockingQueue) throws IOException 
	{
		this.blockingQueue = blockingQueue;
	}
	
	*//**
	 * Add task blocking queue
	 * @param accountId
	 * @param fileId
	 * @param revision
	 * @param projectId
	 * @param baseRevision
	 * @throws IOException
	 *//*
	public final void addTask(final int accountId, final String bucketName, final long fileId, final long revision, 
			final int projectId, final long baseRevision) throws IOException 
	{
		l.debug("Inside addTask in FileStitchSink");
		l.debug("AccountId: "+accountId+" fileId: "+fileId+" projectId: "+projectId+" revision:"+revision);
		byte[] accountB = Storable.putInt(accountId);
		byte[] fileIdB = Storable.putLong(fileId);
		byte[] revisionB = Storable.putLong(revision);
		byte[] projectIdB = Storable.putInt(projectId);
		byte[] baseRevisionIdB = Storable.putLong(baseRevision);
		
		byte[] taskBytes = SortedBytesArray.getInstanceArr().toBytes(accountB, fileIdB, revisionB, projectIdB, baseRevisionIdB);
		if ( null == taskBytes ) return;
		l.debug("adding key to blocking queue");
		blockingQueue.add(taskBytes);
		l.debug("added task in blocking successfully");
	}
	
	*//**
	 * Returns blocking queue size
	 * @return
	 *//*
	public final int getQueueSize() 
	{
		if ( null == blockingQueue) return 0;
		else 
			{
				l.debug("blocking Queue size is : "+blockingQueue.size());
				return blockingQueue.size();
			}
	}
	
	List<byte[]> dataL = new ArrayList<byte[]>(1024);
	public void run() 
	{
		long startTime = System.currentTimeMillis();
		while (true) 
		{
			try 
			{
				int size = this.blockingQueue.size();
				
				*//**
				 * Process 1024 chunk at one time.
				 *//*
				if ( size > 1024 ) size = 1024;
				
				*//**
				 * Block if none is available. 
				 * Retrieves and removes the head of this queue, waiting if necessary 
				 * until an element becomes available.
				 *//*
				if ( size < 1 ) size = 1; 
				l.debug("Taking data from blocking queue and adding them in a list dataL");
				for ( int i=0; i<size; i++) 
				{
					dataL.add(this.blockingQueue.take());
				}
		        
				sink(dataL);
				dataL.clear();
			} 
			catch (Exception ex) 
			{
				l.error("Can't stitch the chunks. there is some problem in sink method");
				l.fatal("BatchProcessor > ",  ex);
			}
			finally
			{
				long endTime = System.currentTimeMillis();
				long sleepFor = BREATHE_TIME_MILLIS - (endTime - startTime);
				
				if ( sleepFor > 0 )
				{
					try 
					{
						Thread.sleep(sleepFor);
						startTime = endTime;
					} 
					catch (InterruptedException e) 
					{
						l.error("Some error while sleeping the thread in FileStitchSink.java:"+e.toString() );
						e.printStackTrace();
						Thread.currentThread().interrupt();
					}
				}
			}
		}
	}
	
	private final void sink(final List<byte[]> dataL) throws Exception 
	{
		SortedBytesArray sba = SortedBytesArray.getInstanceArr();
		
		for ( byte[] data : dataL )
		{
			sba.parse(data);
			int accountId = Storable.getInt(sba.getValueAtReference(0).offset, data);
		
			long fileId = Storable.getLong(sba.getValueAtReference(1).offset, data);
			
			long revision = Storable.getLong(sba.getValueAtReference(2).offset, data);
			
			int projectId = Storable.getInt(sba.getValueAtReference(3).offset, data);
			
			long baseRevision = Storable.getLong(sba.getValueAtReference(4).offset, data);
			
			try{
			FileStitchService.process(accountId, fileId, revision, projectId, baseRevision);
			}
			catch(Exception ex){
				l.error("Problem occur while stitching the chunks in FileStitchService.process method");
			}
		}
	}
}*/