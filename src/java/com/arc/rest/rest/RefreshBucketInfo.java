package com.arc.rest.rest;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.arc.hbase.BucketInfoCache;
import com.arc.util.conf.ArcConfig;
import com.bizosys.hsearch.byteutils.ByteUtil;


public final class RefreshBucketInfo extends HttpServlet
{
    static Logger l = Logger.getLogger(RefreshBucketInfo.class);
	public static final String GET_PARAMETER_ACCOUNT_ID = ArcConfig.ACCOUNT_ID;
	
	@Override
	public void init() throws ServletException
	{
		super.init();
	}
	
	public final void doGet(final HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		String accountId = request.getParameter(GET_PARAMETER_ACCOUNT_ID);
		OutputStream out = response.getOutputStream();
		try
		{
			process(out, accountId);
		}
		catch(Exception e)
		{
			l.error(e.getMessage());
			response.sendError(500, e.getMessage());
		}
		
	}
	
	public final void doPost(final HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		
		String accountId = request.getParameter(GET_PARAMETER_ACCOUNT_ID);

		OutputStream out = response.getOutputStream();
		try
		{
			process(out, accountId);
		}
		catch(Exception e)
		{
			l.error(e.getMessage());
			response.sendError(500, e.getMessage());
		}
	}
	
	private final void process( OutputStream out, final String accountId) throws IOException
	{
		boolean isSuccess = BucketInfoCache.refreshBucketInfo(accountId);
		if(isSuccess)out.write(ByteUtil.toBytes("success"));
		else out.write(ByteUtil.toBytes("failed"));
		
	}
	
	
	
}
