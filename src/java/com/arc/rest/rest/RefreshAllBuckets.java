package com.arc.rest.rest;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.arc.hbase.BucketInfoCache;
import com.bizosys.hsearch.byteutils.ByteUtil;



public final class RefreshAllBuckets extends HttpServlet
{
    static Logger l = Logger.getLogger(RefreshAllBuckets.class);
	
	@Override
	public void init() throws ServletException
	{
		super.init();
	}
	
	public final void doGet(final HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		OutputStream out = response.getOutputStream();
		try
		{
			process(out);
		}
		catch(Exception e)
		{
			response.sendError(500, e.getMessage());
		}
		
	}
	
	public final void doPost(final HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		
		OutputStream out = response.getOutputStream();
		try
		{
			process(out);
		}
		catch(Exception e)
		{
			response.sendError(500, e.getMessage());
		}
	}
	
	private final void process( OutputStream out) throws IOException
	{
		boolean isSuccess = BucketInfoCache.refreshAllBuckets();
		if(isSuccess)out.write(ByteUtil.toBytes("success"));
		else out.write(ByteUtil.toBytes("failed"));
	}
	
	
	
}
