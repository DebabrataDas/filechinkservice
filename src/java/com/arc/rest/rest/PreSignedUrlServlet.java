package com.arc.rest.rest;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.arc.hbase.BucketInfoCache;
import com.arc.hbase.HbaseTableOperation;
import com.arc.filechunk.s3utils.GeneratePresignedUrlAndUploadObject;
import com.arc.util.conf.ArcConfig;
import com.arc.util.log.ArcLogger;
import com.bizosys.hsearch.byteutils.ByteUtil;

public final class PreSignedUrlServlet extends HttpServlet
{
    static Logger l = Logger.getLogger(PreSignedUrlServlet.class);
	private static final long serialVersionUID = 1L;
	
	private static final String DOWNLOAD_URL_TYPE = ArcConfig.DOWNLOAD_URL_TYPE;
	private static final String UPLOAD_URL_TYPE = ArcConfig.UPLOAD_URL_TYPE;
	private static final String ACCOUNT_ID = ArcConfig.ACCOUNT_ID;
	private static final String URL_TYPE = ArcConfig.URL_TYPE;
	private static final String FILE_NAME = ArcConfig.FILE_NAME;
	private static final byte[] COL_DEFAULT_BYTES = ArcConfig.COL_DEFAULT_BYTES;
	private static final byte[] HBASE_FAMILY_BYTES = ArcConfig.HBASE_FAMILY_BYTES;
	private final static String LINKS_TABLE = ArcConfig.LINKS_TABLE;
	private final static String FOLDER_NAME = ArcConfig.FOLDER_NAME;
	private final static String LOGICAL_FILE_NAME = ArcConfig.LOGICAL_FILE_NAME;
	private final static String BUCKET_NAME = "bucketName";
	
	
	
	@Override
	public void init() throws ServletException
	{
		super.init();
	}
	
	public final void doGet(final HttpServletRequest request, HttpServletResponse response)
	{
		
	}
	
	public final void doPost(final HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		//String folderName = request.getParameter(FOLDER_NAME);
		String logicalFileName = request.getParameter(LOGICAL_FILE_NAME);
		String fileName = request.getParameter(FILE_NAME);
		String urlType = request.getParameter(URL_TYPE);
		String accountId = request.getParameter(ACCOUNT_ID);
		String projectId = request.getParameter("projectId");
		
		
		
		String bucketNameReq = request.getParameter(BUCKET_NAME);
		
		String bucketName = BucketInfoCache.getBucketInfo(accountId).getBucketName();
		
		if(!bucketName.equals(bucketNameReq)){
			BucketInfoCache.refreshBucketInfo(accountId);
			bucketName = BucketInfoCache.getBucketInfo(accountId).getBucketName();
		}
		
		
		OutputStream out = response.getOutputStream();
		try
		{
			//process(out, fileName, urlType, accountId, folderName, logicalFileName, bucketName);
			process(out, fileName, urlType, logicalFileName, bucketName, accountId, projectId);
		}
		catch(Exception e)
		{
			response.sendError(500, e.getMessage());
		}
	}
	
	private final void process( OutputStream out, final String fileName, final String urlType, 
									final String logicalFileName, final String bucketNameReq, final String accountId, final String projectId) throws NumberFormatException, Exception
	{
		String preSignedUrl = null;
		//String bucket = getBucketForAccount(Integer.parseInt(accountId));
		
		
		//String finalBucket = "/"+bucket+folderName;
		
		String accessKey = BucketInfoCache.getBucketInfo(accountId).getAccessKey();
		String secretKey = BucketInfoCache.getBucketInfo(accountId).getSecreteKey();
		String bucketName = BucketInfoCache.getBucketInfo(accountId).getBucketName();
		
		if(!bucketName.equals(bucketNameReq)){
			BucketInfoCache.refreshBucketInfo(accountId);
			bucketName = BucketInfoCache.getBucketInfo(accountId).getBucketName();
		}
		AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
		AmazonS3 s3client = new AmazonS3Client(credentials);
		if ( UPLOAD_URL_TYPE.equals(urlType) )
		{
			String s3KeyWithFileName = accountId+"/"+"chunks"+"/"+fileName;
			preSignedUrl = GeneratePresignedUrlAndUploadObject.generatePresignedUploadUrl(s3KeyWithFileName, bucketName, s3client);
			
		}
		else if ( DOWNLOAD_URL_TYPE.equals(urlType) )
		{
			String downloadFileName = accountId+"/"+projectId+"/"+fileName;
			preSignedUrl = GeneratePresignedUrlAndUploadObject.generatePresignedDownloadUrl(downloadFileName, bucketName, logicalFileName, s3client);
		}
		
		else
		{
			l.error("ERROR check your URL_TYPE in Json");
			throw new IOException("Url Type does not match expectation. Please enter either UPLOAD or DOWNLOAD");
		}
		out.write(preSignedUrl.getBytes());
	}
	
	/**
	 * Fetches container for the provided account id
	 * @param accountId
	 * @return
	 * @throws Exception
	 */
	private final String getBucketForAccount(final int accountId) throws Exception
	{
		byte[] key = ByteUtil.toBytes(accountId);
		HbaseTableOperation hbaseOperation = new HbaseTableOperation();
		byte[] value = hbaseOperation.readValueFromTable(LINKS_TABLE, 
								HBASE_FAMILY_BYTES, COL_DEFAULT_BYTES, key);
		
		String result = (null == value) ? null : ByteUtil.toString(value, 0, value.length);
		return result;
	}
	
	
	
}
