/**
Copy Right : C2014 ARC Document Solutions. All Rights Reserved.

This software is the confidential and proprietary information of ARC
Document Solutions, INC. You shall not disclose such Confidential
Information and shall use it only in accordance with the terms of the
license agreement you entered into with ARC.The software may not be resold
or redistributed. Usage is governed by the FOLDER-FILE SYNC TLicense
Agreement.   Unauthorized reproduction or distribution is subject to civil
and criminal penalties. This notice may not be removed from this file
*/
package com.arc.rest.authenticate;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.arc.util.conf.ArcConfig;
import com.arc.util.log.ArcLogger;

public final class SecurityFilter implements Filter
{
	private static final boolean DEBUG_ENABLED = ArcLogger.l.isDebugEnabled();
	HttpURLConnection connection = null;
	
	public final void doFilter(final ServletRequest servletRequest,final ServletResponse servletResponse, 
			final FilterChain chain) throws IOException, ServletException 
	{
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
	
		UserCookieHandler.getInstance().store(request, response);
		UserProfile user = UserCookieHandler.getInstance().getUser(request, response);

		if ( ArcConfig.TEST_MODE ){
			request.setAttribute("__user", user);
			chain.doFilter(request, response);
			UserCookieHandler.getInstance().clear();
			return;
		}

		if ( user != UserProfile.getAnonymous() )
		{
			if ( DEBUG_ENABLED) ArcLogger.l.debug("User Authenticated");
		}
		else
		{
			String authenticationUrl = ArcConfig.AUTHENTICATE_URL;
			
			try
			{
				URL url = new URL(authenticationUrl); 
				connection = (HttpURLConnection) url.openConnection();
				connection.setDoOutput(true);
				connection.setDoInput(true);
				connection.setAllowUserInteraction(false);
				connection.setConnectTimeout(ArcConfig.AUTHENTICATE_TIMEOUT); //Timeout in milliseconds
				connection.setInstanceFollowRedirects(false); 
				connection.setUseCaches (false);

				connection.setRequestMethod("POST"); 
				connection.setRequestProperty("Content-Type", ArcConfig.CONTENT_TYPE); 
				connection.setRequestProperty("loginid", request.getHeader(ArcConfig.LOGIN_PARAMETER));
				connection.setRequestProperty("password", request.getHeader(ArcConfig.PASSWORD_PARAMETER));

		        String arcToken = connection.getHeaderField(ArcConfig.AUTHENTICATE_PARAM_TOKENKEY);
		        
				UserProfile profile = new UserProfile(
					request.getHeader(ArcConfig.LOGIN_PARAMETER), 
					ArcConfig.AUTHENTICATE_PARAM_TOKEN + "," + arcToken);
				
				UserCookieHandler.getInstance().setUser(profile, request, response);
				
				user = UserCookieHandler.getInstance().getUser(request, response);
			}
			catch(Exception e)
			{
				ArcLogger.l.error("Unable to authenticate user." +e.getStackTrace());
			}
			finally{
				connection.disconnect();
			}
		}
		
		request.setAttribute("__user", user);
		chain.doFilter(request, response);
		UserCookieHandler.getInstance().clear();
	}

	public final void init(final FilterConfig config) throws ServletException 
	{
		ArcLogger.l.info("SecurityFilter is on and working.");
	}

	public final void destroy() 
	{
		ArcLogger.l.info("SecurityFilter is going down.");
	}
}
