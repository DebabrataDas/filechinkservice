package com.arc.rest.authenticate;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.arc.util.conf.ArcConfig;
import com.arc.util.log.ArcLogger;

public class AuthenticateServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	private static final boolean INFO_ENABLED = ArcLogger.l.isInfoEnabled();
	
	public AuthenticateServlet()
	{
		if ( INFO_ENABLED ) ArcLogger.l.info("Initializing class: " +this.getClass().getName());
	}
	
	public void init(ServletConfig config) throws ServletException 
	{
		super.init(config);
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		if ( ArcConfig.TEST_MODE )
		{
	    	final String loginId = request.getParameter(ArcConfig.LOGIN_PARAMETER);
	    	final String password = request.getParameter(ArcConfig.PASSWORD_PARAMETER);
			this.process(request, response, loginId, password);
		}
	}
	/**
	 * The post method
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
    	final String loginId = request.getHeader(ArcConfig.LOGIN_PARAMETER);
    	final String password = request.getHeader(ArcConfig.PASSWORD_PARAMETER);
		this.process(request, response, loginId, password);
	}
	
	private void process(HttpServletRequest request, HttpServletResponse response, String loginId, String password) throws IOException
	{
		OutputStream out = response.getOutputStream();
        try 
        {
        	ArcAuthenticate arcAuthenticate = new ArcAuthenticate();
        	final String arcToken = arcAuthenticate.authenticate(loginId, password);
        	
			UserProfile profile = new UserProfile(loginId, arcToken);
			UserCookieHandler.getInstance().setUser(profile, request, response);

			out.write("OK".getBytes());
	 	} 
        catch (Exception ex) 
        {
			ArcLogger.l.fatal("Error in processing request", ex);
			response.sendError(HttpServletResponse.SC_EXPECTATION_FAILED, ArcConfig.AUTHENTICATE_FATAL);
		} 
        finally 
        {
			try { if ( null != out ) out.flush(); } catch (Exception ex) {}
			try { if ( null != out ) out.close(); } catch (Exception ex) {}
		}		
	}

	@Override
	public void destroy() 
	{
		super.destroy();
	}
}
