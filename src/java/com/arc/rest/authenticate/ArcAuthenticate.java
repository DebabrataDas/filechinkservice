package com.arc.rest.authenticate;

import java.net.HttpURLConnection;
import java.net.URL;

import com.arc.util.conf.ArcConfig;

public class ArcAuthenticate 
{
	private static final String METHOD_POST = ArcConfig.METHOD_POST;
	private static final boolean TRUE = true;
	private static final boolean FALSE = false;
	
	public String authenticate(String loginId, String password) throws Exception
	{
		URL url = new URL(ArcConfig.AUTHENTICATE_URL);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setDoOutput(TRUE);
		connection.setDoInput(TRUE);
		connection.setAllowUserInteraction(FALSE);
		connection.setConnectTimeout(ArcConfig.AUTHENTICATE_TIMEOUT); //Timeout in milliseconds
		connection.setInstanceFollowRedirects(FALSE); 
		connection.setUseCaches (FALSE);

		connection.setRequestMethod(METHOD_POST); 
		connection.setRequestProperty("Content-Type", "application/json"); 
		connection.setRequestProperty(ArcConfig.LOGIN_PARAMETER,loginId);
		connection.setRequestProperty(ArcConfig.PASSWORD_PARAMETER,password);
		connection.setUseCaches (FALSE);

        String arcToken = connection.getHeaderField(ArcConfig.AUTHENTICATE_PARAM_TOKENKEY);
        return arcToken;
		
	}
}
