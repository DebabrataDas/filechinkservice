/**
Copy Right : C2014 ARC Document Solutions. All Rights Reserved.

This software is the confidential and proprietary information of ARC
Document Solutions, INC. You shall not disclose such Confidential
Information and shall use it only in accordance with the terms of the
license agreement you entered into with ARC.The software may not be resold
or redistributed. Usage is governed by the FOLDER-FILE SYNC TLicense
Agreement.   Unauthorized reproduction or distribution is subject to civil
and criminal penalties. This notice may not be removed from this file
*/
package com.arc.rest.authenticate;


public final class UserProfile 
{
	public static final String ANY = "ANY";
	public static final String GUEST = "GUEST";

	public String loginid = "";
	public String hexdigest = "";
	public String token = "";
	public String lastLoggedIn;
	
	private boolean isGuest = false;
	
	public final static UserProfile GUEST_PROFILE;
	
	static
	{
		GUEST_PROFILE = new UserProfile(GUEST, "role,"+ GUEST);
		GUEST_PROFILE.isGuest = true;
	}
	
	public UserProfile() 
	{
	}

	public UserProfile(final String loginid, final String token) 
	{
		this.loginid = loginid;
		this.token = token;
	}

	public final boolean isGuest()
	{
		return this.isGuest;
	}
	
	public final static UserProfile getAnonymous()
	{
		return GUEST_PROFILE;
	}
}