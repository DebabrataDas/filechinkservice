/**
Copy Right : C2014 ARC Document Solutions. All Rights Reserved.

This software is the confidential and proprietary information of ARC
Document Solutions, INC. You shall not disclose such Confidential
Information and shall use it only in accordance with the terms of the
license agreement you entered into with ARC.The software may not be resold
or redistributed. Usage is governed by the FOLDER-FILE SYNC TLicense
Agreement.   Unauthorized reproduction or distribution is subject to civil
and criminal penalties. This notice may not be removed from this file
*/
package com.arc.rest.authenticate;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.arc.util.Hash;
import com.arc.util.StringUtils;
import com.arc.util.conf.ArcConfig;
import com.arc.util.log.ArcLogger;

public final class UserCookieHandler
{
	private static final boolean DEBUG_ENABLED = ArcLogger.l.isDebugEnabled();
	private static final String EMPTY = "";
	protected static final String COOKIE_LOGINID = "loginId";
	protected static final String COOKIE_TOKEN = "token";
	protected static final String COOKIE_LASTLOGIN = "lastLogin";
	protected static final String COOKIE_HEXDIGEST = "hexdigest";

	private String key = Hash.KEY_VALUE_DEFAULT;
	private String subDomain = EMPTY;
	private int maxTimeOutInSecs = ArcConfig.AUTHENTICATE_MAX_TIMEOUT;
	private int minTimeOutInSecs = ArcConfig.AUTHENTICATE_MIN_TIMEOUT;
	private int credentialsExpireInSeconds = 0;

	private final Map<String, String> cachedEncodings = new HashMap<String, String>();    

	private static UserCookieHandler handler = null;
	
	public final ThreadLocal<HttpServletRequest> request;
	public final ThreadLocal<HttpServletResponse> response;

	private Random random = new Random();

	public static final UserCookieHandler getInstance()
	{
		if (handler != null) return handler;
		handler = new UserCookieHandler();
		return handler;
	}
	
	private UserCookieHandler()
	{
		this.init();
    	this.request = new ThreadLocal<HttpServletRequest>();
    	this.response = new ThreadLocal<HttpServletResponse>();
	}
	
    private final void init()
	{
		this.key = ArcConfig.conf.get(Hash.KEY_NAME,Hash.KEY_VALUE_DEFAULT);
		this.subDomain = ArcConfig.conf.get("subdomain", "");

		if ( DEBUG_ENABLED ) 
		{
			StringBuilder sb = new StringBuilder(100);
			sb.append("this.subDomain:").append(this.subDomain);
			sb.append(". this.credentialsExpireInSeconds:").append(this.credentialsExpireInSeconds);
			ArcLogger.l.debug(sb.toString());
			sb.delete(0, sb.capacity());
		}
	}

    public final void store(HttpServletRequest request, HttpServletResponse response)
    {
    	this.request.set(request);
    	this.response.set(response);
    }
    
    public final void clear()
    {
    	this.request.remove();
    	this.response.remove();
    }
    
	public final UserProfile getUser( HttpServletRequest request, HttpServletResponse response)
	{
		UserProfile user = this.getUser(request);
		if (user == null) return UserProfile.getAnonymous();

		String browserKey = this.buildBrowserKey(user);
		return (this.isDigestValid(browserKey, user.hexdigest)) ? user: UserProfile.getAnonymous();
	}
	
	public final void setUser(UserProfile user, HttpServletRequest request, HttpServletResponse response ) 
	{
		user.lastLoggedIn = String.valueOf(System.currentTimeMillis());
		String browserKey = this.buildBrowserKey(user);
		user.hexdigest = Hash.createHex(this.key, browserKey);
		if ( DEBUG_ENABLED ) ArcLogger.l.debug("Setting user:" + user.loginid+ " with browserKey: + " + browserKey + " and digest:" + user.hexdigest);

		// Set the expiration randomly between minimum and maximum time to phase out the requests
		this.credentialsExpireInSeconds = random.nextInt((maxTimeOutInSecs - minTimeOutInSecs) + 1) + minTimeOutInSecs; 
		this.storeInCookie(user, response);
	}
	
	public final void setUser(UserProfile user) 
	{
		if (this.request == null || this.response == null) return;
		HttpServletRequest request = this.request.get();
		HttpServletResponse response = this.response.get();
		if (request == null || response == null) return;
		this.setUser(user, request, response);
	}
	
	public final void removeUser(UserProfile user)
	{
		String browserKey = this.buildBrowserKey(user);
		user.hexdigest = Hash.createHex(this.key, browserKey);
		this.removeCookies(user, this.response.get());
	}
	
    /**
     * Private Methods
     */
    
	private final String buildBrowserKey(UserProfile user) {
		StringBuilder sb = new StringBuilder(100);
		sb.append(user.loginid).append(':').append(user.token).append(':').append(user.lastLoggedIn);
		if ( DEBUG_ENABLED ) ArcLogger.l.debug(sb.toString());
		return sb.toString();
	}

	/**
	 * HexDigest is checked against the local cache as follows.
	 * 1. If browser did not send hexdigest then this request is not authenticated.
	 * 2. If browser has sent and matches the digest against local cache it is good.
	 * 3. If browser has sent it but local cache does not have it, regenerate and match. 
	 * @param browserDigest
	 * @return
	 */
	
	protected final boolean isDigestValid(String browserKey, String browserDigest) 
	{
		if ( DEBUG_ENABLED )ArcLogger.l.debug("Checking digest is good:" + browserKey + "-" + browserDigest);
		if (StringUtils.isEmpty(browserDigest))	return false; //Browser did not send the digest
		
		if (this.cachedEncodings.containsKey(browserKey) && browserDigest.equals(this.cachedEncodings.get(browserKey))) return true; //Digests match

		String localDigest = Hash.createHex(this.key, browserKey);	
		if (browserDigest.equals(localDigest)) {
			this.cachedEncodings.put(browserKey, localDigest);
			return true;
		}
		
		ArcLogger.l.warn("CookieSession > Authentication digest seem to be corrupted or compromised. Browser sent a digest and did not match with local digest for key:" + browserKey);
		return false;
	}

	/**
	 * This is constructed from bizosys stamped cookies
	 * This is valid for 8 hours
	 * 
	 * If we don't find in our cookie, check it with SSO service
	 * 
	 * @param request
	 * @return
	 */
	private final UserProfile getUser(HttpServletRequest request)
	{
		Cookie[] cookies = request.getCookies();
		if (cookies != null && cookies.length > 0)
		{
			if (DEBUG_ENABLED ) ArcLogger.l.debug("Got cookies count :" + cookies.length);
			UserProfile user = new UserProfile();
			for (Cookie cookie : cookies)
			{
				if ( DEBUG_ENABLED ) ArcLogger.l.debug("Cookie - " + cookie.getName() + " : " + cookie.getValue());
				
				if (COOKIE_LOGINID.equals(cookie.getName()))
					user.loginid = this.decodeCookieValue(cookie);
				else if (COOKIE_TOKEN.equals(cookie.getName()))
					user.token = this.decodeCookieValue(cookie);
				else if (COOKIE_LASTLOGIN.equals(cookie.getName()))
					user.lastLoggedIn = this.decodeCookieValue(cookie);
				else if (COOKIE_HEXDIGEST.equals(cookie.getName()))
					user.hexdigest = this.decodeCookieValue(cookie);
			}
			if (DEBUG_ENABLED) ArcLogger.l.debug("USER.LOGINID - " + user.loginid + " ::: USER.TOKEN - " + user.token + " ::: USER.LASTLOGIN - " + user.lastLoggedIn);
			return user;
		}
		if ( DEBUG_ENABLED )ArcLogger.l.debug("Did not get cookies.");
		return null;
	}

	private final String decodeCookieValue(Cookie cookie) 
	{
		try 
		{
			return URLDecoder.decode(cookie.getValue(), "UTF-8");
		} 
		catch (UnsupportedEncodingException e) 
		{
			return cookie.getValue();
		}
	}

	private final void storeInCookie(UserProfile user, HttpServletResponse response)
	{
		this.addCookie(COOKIE_LOGINID, user.loginid, response, this.credentialsExpireInSeconds);
		this.addCookie(COOKIE_TOKEN, user.token, response, this.credentialsExpireInSeconds);
		this.addCookie(COOKIE_LASTLOGIN, user.lastLoggedIn, response, this.credentialsExpireInSeconds);
		this.addCookie(COOKIE_HEXDIGEST, user.hexdigest, response, this.credentialsExpireInSeconds);
	}

	private final void removeCookies(UserProfile user, HttpServletResponse response)
	{
		this.addCookie(COOKIE_LOGINID, user.loginid, response, 1);
		this.addCookie(COOKIE_TOKEN, user.token, response, 1);
		this.addCookie(COOKIE_LASTLOGIN, user.lastLoggedIn, response, 1);
		this.addCookie(COOKIE_HEXDIGEST, user.hexdigest, response, 1);
	}

	private final void addCookie(String key, String value, HttpServletResponse response, int expiryInSeconds)
	{
		if (DEBUG_ENABLED) ArcLogger.l.debug("Storing cookie: "+ key + ":" + value + " for " + expiryInSeconds  + " seconds");
		String encodedValue = value;
		try 
		{
			encodedValue = URLEncoder.encode(value, "UTF-8");
		} 
		catch (UnsupportedEncodingException e) 
		{
			encodedValue = value;
		}
		Cookie cookie = new Cookie(key, encodedValue);
		cookie.setMaxAge(expiryInSeconds);
		response.addCookie(cookie);
	}
}