package com.arc.management;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.arc.util.log.ConsoleLogger;
import com.arc.util.log.ILogger;

public class MonitorRequest implements IHealth {

	public ILogger l = new ConsoleLogger(MonitorRequest.class.getName());

	public long lastHealthSyncTime = System.currentTimeMillis();

	private static MonitorRequest singleton = null;

	public static MonitorRequest getInstance() 
	{
		if (null != singleton)
			return singleton;
		synchronized (MonitorRequest.class.getName()) 
		{
			if (null != singleton)
				return singleton;
			singleton = new MonitorRequest();
		}
		return singleton;
	}

	final ThreadLocal<Clock> clock = new ThreadLocal<Clock>() 
	{
		@Override
		protected Clock initialValue() 
		{
			return new Clock();
		}
	};

	List<RequestAPIMetrics> apiMetrics = new ArrayList<RequestAPIMetrics>();
	public void register(RequestAPIMetrics apiMetric) 
	{
		apiMetrics.add(apiMetric);
	}
	
	private MonitorRequest() 
	{
		for (RequestAPIMetrics apiMetric: apiMetrics) 
		{
			apiMetric.totalRequests.set(0);
			apiMetric.activeSessions.set(0);
		}
		lastHealthSyncTime = System.currentTimeMillis();
	}

	public void onEnter(RequestAPIMetrics apiMetric) 
	{
		apiMetric.totalRequests.incrementAndGet();
		apiMetric.activeSessions.incrementAndGet();
		clock.get().time = System.currentTimeMillis();
	}
	
	public void onExit(RequestAPIMetrics apiMetric, MonitorCollector collector, int requestByteSize, int responseByteSize) 
	{
		int currentReqs = apiMetric.activeSessions.decrementAndGet();
		currentReqs++;

		long endTime = System.currentTimeMillis();
		long diff = (endTime - clock.get().time);
		apiMetric.mmaResponseTime.set((int) diff);
		apiMetric.mmaRequestBytes.set(requestByteSize);
		apiMetric.mmaResponseBytes.set(responseByteSize);
	}
	
	private static final class Clock 
	{
		public long time = 0;
	}

	@Override
	public MonitorCollector collect() 
	{
		long currentTime = System.currentTimeMillis();
		long timeDiffInSecs = (currentTime - lastHealthSyncTime)/1000;
		
		MonitorCollector collector = new MonitorCollector();
		for (RequestAPIMetrics apiMetric : apiMetrics) 
		{
			int numAPICalls = apiMetric.totalRequests.getAndSet(0);
			lastHealthSyncTime = currentTime;
			
			collector.add(new MonitorMeasure(apiMetric.metrics[0], apiMetric.activeSessions.get()));
			collector.add(new MonitorMeasure(apiMetric.metrics[2], apiMetric.mmaResponseTime.getMinVal()));
			collector.add(new MonitorMeasure(apiMetric.metrics[3], apiMetric.mmaResponseTime.getAvgVal()));
			collector.add(new MonitorMeasure(apiMetric.metrics[4], apiMetric.mmaResponseTime.getMaxVal()));

			collector.add(new MonitorMeasure(apiMetric.metrics[5], apiMetric.mmaRequestBytes.getMaxVal()));
			collector.add(new MonitorMeasure(apiMetric.metrics[6], apiMetric.mmaRequestBytes.getMaxVal()));
			collector.add(new MonitorMeasure(apiMetric.metrics[7], apiMetric.mmaRequestBytes.getMaxVal()));

			collector.add(new MonitorMeasure(apiMetric.metrics[8], apiMetric.mmaResponseBytes.getMaxVal()));
			collector.add(new MonitorMeasure(apiMetric.metrics[9], apiMetric.mmaResponseBytes.getMaxVal()));
			collector.add(new MonitorMeasure(apiMetric.metrics[10], apiMetric.mmaResponseBytes.getMaxVal()));

			double requestVelocity = ( timeDiffInSecs > 0 ) ? (numAPICalls/timeDiffInSecs) : 0.0;
			collector.add(new MonitorMeasure(apiMetric.metrics[1], requestVelocity));
		}
		return collector;
	}
	
	public static class RequestAPIMetrics {
		
		public AtomicInteger totalRequests = new AtomicInteger();
		public AtomicInteger activeSessions = new AtomicInteger();
		public MonitorMeasureAvg mmaResponseTime = new MonitorMeasureAvg();
		public MonitorMeasureAvg mmaRequestBytes = new MonitorMeasureAvg();
		public MonitorMeasureAvg mmaResponseBytes = new MonitorMeasureAvg();
		

		public String[] metrics = null;
		public RequestAPIMetrics(String name) {
			metrics = new String[] { 
				name + "_ACTIVE_SESSIONS",
				name + "_VELOCITY",
				name + "_MIN_RESPONSE_MILLIS",
				name + "_AVG_RESPONSE_MILLIS",
				name + "_MAX_RESPONSE_MILLIS",
				name + "_MIN_REQUEST_BYTES",
				name + "_AVG_REQUEST_BYTES",
				name + "_MAX_REQUEST_BYTES",
				name + "_MIN_RESPONSE_BYTES",
				name + "_AVG_RESPONSE_BYTES",
				name + "_MAX_RESPONSE_BYTES"
			};
		}
	}
}