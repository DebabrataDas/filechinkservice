package com.arc.management;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.arc.util.log.ArcLogger;
import com.arc.util.log.ConsoleLogger;

public class MonitorHealthServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final boolean INFO_ENABLED = ArcLogger.l.isInfoEnabled();
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		if ( INFO_ENABLED ) ArcLogger.l.info("Health Monitoring booted...");

		try 
		{
			MonitorHealth.getInstance().l = new ConsoleLogger(MonitorHealth.class.getName());
			MonitorRequest.getInstance().l = new ConsoleLogger(MonitorRequest.class.getName());
			MonitorHBase.getInstance().l = new ConsoleLogger(MonitorHBase.class.getName());

			MonitorHealth.getInstance().register(MonitorRequest.getInstance());
			MonitorHealth.getInstance().register(MonitorHBase.getInstance());
		} 
		catch (Exception ex) 
		{
			ex.printStackTrace();
			throw new ServletException(ex);
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException 
	{
		res.setContentType("text/plain");
		res.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
		res.setHeader("Pragma", "no-cache");

		PrintWriter out = res.getWriter();
		try 
		{
			out.write(MonitorHealth.getInstance().healthCheck('\n'));
		} 
		catch (Exception ex) 
		{
			ex.printStackTrace();
		} 
		finally 
		{
			if (null != out) 
			{
				out.flush();
				out.close();
			}
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException 
	{
		
	}
}