package com.arc.management;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.arc.util.log.ArcLogger;

public class HeartBeatServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final boolean INFO_ENABLED = ArcLogger.l.isInfoEnabled();

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		if ( INFO_ENABLED ) ArcLogger.l.info("Heartbeat booted...");
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) 
			throws ServletException, IOException 
	{
		res.setContentType("text/plain");
		res.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
		res.setHeader("Pragma", "no-cache");

		PrintWriter out = res.getWriter();
		try 
		{
			out.write("1");
		} 
		catch (Exception ex) 
		{
			ex.printStackTrace();
		} 
		finally 
		{
			if (null != out) 
			{
				out.flush();
				out.close();
			}
		}
	}
}