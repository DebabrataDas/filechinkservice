package com.arc.management;

import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import com.arc.util.log.ConsoleLogger;
import com.arc.util.log.ILogger;

public class MonitorHealth 
{

	private static MonitorHealth singleton = null;
	public static MonitorHealth getInstance() 
	{
		if ( null != singleton) return singleton;
		synchronized (MonitorHealth.class.getName()) 
		{
			if ( null != singleton) return singleton;
			singleton = new MonitorHealth();
		}
		return singleton;
	}
	
	public ILogger l = new ConsoleLogger(MonitorHealth.class.getName());
	public boolean INFO_ENABLED = l.isInfoEnabled();
	private HealthCheckAgent hcAgent = null;
	private List<IHealth> healthAgents = new ArrayList<IHealth>();
	
	private  MonitorHealth() 
	{
		l.info("Initializing monitor health ------------------------");
		this.hcAgent = new HealthCheckAgent();
	}
	
	public void cancel() 
	{
		this.hcAgent.cancel();
	}
	
	public void register(IHealth healthAgent)
	{
		l.info("Registering: " + healthAgent.getClass().getName());
		healthAgents.add(healthAgent);
	}

	
	//--------------Health Check Module-------------------
	public String healthCheck(char separator) {
		
		StringBuilder sb = new StringBuilder();
		Runtime r = Runtime.getRuntime();
		sb.append("jvm-maxm-mb:").append(r.maxMemory()/1048576).append(separator).
		append("jvm-totm-mb:").append(r.totalMemory()/1048576).append(separator).
		append("jvm-freem-mb:").append(r.freeMemory()/1048576).append(separator).
		append("jvm-threads-no:").append(Thread.activeCount()).append(separator);
		
		for (IHealth health : healthAgents) 
		{
			MonitorCollector collector = health.collect();
			if ( null == collector) continue;

			for (MonitorMeasure mm : collector) 
			{
				sb.append(separator).append(mm.name).append(':').append(mm.val);
			}
		}
		
		return sb.toString();
	}
	
	
	private class HealthCheckAgent extends TimerTask {

		public void run() 
		{
			try 
			{
				if ( INFO_ENABLED)  l.info(healthCheck('\n')); 
			} 
			catch(Exception e) 
			{
				l.warn("Error in running health check", e);
			}
		}
	}
}
