package com.arc.management;

public class MonitorMeasureAvg 
{
	private int minVal = Integer.MAX_VALUE;
	private int maxVal = -1;
	private int sum = 0;
	private int count = 0;
	
	public void set(int val)
	{
		if ( val < minVal ) minVal = val;
		if ( val > maxVal ) maxVal = val;
		sum += val;
		count++;
	}
	
	public double getAvgVal()
	{
		if ( count > 0 ) return sum/count;
		return 0.0;
	}
	
	public int getMinVal()
	{
		return minVal;
	}
	
	public int getMaxVal()
	{
		return maxVal;
	}
}