package com.arc.management;

public class MonitorMeasure 
{
	public String name;
	public double val;
	long start = -1;

	public MonitorMeasure(String name, double val) 
	{
		this.name = name;
		this.val = val;
	}

	public MonitorMeasure(String name) 
	{
		this.name = name;
	}

	public void recordExecutionTimeStart() {
		start = System.currentTimeMillis();
	}

	public void recordExecutionTimeEnd() {
		this.val = System.currentTimeMillis() - start;
	}
}