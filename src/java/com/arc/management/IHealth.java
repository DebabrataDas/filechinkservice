package com.arc.management;

public interface IHealth 
{
	public MonitorCollector collect();
}