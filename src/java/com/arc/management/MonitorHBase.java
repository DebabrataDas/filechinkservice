package com.arc.management;

import java.util.concurrent.atomic.AtomicInteger;

import com.arc.util.conf.ArcConfig;
import com.arc.util.log.ConsoleLogger;
import com.arc.util.log.ILogger;

public class MonitorHBase implements IHealth {

	public ILogger l = new ConsoleLogger(MonitorHBase.class.getName());

	public AtomicInteger activeReadSessions = new AtomicInteger();
	public AtomicInteger totalReads = new AtomicInteger();
	private MonitorMeasureAvg mmaReadRequestTime = new MonitorMeasureAvg();
	private MonitorMeasureAvg mmaReadRequestBytes = new MonitorMeasureAvg();

	public long lastHealthSyncTime = System.currentTimeMillis();

	public AtomicInteger activeWriteSessions = new AtomicInteger();
	public AtomicInteger totalWrites = new AtomicInteger();
	private MonitorMeasureAvg mmaWriteRequestTime = new MonitorMeasureAvg();
	private MonitorMeasureAvg mmaWriteRequestBytes = new MonitorMeasureAvg();
	
	
	private static MonitorHBase singleton = null;

	
	private static final String HBASE_READ_REQUEST_VELOCITY = ArcConfig.HBASE_READ_REQUEST_VELOCITY;
	private static final String HBASE_ACTIVE_READ_SESSION_PARAM = ArcConfig.HBASE_ACTIVE_READ_SESSION_PARAM;
	private static final String HBASE_AVG_READ_RESPONSE_PARAM = ArcConfig.HBASE_AVG_READ_RESPONSE_PARAM;
	private static final String HBASE_MIN_READ_RESPONSE_PARAM = ArcConfig.HBASE_MIN_READ_RESPONSE_PARAM;
	private static final String HBASE_MAX_READ_RESPONSE_PARAM = ArcConfig.HBASE_MAX_READ_RESPONSE_PARAM;

	private static final String HBASE_AVG_READ_BYTES_PARAM = ArcConfig.HBASE_AVG_READ_BYTES_PARAM;
	private static final String HBASE_MIN_READ_BYTES_PARAM = ArcConfig.HBASE_MIN_READ_BYTES_PARAM;
	private static final String HBASE_MAX_READ_BYTES_PARAM = ArcConfig.HBASE_MAX_READ_BYTES_PARAM;
	
	private static final String HBASE_WRITE_REQUEST_VELOCITY = ArcConfig.HBASE_WRITE_REQUEST_VELOCITY;
	private static final String HBASE_ACTIVE_WRITE_SESSION_PARAM = ArcConfig.HBASE_ACTIVE_WRITE_SESSION_PARAM;
	private static final String HBASE_AVG_WRITE_RESPONSE_PARAM = ArcConfig.HBASE_AVG_WRITE_RESPONSE_PARAM;
	private static final String HBASE_MIN_WRITE_RESPONSE_PARAM = ArcConfig.HBASE_MIN_WRITE_RESPONSE_PARAM;
	private static final String HBASE_MAX_WRITE_RESPONSE_PARAM = ArcConfig.HBASE_MAX_WRITE_RESPONSE_PARAM;

	private static final String HBASE_AVG_WRITE_BYTES_PARAM = ArcConfig.HBASE_AVG_WRITE_BYTES_PARAM;
	private static final String HBASE_MIN_WRITE_BYTES_PARAM = ArcConfig.HBASE_MIN_WRITE_BYTES_PARAM;
	private static final String HBASE_MAX_WRITE_BYTES_PARAM = ArcConfig.HBASE_MAX_WRITE_BYTES_PARAM;

	public static MonitorHBase getInstance() 
	{
		if (null != singleton)
			return singleton;
		synchronized (MonitorHBase.class.getName()) 
		{
			if (null != singleton)
				return singleton;
			singleton = new MonitorHBase();
		}
		return singleton;
	}

	final ThreadLocal<Clock> clock = new ThreadLocal<Clock>() 
	{
		@Override
		protected Clock initialValue() 
		{
			return new Clock();
		}
	};

	private MonitorHBase() 
	{
		totalReads.set(0);
		totalWrites.set(0);
		lastHealthSyncTime = System.currentTimeMillis();
		activeReadSessions.set(0);
		activeWriteSessions.set(0);
	}

	public void onReadEnter() 
	{
		totalReads.incrementAndGet();
		activeReadSessions.incrementAndGet();
		clock.get().lastReadTime = System.currentTimeMillis();
	}
	
	public void onWriteEnter() 
	{
		totalWrites.incrementAndGet();
		activeWriteSessions.incrementAndGet();
		clock.get().lastWriteTime = System.currentTimeMillis();
	}

	public void onReadExit(MonitorCollector collector, int readByteSize) 
	{
		int currentReqs = activeReadSessions.decrementAndGet();
		long endTime = System.currentTimeMillis();
		long diff = (endTime - clock.get().lastReadTime);
		mmaReadRequestTime.set((int) diff);
		mmaReadRequestBytes.set(readByteSize);
	}
	
	public void onWriteExit(MonitorCollector collector, int writeByteSize) 
	{
		int currentReqs = activeWriteSessions.decrementAndGet();
		long endTime = System.currentTimeMillis();
		long diff = (endTime - clock.get().lastWriteTime);
		mmaWriteRequestTime.set((int) diff);
		mmaWriteRequestBytes.set(writeByteSize);
	}

	private static final class Clock 
	{
		public long lastReadTime = 0;
		public long lastWriteTime = 0;
	}

	@Override
	public MonitorCollector collect() 
	{
		long currentTime = System.currentTimeMillis();
		long timeDiffInSecs = (currentTime - lastHealthSyncTime)/1000;
		int numReads = totalReads.getAndSet(0);
		int numWrites = totalWrites.getAndSet(0);
		lastHealthSyncTime = currentTime;
		
		MonitorCollector collector = new MonitorCollector();
		collector.add(new MonitorMeasure(HBASE_ACTIVE_READ_SESSION_PARAM, activeReadSessions.get()));
		collector.add(new MonitorMeasure(HBASE_AVG_READ_RESPONSE_PARAM, mmaReadRequestTime.getAvgVal()));
		collector.add(new MonitorMeasure(HBASE_MIN_READ_RESPONSE_PARAM, mmaReadRequestTime.getMinVal()));
		collector.add(new MonitorMeasure(HBASE_MAX_READ_RESPONSE_PARAM, mmaReadRequestTime.getMaxVal()));

		collector.add(new MonitorMeasure(HBASE_AVG_READ_BYTES_PARAM, mmaReadRequestBytes.getAvgVal()));
		collector.add(new MonitorMeasure(HBASE_MIN_READ_BYTES_PARAM, mmaReadRequestBytes.getMinVal()));
		collector.add(new MonitorMeasure(HBASE_MAX_READ_BYTES_PARAM, mmaReadRequestBytes.getMaxVal()));

		double readVelocity = ( timeDiffInSecs > 0 ) ? (numReads/timeDiffInSecs) : 0.0;
		collector.add(new MonitorMeasure(HBASE_READ_REQUEST_VELOCITY, readVelocity));

		collector.add(new MonitorMeasure(HBASE_ACTIVE_WRITE_SESSION_PARAM, activeWriteSessions.get()));
		collector.add(new MonitorMeasure(HBASE_AVG_WRITE_RESPONSE_PARAM, mmaWriteRequestTime.getAvgVal()));
		collector.add(new MonitorMeasure(HBASE_MIN_WRITE_RESPONSE_PARAM, mmaWriteRequestTime.getMinVal()));
		collector.add(new MonitorMeasure(HBASE_MAX_WRITE_RESPONSE_PARAM, mmaWriteRequestTime.getMaxVal()));

		collector.add(new MonitorMeasure(HBASE_AVG_WRITE_BYTES_PARAM, mmaWriteRequestBytes.getAvgVal()));
		collector.add(new MonitorMeasure(HBASE_MIN_WRITE_BYTES_PARAM, mmaWriteRequestBytes.getMinVal()));
		collector.add(new MonitorMeasure(HBASE_MAX_WRITE_BYTES_PARAM, mmaWriteRequestBytes.getMaxVal()));

		double writeVelocity = ( timeDiffInSecs > 0 ) ? (numWrites/timeDiffInSecs) : 0.0;
		collector.add(new MonitorMeasure(HBASE_WRITE_REQUEST_VELOCITY, writeVelocity));

		return collector;
	}
}