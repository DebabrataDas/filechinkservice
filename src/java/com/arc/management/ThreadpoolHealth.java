package com.arc.management;

import java.util.concurrent.ThreadPoolExecutor;

public class ThreadpoolHealth implements IHealth {

	ThreadPoolExecutor pool = null;
	String name = null;

	public ThreadpoolHealth(String name, ThreadPoolExecutor pool) {
		this.pool = pool;
		this.name = name;
	}

	@Override
	public MonitorCollector collect() 
	{
		MonitorCollector collector = new MonitorCollector();
		if (null != this.pool) 
		{
			collector.add(new MonitorMeasure(name + "-max-count", this.pool.getMaximumPoolSize()));
			collector.add(new MonitorMeasure(name + "-active-count", this.pool.getActiveCount()));
			collector.add(new MonitorMeasure(name + "-pool-size", this.pool.getPoolSize()));
			collector.add(new MonitorMeasure(name + "-task-count", this.pool.getTaskCount()));
			collector.add(new MonitorMeasure(name + "-completed-task-count", this.pool.getCompletedTaskCount()));
		}
		return collector;
	}

}