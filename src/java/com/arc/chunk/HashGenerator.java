package com.arc.chunk;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class HashGenerator
{
	public static final String getChunkHash(byte[] fileBytes, int chunkOffset, int chunkLength, 
			String hashAlgo, byte[] chunk) throws NoSuchAlgorithmException 
	{
		if( fileBytes.length < (chunkOffset + chunkLength))
		{
			System.err.println("File size too small for chunking");
			return null;
		}
		
		StringBuilder hashCode = new StringBuilder();
		byte[] chunkMd;

		System.arraycopy(fileBytes, (int) chunkOffset, chunk, 0, chunkLength);
		MessageDigest md = MessageDigest.getInstance(hashAlgo);
		md.update(chunk, 0, (int) chunkLength);
		chunkMd = md.digest();

		for (int i = 0; i < chunkMd.length; i++)
		{
			String hash = Integer.toHexString(0xff & chunkMd[i]);
			if(hash.length() == 1) hashCode.append('0');
			hashCode.append(hash);
		}
		return hashCode.toString();
	}
	
	public String generateHash(byte[] fileBytes, long chunkOffset, int chunkLength, String algoName)
	{
		StringBuilder hashCode = new StringBuilder();
		byte[] chunkMd;
		byte[] chunk = new byte[chunkLength];
		long fileLength = fileBytes.length;
		try
		{
			if( fileLength < (chunkOffset + chunkLength))
			{
				System.err.println("File size too small for chunking");
				return null;
			}
			MessageDigest md = MessageDigest.getInstance(algoName);
			System.arraycopy(fileBytes, (int) chunkOffset, chunk, 0, chunkLength);
			md.update(chunk, 0, (int) chunkLength);
			chunkMd = md.digest();
			for (int i = 0; i < chunkMd.length; i++)
			{
				String hash = Integer.toHexString(0xff & chunkMd[i]);
				if(hash.length() == 1) hashCode.append('0');
				hashCode.append(hash);
			}
		} 
		catch (Exception e)
		{
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return hashCode.toString();
	}
	
	public byte[] getChunk(byte[] fileBytes, long chunkOffset, int chunkLength)
	{
		byte[] chunk = new byte[chunkLength];
		long fileLength = fileBytes.length;
		try
		{
			if( fileLength < (chunkOffset + chunkLength))
			{
				System.err.println("File size too small for chunking");
				return null;
			}
			System.arraycopy(fileBytes, (int) chunkOffset, chunk, 0, chunkLength);
		} 
		catch (Exception e)
		{
			// TODO: handle exception
			e.printStackTrace();
		}
		return chunk;
	}
}
