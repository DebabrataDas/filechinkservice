package com.arc.filechunk;

public class InvalidResponseExcepton extends Exception {
	private String accountId;
	private String bucketName;
	private String accessKey;
	private String secreteKey;
	private String source;
	
	public InvalidResponseExcepton(String source,String accountId, String bucketName, String accessKey, String secreteKey) {
		this.source = source;
		this.accountId = accountId;
		this.bucketName = bucketName;
		this.accessKey = accessKey;
		this.secreteKey = secreteKey;
	}
	
	
	
	@Override
	public String toString() {
		String ex = "Invalid Response from "+source+"  :- accountId = "+accountId+" bucketName= "+bucketName+" accesskey = "+accessKey+" secreteKey "+secreteKey+"  \n";
		return ex;
	}



	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getBucketName() {
		return bucketName;
	}
	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}
	public String getAccessKey() {
		return accessKey;
	}
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	public String getSecreteKey() {
		return secreteKey;
	}
	public void setSecreteKey(String secreteKey) {
		this.secreteKey = secreteKey;
	}
	
	
	

}
