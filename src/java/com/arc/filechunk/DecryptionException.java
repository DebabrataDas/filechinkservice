package com.arc.filechunk;

import java.io.PrintStream;
import java.io.PrintWriter;

public class DecryptionException extends Exception {
	private Exception ex  ;
	
	public DecryptionException(Exception ex){
		this.ex = ex;
	}

	@Override
	public String toString() {
		return ex.toString();
	}

	@Override
	public String getMessage() {
		return ex.getMessage();
	}

	@Override
	public synchronized Throwable initCause(Throwable cause) {
		return ex.initCause(cause);
	}

	@Override
	public void printStackTrace() {
		ex.printStackTrace();
	}

	@Override
	public void printStackTrace(PrintStream s) {
		ex.printStackTrace(s);
	}

	@Override
	public void printStackTrace(PrintWriter s) {
		ex.printStackTrace(s);
	}

	@Override
	public StackTraceElement[] getStackTrace() {
		return ex.getStackTrace();
	}
	
	
	
	
	
}
