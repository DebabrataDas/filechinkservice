package com.arc.filechunk.model;

public class BucketInfo {
	private String bucketName;
	private String accessKey;
	private String secreteKey;
	
	
	public BucketInfo(String bucketName, String accessKey, String secreteKey) {
		this.bucketName = bucketName;
		this.accessKey = accessKey;
		this.secreteKey = secreteKey;
	}
	public String getBucketName() {
		return bucketName;
	}
	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}
	public String getAccessKey() {
		return accessKey;
	}
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	public String getSecreteKey() {
		return secreteKey;
	}
	public void setSecreteKey(String secreteKey) {
		this.secreteKey = secreteKey;
	}
}
