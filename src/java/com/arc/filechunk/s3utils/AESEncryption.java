package com.arc.filechunk.s3utils;


import com.arc.filechunk.DecryptionException;
import com.arc.filechunk.EncryptionException;
import com.arc.util.conf.ArcConfig;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class AESEncryption {
    static Logger l = Logger.getLogger(AESEncryption.class);
    private Cipher cipher;
    private Cipher deCipher;
    private SecretKeySpec key;
    private IvParameterSpec ivSpec;
    private static AESEncryption instance;
    
    public static AESEncryption getInstance() throws EncryptionException {
    	
    	if(null != instance) return instance;
    	
    	synchronized (AESEncryption.class) {
    		if(null != instance) return instance;
    		instance = new AESEncryption();
		}
		return instance;
	}
    
    private AESEncryption() throws EncryptionException {
        try {
            key =  new SecretKeySpec(toHexString(ArcConfig.AES_KEY).getBytes("UTF-8"),"AES");
            byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ivSpec =  new IvParameterSpec(iv);
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key,ivSpec);


            deCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            deCipher.init(Cipher.DECRYPT_MODE, key,ivSpec);

        } catch (UnsupportedEncodingException e) {
            l.error("Got UnsupportedEncodingException while creating  cipher "+e.toString());
            throw new EncryptionException(e);
        } catch (NoSuchAlgorithmException e) {
            l.error("Got NoSuchAlgorithmException while creating  cipher "+e.toString());
            throw new EncryptionException(e);
        } catch (NoSuchPaddingException e) {
            l.error("Got NoSuchPaddingException while creating  cipher "+e.toString());
            throw new EncryptionException(e);
        } catch (InvalidAlgorithmParameterException e) {
            l.error("Got InvalidAlgorithmParameterException while creating  cipher "+e.toString());
            throw new EncryptionException(e);
        } catch (InvalidKeyException e) {
            l.error("Got InvalidKeyException while creating  cipher "+e.toString());
            throw new EncryptionException(e);
        }
    }

    public  byte[]   encrypt(String inputString) throws EncryptionException {
    	
        try {
        	synchronized (this) {
        		return Base64.encodeBase64(cipher.doFinal(inputString.getBytes("UTF-8")));
			}
            
        } catch (IllegalBlockSizeException e) {
            l.error("Got IllegalBlockSizeException while encryption of "+inputString+" ===>>>"+e.toString());
            throw new EncryptionException(e);
        } catch (BadPaddingException e) {
            l.error("Got BadPaddingException while encryption of "+inputString+" ===>>>"+e.toString());
            throw new EncryptionException(e);
        } catch (UnsupportedEncodingException e) {
            l.error("Got UnsupportedEncodingException while encryption of "+inputString+" ===>>>"+e.toString());
            throw new EncryptionException(e);
        }
    }

    public String  decrypt(byte[] encryptedString) throws DecryptionException {
        try {
        	synchronized (this) {
        		byte[] original = deCipher.doFinal(Base64.decodeBase64(encryptedString));
                return  new String(original);
			}
            
        } catch (IllegalBlockSizeException e) {
            l.error("Got IllegalBlockSizeException while decryption ===>>> "+e.toString());
            throw new DecryptionException(e);
        } catch (BadPaddingException e) {
            l.error("Got IllegalBlockSizeException while decryption ===>>> "+e.toString());
            throw new DecryptionException(e);
        }
    }




    private  String toHexString(String key) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(key.getBytes());

        byte byteData[] = md.digest();
        return new String(Hex.encodeHex(byteData));
    }


    public static void main(String args[]) throws EncryptionException, DecryptionException {
    	getInstance();
        byte[] encryptedString1 = instance.encrypt("dhanraj");
        System.out.println(new String(encryptedString1));
        byte[] encryptedString2 = instance.encrypt("rajar");
        System.out.println(new String(encryptedString2));
        String orig1 = instance.decrypt(encryptedString1);
        System.out.println(orig1);
        String orig2 = instance.decrypt(encryptedString2);
        System.out.println(orig2);
    }
}
