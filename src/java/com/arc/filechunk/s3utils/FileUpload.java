package com.arc.filechunk.s3utils;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class FileUpload 
{
	/**
	 * Upload file to s3 using the given presigned url
	 * @param url - presigned upload url for s3
	 * @param fileBytes to be written
	 * @return response code
	 * @throws IOException
	 */
	public final static int uploadObject(URL url, byte[] fileBytes) throws IOException 
	{
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setDoOutput(true);
		connection.setRequestMethod("PUT");
		DataOutputStream out = new DataOutputStream(connection.getOutputStream());
		out.write(fileBytes);
		out.flush();
		out.close();
		return connection.getResponseCode();
	}
	
	public static void main(String[] args) throws MalformedURLException, IOException 
	{
//		String urlS = GeneratePresignedUrlAndUploadObject.generatePresignedUploadUrl("stitchFile.txt");
//		File file = new File("data/download");
//		byte[] fileBytes = FileReaderUtil.getBytes(file);
//		URL uploadUrl = new URL(urlS);
//		UploadObject(uploadUrl, fileBytes);
	}

}
