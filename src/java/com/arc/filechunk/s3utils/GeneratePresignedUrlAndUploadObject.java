package com.arc.filechunk.s3utils;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.log4j.Logger;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ResponseHeaderOverrides;
import com.arc.rest.rest.RestAPIServlet;
import com.arc.util.conf.ArcConfig;

public class GeneratePresignedUrlAndUploadObject 
{

	private static final String EMPTY_STRING = "";
	static Logger l = Logger.getLogger(GeneratePresignedUrlAndUploadObject.class);
	/**
	 * Generate AWS presigned url for file upload
	 * @param fileName Name of the file to be uploaded
	 * @param bucketName Name of the bucket into which file is uploaded
	 * @return presignedUrl
	 * @throws Exception 
	 */
	public final static String generatePresignedUploadUrl(String fileName, String bucketName, AmazonS3 s3client ) throws Exception
	{
		/*AmazonS3 s3client = new AmazonS3Client(
				new BasicAWSCredentials( S3_ACCESS_KEY, 
						S3_SECRET_KEY));*/

		try 
		{
			java.util.Date expiration = new java.util.Date();
			long milliSeconds = expiration.getTime();
			milliSeconds += 1000 * 60 * 60; // Add 1 hour.
			expiration.setTime(milliSeconds);

			GeneratePresignedUrlRequest generatePresignedUrlRequest = 
					new GeneratePresignedUrlRequest(bucketName, fileName);
			generatePresignedUrlRequest.setMethod(HttpMethod.PUT);
			generatePresignedUrlRequest.setExpiration(expiration);

			URL url = s3client.generatePresignedUrl(generatePresignedUrlRequest);
			
			return url.toString();
		} 
		catch (AmazonServiceException exception) 
		{
			//System.out.println("Caught an AmazonServiceException, "
			//		+ "which means your request made it "
			//		+ "to Amazon S3, but was rejected with an error response "
			//		+ "for some reason.");
			
			l.error("Caught an AmazonServiceException, "
					+ "which means your request made it "
					+ "to Amazon S3, but was rejected with an error response "
					+ "for some reason.");
			l.error(exception.getMessage() +" "+exception.getStatusCode()+" "+exception.getErrorCode());
			//System.out.println("Error Message: " + exception.getMessage());
			//System.out.println("HTTP  Code: " + exception.getStatusCode());
			//System.out.println("AWS Error Code:" + exception.getErrorCode());
			//System.out.println("Error Type:    " + exception.getErrorType());
			//System.out.println("Request ID:    " + exception.getRequestId());
			
			throw new Exception(exception.getMessage());
		} 
		catch (AmazonClientException ace) 
		{
			//System.out.println("Caught an AmazonClientException, "
			//		+ "which means the client encountered "
			//		+ "an internal error while trying to communicate"
			//		+ " with S3, "
			//		+ "such as not being able to access the network.");
			//System.out.println("Error Message: " + ace.getMessage());
			l.error(ace.getMessage());
			throw new Exception(ace.getMessage());
		}
		
		
	}
	
	/**
	 * Generate AWS presigned url for file upload
	 * 
	 * @param fileName Name of the file to be downloaded
	 * @param bucketName bucket from which file is downloaded
	 * @return presignedUrl
	 * @throws Exception 
	 */
	public final static String generatePresignedDownloadUrl(String fileName, String bucketName, String logicalFileName, AmazonS3 s3client) throws Exception
	{
		/*AmazonS3 s3client = new AmazonS3Client(
				new BasicAWSCredentials( S3_ACCESS_KEY, 
						S3_SECRET_KEY));*/

		try 
		{
			java.util.Date expiration = new java.util.Date();
			long milliSeconds = expiration.getTime();
			milliSeconds += 1000 * 60 * 60; // Add 1 hour.
			expiration.setTime(milliSeconds);

			GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, fileName);
			generatePresignedUrlRequest.setExpiration(expiration);
			//generatePresignedUrlRequest.setMethod(HttpMethod.GET);
			//generatePresignedUrlRequest.setExpiration(expiration);

			
			
			if(logicalFileName!=null)
			{
				ResponseHeaderOverrides headerOverrides =  new ResponseHeaderOverrides().withContentDisposition("filename=\""+logicalFileName+"\"");
				generatePresignedUrlRequest.setResponseHeaders(headerOverrides );
				try{
					    
					    generatePresignedUrlRequest.setMethod(HttpMethod.HEAD);
					    URL obj = new URL(s3client.generatePresignedUrl(generatePresignedUrlRequest).toString());
						HttpURLConnection con = (HttpURLConnection) obj.openConnection();
				 		// optional default is GET
						con.setRequestMethod("HEAD");
						l.info("Response Code:"+con.getResponseCode());
				 	    if(con.getResponseCode()==200)  {
					    	generatePresignedUrlRequest.setMethod(HttpMethod.GET);
					    	l.info("URL Logical File: "+s3client.generatePresignedUrl(generatePresignedUrlRequest).toString());
					    	return s3client.generatePresignedUrl(generatePresignedUrlRequest).toString();
					    } else {
					    	return EMPTY_STRING;
					    }
					
				}catch(Exception ex){
					throw new Exception(ex.getMessage());
				}
				
			}else if(logicalFileName==null ||  logicalFileName.equals(EMPTY_STRING)) {
					generatePresignedUrlRequest.setMethod(HttpMethod.GET);
					l.info("URL File: "+s3client.generatePresignedUrl(generatePresignedUrlRequest).toString());
					
					return s3client.generatePresignedUrl(generatePresignedUrlRequest).toString();
				}

		} 
		catch (AmazonServiceException exception) 
		{
			//System.out.println("Caught an AmazonServiceException, "
			//		+ "which means your request made it "
			//		+ "to Amazon S3, but was rejected with an error response "
			//		+ "for some reason.");
			
			l.error(exception.getMessage());
			l.error(exception.getStatusCode());
			//System.out.println("Error Message: " + exception.getMessage());
			//System.out.println("HTTP  Code: " + exception.getStatusCode());
			//System.out.println("AWS Error Code:" + exception.getErrorCode());
			//System.out.println("Error Type:    " + exception.getErrorType());
			//System.out.println("Request ID:    " + exception.getRequestId());
			
			throw new Exception(exception.getMessage());
		} 
		catch (AmazonClientException ace) 
		{
			//System.out.println("Caught an AmazonClientException, "
			//		+ "which means the client encountered "
			//		+ "an internal error while trying to communicate"
			//		+ " with S3, "
			//		+ "such as not being able to access the network.");
			//System.out.println("Error Message: " + ace.getMessage());
			l.error(ace.getMessage());
			throw new Exception(ace.getMessage());
		}
		
		return null;
	}
	
	
	
	public final static String generatePresignedUploadUrlStitch(String fileName, String bucketName, AmazonS3 s3client ) throws Exception
	{
		/*AmazonS3 s3client = new AmazonS3Client(
				new BasicAWSCredentials( S3_ACCESS_KEY, 
						S3_SECRET_KEY));*/

		try 
		{
			java.util.Date expiration = new java.util.Date();
			long milliSeconds = expiration.getTime();
			milliSeconds += 1000 * 60 * 60; // Add 1 hour.
			expiration.setTime(milliSeconds);

			GeneratePresignedUrlRequest generatePresignedUrlRequest = 
					new GeneratePresignedUrlRequest(bucketName, fileName);
			generatePresignedUrlRequest.setMethod(HttpMethod.PUT);
			generatePresignedUrlRequest.setExpiration(expiration);

			URL url = s3client.generatePresignedUrl(generatePresignedUrlRequest);
			
			return url.toString();
		} 
		catch (AmazonServiceException exception) 
		{
			//System.out.println("Caught an AmazonServiceException, "
			//		+ "which means your request made it "
			//		+ "to Amazon S3, but was rejected with an error response "
			//		+ "for some reason.");
			
			l.error("Caught an AmazonServiceException, "
					+ "which means your request made it "
					+ "to Amazon S3, but was rejected with an error response "
					+ "for some reason.");
			l.error(exception.getMessage() +" "+exception.getStatusCode()+" "+exception.getErrorCode());
			//System.out.println("Error Message: " + exception.getMessage());
			//System.out.println("HTTP  Code: " + exception.getStatusCode());
			//System.out.println("AWS Error Code:" + exception.getErrorCode());
			//System.out.println("Error Type:    " + exception.getErrorType());
			//System.out.println("Request ID:    " + exception.getRequestId());
			
			throw new Exception(exception.getMessage());
		} 
		catch (AmazonClientException ace) 
		{
			//System.out.println("Caught an AmazonClientException, "
			//		+ "which means the client encountered "
			//		+ "an internal error while trying to communicate"
			//		+ " with S3, "
			//		+ "such as not being able to access the network.");
			//System.out.println("Error Message: " + ace.getMessage());
			l.error(ace.getMessage());
			throw new Exception(ace.getMessage());
		}
		
		
	}
	
	public final static String generatePresignedDownloadUrlStitch(String fileName, String bucketName, AmazonS3 s3client) throws Exception
	{
		/*AmazonS3 s3client = new AmazonS3Client(
				new BasicAWSCredentials( S3_ACCESS_KEY, 
						S3_SECRET_KEY));*/

		try 
		{
			java.util.Date expiration = new java.util.Date();
			long milliSeconds = expiration.getTime();
			milliSeconds += 1000 * 60 * 60; // Add 1 hour.
			expiration.setTime(milliSeconds);

			GeneratePresignedUrlRequest generatePresignedUrlRequest = 
					new GeneratePresignedUrlRequest(bucketName, fileName);
			generatePresignedUrlRequest.setMethod(HttpMethod.GET);
			generatePresignedUrlRequest.setExpiration(expiration);

			
			
			
			URL url = s3client.generatePresignedUrl(generatePresignedUrlRequest);
			
			l.info("Pre-Signed URL = " + url.toString());
			return url.toString();
		} 
		catch (AmazonServiceException exception) 
		{
			//System.out.println("Caught an AmazonServiceException, "
			//		+ "which means your request made it "
			//		+ "to Amazon S3, but was rejected with an error response "
			//		+ "for some reason.");
			l.error(exception.getMessage());
			l.error(exception.getStatusCode());
			//System.out.println("Error Message: " + exception.getMessage());
			//System.out.println("HTTP  Code: " + exception.getStatusCode());
			//System.out.println("AWS Error Code:" + exception.getErrorCode());
			//System.out.println("Error Type:    " + exception.getErrorType());
			//System.out.println("Request ID:    " + exception.getRequestId());
			
			throw new Exception(exception.getMessage());
		} 
		catch (AmazonClientException ace) 
		{
			//System.out.println("Caught an AmazonClientException, "
			//		+ "which means the client encountered "
			//		+ "an internal error while trying to communicate"
			//		+ " with S3, "
			//		+ "such as not being able to access the network.");
			//System.out.println("Error Message: " + ace.getMessage());
			l.error(ace.getMessage());
			throw new Exception(ace.getMessage());
		}
		
		
	}
	public static void main(String[] args) throws IOException {
		/*GeneratePresignedUrlAndUploadObject obj = new GeneratePresignedUrlAndUploadObject();
		obj.generatePresignedDownloadUrl("2497/task", "2495", "gdhfj");*/
	}

}
