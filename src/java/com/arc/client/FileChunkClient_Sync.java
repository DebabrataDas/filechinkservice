package com.arc.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.metastatic.rsync.ChecksumPair;
import org.metastatic.rsync.Rdiff;

import com.arc.chunk.ChunkSetting;
import com.arc.dao.ChunkMeta;
import com.arc.dao.FileChunkMeta;
import com.arc.dao.FileChunkOffsets;
import com.arc.filechunk.s3utils.FileUpload;
import com.arc.util.FileReaderUtil;
import com.arc.util.conf.ArcConfig;
import com.arc.util.log.ArcLogger;
import com.bizosys.hsearch.treetable.compiler.FileWriterUtil;

public final class FileChunkClient_Sync 
{
	static Logger l = Logger.getLogger(FileChunkClient_Sync.class);
	private static final String UPLOAD = ArcConfig.UPLOAD_URL_TYPE;
	private static final String DOWNLOAD = ArcConfig.DOWNLOAD_URL_TYPE;
	private static final String CHUNK_FOLDER = "chunks";
	
	private static final QueryAndResponseBuilder responseBuilder = new QueryAndResponseBuilder();
	private static final FileChunkSplitter chunkSplitter = new FileChunkSplitter();
	private static final Rdiff rdiff = new Rdiff();
	
	public final void setContainerForAccount(final int accountId, final String containerUrl, String folderName) 
			throws IOException
	{
		QueryAndResponseBuilder responseBuilderHBase = new QueryAndResponseBuilder();
		//responseBuilderHBase.setContainerForAccount(accountId, containerUrl, folderName);
	}
	
	public final long getFileId() throws IOException
	{
		QueryAndResponseBuilder responseBuilderHBase = new QueryAndResponseBuilder();
		return responseBuilderHBase.getFileId();
	}
	
	public final long getRevisionId() throws IOException
	{
		QueryAndResponseBuilder responseBuilderHBase = new QueryAndResponseBuilder();
		return responseBuilderHBase.getRevisionId();
	}
	
	public final void insertOrUpdateFile(final String filePath, final int accountId, final String bucketName, final int projectId, 
			 long fileId, final long baseRevision,  long currentRevision, final boolean isNewRevision, final String folderName,final String ext) 
					throws IOException, NoSuchAlgorithmException
	{
		l.debug("Inside insertOrUpdateFile");
		File inputFile = new File(filePath);
		byte[] fileBytes = FileReaderUtil.getBytes(inputFile);
		String fileExtension = filePath.substring(filePath.lastIndexOf('.', filePath.length()-1) + 1);
		l.debug("Uploading file with fileId: "+fileId);
//		1 - Get cutting setting based of file extension
		ChunkSetting chunkSetting = ArcConfig.chunkSplitSettings.get(fileExtension);
		
// 		Get file thumbnail 
		FileThumbnail thumbnail = chunkSplitter.getFileThumbnail(fileBytes, 
				chunkSetting.magicSectionChunkSize);
		
		// Find duplicates for the file
		Map<Long, Set<Long>> duplicates = findPossibleDuplicates( accountId, thumbnail, folderName);
		
		
		// If we do not find duplicates, check whether it is a new version
		if ( null == duplicates )
		{
			l.debug("No Duplicates found");
			// If it is a new version
			if ( isNewRevision )
			{
				l.debug("The file has new revision.");
				// Get the s3 bucket and form the url
				String signatureFileName = fileId +"_" +baseRevision +".sign";
				
				// Download the signature. For simplicity we are loading from local path. Has to be a server call.
				l.debug("downloading sign file...................");
				String signatureFileUrlS = responseBuilder.getPresignedUrl(accountId, projectId, signatureFileName, DOWNLOAD, bucketName);
				URL signatureFileURL = new URL(signatureFileUrlS);
				
				File signatureFileLocal = new File("data/" +signatureFileName);
				FileWriterUtil.downloadToFile(signatureFileURL.openStream(), signatureFileLocal);
				
				// Create local patch file for uploading
				String patchFileName = fileId +"_" +currentRevision + ".patch";
			    File patchFileLocal = new File("data/" +patchFileName);
			    
				// Compute the patch file
			    l.debug("Making patch file");
			    List<ChecksumPair> sigs = rdiff.readSignatures(new FileInputStream(signatureFileLocal));
			    rdiff.makeDeltas(sigs, new FileInputStream(inputFile),new FileOutputStream(patchFileLocal));
				
				// Check whether patch file has less than 80% changes
				boolean rebaseFile = ( patchFileLocal.length() > (0.8 * inputFile.length()) ); 
				
				if ( rebaseFile )
				{
					System.out.println("CHANGES GREATER THAN 80%. REBASING FILE");
					// If there is a need to rebase, find the duplicate chunks in the account and the subsequent chunks to upload
					checkDuplicateChunksAndUploadNewFile(accountId,bucketName, projectId, fileId, currentRevision, 
							inputFile, fileBytes, thumbnail, chunkSetting, folderName,ext);
				}
				else
				{
					byte[] patchFileBytes = FileReaderUtil.getBytes(patchFileLocal);
					List<FileChunkMeta> fileChunkMetaL = chunkSplitter.getHeaderAndHash(patchFileBytes, 
													chunkSetting.magicSectionChunkSize, chunkSetting.magicSectionChunkSize
													, chunkSetting.hashAlgo);
					// Generate presigned urls for the patch file
					String patchUploadUrlS = responseBuilder.getPresignedUrl(accountId, projectId, patchFileName, UPLOAD, bucketName);
					URL patchUploadUrl = new URL(patchUploadUrlS);
					
					// Upload the patch file to S3
					l.debug("Uploading patch file");
					FileUpload.uploadObject( patchUploadUrl, patchFileBytes );
					
					// Update backend server
					
					final List<ChunkMeta> nonMatchingChunksL = new ArrayList<ChunkMeta>();
					
					ChunkMeta fileMeta = new ChunkMeta();
					fileMeta.accountId = accountId;
					fileMeta.chunkHeaderBase64 = fileChunkMetaL.get(0).chunkHeaderBase64;
					fileMeta.chunkHashBase64 = fileChunkMetaL.get(0).chunkHashBase64;
					nonMatchingChunksL.add(fileMeta);
					
					responseBuilder.uploadNewFile(accountId, bucketName, thumbnail.magicHeaderChunkBase64, thumbnail.startByte, 
							thumbnail.endByte, thumbnail.fileLength, fileId, baseRevision, currentRevision, projectId, 
							fileChunkMetaL, folderName,ext);
				}
			}
			else
			{
				// If it is a different file, find the duplicate chunks for the file and upload
				checkDuplicateChunksAndUploadNewFile(accountId, bucketName, projectId, fileId, currentRevision, inputFile, fileBytes, 
						thumbnail, chunkSetting, folderName,ext);
			}
		}
		else
		{
			l.debug("Total number of possible file: "+ duplicates.size());
			boolean matching = false;
			
			Set<Long> fileIdS = duplicates.keySet();
			for(long id : fileIdS)
			{
				Set<Long> revisionS = duplicates.get(id);
				for(long revision : revisionS)
				{
					//Get chunk offset for the file
					FileChunkOffsets fileChunkOffsets = responseBuilder.getFileOffsets(accountId, projectId, id, revision);
					//Cut file based on chunk offset
					List<FileChunkMeta> fileChunkMetaL = chunkSplitter.cutSpecifiedChunks(fileChunkOffsets, fileBytes, chunkSetting.hashAlgo);
					
					matching = responseBuilder.findMatchingChunksInFile(accountId, projectId, id, revision, fileChunkMetaL);
					
					if( matching ) break;
				}
				
				if( matching ) break;
			}
			
			if( matching )
			{
				//Exact duplicate found, upload after generating new fileid and revisionId
				
				fileId = getFileId();
				currentRevision = getRevisionId();
				checkDuplicateChunksAndUploadNewFile(accountId, bucketName, projectId, fileId, currentRevision, inputFile, fileBytes,
						thumbnail, chunkSetting, folderName,ext);
			}
			else
			{
				if( isNewRevision )
				{
					l.debug("File having new revision and No Exact matching found ");
					// Get the s3 bucket and form the url 
					String signatureFileName = fileId +"_" +baseRevision +".sign";
					
					// Download the signature. For simplicity we are loading from local path. Has to be a server call.
					String signatureFileUrlS = responseBuilder.getPresignedUrl(accountId, projectId, signatureFileName, DOWNLOAD, bucketName);
					URL signatureFileURL = new URL(signatureFileUrlS);
					l.debug("Downloading sign file");
					File signatureFileLocal = new File("data/" +signatureFileName);
					FileWriterUtil.downloadToFile(signatureFileURL.openStream(), signatureFileLocal);
					
					// Create local patch file for uploading
					String patchFileName = fileId +"_" +currentRevision + ".patch";
				    File patchFileLocal = new File("data/" +patchFileName);
				    
					// Compute the patch file
				    l.debug("making patch file");
				    List<ChecksumPair> sigs = rdiff.readSignatures(new FileInputStream(signatureFileLocal));
				    rdiff.makeDeltas(sigs, new FileInputStream(inputFile),new FileOutputStream(patchFileLocal));
					
					// Check whether patch file has less than 80% changes
					boolean rebaseFile = ( patchFileLocal.length() > (0.8 * inputFile.length()) ); 
					
					if ( rebaseFile )
					{
						System.out.println("CHANGES GREATER THAN 80%. REBASING FILE");
						// If there is a need to rebase, find the duplicate chunks in the account and the subsequent chunks to upload
						checkDuplicateChunksAndUploadRevisionFile(accountId, bucketName, projectId, fileId, currentRevision, currentRevision, inputFile,
								fileBytes, thumbnail, chunkSetting, folderName,ext);
					}
					else
					{
						byte[] patchFileBytes = FileReaderUtil.getBytes(patchFileLocal);
						List<FileChunkMeta> fileChunkMetaL = chunkSplitter.getHeaderAndHash(patchFileBytes, 
														chunkSetting.magicSectionChunkSize, chunkSetting.magicSectionChunkSize
														, chunkSetting.hashAlgo);
						// Generate presigned urls for the patch file
						String patchUploadUrlS = responseBuilder.getPresignedUrl(accountId, projectId, patchFileName, UPLOAD, bucketName);
						URL patchUploadUrl = new URL(patchUploadUrlS);
						
						// Upload the patch file to S3
						l.debug("Uploading patch file");
						FileUpload.uploadObject( patchUploadUrl, patchFileBytes );
						
						// Update backend server
						
						final List<ChunkMeta> nonMatchingChunksL = new ArrayList<ChunkMeta>();
						
						ChunkMeta fileMeta = new ChunkMeta();
						fileMeta.accountId = accountId;
						fileMeta.chunkHeaderBase64 = fileChunkMetaL.get(0).chunkHeaderBase64;
						fileMeta.chunkHashBase64 = fileChunkMetaL.get(0).chunkHashBase64;
						nonMatchingChunksL.add(fileMeta);
						
						responseBuilder.uploadRevisionFile(accountId, bucketName, thumbnail.magicHeaderChunkBase64, thumbnail.startByte, 
								thumbnail.endByte, thumbnail.fileLength, fileId, baseRevision, currentRevision, projectId, 
								fileChunkMetaL,ext);
					}
				}
				else
				{
					checkDuplicateChunksAndUploadNewFile(accountId, bucketName, projectId, fileId, currentRevision, inputFile, fileBytes, thumbnail,
							chunkSetting, folderName,ext);
				}
			}
		}
	}
	
	private void checkDuplicateChunksAndUploadRevisionFile(final int accountId, final String bucketName, final int projectId, final long fileId, 
			final long baseRevision, final long currentRevision, final File inputFile, final byte[] fileBytes, 
			final FileThumbnail thumbnail,final ChunkSetting chunkSetting, final String folderName,final String ext)
			throws IOException, NoSuchAlgorithmException, FileNotFoundException 
	{
//		2 - Cut the file according to the setting to get forward chunks
		List<FileChunkMeta> fileChunkMetaL = null;
		l.debug("CUTTING FILE INTO CHUNKS FOR THE FIRST TIME");
		fileChunkMetaL = chunkSplitter.cutFirstTimeChunks(fileBytes, chunkSetting.magicSectionChunkSize, 
				chunkSetting.contentSectionChunkSize, chunkSetting.hashAlgo);
		
		List<ChunkMeta> matchingChunksL = getMatchingChunksInAccount(fileChunkMetaL, accountId);
		List<ChunkMeta> nonMatchingChunksL = getChunksToUpload(matchingChunksL, fileChunkMetaL, accountId);
		
		// Create signature file in local path
		l.debug("making sign file");
		String signatureFileName = fileId +"_" +currentRevision +".sign";
		File signatureFileLocal = new File("data/" +signatureFileName);
	    rdiff.makeSignatures(new FileInputStream(inputFile), new FileOutputStream(signatureFileLocal));
		
		// Generate presigned url for signature file
		String signatureUploadUrlString = responseBuilder.getPresignedUrl(accountId, projectId, signatureFileName, UPLOAD, bucketName);
		URL signatureUploadUrl = new URL(signatureUploadUrlString);
		l.debug("uploading sign file");
		FileUpload.uploadObject(signatureUploadUrl, FileReaderUtil.getBytes(signatureFileLocal));
	    
		
		// Generate presigned url for each chunk
		if( null != nonMatchingChunksL )
		{
			for ( ChunkMeta chunkToUpload : nonMatchingChunksL )
			{
				String chunkFileName = chunkToUpload.chunkHeaderBase64 +"_" +chunkToUpload.chunkHashBase64;
				String chunkUploadUrl = responseBuilder.getPresignedUrl(accountId, projectId, chunkFileName, UPLOAD, bucketName);
				try
				{
					l.debug("uploading chunk with key:"+chunkFileName);
					FileUpload.uploadObject(new URL(chunkUploadUrl), chunkToUpload.chunkBytes);
				}
				catch( Exception e )
				{
					throw e;
				}
			}
		}
	    
		// Upload the chunks and the signature
		responseBuilder.uploadRevisionFile(accountId,bucketName, thumbnail.magicHeaderChunkBase64, thumbnail.startByte, 
				thumbnail.endByte, thumbnail.fileLength, fileId, baseRevision, currentRevision, projectId, 
				fileChunkMetaL,ext);
	}
	
	public void downloadFile(final int accountId, final String bucketName, final int projectId, final String fileId, final String revision, final String folderName, final String logicalFileName ) throws IOException
	{
		String fileName = fileId +"_" +revision;
		//String fileName = "UmFyIRoHAM8=_MmNhM2EyOWJkZDU3NTY2NzdiMmIyMWU2YjVlNmFmY2E=";
		String downloadUrl = responseBuilder.getPresignedDownloadUrl(accountId, projectId, fileName, DOWNLOAD, bucketName, logicalFileName);
		System.out.println(downloadUrl);
	}
	
	public void deleteFile( final String filePath, final int accountId, final int projectId, final long fileId, 
						final long revision ) throws IOException
	{
		File inputFile = new File(filePath);
		byte[] fileBytes = FileReaderUtil.getBytes(inputFile);
		
		List<FileChunkMeta> fileChunkMetaL = responseBuilder.getChunksInFile(accountId, fileId, revision, projectId);
		ChunkSetting chunkSetting = ArcConfig.chunkSplitSettings.get("txt");
		FileThumbnail thumbnail = chunkSplitter.getFileThumbnail(fileBytes, chunkSetting.magicSectionChunkSize);
		responseBuilder.deleteFile(accountId, thumbnail.magicHeaderChunkBase64, thumbnail.startByte, thumbnail.endByte,
							thumbnail.fileLength, fileId, revision, projectId, fileChunkMetaL);
	}

		
	private void checkDuplicateChunksAndUploadNewFile(final int accountId, final String bucketName, final int projectId, final long fileId, 
			final long currentRevision, final File inputFile, final byte[] fileBytes, 
			final FileThumbnail thumbnail, 
			final ChunkSetting chunkSetting, final String folderName,final String ext) throws IOException, NoSuchAlgorithmException, FileNotFoundException 
	{
//		2 - Cut the file according to the setting to get forward chunks
		List<FileChunkMeta> fileChunkMetaL = null;
		l.debug("cutting file into chunks for the first time");
		fileChunkMetaL = chunkSplitter.cutFirstTimeChunks(fileBytes, chunkSetting.magicSectionChunkSize, 
				chunkSetting.contentSectionChunkSize, chunkSetting.hashAlgo);

		List<ChunkMeta> matchingChunksL = getMatchingChunksInAccount(fileChunkMetaL, accountId);

		List<ChunkMeta> nonMatchingChunksL = getChunksToUpload(matchingChunksL, fileChunkMetaL, accountId);
		
		
		// Create signature file in local path
		l.debug("making sign file");
		String signatureFileName = fileId +"_" +currentRevision +".sign";
		File signatureFileLocal = new File("data/" +signatureFileName);
	    rdiff.makeSignatures(new FileInputStream(inputFile), new FileOutputStream(signatureFileLocal));
		
		// Generate presigned url for signature file
		String signatureUploadUrlString = responseBuilder.getPresignedUrl(accountId, projectId, signatureFileName, UPLOAD, bucketName);
		URL signatureUploadUrl = new URL(signatureUploadUrlString);
		l.debug("uploading sign file");
		FileUpload.uploadObject(signatureUploadUrl, FileReaderUtil.getBytes(signatureFileLocal));
	    
		int i =0;
		List<ChunkMeta> successfulUploadChunks = new ArrayList<ChunkMeta>();
		
		// Generate presigned url for each chunk
		if( null != nonMatchingChunksL)
		{
			l.debug("Total chunk to upload: "+nonMatchingChunksL.size());
			System.out.println(nonMatchingChunksL.size());
			for ( ChunkMeta chunkToUpload : nonMatchingChunksL )
			{
				
				ChunkMeta chm = new ChunkMeta();
				String chunkFileName = chunkToUpload.chunkHeaderBase64 +"_" +chunkToUpload.chunkHashBase64;
				String chunkUploadUrl = responseBuilder.getPresignedUrl(accountId, projectId, chunkFileName, UPLOAD, bucketName);
				System.out.println(chunkUploadUrl);
				try
				{
					i++;
					
					l.debug("uploading chunks: "+chunkFileName);
					FileUpload.uploadObject(new URL(chunkUploadUrl), chunkToUpload.chunkBytes);
					chm.accountId = accountId;
					chm.chunkHeaderBase64 = chunkToUpload.chunkHeaderBase64;
					chm.chunkHashBase64 = chunkToUpload.chunkHashBase64;
					
					successfulUploadChunks.add(chm);
					
					//if(i>=2)
					//responseBuilder.insertToChunkTable(successfulUploadChunks);
					
				}
				catch( Exception e )
				{
					throw e;
				}
			}
		}
		else{
			l.debug("Nonmatching ins null");
		
		}
	    
		// Upload the chunks and the signature
		responseBuilder.uploadNewFile(accountId, bucketName, thumbnail.magicHeaderChunkBase64, thumbnail.startByte, 
				thumbnail.endByte, thumbnail.fileLength, fileId, currentRevision, currentRevision, projectId, 
				fileChunkMetaL, folderName,ext);
	}
	
	private final List<ChunkMeta> getChunksToUpload( final List<ChunkMeta> matchingChunksL, 
			final List<FileChunkMeta> fileChunkMetaL, final int accountId )
	{
		l.debug("Inside getChunksToUpload");
		List<ChunkMeta> nonMatchingChunksL = null;
		if ( null != matchingChunksL )
		{
			for ( FileChunkMeta fileChunkMeta : fileChunkMetaL )
			{
				boolean isPresent = false;
				for ( ChunkMeta matchingChunk : matchingChunksL )
				{
					if ( fileChunkMeta.chunkHashBase64.equals(matchingChunk.chunkHashBase64)
							&& fileChunkMeta.chunkHeaderBase64.equals(matchingChunk.chunkHeaderBase64) )
					{
						isPresent = true;
						break;
					}
				}
				if ( !isPresent ) 
				{
					if ( null == nonMatchingChunksL ) 
						nonMatchingChunksL = new ArrayList<ChunkMeta>();
					
					ChunkMeta nonMatchingChunk = new ChunkMeta();
					nonMatchingChunk.accountId = accountId;
					nonMatchingChunk.chunkHashBase64 = fileChunkMeta.chunkHashBase64;
					nonMatchingChunk.chunkHeaderBase64 = fileChunkMeta.chunkHeaderBase64;
					nonMatchingChunk.chunkBytes = fileChunkMeta.chunkBytes;
					nonMatchingChunksL.add(nonMatchingChunk);
				}
			}
		}
		else
		{
			for ( FileChunkMeta fileChunkMeta : fileChunkMetaL )
			{
				if ( null == nonMatchingChunksL ) 
					nonMatchingChunksL = new ArrayList<ChunkMeta>();
				
				ChunkMeta nonMatchingChunk = new ChunkMeta();
				nonMatchingChunk.accountId = accountId;
				nonMatchingChunk.chunkHashBase64 = fileChunkMeta.chunkHashBase64;
				nonMatchingChunk.chunkHeaderBase64 = fileChunkMeta.chunkHeaderBase64;
				nonMatchingChunk.chunkBytes = fileChunkMeta.chunkBytes;
				nonMatchingChunksL.add(nonMatchingChunk);
			}
		}
		//l.debug("Total chunks to Upload: "+nonMatchingChunksL.size());
		return nonMatchingChunksL;
	}
	
	private final List<ChunkMeta> getMatchingChunksInAccount(final List<FileChunkMeta> fileChunkMetaL,
			final int accountId) throws IOException
	{
//		3 - Find the duplicate chunk in the accountId
		return responseBuilder.getMatchingChunksInAccount(accountId, fileChunkMetaL);
	}
	
	private final Map<Long, Set<Long>> findPossibleDuplicates( final int accountId, 
			final FileThumbnail thumbnail , final String folderName) throws IOException
	{
		
//		2- Find any possible match For the File
		return responseBuilder.findPossibleMatch(accountId, 
				thumbnail.magicHeaderChunkBase64, thumbnail.startByte, thumbnail.endByte, 
				thumbnail.fileLength, folderName);
	}
	
	public static void main(String[] args) throws IOException, NoSuchAlgorithmException 
	{
		FileChunkClient_Sync client = new FileChunkClient_Sync();

		int accountId = 11616507;
		int projectId = 269;

		long fileId;
		long previousRevision;
		long currentRevision;
		boolean isNewRevision;
		long start;
		long end;
		String folderName= Integer.toString(projectId);
		
		
		
 
//		//String container = "chunk-info-file-upload";
		//String container = "filechunk-test-dev1";
		//String container = Integer.toString(accountId);
		String bucketName = "skysite-production";
//		// 1 - Set Container Path for the account id - Only once when a bucket is added into s3
     client.setContainerForAccount(accountId, bucketName, folderName);
//		
		// Vanilla System. Insert a new file.
			
		fileId = client.getFileId();
		currentRevision = client.getRevisionId();
		
		previousRevision = -1;
		//previousRevision = 604424069;
		
		isNewRevision = false;
		System.out.println("fileId: "+fileId+" and revisionId: "+currentRevision);
      
		start = System.currentTimeMillis();
		l.debug("************FILE UPLOAD STARTED WITH FILE_ID: "+fileId+" ACCOUNT_ID:"+accountId+"***************");
		
	/*	String fileName="/home/arc/Downloads/java_tutorial.pdf";
		String ext= ".pdf";*/
		
		String fileName="/home/rajar/Desktop/Ambari_Users_Guide_Hortonworks_Installation-20150721.pdf";
		String ext= ".pdf";
		client.insertOrUpdateFile( fileName, accountId, bucketName, projectId, fileId, previousRevision, currentRevision, isNewRevision, folderName,ext );

		responseBuilder.getChunksInFile(accountId, fileId, currentRevision, projectId);
		end = System.currentTimeMillis();
		//l.debug("************** FILE UPLOAD ENDED WITH  FILE_ID: "+fileId+" ACCOUNT_ID:"+accountId+"***************");
		System.out.println("Time taken - " + (end - start));
		
//		previousRevision = currentRevision;
//		
//		// Insert a duplicate file
//		isNewRevision = true;
//		currentRevision = client.getRevisionId();
//		client.insertOrUpdateFile( "testData/igbase.bmp", accountId, projectId, fileId, previousRevision, currentRevision, isNewRevision );
//		
//		// Insert end changed file 
//		currentRevision = client.getRevisionId();
//		isNewRevision = true;
//		client.insertOrUpdateFile( "testData/igbase_1.bmp", accountId, projectId, fileId, previousRevision, currentRevision, isNewRevision );
//		
//		//Insert begining changed file
//		currentRevision = client.getRevisionId();
//		isNewRevision = true;
//		client.insertOrUpdateFile( "testData/2_beg.png", accountId, projectId, fileId, previousRevision, currentRevision, isNewRevision );
//		
//		//Insert begining and end changed file
//		currentRevision = client.getRevisionId();
//		isNewRevision = true;
//		client.insertOrUpdateFile( "testData/2_beg_end.png", accountId, projectId, fileId, previousRevision, currentRevision, isNewRevision );
//		
//		//Insert all upload changed file
//		currentRevision = client.getRevisionId();
//		isNewRevision = true;
//		client.insertOrUpdateFile( "testData/2_all_upd.png", accountId, projectId, fileId, previousRevision, currentRevision, isNewRevision );
//		
//		// Insert a different file
//		fileId = 1003;
//		previousRevision = -1;
//		currentRevision = 0;
//		isNewRevision = false;
//		client.insertOrUpdateFile( "data/input2.txt", accountId, projectId, fileId, previousRevision, currentRevision, isNewRevision );
//		
//		// Download a file
//		fileId = 15215742;
//		currentRevision = 15418967;
		long start3 = System.currentTimeMillis();
		//client.downloadFile(accountId, projectId, "UmFyIRoHAM8=", "MmNhM2EyOWJkZDU3NTY2NzdiMmIyMWU2YjVlNmFmY2E=", folderName, "manishs3");
		long end3 = System.currentTimeMillis();
		
		System.out.println("download time is :"+(end3-start3));
        
//		// Delete a file
//		fileId = 1001;
//		currentRevision = 0;
//		client.deleteFile("data/input.txt", accountId, projectId, fileId, currentRevision);
		
	}
}