package com.arc.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;

import com.arc.dao.ChunkMeta;
import com.arc.dao.ChunkOffsets;
import com.arc.dao.FileChunkMeta;
import com.arc.dao.FileChunkOffsets;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public final class QueryAndResponseBuilder
{
	static Logger l = Logger.getLogger(QueryAndResponseBuilder.class);
	private static final String CHUNK_HEADER = "chunkHeader";
	private static final char REVISION_FILE_THUMBNAIL = 'A';
	private static final char NEW_FILE_THUMBNAIL = 'N';
	private static final String REQUEST_PARAM_KEY = "jsondata";
	private static final String RESPONSE_JSON_HEAD = "values";
	
	private static final String MIMETYPE_VALUE = "text/*";
	private static final String MIMETYPE = "Mimetype";
	//private static final String baseUrlRestAPI = "http://192.168.133.213:8080/arcfilechunk/rest"; 
	//private static final String baseUrlPresign = "http://192.168.133.213:8080/arcfilechunk/presign"; 
	//private static final String baseUrlRestAPI = "http://192.168.133.155/arcfilechunk/rest"; 
	//private static final String baseUrlPresign = "http://192.168.133.155/arcfilechunk/presign"; 


    //private static final String baseUrlRestAPI = "http://10.1.1.231:8080/arcfilechunkTEST/rest"; 
	//private static final String baseUrlPresign = "http://10.1.1.231:8080/arcfilechunkTEST/presign"; 

	//private static final String baseUrlRestAPI = "http://uploadfc-skysite-com-1954243555.us-west-1.elb.amazonaws.com/arcfilechunk/rest"; 
	//private static final String baseUrlPresign = "http://uploadfc-skysite-com-1954243555.us-west-1.elb.amazonaws.com/arcfilechunk/presign"; 
	
	//private static final String baseUrlRestAPI = "http://192.168.50.43/arcfilechunkSKYSITE/rest"; 
	//private static final String baseUrlPresign = "http://192.168.50.43/arcfilechunkSKYSITE/presign"; 
	
/*	private static final String baseUrlRestAPI = "http://10.98.10.21/arcfilechunkSKYSITE/rest"; 
	private static final String baseUrlPresign = "http://10.98.10.21/arcfilechunkSKYSITE/presign";*/
	
	/*private static final String baseUrlRestAPI = "http://192.168.50.43/arcfilechunkSKYSITE/rest"; 
	private static final String baseUrlPresign = "http://192.168.50.43/arcfilechunkSKYSITE/presign";*/
		
	private static final String baseUrlRestAPI = "http://plfcstg-planwellcollaborate-com-521794439.us-west-1.elb.amazonaws.com/arcfilechunk/rest"; 
	private static final String baseUrlPresign = "http://plfcstg-planwellcollaborate-com-521794439.us-west-1.elb.amazonaws.com/arcfilechunk/presign"; 
	
	//private static final String baseUrlRestAPI = "http://chunk-ELB-1670394128.us-west-1.elb.amazonaws.com/arcfilechunk/rest"; 
	//private static final String baseUrlPresign = "http://chunk-ELB-1670394128.us-west-1.elb.amazonaws.com/arcfilechunk/presign"; 

	
	private static final String CHUNK_HASH = "chunkHash";
	private static final String CHUNKS_JSON = "chunksJSON";
	private static final String CHUNK_OFFSET_JSON = "chunkOffsetJSON";
	private static final String S3_CONTAINER_PATH = "s3ContainerPath";
	private static final String FOLDER_NAME = "folderName";
	private static final String LOGICAL_FILE_NAME = "logicalFileName";
	private static final String ACCOUNT_ID = "accountId";
	private static final String PROJECT_ID = "projectId";
	private static final String REVISION = "revision";
	private static final String BASE_REVISION = "baseRevision";
	private static final String FILE_ID = "fileId";
	private static final String REVISION_ID = "revisionId";
	private static final String FILE_LENGTH = "fileLength";
	private static final String END_BYTE = "endByte";
	private static final String START_BYTE = "startByte";
	private static final String MAGIC_HEADER = "magicHeader";
	private static final String THUMBNAIL_TYPE = "thumbnailType";
	private static final String CHUNKS_REFERENCED = "chunksReferenced";
	
	private static final char SQUARE_BRACKET_START = '[';
	private static final char SQUARE_BRACKET_END = ']';
	private static final char CURLY_BRACE_START = '{';
	private static final char CURLY_BRACE_END = '}';
	private static final char SLASH = '\\';
	private static final char COMMA = ',';
	private static final char COLON = ':';
	private static final char QUOTE = '"';
	
	private static final String ACTION = "action";
	private static final String EXT_NAME = "ext";
	private static final String ROW_KEY = "rowKey";
	
	private StringBuilder queryB = new StringBuilder();
	
	private final String getResponse() throws IOException
	{
		System.out.println(queryB);
		Connection.Response response = Jsoup.connect(baseUrlRestAPI)
		.header(MIMETYPE, MIMETYPE_VALUE)
		.ignoreContentType(true)
		.data(REQUEST_PARAM_KEY, queryB.toString()).timeout(90000)
		.method(Method.POST).execute();
		
		return response.body();
	}
	
	//DONE
	public final void uploadNewFile( final int accountId, final String bucketName, final String magicHeader, 
			final short startByte, final short endByte, final int fileLength, final long fileId, 
			final long baseRevision, final long currentRevision, final int projectId,
			final List<FileChunkMeta> fileChunkMetaL, final String folderName,final String ext ) throws IOException
	{
		queryB.setLength(0);
			
		String action = "UPLOAD_FILE";
		queryB.append(CURLY_BRACE_START)
			.append(QUOTE).append(ACTION).append(QUOTE).append(COLON)
			.append(QUOTE).append(action).append(QUOTE).append(COMMA)
			.append(QUOTE).append(THUMBNAIL_TYPE).append(QUOTE).append(COLON)
			.append(QUOTE).append(NEW_FILE_THUMBNAIL).append(QUOTE).append(COMMA)
			.append(ACCOUNT_ID).append(COLON).append(accountId).append(COMMA)
			.append("bucketName").append(COLON).append(bucketName).append(COMMA)
			.append(MAGIC_HEADER).append(COLON).append(QUOTE).append(magicHeader)
			.append(QUOTE).append(COMMA)
			.append(START_BYTE).append(COLON).append(startByte).append(COMMA)
			.append(END_BYTE).append(COLON).append(endByte).append(COMMA)
			.append(FILE_LENGTH).append(COLON).append(fileLength).append(COMMA)
			.append(FILE_ID).append(COLON).append(fileId).append(COMMA)
			.append(REVISION).append(COLON).append(currentRevision).append(COMMA)
			.append(BASE_REVISION).append(COLON).append(baseRevision).append(COMMA)
			.append(FOLDER_NAME).append(COLON).append(folderName).append(COMMA)
			.append(EXT_NAME).append(COLON).append(ext).append(COMMA)
			.append(PROJECT_ID).append(COLON).append(projectId).append(COMMA);
			
		boolean isFirst = true;
		StringBuilder offsetJsonB = new StringBuilder();
		StringBuilder hashJsonB = new StringBuilder();
		StringBuilder chunksReferenced = new StringBuilder();
		
		for ( FileChunkMeta fileMeta : fileChunkMetaL )
		{
			if ( isFirst ) 
			{
				offsetJsonB.append(SQUARE_BRACKET_START);
				hashJsonB.append(SQUARE_BRACKET_START);
				chunksReferenced.append(SQUARE_BRACKET_START);
				isFirst = false;
			}
			else 
			{
				offsetJsonB.append(COMMA);
				hashJsonB.append(COMMA);
				chunksReferenced.append(COMMA);
			}
			
			offsetJsonB.append(CURLY_BRACE_START).append("chunkOffset").append(COLON)
				.append(fileMeta.offset).append(COMMA)
				.append("chunkLength").append(COLON).append(fileMeta.chunkLength)
				.append(CURLY_BRACE_END);

			hashJsonB.append(CURLY_BRACE_START).append(CHUNK_HEADER).append(COLON)
				.append(SLASH).append(QUOTE).append(fileMeta.chunkHeaderBase64).append(SLASH)
				.append(QUOTE).append(COMMA)
			.append("chunkHash").append(COLON).append(SLASH).append(QUOTE)
				.append(fileMeta.chunkHashBase64).append(SLASH).append(QUOTE)
			.append(CURLY_BRACE_END);

			chunksReferenced.append(CURLY_BRACE_START)
				.append(CHUNK_HEADER).append(COLON).append(QUOTE).append(fileMeta.chunkHeaderBase64)
					.append(QUOTE).append(COMMA)
				.append(CHUNK_HASH).append(COLON).append(QUOTE).append(fileMeta.chunkHashBase64)
					.append(QUOTE)
			.append(CURLY_BRACE_END);
		}
		offsetJsonB.append(SQUARE_BRACKET_END);
		hashJsonB.append(SQUARE_BRACKET_END);
		chunksReferenced.append(SQUARE_BRACKET_END);
		
		queryB.append(CHUNK_OFFSET_JSON).append(COLON).append(QUOTE)
			.append(offsetJsonB.toString()).append(QUOTE).append(COMMA)
				.append(CHUNKS_JSON).append(COLON).append(QUOTE).append(hashJsonB.toString())
					.append(QUOTE).append(COMMA)
				.append(CHUNKS_REFERENCED).append(COLON).append(chunksReferenced.toString())
			.append(CURLY_BRACE_END);
		
		
		System.out.println(queryB);
		String responseBody = getResponse();

		System.out.println(responseBody);
		
		
		
		System.out.println("Status for method UPLOAD FILE ended");
		
	}
	
	//DONE
	public final Map<Long, Set<Long>> findPossibleMatch(final int accountId, 
			final String magicHeader, final short startByte, final short endByte, 
			final int fileLength, final String folderName) throws IOException
	{
		queryB.setLength(0);
				
		String action = "FIND_POSSIBLE_FILE_DUPLICATE";
		queryB.append(CURLY_BRACE_START)
			.append(QUOTE).append(ACTION).append(QUOTE).append(COLON)
			.append(QUOTE).append(action).append(QUOTE).append(COMMA)
			.append(ACCOUNT_ID).append(COLON).append(accountId).append(COMMA)
			.append(MAGIC_HEADER).append(COLON).append(QUOTE).append(magicHeader).append(QUOTE)
				.append(COMMA)
			.append(START_BYTE).append(COLON).append(startByte).append(COMMA)
			.append(END_BYTE).append(COLON).append(endByte).append(COMMA)
			.append(FILE_LENGTH).append(COLON).append(fileLength)
		.append(CURLY_BRACE_END);
		
		String responseBody = getResponse();
		
		JsonElement element = new JsonParser().parse(responseBody);
		JsonArray responseArray = element.getAsJsonObject().getAsJsonArray(RESPONSE_JSON_HEAD);
		Map<Long, Set<Long>> fileRevisionM = new HashMap<Long, Set<Long>>();
		
		if ( responseArray.size() > 0 )
		{
			for ( int i = 0; i < responseArray.size(); i++ )
			{
				long fileId = Long.parseLong(responseArray.get(i).getAsJsonObject()
						.getAsJsonPrimitive("fileId").getAsString());
				long revision = Long.parseLong(responseArray.get(i).getAsJsonObject()
						.getAsJsonPrimitive("revision").getAsString());
				
				if ( fileRevisionM.containsKey(fileId) )
				{
					fileRevisionM.get(fileId).add(revision);
				}
				else
				{
					Set<Long> revisionSet = new TreeSet<Long>();
					revisionSet.add(revision);
					fileRevisionM.put(fileId, revisionSet);
				}
			}
			return fileRevisionM;
		}
		
		return null;
	}
	
	//DONE
	public final FileChunkOffsets getFileOffsets(final int accountId, final int projectId, 
			final long fileId, final long revision) throws IOException
	{
		String action = "FIND_FILE_CHUNK_OFFSETS";
		queryB.setLength(0);
				
		queryB.append(CURLY_BRACE_START)
			.append(QUOTE).append(ACTION).append(QUOTE).append(COLON)
			.append(QUOTE).append(action).append(QUOTE).append(COMMA)
			.append(ACCOUNT_ID).append(COLON).append(accountId).append(COMMA)
			.append(PROJECT_ID).append(COLON).append(projectId).append(COMMA)
			.append(FILE_ID).append(COLON).append(fileId).append(COMMA)
			.append(REVISION).append(COLON).append(revision)
		.append(CURLY_BRACE_END);
		
		String responseBody = getResponse();
		JsonElement element = new JsonParser().parse(responseBody);
		JsonArray responseArray = element.getAsJsonObject().getAsJsonArray(RESPONSE_JSON_HEAD);
		
		FileChunkOffsets fileChunkOffsets = null;
		if ( responseArray.size() > 0 )
		{
			for ( int i = 0; i < responseArray.size(); i++ )
			{
				fileChunkOffsets = new FileChunkOffsets();
				List<ChunkOffsets> chunkOffsetsL = new ArrayList<ChunkOffsets>();
				
				JsonObject offsetJson = responseArray.get(i).getAsJsonObject();
				fileChunkOffsets.accountId = offsetJson.getAsJsonPrimitive("accountId").getAsInt();
				fileChunkOffsets.projectId = offsetJson.getAsJsonPrimitive("projectId").getAsInt();
				fileChunkOffsets.fileId = offsetJson.getAsJsonPrimitive("fileId").getAsInt();
				fileChunkOffsets.revision = offsetJson.getAsJsonPrimitive("revision").getAsInt();
				
				JsonArray offsetArray = new JsonParser()
					.parse(offsetJson.getAsJsonPrimitive("chunkOffsetJSON").getAsString())
					.getAsJsonArray();
				for ( int j = 0; j < offsetArray.size(); j++ )
				{
					JsonObject obj = offsetArray.get(j).getAsJsonObject();
					ChunkOffsets chunkOffset = new ChunkOffsets();
					chunkOffset.offset = obj.getAsJsonPrimitive("chunkOffset").getAsLong(); 
					chunkOffset.length = obj.getAsJsonPrimitive("chunkLength").getAsInt();
					chunkOffsetsL.add(chunkOffset);
				}
				fileChunkOffsets.chunkOffsets = chunkOffsetsL;
			}
		}
		return fileChunkOffsets;
	}
	
	//DONE
	public final boolean findMatchingChunksInFile(final int accountId, final int projectId, 
			final long fileId, final long revision, 
			final List<FileChunkMeta> fileChunkMetaL) throws IOException
	{
		String action = "FIND_DUPLICATE_FILE";
		queryB.setLength(0);
		
		queryB.append(CURLY_BRACE_START)
			.append(QUOTE).append(ACTION).append(QUOTE).append(COLON)
			.append(QUOTE).append(action).append(QUOTE).append(COMMA)
			.append(ACCOUNT_ID).append(COLON).append(accountId).append(COMMA)
			.append(PROJECT_ID).append(COLON).append(projectId).append(COMMA)
			.append(FILE_ID).append(COLON).append(fileId).append(COMMA)
			.append(REVISION).append(COLON).append(revision).append(COMMA)
			.append(CHUNKS_JSON).append(COLON);
				
		boolean isFirst = true;
		StringBuilder hashJsonB = new StringBuilder();

		for ( FileChunkMeta fileMeta : fileChunkMetaL )
		{
			if ( isFirst ) 
			{
				hashJsonB.append(QUOTE).append('[');
				isFirst = false;
			}
			else 
			{
				hashJsonB.append(COMMA);
			}
			
			hashJsonB.append('{').append(CHUNK_HEADER).append(':').append(SLASH)
				.append(QUOTE).append(fileMeta.chunkHeaderBase64).append(SLASH)
				.append(QUOTE).append(COMMA)
			.append("chunkHash").append(':').append(SLASH).append(QUOTE)
				.append(fileMeta.chunkHashBase64).append(SLASH).append(QUOTE).append('}');
			
		}
		hashJsonB.append(']').append(QUOTE);
				
			queryB.append(hashJsonB.toString())
		.append(CURLY_BRACE_END);
		
		String responseBody = getResponse();
		JsonElement element = new JsonParser().parse(responseBody);
		JsonArray responseArray = element.getAsJsonObject().getAsJsonArray(RESPONSE_JSON_HEAD);
		for ( int i = 0; i < responseArray.size(); i++ )
		{
			String chunksJson = responseArray.get(i).getAsJsonObject()
					.getAsJsonPrimitive("chunksJSON").getAsString();
			if ( null != chunksJson ) return true;
		}
		return false;
	}
	
	//DONE
	public final void uploadRevisionFile(final int accountId, final String bucketName,  final String magicHeader, 
			final byte startByte, final byte endByte, final int fileLength, final long fileId, 
			final long baseRevision, final long currentRevision, final int projectId, 
			final List<FileChunkMeta> fileChunkMetaL,final String ext) throws IOException
	{
		queryB.setLength(0);
		
		String action = "UPLOAD_FILE";
		queryB.append(CURLY_BRACE_START)
			.append(QUOTE).append(ACTION).append(QUOTE).append(COLON)
			.append(QUOTE).append(action).append(QUOTE).append(COMMA)
			.append(QUOTE).append(THUMBNAIL_TYPE).append(QUOTE).append(COLON)
			.append(QUOTE).append(REVISION_FILE_THUMBNAIL).append(QUOTE).append(COMMA)
			.append(ACCOUNT_ID).append(COLON).append(accountId).append(COMMA)
			.append("bucketName").append(COLON).append(bucketName).append(COMMA)
			.append(MAGIC_HEADER).append(COLON).append(QUOTE).append(magicHeader)
				.append(QUOTE).append(COMMA)
			.append(START_BYTE).append(COLON).append(startByte).append(COMMA)
			.append(END_BYTE).append(COLON).append(endByte).append(COMMA)
			.append(FILE_LENGTH).append(COLON).append(fileLength).append(COMMA)
			.append(FILE_ID).append(COLON).append(fileId).append(COMMA)
			.append(REVISION).append(COLON).append(currentRevision).append(COMMA)
			.append(BASE_REVISION).append(COLON).append(baseRevision).append(COMMA)
			.append(EXT_NAME).append(COLON).append(ext).append(COMMA)
			.append(PROJECT_ID).append(COLON).append(projectId).append(COMMA);
		
		boolean isFirst = true;
		StringBuilder offsetJsonB = new StringBuilder();
		StringBuilder hashJsonB = new StringBuilder();
		StringBuilder chunksReferenced = new StringBuilder();
		
		for ( FileChunkMeta fileMeta : fileChunkMetaL )
		{
			if ( isFirst ) 
			{
				offsetJsonB.append(SQUARE_BRACKET_START);
				hashJsonB.append(SQUARE_BRACKET_START);
				chunksReferenced.append(SQUARE_BRACKET_START);
				isFirst = false;
			}
			else 
			{
				offsetJsonB.append(COMMA);
				hashJsonB.append(COMMA);
				chunksReferenced.append(COMMA);
			}
			

			
			offsetJsonB.append(CURLY_BRACE_START).append("chunkOffset").append(COLON)
				.append(fileMeta.offset).append(COMMA)
				.append("chunkLength").append(COLON).append(fileMeta.chunkLength)
				.append(CURLY_BRACE_END);

			hashJsonB.append(CURLY_BRACE_START).append(CHUNK_HEADER).append(COLON)
				.append(SLASH).append(QUOTE).append(fileMeta.chunkHeaderBase64).append(SLASH)
				.append(QUOTE).append(COMMA)
			.append("chunkHash").append(COLON).append(SLASH).append(QUOTE)
				.append(fileMeta.chunkHashBase64).append(SLASH).append(QUOTE)
			.append(CURLY_BRACE_END);
			
			chunksReferenced.append(CURLY_BRACE_START)
				.append(CHUNK_HEADER).append(COLON).append(QUOTE).append(fileMeta.chunkHeaderBase64)
					.append(QUOTE).append(COMMA)
				.append(CHUNK_HASH).append(COLON).append(QUOTE).append(fileMeta.chunkHashBase64)
					.append(QUOTE)
			.append(CURLY_BRACE_END);
		}
		offsetJsonB.append(SQUARE_BRACKET_END);
		hashJsonB.append(SQUARE_BRACKET_END);
		chunksReferenced.append(SQUARE_BRACKET_END);
		
		queryB.append(CHUNK_OFFSET_JSON).append(COLON).append(QUOTE)
			.append(offsetJsonB.toString()).append(QUOTE).append(COMMA)
				.append(CHUNKS_JSON).append(COLON).append(QUOTE).append(hashJsonB.toString())
					.append(QUOTE).append(COMMA)
				.append(CHUNKS_REFERENCED).append(COLON).append(chunksReferenced.toString())
			.append(CURLY_BRACE_END);
	
		getResponse();
		
	}
	
	//DONE
	public final List<ChunkMeta> getMatchingChunksInAccount(final int accountId, 
			final List<FileChunkMeta> chunksL) throws IOException
	{
		String action = "FIND_MATCHING_CHUNKS";
		queryB.setLength(0);
		
		queryB.append(CURLY_BRACE_START)
			.append(QUOTE).append(ACTION).append(QUOTE).append(COLON)
			.append(QUOTE).append(action).append(QUOTE).append(COMMA)
			.append("chunks").append(COLON).append(SQUARE_BRACKET_START);
		
		boolean isFirst = true;
		for ( ChunkMeta chunkMeta : chunksL )
		{
			if ( isFirst ) isFirst = false;
			else {
				queryB.append(COMMA);
			}
			queryB.append(CURLY_BRACE_START)
					.append(ACCOUNT_ID).append(COLON).append(accountId).append(COMMA)
					.append(CHUNK_HEADER).append(COLON).append(QUOTE)
						.append(chunkMeta.chunkHeaderBase64).append(QUOTE).append(COMMA)
					.append(CHUNK_HASH).append(COLON).append(QUOTE)
						.append(chunkMeta.chunkHashBase64).append(QUOTE)
				.append(CURLY_BRACE_END);
		}
		queryB.append(SQUARE_BRACKET_END).append(CURLY_BRACE_END);
		
		String responseBody = getResponse();
		
		JsonElement element = new JsonParser().parse(responseBody);
		JsonArray responseArray = element.getAsJsonObject().getAsJsonArray(RESPONSE_JSON_HEAD);
		List<ChunkMeta> matchingChunksL = null;
		for ( int i = 0; i < responseArray.size(); i++ )
		{
			if ( null == matchingChunksL ) matchingChunksL = new ArrayList<ChunkMeta>();
			ChunkMeta chunkMeta = new ChunkMeta();
			
			JsonObject matchingObj = responseArray.get(i).getAsJsonObject();
			chunkMeta.accountId = matchingObj.getAsJsonPrimitive("accountId").getAsInt();
			chunkMeta.chunkHeaderBase64 = matchingObj.getAsJsonPrimitive(CHUNK_HEADER).getAsString();
			chunkMeta.chunkHashBase64 = matchingObj.getAsJsonPrimitive("chunkHash").getAsString();
			matchingChunksL.add(chunkMeta);
		}
		return matchingChunksL;
	}	
	
	//DONE
	public final List<FileChunkMeta> getChunksInFile(final int accountId, final long fileId, 
			final long revision, final int projectId) throws IOException
	{
		queryB.setLength(0);
		
		String action = "GET_FILE_CHUNKS";
		queryB.append(CURLY_BRACE_START)
			.append(QUOTE).append(ACTION).append(QUOTE).append(COLON)
			.append(QUOTE).append(action).append(QUOTE).append(COMMA)
			.append(ACCOUNT_ID).append(COLON).append(accountId).append(COMMA)
			.append(PROJECT_ID).append(COLON).append(projectId).append(COMMA)
			.append(FILE_ID).append(COLON).append(fileId).append(COMMA)
			.append(REVISION).append(COLON).append(revision)
		.append(CURLY_BRACE_END);
		
		String responseBody = getResponse();
		System.out.println(responseBody);
		List<FileChunkMeta> fileChunkMetaL = null;
		
		JsonElement element = new JsonParser().parse(responseBody);
		JsonArray responseArray = element.getAsJsonObject().getAsJsonArray(RESPONSE_JSON_HEAD);
		for ( int i = 0; i < responseArray.size(); i++ )
		{
			String chunksJson = responseArray.get(i).getAsJsonObject()
					.getAsJsonPrimitive("chunksJSON").getAsString();
			
			if( null != chunksJson )
			{
				JsonArray chunksJsonArray = new JsonParser().parse(chunksJson).getAsJsonArray();
				
				for ( int j = 0; j < chunksJsonArray.size(); j++ )
				{
					JsonObject chunkObj = chunksJsonArray.get(j).getAsJsonObject(); 
					if ( null == fileChunkMetaL ) fileChunkMetaL = new ArrayList<FileChunkMeta>();
					FileChunkMeta fileChunkMeta = new FileChunkMeta();
					fileChunkMeta.chunkHeaderBase64 = chunkObj.getAsJsonPrimitive(CHUNK_HEADER)
							.getAsString();
					fileChunkMeta.chunkHashBase64 = chunkObj.getAsJsonPrimitive("chunkHash")
							.getAsString();
					
					fileChunkMetaL.add(fileChunkMeta);
				}
			}
		}
		
		return fileChunkMetaL;
	}
	
	public final void deleteFile( final int accountId, final String magicHeader, 
			final short startByte, final short endByte, final int fileLength, 
			final long fileId, final long revision, final int projectId, 
			final List<FileChunkMeta> fileChunkMetaL ) throws IOException
	{
		String action = "DELETE_FILE";
		queryB.setLength(0);
		
		queryB.append(CURLY_BRACE_START)
			.append(QUOTE).append(ACTION).append(QUOTE).append(COLON)
			.append(QUOTE).append(action).append(QUOTE).append(COMMA)
			.append(ACCOUNT_ID).append(COLON).append(accountId).append(COMMA)
			.append(MAGIC_HEADER).append(COLON).append(QUOTE).append(magicHeader)
				.append(QUOTE).append(COMMA)
			.append(START_BYTE).append(COLON).append(startByte).append(COMMA)
			.append(END_BYTE).append(COLON).append(endByte).append(COMMA)
			.append(FILE_LENGTH).append(COLON).append(fileLength).append(COMMA)
			.append(FILE_ID).append(COLON).append(fileId).append(COMMA)
			.append(REVISION).append(COLON).append(revision).append(COMMA)
			.append(PROJECT_ID).append(COLON).append(projectId).append(COMMA);
		
		boolean isFirst = true;
		StringBuilder chunksReferenced = new StringBuilder();
		
		for ( FileChunkMeta fileMeta : fileChunkMetaL )
		{
			if ( isFirst ) 
			{
				chunksReferenced.append(SQUARE_BRACKET_START);
				isFirst = false;
			}
			else 
			{
				chunksReferenced.append(COMMA);
			}
			
			chunksReferenced.append(CURLY_BRACE_START)
				.append(CHUNK_HEADER).append(COLON).append(QUOTE)
					.append(fileMeta.chunkHeaderBase64).append(QUOTE).append(COMMA)
				.append(CHUNK_HASH).append(COLON).append(QUOTE)
					.append(fileMeta.chunkHashBase64).append(QUOTE)
			.append(CURLY_BRACE_END);
		}
		chunksReferenced.append(SQUARE_BRACKET_END);
		queryB.append(CHUNKS_REFERENCED).append(COLON).append(chunksReferenced.toString())
	.append(CURLY_BRACE_END);
		
		getResponse();
	}
	
	//DONE
	public final String getContainerPath(final int accountId ) throws IOException
	{
		queryB.setLength(0);
		
		String action = "GET_DOWNLOAD_PATH";
		queryB.append(CURLY_BRACE_START)
			.append(QUOTE).append(ACTION).append(QUOTE).append(COLON)
			.append(QUOTE).append(action).append(QUOTE).append(COMMA)
			.append(ACCOUNT_ID).append(COLON).append(accountId)
		.append(CURLY_BRACE_END);
		
		String responseBody = getResponse();
		String s3containerPath = null;
		
		JsonElement element = new JsonParser().parse(responseBody);
		JsonArray responseArray = element.getAsJsonObject().getAsJsonArray(RESPONSE_JSON_HEAD);
		for ( int i = 0; i < responseArray.size(); i++ )
		{
			s3containerPath = responseArray.get(i).getAsJsonObject()
					.getAsJsonPrimitive("s3ContainerPath").getAsString();
		}
		
		return s3containerPath;
	}
	
	//DONE
	public final void setContainerForAccount(final int accountId, 
			final String s3ContainerPath, final String folderName) throws IOException
	{
		queryB.setLength(0);
		
		String action = "ADD_CONTAINER_FOR_ACCOUNT";
		queryB.append(CURLY_BRACE_START)
			.append(QUOTE).append(ACTION).append(QUOTE).append(COLON)
			.append(QUOTE).append(action).append(QUOTE).append(COMMA)
			.append(ACCOUNT_ID).append(COLON).append(accountId).append(COMMA)
			.append(S3_CONTAINER_PATH).append(COLON).append(QUOTE).append(s3ContainerPath).append(QUOTE).append(COMMA)
			.append(FOLDER_NAME).append(COLON).append(QUOTE).append(folderName).append(QUOTE)
		.append(CURLY_BRACE_END);
		//System.out.println(queryB);
		String responseBody = getResponse();
		
		/*JsonElement element = new JsonParser().parse(responseBody);
		
		JsonArray responseArray = element.getAsJsonObject().getAsJsonArray(RESPONSE_JSON_HEAD);
		
		System.out.println("Status for method Add container coming from server");
		if ( responseArray.size() > 0 )
		{
			for ( int i = 0; i < responseArray.size(); i++ )
			{
				
				JsonObject offsetJson = responseArray.get(i).getAsJsonObject();
				String exception  = offsetJson.getAsJsonPrimitive("Exception").getAsString();
				String errorcode  = offsetJson.getAsJsonPrimitive("errorCode").getAsString();
				String errortype  = offsetJson.getAsJsonPrimitive("errorType").getAsString();
				String statuscode  = offsetJson.getAsJsonPrimitive("statusCode").getAsString();
				String successmsg  = offsetJson.getAsJsonPrimitive("successMsg").getAsString();
				
				System.out.println(exception);
				System.out.println(errorcode);
				System.out.println(errortype);
				System.out.println(statuscode);
				System.out.println(successmsg);
			}
		}
		*/
		System.out.println("Status for method Add container ended");
		
	}
	
	public final String getPresignedUrl(final int accountId, final int projectId, final String fileName, final String urlType, final String bucketName ) throws IOException
	{
		queryB.setLength(0);
		Connection.Response response = Jsoup.connect(baseUrlPresign)
				.header(MIMETYPE, MIMETYPE_VALUE)
				.ignoreContentType(true)
				.data("accountId", Integer.toString(accountId)).data("projectId", Integer.toString(projectId)).data("fileName", fileName).data("urlType", urlType).data("bucketName", bucketName)
				.method(Method.POST).execute();
		
		return response.body();
	}
	
	public final String getPresignedDownloadUrl(final int accountId, final int projectId, final String fileName, final String urlType, final String bucketName, final String logicalFileName ) throws IOException
	{
		if(logicalFileName==null){
		queryB.setLength(0);
		Connection.Response response = Jsoup.connect(baseUrlPresign)
				.header(MIMETYPE, MIMETYPE_VALUE)
				.ignoreContentType(true)
				.data("fileName", fileName).data("urlType", urlType).data("bucketName", bucketName)
				.method(Method.POST).execute();
		return response.body();
		}
		else
		{
			queryB.setLength(0);
			Connection.Response response = Jsoup.connect(baseUrlPresign)
					.header(MIMETYPE, MIMETYPE_VALUE)
					.ignoreContentType(true)
					.data("fileName", fileName).data("urlType", urlType).data("bucketName", bucketName).data("logicalFileName", logicalFileName)
					.method(Method.POST).execute();
			return response.body();
		}
		
	}
	
	public final long getFileId() throws IOException
	{
		queryB.setLength(0);
		
		String action = "GET_FILE_ID";
		queryB.append(CURLY_BRACE_START)
			.append(QUOTE).append(ACTION).append(QUOTE).append(COLON)
			.append(QUOTE).append(action).append(QUOTE)
		.append(CURLY_BRACE_END);
		String responseBody = getResponse();
		System.out.println(responseBody);
		JsonElement element = new JsonParser().parse(responseBody);
		JsonArray responseArray = element.getAsJsonObject().getAsJsonArray(RESPONSE_JSON_HEAD);
		long fileId = responseArray.get(0).getAsJsonObject()
					.getAsJsonPrimitive(FILE_ID).getAsLong();
		
		return fileId;
	}
	
	public final long getRevisionId() throws IOException
	{
		queryB.setLength(0);
		
		String action = "GET_REVISION_ID";
		queryB.append(CURLY_BRACE_START)
			.append(QUOTE).append(ACTION).append(QUOTE).append(COLON)
			.append(QUOTE).append(action).append(QUOTE)
		.append(CURLY_BRACE_END);
		
		
		String responseBody = getResponse();
		
		System.out.println("Response body"+responseBody);
		JsonElement element = new JsonParser().parse(responseBody);
		JsonArray responseArray = element.getAsJsonObject().getAsJsonArray(RESPONSE_JSON_HEAD);
		long revisionId = responseArray.get(0).getAsJsonObject()
					.getAsJsonPrimitive(REVISION_ID).getAsLong();
		
		return revisionId;
		
	}

	public void insertToChunkTable(List<ChunkMeta> successfulUploadChunks) throws IOException {
		queryB.setLength(0);
		
		String action = "INSERT_TO_CHUNKS_TABLE_WITH_ZERO_REFERENCE";
		
		queryB.append(CURLY_BRACE_START)
		.append(QUOTE).append(ACTION).append(QUOTE).append(COLON)
		.append(QUOTE).append(action).append(QUOTE).append(COMMA);
		
		boolean isFirst = true;
		
		StringBuilder chunksReferenced = new StringBuilder();
		for ( ChunkMeta fileMeta : successfulUploadChunks )
		{
			if ( isFirst ) 
			{
				chunksReferenced.append(SQUARE_BRACKET_START);
				isFirst = false;
			}
			else 
			{
				chunksReferenced.append(COMMA);
			}
		
			chunksReferenced.append(CURLY_BRACE_START)
				.append(ACCOUNT_ID).append(COLON).append(fileMeta.accountId).append(COMMA)
				.append(CHUNK_HEADER).append(COLON).append(QUOTE).append(fileMeta.chunkHeaderBase64)
					.append(QUOTE).append(COMMA)
				.append(CHUNK_HASH).append(COLON).append(QUOTE).append(fileMeta.chunkHashBase64)
					.append(QUOTE)
			.append(CURLY_BRACE_END);
		}
		
		chunksReferenced.append(SQUARE_BRACKET_END);
		
		queryB.append(CHUNKS_REFERENCED).append(COLON).append(chunksReferenced.toString())
			.append(CURLY_BRACE_END);
		
		System.out.println(chunksReferenced);
		getResponse();
		
	}
	
}