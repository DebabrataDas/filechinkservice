package com.arc.client;

public final class FileThumbnail
{
	public byte startByte;
	public byte endByte;
	public String magicHeaderChunkBase64;
	public int fileLength;
	
	public final String toString(){
		return "Start Byte: " + startByte + ", End Byte: " + endByte 
				+ ", Magic Header: " + magicHeaderChunkBase64 + ", File Length: " 
				+ fileLength;
	}
}