package com.arc.client;

import java.util.ArrayList;
import java.util.List;

import com.arc.chunk.HashGenerator;
import com.arc.dao.ChunkOffsets;
import com.arc.dao.FileChunkMeta;
import com.arc.dao.FileChunkOffsets;
import com.thoughtworks.xstream.core.util.Base64Encoder;

public final class FileChunkSplitter
{
	public final FileThumbnail getFileThumbnail(final byte[] fileBytes, 
			final int magicChunkHeaderSize)
	{
		if(fileBytes.length <= 0) return null;

		FileThumbnail fileThumbnail = new FileThumbnail();
		byte[] magicChunkHeaderByte = new byte[magicChunkHeaderSize];
		Base64Encoder encoder = new Base64Encoder();

		fileThumbnail.fileLength = fileBytes.length;
		fileThumbnail.startByte = fileBytes[0];
		fileThumbnail.endByte = fileBytes[(fileThumbnail.fileLength-1)];
		System.arraycopy(fileBytes, 0, magicChunkHeaderByte, 0, magicChunkHeaderSize);

		fileThumbnail.magicHeaderChunkBase64 = encoder.encode(magicChunkHeaderByte);
		if( fileThumbnail.magicHeaderChunkBase64.indexOf("/") > -1 )
			fileThumbnail.magicHeaderChunkBase64 = fileThumbnail.magicHeaderChunkBase64.replaceAll("/", "");
		if( fileThumbnail.magicHeaderChunkBase64.indexOf("\\") > -1 )
			fileThumbnail.magicHeaderChunkBase64 = fileThumbnail.magicHeaderChunkBase64.replaceAll("\\", "");
		
		return fileThumbnail;
	}
	
	public final List<FileChunkMeta> cutSpecifiedChunks(final FileChunkOffsets fileChunkOffsets, 
			final byte[] fileBytes, final String hashAlgo)
	{
		List<FileChunkMeta> fileChunks = new ArrayList<FileChunkMeta>();
		Base64Encoder encoder = new Base64Encoder();
		byte[] header = new byte[8];
		HashGenerator hashGenerator = new HashGenerator();
		String hash;
		
		for(ChunkOffsets chunkOffset : fileChunkOffsets.chunkOffsets)
		{
			FileChunkMeta fileChunkMeta = new FileChunkMeta();
			fileChunkMeta.offset = chunkOffset.offset;
			fileChunkMeta.chunkLength = chunkOffset.length;
			System.arraycopy(fileBytes,(int) fileChunkMeta.offset, header, 0, 8);
			
			fileChunkMeta.chunkHeaderBase64 = encoder.encode(header);
			if( fileChunkMeta.chunkHeaderBase64.indexOf("/") > -1 )
				fileChunkMeta.chunkHeaderBase64 = fileChunkMeta.chunkHeaderBase64.replaceAll("/", "");
			if( fileChunkMeta.chunkHeaderBase64.indexOf("\\") > -1 )
				fileChunkMeta.chunkHeaderBase64 = fileChunkMeta.chunkHeaderBase64.replaceAll("\\", "");
			hash = hashGenerator.generateHash(fileBytes, fileChunkMeta.offset, 
					fileChunkMeta.chunkLength,hashAlgo);
			fileChunkMeta.chunkBytes = hashGenerator.getChunk(fileBytes, fileChunkMeta.offset, 
					fileChunkMeta.chunkLength);
			fileChunkMeta.chunkHashBase64 = encoder.encode(hash.getBytes());
			if( fileChunkMeta.chunkHashBase64.indexOf("/") > -1 )
				fileChunkMeta.chunkHashBase64 = fileChunkMeta.chunkHashBase64.replaceAll("/", "");
			if( fileChunkMeta.chunkHashBase64.indexOf("\\") > -1 )
				fileChunkMeta.chunkHashBase64 = fileChunkMeta.chunkHashBase64.replaceAll("\\", "");
			fileChunks.add(fileChunkMeta);
		}
		return fileChunks;
	}
	

	public final List<FileChunkMeta> cutFirstTimeChunks(final byte[] fileBytes, 
			final int magicChunkHeaderSize, final int contentSize, final String hashingAlgoToUse)
	{
		List<FileChunkMeta> fileChunks = new ArrayList<FileChunkMeta>();
		Base64Encoder encoder = new Base64Encoder();
		byte[] header = new byte[8];
		int fileLength = fileBytes.length;
		FileChunkMeta fileChunk;
		String hash;
		int bytesRead = 0;
		HashGenerator hashGenerator = new HashGenerator();
		if( magicChunkHeaderSize < fileLength )
		{
			fileChunk = new FileChunkMeta();
			System.arraycopy(fileBytes,(int) fileChunk.offset, header, 0, 8);
			fileChunk.offset = bytesRead;
			fileChunk.chunkLength = magicChunkHeaderSize;
			fileChunk.chunkHeaderBase64 = encoder.encode(header);
			if( fileChunk.chunkHeaderBase64.indexOf("/") > -1 )
				fileChunk.chunkHeaderBase64 = fileChunk.chunkHeaderBase64.replaceAll("/", "");
			if( fileChunk.chunkHeaderBase64.indexOf("\\") > -1 )
				fileChunk.chunkHeaderBase64 = fileChunk.chunkHeaderBase64.replaceAll("\\", "");
			hash = hashGenerator.generateHash(fileBytes, fileChunk.offset, fileChunk.chunkLength, 
					hashingAlgoToUse);
			fileChunk.chunkBytes = hashGenerator.getChunk(fileBytes, fileChunk.offset, 
					fileChunk.chunkLength);
			fileChunk.chunkHashBase64 = encoder.encode(hash.getBytes());
			if( fileChunk.chunkHashBase64.indexOf("/") > -1 )
				fileChunk.chunkHashBase64 = fileChunk.chunkHashBase64.replaceAll("/", "");
			if( fileChunk.chunkHashBase64.indexOf("\\") > -1 )
				fileChunk.chunkHashBase64 = fileChunk.chunkHashBase64.replaceAll("\\", "");
			fileChunks.add(fileChunk);
			bytesRead += fileChunk.chunkLength;
			while( bytesRead < fileLength )
			{
				int bytesToRead = fileLength - bytesRead;
				fileChunk = new FileChunkMeta();
				fileChunk.offset = bytesRead;
				
				if( bytesToRead > contentSize )
					fileChunk.chunkLength = contentSize;
				else
					fileChunk.chunkLength = bytesToRead ;
				
				if( fileChunk.chunkLength > 8 )
					System.arraycopy(fileBytes,(int) fileChunk.offset, header, 0, 8);
				else
					System.arraycopy(fileBytes,(int) fileChunk.offset, header, 0, fileChunk.chunkLength);

				fileChunk.chunkHeaderBase64 = encoder.encode(header);
				if( fileChunk.chunkHeaderBase64.indexOf("/") > -1 )
					fileChunk.chunkHeaderBase64 = fileChunk.chunkHeaderBase64.replaceAll("/", "");
				if( fileChunk.chunkHeaderBase64.indexOf("\\") > -1 )
					fileChunk.chunkHeaderBase64 = fileChunk.chunkHeaderBase64.replaceAll("\\", "");
				hash = hashGenerator.generateHash(fileBytes, fileChunk.offset, 
						fileChunk.chunkLength, hashingAlgoToUse);
				fileChunk.chunkBytes = hashGenerator.getChunk(fileBytes, fileChunk.offset, 
						fileChunk.chunkLength);
				fileChunk.chunkHashBase64 = encoder.encode(hash.getBytes());
				if( fileChunk.chunkHashBase64.indexOf("/") > -1 )
					fileChunk.chunkHashBase64 = fileChunk.chunkHashBase64.replaceAll("/", "");
				if( fileChunk.chunkHashBase64.indexOf("\\") > -1 )
					fileChunk.chunkHashBase64 = fileChunk.chunkHashBase64.replaceAll("\\", "");
				fileChunks.add(fileChunk);
				bytesRead += fileChunk.chunkLength;
			}
		}
		else
		{
			fileChunk = new FileChunkMeta();
			fileChunk.offset = bytesRead;
			fileChunk.chunkLength = fileLength;
			
			if( fileChunk.chunkLength > 8 )
				System.arraycopy(fileBytes,(int) fileChunk.offset, header, 0, 8);
			else
				System.arraycopy(fileBytes,(int) fileChunk.offset, header, 0, fileChunk.chunkLength);

			fileChunk.chunkHeaderBase64 = encoder.encode(header);
			if( fileChunk.chunkHeaderBase64.indexOf("/") > -1 )
				fileChunk.chunkHeaderBase64 = fileChunk.chunkHeaderBase64.replaceAll("/", "");
			if( fileChunk.chunkHeaderBase64.indexOf("\\") > -1 )
				fileChunk.chunkHeaderBase64 = fileChunk.chunkHeaderBase64.replaceAll("\\", "");
			hash = hashGenerator.generateHash(fileBytes, fileChunk.offset, 
					fileChunk.chunkLength, hashingAlgoToUse);
			fileChunk.chunkBytes = hashGenerator.getChunk(fileBytes, fileChunk.offset, 
					fileChunk.chunkLength);
			fileChunk.chunkHashBase64 = encoder.encode(hash.getBytes());
			if( fileChunk.chunkHashBase64.indexOf("/") > -1 )
				fileChunk.chunkHashBase64 = fileChunk.chunkHashBase64.replaceAll("/", "");
			if( fileChunk.chunkHashBase64.indexOf("\\") > -1 )
				fileChunk.chunkHashBase64 = fileChunk.chunkHashBase64.replaceAll("\\", "");
			fileChunks.add(fileChunk);
		}
		return fileChunks;
	}
	
	public final List<FileChunkMeta> getHeaderAndHash(final byte[] fileBytes, 
			final int magicChunkHeaderSize, final int contentSize, final String hashingAlgoToUse)
	{
		List<FileChunkMeta> fileChunks = new ArrayList<FileChunkMeta>();
		Base64Encoder encoder = new Base64Encoder();
		byte[] header = new byte[8];
		int fileLength = fileBytes.length;
		String hash;
		HashGenerator hashGenerator = new HashGenerator();
		FileChunkMeta fileChunk = new FileChunkMeta();
		fileChunk.offset = 0;
		fileChunk.chunkLength = fileLength;
			
		if( fileChunk.chunkLength > 8 )
			System.arraycopy(fileBytes,(int) fileChunk.offset, header, 0, 8);
		else
			System.arraycopy(fileBytes,(int) fileChunk.offset, header, 0, fileChunk.chunkLength);

		fileChunk.chunkHeaderBase64 = encoder.encode(header);
		if( fileChunk.chunkHeaderBase64.indexOf("/") > -1 )
			fileChunk.chunkHeaderBase64 = fileChunk.chunkHeaderBase64.replaceAll("/", "");
		if( fileChunk.chunkHeaderBase64.indexOf("\\") > -1 )
			fileChunk.chunkHeaderBase64 = fileChunk.chunkHeaderBase64.replaceAll("\\", "");
		hash = hashGenerator.generateHash(fileBytes, fileChunk.offset, 
					fileChunk.chunkLength, hashingAlgoToUse);
		fileChunk.chunkBytes = hashGenerator.getChunk(fileBytes, fileChunk.offset, 
					fileChunk.chunkLength);
		fileChunk.chunkHashBase64 = encoder.encode(hash.getBytes());
		if( fileChunk.chunkHashBase64.indexOf("/") > -1 )
			fileChunk.chunkHashBase64 = fileChunk.chunkHashBase64.replaceAll("/", "");
		if( fileChunk.chunkHashBase64.indexOf("\\") > -1 )
			fileChunk.chunkHashBase64 = fileChunk.chunkHashBase64.replaceAll("\\", "");
		fileChunks.add(fileChunk);
		return fileChunks;
	}
}