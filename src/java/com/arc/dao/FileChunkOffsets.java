package com.arc.dao;

import java.util.List;


public class FileChunkOffsets  
{
	public int accountId;
	public int projectId;
	public int fileId;
	public int revision;
	public List<ChunkOffsets> chunkOffsets;
}