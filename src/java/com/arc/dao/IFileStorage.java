package com.arc.dao;

public interface IFileStorage 
{
	void upload(String accountId, String runningPartition, 
		String fileId, String fileLocalChunkFolder, String chunkMetaJSon);
	
	void deleteFile(String accountId, String aPartition, String fileId);

	void deleteChunk(String accountId, String aPartition, String chunkMetaJSon);
}
