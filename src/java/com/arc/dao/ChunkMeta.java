package com.arc.dao;

public class ChunkMeta 
{
	/**
	 * First 8 byte of the chunk
	 */
	public int accountId;
	
	public char chunkType;
	
	/**
	 * First 8 byte of the chunk
	 */
	public String chunkHeaderBase64;
	
	/**
	 * Hash of all bytes inside the chunk (MD5)
	 */
	public String chunkHashBase64;
	
	public byte[] chunkBytes;


}
