package com.arc.dao;

import java.util.List;


/**
 * Reference: x
 * Active	  : 1
 * Inactive	  : 2
 * Deleted	  : 3 	
 * 
 * @author Abinash
 *
 */
public interface IDaoFacade {
	
	String deleteFromStorage(String accountId, String partition, String chunkId);
	
	String storeFile(String accountId, String partition, 
		String filename, int headbyte, int tailbyte, long length,
		List<FileChunkMeta> fileChunks, List<ChunkMeta> existingChunks);
}
