package com.arc.hbase;

public class HbaseTableOperationTest
{
	public static void main(String[] args) throws Exception
	{
		HbaseTableOperation hto = new HbaseTableOperation();
//		 //Insert a new file to vanila system
//		hto.addLinkToAccount(1, "www.s3.com");
//		hto.addFileToThumbnail(1, "qwertyasdfg",(short) 11,(short) 18, 100, 1001, 00);
//		hto.addFileToFileChunks(1, 99, 1001, 00, "{chunkOffset:0,chunkLength:100}","{chunkHeader64:\"UEsDBBQACAg=\",chunkHash:\"N2ViZTY0ZjMyNDNiYTA4OWY2YmIwMzI1ZDM4OGRlNGE=\"}");
//		hto.addToChunks(1,"UEsDBBQACAg=","N2ViZTY0ZjMyNDNiYTA4OWY2YmIwMzI1ZDM4OGRlNGE=");
//		hto.incrementChunkReference(1,"UEsDBBQACAg=","N2ViZTY0ZjMyNDNiYTA4OWY2YmIwMzI1ZDM4OGRlNGE=");
		
//		// Duplicate file is uploaded
//		System.out.println(hto.findPossibleFileDuplicate(1, "qwertyasdfg",(short) 11,(short) 18, 100));
//		System.out.println(hto.findFileChunkOffsets(1,99,1001,00));
//		System.out.println(hto.findDuplicateFile(1,99,1001,00,"{chunkHeader64:\"UEsDBBQACAg=\",chunkHash:\"N2ViZTY0ZjMyNDNiYTA4OWY2YmIwMzI1ZDM4OGRlNGE=\"}"));
//		hto.appendFileToThumbnail(1001, 01, 1, "qwertyasdfg",(short) 11,(short) 18, 100);
//		hto.addFileToFileChunks(1,99,1001,01,"{chunkOffset:0,chunkLength:100}","{chunkHeader64:\"UEsDBBQACAg=\",chunkHash:\"N2ViZTY0ZjMyNDNiYTA4OWY2YmIwMzI1ZDM4OGRlNGE=\"}");
//		hto.incrementChunkReference(1,"UEsDBBQACAg=","N2ViZTY0ZjMyNDNiYTA4OWY2YmIwMzI1ZDM4OGRlNGE=");
		
//		// Modified file is uploaded
//		System.out.println(hto.findPossibleFileDuplicate(1, "qwertyasdfg",(short) 11,(short) 25, 120));
//		System.out.println(hto.findDuplicateFile(1,99,1001,00,"{chunkHeader64:\"QcOqaUFxMNE=\",chunkHash:\"NjMwZjM4NzA0NThiZjQzZTg0MzRmZTU0Zjk5YzNiNGU=\"}"));
//		System.out.println(hto.findMatchingChunks(1,"QcOqaUFxMNE=","NjMwZjM4NzA0NThiZjQzZTg0MzRmZTU0Zjk5YzNiNGU="));
//		hto.addFileToThumbnail(1, "qwertyasdfg",(short) 11,(short) 25, 120, 1002, 00);
//		hto.addFileToFileChunks(1,99,1002,00,"{chunkOffset:0,chunkLength:120}","{chunkHeader64:\"QcOqaUFxMNE=\",chunkHash:\"NjMwZjM4NzA0NThiZjQzZTg0MzRmZTU0Zjk5YzNiNGU=\"}");
//		hto.addToChunks(1,"QcOqaUFxMNE=","NjMwZjM4NzA0NThiZjQzZTg0MzRmZTU0Zjk5YzNiNGU=");
//		hto.incrementChunkReference(1,"QcOqaUFxMNE=","NjMwZjM4NzA0NThiZjQzZTg0MzRmZTU0Zjk5YzNiNGU=");
		
//		//Delete a file
//		System.out.println(hto.getFileChunks(1,99,1001,00));
//		hto.deleteFileFromFileParrotHash(1001, 00, 1, "qwertyasdfg",(short) 11,(short) 18, 100);
//		hto.deleteFileFromFileChunks(1,99,1001,00);
//		hto.decrementChunkReference(1,"UEsDBBQACAg=","N2ViZTY0ZjMyNDNiYTA4OWY2YmIwMzI1ZDM4OGRlNGE=");
		
//		//Download a file
//		System.out.println(hto.getDownloadPath(1));
//		System.out.println(hto.getFileChunks(1,99,1001,01));
		
	}
}
