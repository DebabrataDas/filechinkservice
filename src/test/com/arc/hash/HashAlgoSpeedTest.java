package com.arc.hash;

import java.io.File;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import com.arc.util.FileReaderUtil;
import com.bizosys.hsearch.util.LineReaderUtil;

public class HashAlgoSpeedTest
{
	public LinkedHashMap<String, byte[]> generateHashChunks(byte[] fileByte, int chunkSize, int magicChunkHeaderSize, String hashAlgo)
	{
		StringBuilder hashCode = new StringBuilder();
		LinkedHashMap<String, byte[]> chunks = new LinkedHashMap<String, byte[]>();
		int chunkHeadSize = 8;
		int bytesRead = 0;
		byte[] chunkMd;
		int sourcePosition = 0;
		try
		{
			MessageDigest md = MessageDigest.getInstance(hashAlgo);
			byte[] chunk = new byte[chunkSize];
			byte[] magicChunkHeader = new byte[magicChunkHeaderSize];
			byte[] chunkHead = new byte[chunkHeadSize];
			int bytesInFile = fileByte.length;
			//Check if total bytes in file is greater than headSize
			if( magicChunkHeaderSize < bytesInFile )
			{
				System.arraycopy(fileByte, sourcePosition, magicChunkHeader, 0, magicChunkHeaderSize);
				System.arraycopy(magicChunkHeader, 0, chunkHead, 0, chunkHeadSize);
				md.update(magicChunkHeader, 0, magicChunkHeaderSize);
				chunkMd = md.digest();
				for (int i = 0; i < chunkMd.length; i++)
				{
					String hash = Integer.toHexString(0xff & chunkMd[i]);
					if(hash.length() == 1) hashCode.append('0');
					hashCode.append(hash);
				}
//				System.out.println("header - " + hashCode.toString());
				chunks.put(hashCode.toString(), magicChunkHeader);
				hashCode.delete(0, hashCode.length());
				sourcePosition += magicChunkHeaderSize;
				while( sourcePosition < bytesInFile )
				{
					int bytesToRead = bytesInFile - sourcePosition;
					if( bytesToRead > chunkSize )
					{
						bytesRead = chunkSize;
					}
					else
					{
						bytesRead = bytesToRead ;
					}
					System.arraycopy(fileByte, sourcePosition, chunk, 0, bytesRead);
					md.update(chunk, 0, bytesRead);
					chunkMd = md.digest();
					for (int i = 0; i < chunkMd.length; i++)
					{
						String hash = Integer.toHexString(0xff & chunkMd[i]);
						if(hash.length() == 1) hashCode.append('0');
						hashCode.append(hash);
					}
					chunks.put(hashCode.toString(), chunk);
					hashCode.delete(0, hashCode.length());
					sourcePosition += bytesRead;
				}
			}
			else
			{
				System.arraycopy(fileByte, sourcePosition, magicChunkHeader, 0, bytesInFile);
				md.update(magicChunkHeader, 0, bytesInFile);
				chunkMd = md.digest();
				for (int i = 0; i < chunkMd.length; i++)
				{
					String hash = Integer.toHexString(0xff & chunkMd[i]);
					if(hash.length() == 1) hashCode.append('0');
					hashCode.append(hash);
				}
				chunks.put(hashCode.toString(), magicChunkHeader);
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			System.err.println("Exception in generateHashChunks " + e.getMessage());
		}
		return chunks;
	}
	
	public static void main(String[] args)
	{
		if( args.length != 3 )
		{
			System.err.println("Usage : java HashGenerator <<filePath>> <<chunkSize>> <<headSize>>");
			System.err.println("<<filePath>> - Path of the file to be read");
			System.err.println("<<chunkSize>> - Number of bytes in one chunk");
			System.err.println("<<headSize>> - Number of bytes for head");
			System.exit(0);
		}
		HashAlgoSpeedTest hast = new HashAlgoSpeedTest();
		String filePaths = args[0];
		
		List<String> filePathL = new ArrayList<String>();
		LineReaderUtil.fastSplit(filePathL, filePaths, ',');
		
		int chunkSize = Integer.parseInt(args[1]);
		int headSize = Integer.parseInt(args[2]);
		String[] hashAlgos = {"MD5", "SHA-1", "SHA-256"};
		
		System.out.println("Time taken in cutting into chunks and hashing.");
		for ( String filePath : filePathL )
		{
			File file = new File(filePath);
			byte[] fileByte = FileReaderUtil.getBytes(file);
			
			System.out.println("\nFile Type: " +filePath.substring(filePath.lastIndexOf('.')) +", File Size: " +file.length()/1024 +" KB");
			System.out.print("\n");
			for ( int i = 0; i < 3; i++ )
			{
				for ( String hashAlgo : hashAlgos )
				{
					long startTime = System.currentTimeMillis();
					hast.generateHashChunks(fileByte, chunkSize, headSize, hashAlgo);
					long endTime = System.currentTimeMillis();
					System.out.print(hashAlgo +": " +(endTime - startTime) +"\t");
				}
				System.out.print("\n");
			}
		}
	}
}
