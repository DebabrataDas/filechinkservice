package com.arc.hash;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import com.arc.dao.FileChunkMeta;
import com.arc.util.FileReaderUtil;
import com.bizosys.hsearch.byteutils.ByteUtil;
import com.thoughtworks.xstream.core.util.Base64Encoder;

public class HashGeneratorTest
{
	public String generateHash(byte[] fileBytes, long chunkOffset, int chunkLength, String algoName)
	{
		StringBuilder hashCode = new StringBuilder();
		byte[] chunkMd;
		byte[] chunk = new byte[chunkLength];
		String chunkS;
		long fileLength = fileBytes.length;
		try
		{
			if( fileLength < (chunkOffset + chunkLength))
			{
				System.err.println("File size too small for chunking");
				return null;
			}
			MessageDigest md = MessageDigest.getInstance(algoName);
			System.out.println(new String());
			chunkS = ByteUtil.toString(fileBytes,(int) chunkOffset, chunkLength);
			chunk = ByteUtil.toBytes(chunkS);
//			System.arraycopy(fileBytes, (int) chunkOffset, chunk, 0, chunkLength);
//			System.out.println(new String(chunk));
			md.update(chunk, 0, (int) chunkLength);
			chunkMd = md.digest();
			for (int i = 0; i < chunkMd.length; i++)
			{
				String hash = Integer.toHexString(0xff & chunkMd[i]);
				if(hash.length() == 1) hashCode.append('0');
				hashCode.append(hash);
			}
		} 
		catch (Exception e)
		{
			// TODO: handle exception
		}
		System.out.println(hashCode.toString());
		return hashCode.toString();
	}
	
		
	public LinkedHashMap<String, byte[]> generateForwardHashChunks(byte[] fileByte, int chunkSize, int headSize, String algoName)
	{
		StringBuffer hashCode = new StringBuffer();
		LinkedHashMap<String, byte[]> chunks = new LinkedHashMap<String, byte[]>();
		int bytesRead = 0;
		byte[] chunkMd;
		int sourcePosition = 0;
		try
		{
			MessageDigest md = MessageDigest.getInstance(algoName);
			byte[] chunk = new byte[chunkSize];
			byte[] chunkHeader = new byte[headSize];
			int bytesInFile = fileByte.length;
			if( headSize < bytesInFile )
			{
				System.arraycopy(fileByte, sourcePosition, chunkHeader, 0, headSize);
				md.update(chunkHeader, 0, headSize);
				chunkMd = md.digest();
				for (int i = 0; i < chunkMd.length; i++)
				{
					String hash = Integer.toHexString(0xff & chunkMd[i]);
					if(hash.length() == 1) hashCode.append('0');
					hashCode.append(hash);
				}
				System.out.println("header - " + hashCode.toString());
				chunks.put(hashCode.toString(), chunkHeader);
				hashCode.delete(0, hashCode.length());
				sourcePosition += headSize;
				while( sourcePosition < bytesInFile )
				{
					if( ( bytesInFile - sourcePosition ) > chunkSize )
					{
						bytesRead = chunkSize;
					}
					else
					{
						bytesRead = bytesInFile - sourcePosition ;
					}
					System.arraycopy(fileByte, sourcePosition, chunk, 0, bytesRead);
					md.update(chunk, 0, bytesRead);
					chunkMd = md.digest();
					for (int i = 0; i < chunkMd.length; i++)
					{
						String hash = Integer.toHexString(0xff & chunkMd[i]);
						if(hash.length() == 1) hashCode.append('0');
						hashCode.append(hash);
					}
					chunks.put(hashCode.toString(), chunk);
					hashCode.delete(0, hashCode.length());
					sourcePosition += bytesRead;
				}
			}
			else
			{
				System.arraycopy(fileByte, sourcePosition, chunkHeader, 0, bytesInFile);
				md.update(chunkHeader, 0, bytesInFile);
				chunkMd = md.digest();
				for (int i = 0; i < chunkMd.length; i++)
				{
					String hash = Integer.toHexString(0xff & chunkMd[i]);
					if(hash.length() == 1) hashCode.append('0');
					hashCode.append(hash);
				}
				chunks.put(hashCode.toString(), chunkHeader);
			}
		} 
		catch (Exception e)
		{
			System.err.println("Exception in generateForwardHash " + e.getMessage());
		}
		return chunks;
	}
	
	public LinkedHashMap<String , byte[]>	generateBackwardHashChunks(byte[] fileByte, int chunkSize, String algoName)
	{
		LinkedHashMap<String , byte[]> backwardChunks = new LinkedHashMap<String, byte[]>();
		StringBuffer hashCode = new StringBuffer();
		int bytesRead = 0;
		byte[] chunkMd;
		int sourcePosition = 0;
		try
		{
			MessageDigest md = MessageDigest.getInstance(algoName);
			byte[] chunk = new byte[chunkSize];
			int bytesInFile = fileByte.length;
			while( bytesInFile > 0)
			{
				if( bytesInFile > chunkSize )
				{
					sourcePosition = bytesInFile-chunkSize;
					bytesRead = chunkSize;
				}
				else
				{
					sourcePosition = 0;
					bytesRead = bytesInFile;
				}
				System.arraycopy(fileByte, sourcePosition, chunk, 0, bytesRead);
				md.update(chunk, 0, bytesRead);
				chunkMd = md.digest();
				for (int i = 0; i < chunkMd.length; i++)
				{
					String hash = Integer.toHexString(0xff & chunkMd[i]);
					if(hash.length() == 1) hashCode.append('0');
					hashCode.append(hash);
				}
				backwardChunks.put(hashCode.toString(), chunk);
				hashCode.delete(0, hashCode.length());
				bytesInFile = sourcePosition;
			}
			
		} catch (Exception e)
		{
			System.err.println("Exception in generateBackwardHash " + e.getMessage());
		} 
		
		return backwardChunks;
	}
	
	public List<FileChunkMeta> cutFirstTimeChunk(byte[] fileBytes, int magicChunkHeaderSize, int contentSize, String algoName)
	{
		List<FileChunkMeta> fileChunks = new ArrayList<FileChunkMeta>();
		Base64Encoder encoder = new Base64Encoder();
		byte[] header = new byte[8];
		int fileLength = fileBytes.length;
		FileChunkMeta fileChunk;
		String hash;
		int bytesRead = 0;
		if( magicChunkHeaderSize < fileLength )
		{
			fileChunk = new FileChunkMeta();
			System.arraycopy(fileBytes,(int) fileChunk.offset, header, 0, 8);
			fileChunk.offset = bytesRead;
			fileChunk.chunkLength = magicChunkHeaderSize;
			fileChunk.chunkHeaderBase64 = encoder.encode(header);
			hash = generateHash(fileBytes, fileChunk.offset, fileChunk.chunkLength,algoName);
			fileChunk.chunkHashBase64 = encoder.encode(hash.getBytes());
			fileChunks.add(fileChunk);
			bytesRead += fileChunk.chunkLength;
			while( bytesRead < fileLength )
			{
				int bytesToRead = fileLength - bytesRead;
				fileChunk = new FileChunkMeta();
				fileChunk.offset = bytesRead;
				if( bytesToRead > contentSize )
				{
					fileChunk.chunkLength = contentSize;
				}
				else
				{
					fileChunk.chunkLength = bytesToRead ;
				}
				
				if( fileChunk.chunkLength > 8 )
				{
					System.arraycopy(fileBytes,(int) fileChunk.offset, header, 0, 8);
				}
				else
				{
					System.arraycopy(fileBytes,(int) fileChunk.offset, header, 0, fileChunk.chunkLength);
				}
				fileChunk.chunkHeaderBase64 = encoder.encode(header);
				hash = generateHash(fileBytes, fileChunk.offset, fileChunk.chunkLength, algoName);
				fileChunk.chunkHashBase64 = encoder.encode(hash.getBytes());
				fileChunks.add(fileChunk);
				bytesRead += fileChunk.chunkLength;
			}
		}
		else
		{
			fileChunk = new FileChunkMeta();
			fileChunk.offset = bytesRead;
			fileChunk.chunkLength = fileLength;
			if( fileChunk.chunkLength > 8 )
			{
				System.arraycopy(fileBytes,(int) fileChunk.offset, header, 0, 8);
			}
			else
			{
				System.arraycopy(fileBytes,(int) fileChunk.offset, header, 0, fileChunk.chunkLength);
			}
			fileChunk.chunkHeaderBase64 = encoder.encode(header);
			hash = generateHash(fileBytes, fileChunk.offset, fileChunk.chunkLength, algoName);
			fileChunk.chunkHashBase64 = encoder.encode(hash.getBytes());
			fileChunks.add(fileChunk);
		}
		return fileChunks;
	}
	
	public static void main(String[] args)
	{
		HashGeneratorTest hgt = new HashGeneratorTest();
		String filePath = "data/test2.ods";
		int magicChunkHeaderSize = 8;
		String file = FileReaderUtil.toString(filePath);
		byte[] fileBytes = ByteUtil.toBytes(file);
//		System.out.println(new String(fileBytes));
		int contentSize = 4096;
		String algoName = "MD5";
		
		hgt.cutFirstTimeChunk(fileBytes, magicChunkHeaderSize, contentSize, algoName);
		
	}
}
