package com.arc.rest.rest;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

import javax.servlet.Servlet;

import junit.framework.TestCase;
import junit.framework.TestFerrari;

import com.oneline.ferrari.TestAll;

public class RestAPIServletTest extends TestCase 
{
	
	private static final String CHUNK_HASH = "chunkHash";
	private static final String CHUNK_HEADER = "chunkHeader";
	private static final String CHUNKS_JSON = "chunksJSON";
	private static final String CHUNK_OFFSET_JSON = "chunkOffsetJSON";
	private static final String S3_CONTAINER_PATH = "s3ContainerPath";
	private static final String ACCOUNT_ID = "accountId";
	private static final String PROJECT_ID = "projectId";
	private static final String REVISION = "revision";
	private static final String FILE_ID = "fileId";
	private static final String FILE_LENGTH = "fileLength";
	private static final String END_BYTE = "endByte";
	private static final String START_BYTE = "startByte";
	private static final String MAGIC_HEADER = "magicHeader";
	private static final String THUMBNAIL_TYPE = "thumbnailType";
	private static final String CHUNKS_UPLOADED = "chunksUploaded";
	private static final String CHUNKS_REFERENCED = "chunksReferenced";
	
	private static final char SQUARE_BRACKET_START = '[';
	private static final char SQUARE_BRACKET_END = ']';
	private static final char CURLY_BRACE_START = '{';
	private static final char CURLY_BRACE_END = '}';
	private static final char SLASH = '\\';
	private static final char COMMA = ',';
	private static final char COLON = ':';
	private static final char QUOTE = '"';

	
	private static final RestAPIServlet restServlet = new RestAPIServlet();
	public final static String[] modes = new String[] { "all", "random", "method" };
	public final static String mode = modes[2];

	public final static void main(String[] args) throws Exception {
		final RestAPIServletTest t = new RestAPIServletTest();

		if (modes[0].equals(mode)) {
			TestAll.run(new TestCase[] { t });
		} else if (modes[1].equals(mode)) {
			TestFerrari.testRandom(t);

		} else if (modes[2].equals(mode)) {
			t.setUp();
			t.testAddContainerForAccount();
			t.tearDown();
		}
	}

	@Override
	protected final void setUp() throws Exception 
	{
		restServlet.init();
	}

	@Override
	protected final void tearDown() throws Exception 
	{
	}
	
	public final void testAddContainerForAccount() throws Exception 
	{
		OutputStream out = System.out;
		for ( int i = 0; i < 1000000; i++){
			String reqJson = "{action:\"ADD_CONTAINER_FOR_ACCOUNT\",accountId:" +i +",s3ContainerPath:\"data/container\"}";
			restServlet.process(out, reqJson);
		}
	}

}
