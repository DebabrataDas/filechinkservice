-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.41-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for onelinedb
DROP DATABASE IF EXISTS `onelinedb`;
CREATE DATABASE IF NOT EXISTS `onelinedb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `onelinedb`;


-- Dumping structure for table onelinedb.app_config
DROP TABLE IF EXISTS `app_config`;
CREATE TABLE IF NOT EXISTS `app_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `configtype` char(1) NOT NULL,
  `title` varchar(64) NOT NULL,
  `body` text NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'A',
  `touchTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createTime` datetime NOT NULL DEFAULT '2012-01-01 00:00:00',
  `installation` varchar(255) DEFAULT '-i-',
  `client_version` int(11) DEFAULT '0',
  `server_version` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `BYTYPE` (`configtype`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COMMENT='Application configuration';

-- Dumping data for table onelinedb.app_config: ~36 rows (approximately)
/*!40000 ALTER TABLE `app_config` DISABLE KEYS */;
INSERT INTO `app_config` (`id`, `configtype`, `title`, `body`, `status`, `touchTime`, `createTime`, `installation`, `client_version`, `server_version`) VALUES
    (1, 'S', 'GET_ROLES', 'SELECT ROLE_ID AS roleId, ROLE_NAME AS roleName FROM roles', 'A', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (2, 'S', 'GET_SAVED_QUERY_BY_ID', 'SELECT QUERY AS query FROM savedquery WHERE USER_ID = \'__userid\' AND TENANT_ID = __tenantid AND QUERY_ID = ?', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (3, 'S', 'FIND_SAVED_QUERIES_BY_TITLE', 'SELECT QUERY_ID AS queryId, QUERY_TITLE AS queryTitle, QUERY_CONTEXT AS queryContext, QUERY AS query FROM savedquery WHERE USER_ID = \'__userid\' AND TENANT_ID = __tenantid AND QUERY_TITLE LIKE __like', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (4, 'D', 'DELETE_A_SAVED_QUERY', 'DELETE FROM savedquery WHERE QUERY_ID = ? AND USER_ID = __userid AND TENANT_ID = __tenantid', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (5, 'I', 'SAVE_A_QUERY', 'INSERT INTO savedquery (USER_ID,TENANT_ID, QUERY_TITLE, QUERY_CONTEXT, QUERY) values (__userid, __tenantid, ?, ?, ?)', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (6, 'S', 'GET_APP_CONFIG', 'SELECT id, configtype, title, body, status, touchTime, createTime, installation, client_version, server_version FROM app_config WHERE id > 0 AND ( __where ) ORDER BY __sort LIMIT __limit OFFSET __offset', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (7, 'S', 'GET_APP_CONFIG_COUNT', 'SELECT COUNT(id) AS total FROM app_config WHERE id > 0 AND ( __where )', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (8, 'D', 'DELETE_FROM_APP_CONFIG_BY_ID', 'DELETE FROM app_config WHERE id = ?', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (9, 'D', 'EMPTY_APP_CONFIG', 'DELETE FROM app_config', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (10, 'S', 'GET_AUTHORIZED_USERS_FOR_AN_ACTION', 'SELECT obj.OBJECT_ID as objectid, sauth.rolename, sauth.objecttype, sauth.objectname FROM objects obj, sauthorization sauth WHERE sauth.objectname = obj.OBJECT_NAME AND obj.OBJECT_NAME = ?', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (11, 'I', 'ADD_AN_APP_CONFIG', 'INSERT INTO app_config(title, body, configtype, status, createTime) VALUES(?,?,\'Q\',?,NOW())', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (12, 'U', 'UPDATE_APP_CONFIG', 'UPDATE app_config SET title = ?, body = ?, status = ? WHERE id = ?', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (13, 'D', 'REVOKE_PERMISSION_FOR_A_USER', 'DELETE FROM roleprivileges WHERE ROLE_ID = (SELECT ROLE_ID FROM roles WHERE ROLE_NAME = ?) AND OBJECT_ID = ?', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (14, 'S', 'GET_MAX_OBJECT_ID', 'SELECT MAX(OBJECT_ID) as maxObjectId FROM objects', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (15, 'I', 'ADD_A_NEW_OBJECT', 'INSERT INTO objects(OBJECT_ID, OBJECT_NAME, OBJECT_TYPE) VALUES(?,?,?)', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (16, 'U', 'CHANGE_PERMISSION_FOR_A_USER', 'UPDATE roleprivileges SET ROLE_ID = ? WHERE ROLE_ID = ? AND OBJECT_ID = ?', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (17, 'S', 'GET_EXISTING_OBJECT_TYPES', 'SELECT DISTINCT OBJECT_TYPE as objectTypes FROM objects', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (18, 'I', 'GRANT_NEW_USER_PERMISSION', 'INSERT INTO roleprivileges(ROLE_ID, OBJECT_ID, PERMISSION_LEVEL) VALUES(?,?,\'--x\')', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (19, 'S', 'CHECK_OBJECT_EXISTENCE', 'SELECT COUNT(OBJECT_NAME) AS count, OBJECT_ID, OBJECT_TYPE FROM objects WHERE OBJECT_NAME = ?', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (20, 'D', 'DELETE_OBJECT_BY_ID', 'DELETE FROM objects WHERE OBJECT_ID = ?', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (21, 'D', 'REMOVE_PERMISSION_BY_OBJECT_ID', 'DELETE FROM roleprivileges WHERE OBJECT_ID = ?', 'Y', '2015-01-11 20:02:04', '2015-01-11 20:02:04', '-i-', 0, 0),
    (22, 'S', 'FIND_POSSIBLE_FILE_DUPLICATE', 'Select fileIds_revision from fileparrothash where accountId_magicHeader_startByte_endByte_fileLength = CONCAT(?,\'_\',?,\'_\',?,\'_\',?,\'_\',?)', 'A', '2015-01-12 16:56:27', '2012-01-01 00:00:00', '-i-', 0, 0),
    (23, 'S', 'FIND_FILE_CHUNK_OFFSETS', 'Select SUBSTRING_INDEX( `accountId_projectId_fileId_revision` , \'_\', 1 ) AS accountId, SUBSTRING_INDEX(SUBSTRING_INDEX( `accountId_projectId_fileId_revision` , \'_\', 2 ), \'_\',-1) AS projectId, SUBSTRING_INDEX(SUBSTRING_INDEX( `accountId_projectId_fileId_revision` , \'_\', -2 ), \'_\',1) AS fileId, SUBSTRING_INDEX( `accountId_projectId_fileId_revision` , \'_\', -1 ) AS revision, chunkOffsetJSON from filechunks where accountId_projectId_fileId_revision = CONCAT(?,\'_\',?,\'_\',?,\'_\',?)', 'A', '2015-01-13 10:04:29', '2012-01-01 00:00:00', '-i-', 0, 0),
    (24, 'S', 'FIND_DUPLICATE_FILE', 'Select chunksJSON from filechunks where accountId_projectId_fileId_revision = CONCAT(?,\'_\',?,\'_\',?,\'_\',?) AND chunksJSON = ?', 'A', '2015-01-12 16:56:39', '2012-01-01 00:00:00', '-i-', 0, 0),
    (25, 'S', 'FIND_MATCHING_CHUNKS', 'Select SUBSTRING_INDEX( `accountId_chunkHeader_chunkHash` , \'_\', 1 ) AS accountId, SUBSTRING_INDEX(SUBSTRING_INDEX( `accountId_chunkHeader_chunkHash` , \'_\', 2 ), \'_\',-1) AS chunkHeader, SUBSTRING_INDEX( `accountId_chunkHeader_chunkHash` , \'_\', -1 ) AS chunkHash from chunks where accountId_chunkHeader_chunkHash = CONCAT(?,\'_\',?,\'_\',?)', 'A', '2015-01-13 09:59:32', '2012-01-01 00:00:00', '-i-', 0, 0),
    (26, 'U', 'APPEND_FILE_TO_THUMBNAIL', 'update fileparrothash set fileIds_revision = CONCAT(fileIds_revision,\',\',CONCAT(?,\'_\',?)) where accountId_magicHeader_startByte_endByte_fileLength = CONCAT(?,\'_\',?,\'_\',?,\'_\',?,\'_\',?)', 'A', '2015-01-12 16:57:01', '2012-01-01 00:00:00', '-i-', 0, 0),
    (27, 'I', 'ADD_FILE_TO_FILECHUNKS', 'Insert into filechunks ( accountId_projectId_fileId_revision, chunkOffsetJSON, chunksJSON ) VALUES (CONCAT(?,\'_\',?,\'_\',?,\'_\',?),?,?)', 'A', '2015-01-12 16:57:05', '2012-01-01 00:00:00', '-i-', 0, 0),
    (29, 'I', 'ADD_TO_CHUNKS', 'Insert Into chunks ( accountId_chunkHeader_chunkHash ) VALUES (CONCAT(?,\'_\',?,\'_\',?))', 'A', '2015-01-12 16:57:13', '2012-01-01 00:00:00', '-i-', 0, 0),
    (31, 'I', 'ADD_FILE_TO_THUMBNAIL', 'Insert into fileparrothash (accountId_magicHeader_startByte_endByte_fileLength, fileIds_revision ) VALUES ( CONCAT(?,\'_\',?,\'_\',?,\'_\',?,\'_\',?), CONCAT(?,\'_\',?))', 'A', '2015-01-12 16:57:17', '2012-01-01 00:00:00', '-i-', 0, 0),
    (32, 'I', 'ADD_LINK_TO_ACCOUNT', 'Insert into s3links (accountId, s3containerPath ) VALUES ( ?,?)', 'A', '2015-01-12 16:57:17', '2012-01-01 00:00:00', '-i-', 0, 0),
    (35, 'U', 'INCREMENT_CHUNK_REFERENCE', 'Update chunks Set reference = reference +1 where accountId_chunkHeader_chunkHash = CONCAT(?,\'_\',?,\'_\',?)', 'A', '2015-01-12 16:57:22', '2012-01-01 00:00:00', '-i-', 0, 0),
    (37, 'U', 'DELETE_FILE_FROM_FILEPARROTHASH', 'update fileparrothash set fileIds_revision = TRIM(LEADING \',\' FROM CONCAT(TRIM(TRAILING \',\' FROM SUBSTRING_INDEX(`fileIds_revision`,CONCAT(?,\'_\',?),1)),SUBSTRING_INDEX(`fileIds_revision`,CONCAT(?,\'_\',?),-1))) where fileIds_revision like CONCAT(\'%\',?,\'_\',?,\'%\')', 'A', '2015-01-12 16:57:05', '2012-01-01 00:00:00', '-i-', 0, 0),
    (38, 'D', 'DELETE_FILE_FROM_FILECHUNKS', 'delete from filechunks where accountId_projectId_fileId_revision = CONCAT(?,\'_\',?,\'_\',?,\'_\',?)', 'A', '2015-01-12 16:57:05', '2012-01-01 00:00:00', '-i-', 0, 0),
    (39, 'U', 'DECREMENT_CHUNK_REFERENCE', 'Update chunks Set reference = reference -1 where accountId_chunkHeader_chunkHash = CONCAT(?,\'_\',?,\'_\',?)', 'A', '2015-01-12 16:57:22', '2012-01-01 00:00:00', '-i-', 0, 0),
    (40, 'S', 'GET_FILE_CHUNKS', 'Select chunksJSON from filechunks where accountId_projectId_fileId_revision = CONCAT(?,\'_\',?,\'_\',?,\'_\',?)', 'A', '2015-01-13 10:04:29', '2012-01-01 00:00:00', '-i-', 0, 0),
    (41, 'S', 'GET_DOWNLOAD_PATH', 'Select s3containerPath from s3links where accountId = ?', 'A', '2015-01-13 10:04:29', '2012-01-01 00:00:00', '-i-', 0, 0);
/*!40000 ALTER TABLE `app_config` ENABLE KEYS */;


-- Dumping structure for table onelinedb.s3links
DROP TABLE IF EXISTS `s3links`;
CREATE TABLE IF NOT EXISTS `s3links` (
  `accountId` int(11) NOT NULL,
  `s3containerPath` varchar(512) NOT NULL,
  PRIMARY KEY (`accountId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table onelinedb.s3links: 0 rows
/*!40000 ALTER TABLE `s3links` DISABLE KEYS */;
/*!40000 ALTER TABLE `s3links` ENABLE KEYS */;


-- Dumping structure for table onelinedb.chunks
DROP TABLE IF EXISTS `chunks`;
CREATE TABLE IF NOT EXISTS `chunks` (
  `accountId_chunkHeader_chunkHash` varchar(512) NOT NULL,
  `reference` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`accountId_chunkHeader_chunkHash`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table onelinedb.chunks: 0 rows
/*!40000 ALTER TABLE `chunks` DISABLE KEYS */;
/*!40000 ALTER TABLE `chunks` ENABLE KEYS */;


-- Dumping structure for table onelinedb.filechunks
DROP TABLE IF EXISTS `filechunks`;
CREATE TABLE IF NOT EXISTS `filechunks` (
  `accountId_projectId_fileId_revision` varchar(256) NOT NULL,
  `chunkOffsetJSON` varchar(2048) DEFAULT NULL,
  `chunksJSON` varchar(4096) DEFAULT NULL,
  PRIMARY KEY (`accountId_projectId_fileId_revision`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table onelinedb.filechunks: 0 rows
/*!40000 ALTER TABLE `filechunks` DISABLE KEYS */;
/*!40000 ALTER TABLE `filechunks` ENABLE KEYS */;


-- Dumping structure for table onelinedb.fileparrothash
DROP TABLE IF EXISTS `fileparrothash`;
CREATE TABLE IF NOT EXISTS `fileparrothash` (
  `accountId_magicHeader_startByte_endByte_fileLength` varchar(512) NOT NULL,
  `fileIds_revision` varchar(512) NOT NULL,
  PRIMARY KEY (`accountId_magicHeader_startByte_endByte_fileLength`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table onelinedb.fileparrothash: 0 rows
/*!40000 ALTER TABLE `fileparrothash` DISABLE KEYS */;
/*!40000 ALTER TABLE `fileparrothash` ENABLE KEYS */;


-- Dumping structure for table onelinedb.objects
DROP TABLE IF EXISTS `objects`;
CREATE TABLE IF NOT EXISTS `objects` (
  `OBJECT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `OBJECT_NAME` varchar(64) DEFAULT NULL,
  `OBJECT_TYPE` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`OBJECT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- Dumping data for table onelinedb.objects: ~32 rows (approximately)
/*!40000 ALTER TABLE `objects` DISABLE KEYS */;
INSERT INTO `objects` (`OBJECT_ID`, `OBJECT_NAME`, `OBJECT_TYPE`) VALUES
    (1, 'login', 'user'),
    (2, 'tenantlogin', 'user'),
    (3, 'logout', 'user'),
    (4, 'getloggedinuser', 'user'),
    (5, 'register', 'user'),
    (6, 'changepassword', 'user'),
    (7, 'resetpassword', 'user'),
    (8, 'activate', 'user'),
    (9, 'select', 'sql'),
    (10, 'insert', 'sql'),
    (11, 'update', 'sql'),
    (12, 'refresh', 'sql'),
    (13, 'generateid', 'sql'),
    (14, 'execute', 'sql'),
    (15, 'upload', 'upload'),
    (16, 'GET_ROLES', 'sql'),
    (17, 'GET_APP_CONFIG', 'sql'),
    (18, 'GET_APP_CONFIG_COUNT', 'sql'),
    (19, 'DELETE_FROM_APP_CONFIG_BY_ID', 'sql'),
    (20, 'GET_AUTHORIZED_USERS_FOR_AN_ACTION', 'sql'),
    (21, 'ADD_AN_APP_CONFIG', 'sql'),
    (22, 'ADD_A_NEW_OBJECT', 'sql'),
    (23, 'UPDATE_APP_CONFIG', 'sql'),
    (24, 'REVOKE_PERMISSION_FOR_A_USER', 'sql'),
    (25, 'GET_MAX_OBJECT_ID', 'sql'),
    (26, 'CHANGE_PERMISSION_FOR_A_USER', 'sql'),
    (27, 'GET_EXISTING_OBJECT_TYPES', 'sql'),
    (28, 'GRANT_NEW_USER_PERMISSION', 'sql'),
    (29, 'CHECK_OBJECT_EXISTENCE', 'sql'),
    (30, 'DELETE_OBJECT_BY_ID', 'sql'),
    (31, 'REMOVE_PERMISSION_BY_OBJECT_ID', 'sql'),
    (32, 'EMPTY_APP_CONFIG', 'sql');
/*!40000 ALTER TABLE `objects` ENABLE KEYS */;


-- Dumping structure for table onelinedb.roleprivileges
DROP TABLE IF EXISTS `roleprivileges`;
CREATE TABLE IF NOT EXISTS `roleprivileges` (
  `TENANT_ID` int(11) DEFAULT NULL,
  `ROLE_ID` int(11) NOT NULL,
  `OBJECT_ID` int(11) NOT NULL,
  `PERMISSION_LEVEL` varchar(32) NOT NULL,
  KEY `ROLE_ID_OBJECT_ID_idx` (`ROLE_ID`,`OBJECT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table onelinedb.roleprivileges: ~33 rows (approximately)
/*!40000 ALTER TABLE `roleprivileges` DISABLE KEYS */;
INSERT INTO `roleprivileges` (`TENANT_ID`, `ROLE_ID`, `OBJECT_ID`, `PERMISSION_LEVEL`) VALUES
    (NULL, 1, 1, '--x'),
    (NULL, 1, 2, '--x'),
    (NULL, 1, 3, '--x'),
    (NULL, 1, 4, '--x'),
    (NULL, 3, 5, '--x'),
    (NULL, 1, 6, '--x'),
    (NULL, 1, 7, '--x'),
    (NULL, 3, 8, '--x'),
    (NULL, 3, 8, '--x'),
    (NULL, 1, 9, '--x'),
    (NULL, 1, 10, '--x'),
    (NULL, 1, 11, '--x'),
    (NULL, 3, 12, '--x'),
    (NULL, 1, 13, '--x'),
    (NULL, 1, 14, '--x'),
    (NULL, 1, 15, '--x'),
    (NULL, 3, 16, '--x'),
    (NULL, 3, 17, '--x'),
    (NULL, 3, 18, '--x'),
    (NULL, 3, 19, '--x'),
    (NULL, 3, 20, '--x'),
    (NULL, 3, 21, '--x'),
    (NULL, 3, 22, '--x'),
    (NULL, 3, 23, '--x'),
    (NULL, 3, 24, '--x'),
    (NULL, 3, 25, '--x'),
    (NULL, 3, 26, '--x'),
    (NULL, 3, 27, '--x'),
    (NULL, 3, 28, '--x'),
    (NULL, 3, 29, '--x'),
    (NULL, 3, 30, '--x'),
    (NULL, 3, 31, '--x'),
    (NULL, 3, 32, '--x');
/*!40000 ALTER TABLE `roleprivileges` ENABLE KEYS */;


-- Dumping structure for table onelinedb.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `ROLE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROLE_NAME` varchar(64) NOT NULL,
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table onelinedb.roles: ~5 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`ROLE_ID`, `ROLE_NAME`) VALUES
    (1, 'all'),
    (2, 'guest'),
    (3, 'root'),
    (4, 'tenantadmin'),
    (5, 'tenantuser');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;


-- Dumping structure for view onelinedb.sauthorization
DROP VIEW IF EXISTS `sauthorization`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `sauthorization` (
    `rolename` VARCHAR(64) NOT NULL COLLATE 'utf8_general_ci',
    `objecttype` VARCHAR(64) NULL COLLATE 'utf8_general_ci',
    `objectname` VARCHAR(64) NULL COLLATE 'utf8_general_ci',
    `permission` VARCHAR(32) NOT NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;


-- Dumping structure for table onelinedb.systemlog
DROP TABLE IF EXISTS `systemlog`;
CREATE TABLE IF NOT EXISTS `systemlog` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `COMPANY_ID` int(11) NOT NULL,
  `ACTION` varchar(64) NOT NULL,
  `SEGMENT` varchar(64) DEFAULT NULL,
  `HOSTNAME` varchar(64) DEFAULT NULL,
  `INPUT_JSON` text,
  `AUTHORIZTION` text,
  `USER_ID` varchar(255) NOT NULL,
  `TOUCH_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=utf8;

-- Dumping data for table onelinedb.systemlog: ~191 rows (approximately)
/*!40000 ALTER TABLE `systemlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `systemlog` ENABLE KEYS */;


-- Dumping structure for table onelinedb.user_login
DROP TABLE IF EXISTS `user_login`;
CREATE TABLE IF NOT EXISTS `user_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenantid` int(11) NOT NULL,
  `active` char(1) NOT NULL DEFAULT 'Y',
  `loginid` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `profile` text NOT NULL,
  `touchtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_key` (`loginid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table onelinedb.user_login: ~1 rows (approximately)
/*!40000 ALTER TABLE `user_login` DISABLE KEYS */;
INSERT INTO `user_login` (`id`, `tenantid`, `active`, `loginid`, `password`, `profile`, `touchtime`) VALUES
    (3, 1, 'Y', 'root@bizosys.com', '83FA7A7883F9FDE5D46139C439F1E1C1', 'role|root,user_display_name|root', '2015-01-13 09:15:34');
/*!40000 ALTER TABLE `user_login` ENABLE KEYS */;


-- Dumping structure for view onelinedb.sauthorization
DROP VIEW IF EXISTS `sauthorization`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `sauthorization`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `sauthorization` AS select ROLE_NAME as rolename, OBJECT_TYPE as objecttype, 
OBJECT_NAME as objectname, PERMISSION_LEVEL as permission  
from roles r, objects o, roleprivileges rp  
where rp.ROLE_ID = r.ROLE_ID AND rp.OBJECT_ID = o.OBJECT_ID ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;