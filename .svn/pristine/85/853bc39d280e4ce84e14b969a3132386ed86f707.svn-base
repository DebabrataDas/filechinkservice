#!/usr/bin/env python

import os
import threading
import time
import urllib2
import traceback
import re
import copy

# Metrics store object
METRICS_GLOBAL_STORE_OBJ = {
    'time' : 0,
    'data' : {}
}

# Metric data object array store - from REST API
metrics  = {}

# Metric data object template
metric_obj_template = {}

# Metric data object array - updated
descriptors = list()

# Metric REST API URL
SERVER_STATUS_URL = ""

# Metric Initialization
def metric_init(params):
	global descriptors, SERVER_STATUS_URL
	  
	if "metric_group" not in params:
		params["metric_group"] = "filechunk"

	if "refresh_rate" not in params:
		params["refresh_rate"] = 2

	if "url" not in params:
		params["url"] = "http://localhost/filechunk/health"
		
	SERVER_STATUS_URL = params["url"]
	
	metric_obj_template = {
		'name': 'jvm-maxm-mb',
        'call_back': get_value,
        'time_max': 90,
        'value_type': 'uint',
        'units': '',
        'slope': 'both',
        'format': '%u',
        'description': 'filechunk',
        'groups': "filechunk"
	}
	metrics = get_metrics()[0]['data']
	descriptors = list();
	for m in metrics:
		descriptors.append(create_desc({
					"name"       : m,
					"value_type" : "float",
					"units"      : "",
					"call_back"   : get_value,                
					"format"     : "%f",
					"description": m,
					'groups': "filechunk"
					}))
	return descriptors

# Create Metric object from template
def create_desc(prop):
	d = metric_obj_template.copy()
	for k,v in prop.iteritems():
		d[k] = v
	return d

# Get value for a metric
def get_value(name):
	
	metrics = get_metrics()[0]

	try:
		result = metrics['data'][name]
	except StandardError:
		result = 0

	return result

# Read metrics from REST API
def get_metrics():
	global METRICS_GLOBAL_STORE_OBJ, SERVER_STATUS_URL, descriptors
	
	try:
		req = urllib2.Request(SERVER_STATUS_URL + "")
		
		# Read stats from REST API
		res = urllib2.urlopen(req, None, 2)

		metrics = {};
		for line in res:
			split_line = line.rstrip().split(":")
			metric_name = split_line[0]
			metrics[metric_name] = split_line[1]
	except	urllib2.URLError:
		traceback.print_exc()
	
	METRICS_GLOBAL_STORE_OBJ	=	{
		'time': time.time(),
		'data': metrics
    }

	return [METRICS_GLOBAL_STORE_OBJ]

# Clean up method	
def metric_cleanup():
	'''Clean up the metric module.'''
	pass	

# Main function for debug
if __name__ == '__main__':
	try:
		params = {
            'url'  : 'http://localhost/filechunk/health',
        }
		metric_init(params)
		get_metrics()
		for d in descriptors:
			v = d['call_back'](d['name'])
			print 'value for %s is %s' % (d['name'],  v)
	except KeyboardInterrupt:
		os._exit(1)
