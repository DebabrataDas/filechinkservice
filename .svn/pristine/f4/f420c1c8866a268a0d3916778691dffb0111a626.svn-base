package com.arc.management;

import com.arc.util.log.ArcLogger;

public class MonitorDayEndJob implements IHealth {

	private final boolean DEBUG_ENABLED = ArcLogger.l.isDebugEnabled();
	private MonitorMeasureAvg mma = new MonitorMeasureAvg();
	private static MonitorDayEndJob singleton = null;

	public static MonitorDayEndJob getInstance() 
	{
		if (null != singleton)
			return singleton;
		synchronized (MonitorDayEndJob.class.getName()) 
		{
			if (null != singleton)
				return singleton;
			singleton = new MonitorDayEndJob();
		}
		return singleton;
	}

	final ThreadLocal<Clock> clock = new ThreadLocal<Clock>() 
	{
		@Override
		protected Clock initialValue() 
		{
			return new Clock();
		}
	};

	private MonitorDayEndJob() 
	{
	}

	public void onEnter() {
		clock.get().time = System.currentTimeMillis();
	}

	public void onExit(MonitorCollector collector) 
	{
		long endTime = System.currentTimeMillis();
		long diff = (endTime - clock.get().time);
		mma.set((int) diff);
		
		if (!DEBUG_ENABLED) return;
		
		StringBuilder sb = new StringBuilder();
		sb.append("\treq-res-ms:").append(diff);
		
		if (null != collector) 
		{
			for (MonitorMeasure mm : collector) 
			{
				sb.append('\t').append(mm.name).append(':').append(mm.val);
			}
		}
		ArcLogger.l.info(sb.toString());
	}
	
	public void setJobRunTime(int runTime) 
	{
		mma.set((int) runTime);
	}

	private static final class Clock {
		public long time = 0;
	}

	@Override
	public MonitorCollector collect() 
	{
		MonitorCollector collector = new MonitorCollector();
		collector.add(new MonitorMeasure("day-end-response-avg", mma.getAvgVal()));
		collector.add(new MonitorMeasure("day-end-response-min", mma.getMinVal()));
		collector.add(new MonitorMeasure("day-end-response-max", mma.getMaxVal()));
		return collector;
	}

}