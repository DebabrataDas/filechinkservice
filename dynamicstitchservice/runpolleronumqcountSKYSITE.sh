#!/bin/bash

#PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root/chunkcode/umqcount
#Java files workspace package - com.arc.umq.dynamicservice
#While deploying in the server first move the class files of com.arc.umq.dynamicservice to default package and then deploy


cd /root/chunkcode/umqcount

#auto startup and killing of pollerservice depending on umq ready message count

#fetch how many messages is in ready state
output="$(java -cp .:\lib/\* UMQCount)"
echo "$output"
queuename='SkysiteStitchProd'
limitpollerservice=5 #max number of pollerservice that can run

#if program is running for 1st time create a temp file to store initial instanceid as 100
if [ ! -f /root/chunkcode/temp.txt ]; then
    count=100
    echo $count >> /root/chunkcode/temp.txt
fi

#if file exists but is empty
if [ ! -s /root/chunkcode/temp.txt ]; then
    count=100
    echo $count >> /root/chunkcode/temp.txt
fi


if [ $output -eq 0 ]; then
	numofprocess="$(ps -aux | grep -c PollerService)"
        numofprocess=$((numofprocess-1)) #check number of pollerservices running
	
	#if no pollerservices running then exit
        if [ $numofprocess -eq 0 ]
         then
                echo "No pollerservice running. Nothing to kill hence exiting"
                exit
        fi

       	#the below loop will running for only killing the dynamic pollerservice processes
	
	for (( i=1; i <= $numofprocess; i++ ))
	do
 
	pid="$(head -n 1 /root/chunkcode/pidinstidmapr.properties | egrep -o '^[^=]+')" # #fetch the first pid PollerService started from properties file
        
	echo "killing process with pid no "$pid
        kill -9 $pid    #kill the first PollerService process
        output="$(java -cp .:\lib/\* RePublishMessage $pid)"    #Republish all the messages corresponding to that pid
        echo $output

	#delete the pid and instanceid from the temporary pidinstidmapr.properties file mapped

                if [ ! -z "$pid" ]; then
                        deletepid=$(sed -n '/'$pid'/p' /root/chunkcode/pidinstidmapr.properties)
                        sed -i '/^'$deletepid'\b/d' /root/chunkcode/pidinstidmapr.properties
                fi	
	done	
   exit
fi



if [ $output -gt 10 ]; then
	
	numofprocess="$(ps -aux | grep -c PollerService)"
        numofprocess=$((numofprocess-1)) #check number of pollerservices running
        if [ $numofprocess -ge $limitpollerservice ] #limits the pollerservice count
         then
                echo "Pollerservice limit reached. Hence exiting"
                exit
        fi
  	
	value="$(cat /root/chunkcode/temp.txt)"  #fetch the instanceid from temp file
	echo "Instance id fetched from temp file: $value"
	value=$((value+1)) #increment the instanceid for using 	
	#pass the instanceid via command line	
        value1=$queuename$value
	echo 'Instance id started: '$value1
	java -cp /root/chunkcode/arcfilechunk.jar:/root/chunkcode/lib/*:/etc/hbase/conf com.arc.umq.service.PollerService $value1 &
	 
	echo $value > /root/chunkcode/temp.txt	#store the instanceid in the temp file by overriding it
	#numofprocess="$(ps -aux | grep -c PollerService)"
	#numofprocess=$((numofprocess-1))
	#echo $numofprocess
	pid=$(ps aux --sort +start_time | grep -i 'PollerService' | tail -n 2 | awk 'NR==1{print $2}')	#get pid of the latest PollerService
	echo 'PollerService PID started: '$pid
	echo $pid'='$queuename$value >> /root/chunkcode/pidinstidmapr.properties	#store pid and instanceid in a properties file for future reference
	echo $pid'='$queuename$value >> /root/chunkcode/pidinstidmaprdatabase.properties 
fi


if [ $output -lt 5 ]
 then

	numofprocess="$(ps -aux | grep -c PollerService)"
        numofprocess=$((numofprocess-1)) #check number of pollerservices running
	numofprocesstorun=0	#use it to keep the number of pollerservices running	
	if [ $numofprocess -eq $numofprocesstorun ]
	 then
		echo $numofprocesstorun" pollerservice running. Nothing to kill hence exiting"
		exit
	fi

        #pid="$(ps aux | grep -i 'PollerService' | grep -v grep | awk 'NR <= 1' | awk {'print $2'})" #fetch the first pid PollerService started
	pid="$(head -n 1 /root/chunkcode/pidinstidmapr.properties | egrep -o '^[^=]+')" # #fetch the first pid PollerService started from properties file
	echo "killing process with pid no "$pid
	kill -9 $pid    #kill the first PollerService process
        output="$(java -cp .:\lib/\* RePublishMessage $pid)"    #Republish all the messages corresponding to that pid
        echo $output

	#delete the pid and instanceid from the temporary pidinstidmapr.properties file mapped

                if [ ! -z "$pid" ]; then
                        deletepid=$(sed -n '/'$pid'/p' /root/chunkcode/pidinstidmapr.properties)
                        sed -i '/^'$deletepid'\b/d' /root/chunkcode/pidinstidmapr.properties
                fi

fi	

