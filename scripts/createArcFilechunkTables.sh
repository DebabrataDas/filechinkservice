echo "disable 'S3LINKS'" | hbase shell
echo "drop 'S3LINKS'" | hbase shell
echo "create 'S3LINKS', {NAME => '1'}" | hbase shell

echo "disable 'CHUNKS'" | hbase shell
echo "drop 'CHUNKS'" | hbase shell
echo "create 'CHUNKS', {NAME => '1'}" | hbase shell

echo "disable 'FILEPARROTHASH'" | hbase shell
echo "drop 'FILEPARROTHASH'" | hbase shell
echo "create 'FILEPARROTHASH', {NAME => '1'}" | hbase shell

echo "disable 'FILECHUNKS'" | hbase shell
echo "drop 'FILECHUNKS'" | hbase shell
echo "create 'FILECHUNKS', {NAME => '1'}" | hbase shell

echo "disable 'FILEID'" | hbase shell
echo "drop 'FILEID'" | hbase shell
echo "create 'FILEID', {NAME => '1'}" | hbase shell

echo "disable 'REVISIONID'" | hbase shell
echo "drop 'REVISIONID'" | hbase shell
echo "create 'REVISIONID', {NAME => '1'}" | hbase shell